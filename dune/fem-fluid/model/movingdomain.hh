#ifndef DUNE_FEM_FLUID_MODEL_MOVINGDOMAIN_HH
#define DUNE_FEM_FLUID_MODEL_MOVINGDOMAIN_HH

#include <dune/common/exceptions.hh>

/**********************************************
 * Default model
 *********************************************/
template< class Traits >
class DefaultModel
{
public:
  static const int dimDomain = Traits::dimDomain;
  static const int dimRange  = Traits::dimRange;

  typedef typename Traits::DomainType DomainType;
  typedef typename Traits::RangeType RangeType;
  typedef typename Traits::FaceDomainType FaceDomainType;
  typedef typename Traits::JacobianRangeType JacobianRangeType;

  typedef typename Traits::EntityType EntityType;
  typedef typename Traits::IntersectionType IntersectionType;

  void setRelaxFactor ( const double f ) const {}

  void setDeltaT ( const double dt ) const {}
  void switchOffDeltaT () const {}

  void weakSource ( const EntityType &en,
                    const double time,
                    const DomainType &x,
                    JacobianRangeType &s ) const
  {}

  double source ( const EntityType &en,
                  const double time,
                  const DomainType &x,
                  const DomainType &domainVelocity,
                  const RangeType &u,
                  const JacobianRangeType &jac,
                  RangeType &s ) const
  {
    return 0;
  }

  void stiffSource ( const EntityType &en,
                     const double time,
                     const DomainType &x,
                     const DomainType &domainVelocity,
                     const RangeType &u,
                     const JacobianRangeType &jac,
                     RangeType &s ) const
  {
  }

  void linearSource ( const EntityType &en,
                      const double time,
                      const DomainType &x,
                      const DomainType &domainVelocity,
                      const RangeType &uStar,
                      const JacobianRangeType &jacStar,
                      const RangeType &u,
                      const JacobianRangeType &jac,
                      RangeType &s ) const
  {}


  void linearStiffSource ( const EntityType &en,
                           const double time,
                           const DomainType &x,
                           const DomainType &domainVelocity,
                           const RangeType &uStar,
                           const JacobianRangeType &jacStar,
                           const RangeType &u,
                           const JacobianRangeType &jac,
                           RangeType &s ) const
  {}


  /**
   * @brief advection term \f$F\f$
   *
   * @param en entity on which to evaluate the advection term
   * @param time current time of TimeProvider
   * @param x coordinate local to entity
   * @param u \f$U\f$
   * @param f \f$f(U)\f$
   */
  void advection ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const DomainType &domainVelocity,
                   const RangeType &u,
                   JacobianRangeType &f ) const
  {
    f = 0;
  }

  void linearAdvection ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const DomainType &domainVelocity,
                         const RangeType &uStar,
                         const RangeType &u,
                         JacobianRangeType &f ) const
  {
    f = 0;
  }

  void eigenValues ( const EntityType &en,
                     const DomainType &x,
                     const double time,
                     const RangeType &u,
                     RangeType &maxValue ) const
  {
    DUNE_THROW( Dune::NotImplemented, "DefaultModel::eigenValues is not implemented" );
  }

  void eigenValues ( const EntityType &en,
                     const DomainType &x,
                     const double time,
                     const RangeType &uStar,
                     const RangeType &u,
                     RangeType &maxValue ) const
  {
    DUNE_THROW( Dune::NotImplemented, "DefaultModel::eigenValues is not implemented" );
  }


  /**
   * @brief diffusion term \f$A\f$
   */
  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   JacobianRangeType &A ) const
  {}

  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &uStar,
                         const JacobianRangeType &jacStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         JacobianRangeType &A ) const
  {}

  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac1,
                   const JacobianRangeType &jac2,
                   JacobianRangeType &A ) const
  {}

  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &uStar,
                         const JacobianRangeType &jac1Star,
                         const JacobianRangeType &jac2Star,
                         const RangeType &u,
                         const JacobianRangeType &jac1,
                         const JacobianRangeType &jac2,
                         JacobianRangeType &A ) const
  {}

  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac1,
                   const JacobianRangeType &jac2,
                   const JacobianRangeType &shift,
                   JacobianRangeType &A ) const
  {}

  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &uStar,
                         const JacobianRangeType &jac1Star,
                         const JacobianRangeType &jac2Star,
                         const JacobianRangeType &shiftStar,
                         const RangeType &u,
                         const JacobianRangeType &jac1,
                         const JacobianRangeType &jac2,
                         const JacobianRangeType &shift,
                         JacobianRangeType &A ) const
  {}


  /**
   * @brief checks for existence of dirichlet boundary values
   */
  bool hasBoundaryValue ( const IntersectionType &it ) const
  {
    return true;
  }

  bool hasLinearBoundaryValue ( const IntersectionType &it ) const
  {
    return true;
  }


  /**
   * @brief neuman boundary values \f$g_N\f$ for pass2
   */
  double boundaryFlux ( const IntersectionType &it,
                        const EntityType &entity,
                        const double time,
                        const FaceDomainType &x,
                        const DomainType &normal,
                        const DomainType &domainVelocity,
                        const RangeType &uLeft,
                        const JacobianRangeType &jac,
                        RangeType &gLeft ) const
  {
    gLeft = 0.;
    return 0.;
  }


  double linearBoundaryFlux ( const IntersectionType &it,
                              const EntityType &entity,
                              const double time,
                              const FaceDomainType &x,
                              const DomainType &normal,
                              const DomainType &domainVelocity,
                              const RangeType &uLeft,
                              const JacobianRangeType &jac,
                              RangeType &gLeft ) const
  {
    gLeft = 0.;
    return 0.;
  }

  /**
   * @brief dirichlet boundary values
   */
  void boundaryValue ( const IntersectionType &it,
                       const EntityType &en,
                       const double time,
                       const FaceDomainType &x,
                       const DomainType &domainVelocity,
                       const RangeType &uLeft,
                       const JacobianRangeType &jac,
                       RangeType &uRight ) const
  {}


  void linearBoundaryValue ( const IntersectionType &it,
                             const EntityType &en,
                             const double time,
                             const FaceDomainType &x,
                             const DomainType &domainVelocity,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const RangeType &uLeft,
                             const JacobianRangeType &jacLeft,
                             RangeType &uRight ) const
  {}

};
#endif //#ifndef DUNE_FEM_FLUID_MODEL_MOVINGDOMAIN_HH
