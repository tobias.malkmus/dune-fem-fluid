#ifndef DUNE_PROBLEMINTERFACE_HH
#define DUNE_PROBLEMINTERFACE_HH

#include <dune/common/version.hh>
#include <dune/fem/function/common/instationary.hh>
#include <dune/fem/misc/gridsolution.hh>

namespace Dune
{

  namespace Fem
  {

    /**
     * @brief describes the interface for
     * initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class EvolutionProblemInterface
    {
      typedef EvolutionProblemInterface< FunctionSpace > ThisType;

    public:
      typedef FunctionSpace FunctionSpaceType;

      static const int dimDomain = FunctionSpaceType::dimDomain;
      static const int dimRange  = FunctionSpaceType::dimRange;

      typedef typename FunctionSpaceType::DomainType DomainType;
      typedef typename FunctionSpaceType::RangeType RangeType;
      typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
      typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
      typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

      /**
       * @brief define problem parameters
       */

    protected:
      EvolutionProblemInterface () {}

    public:
      typedef InstationaryFunction< ThisType, __InstationaryFunction::HoldReference > TimeDependentFunctionType;

      //! turn timedependent function into function by fixing time
      TimeDependentFunctionType fixedTimeFunction ( const double time ) const
      {
        return TimeDependentFunctionType( *this, time );
      }

      // return prefix for data loops
      virtual std::string dataPrefix () const
      {
        return std::string( "loop" );
      }

      //! destructor
      virtual ~EvolutionProblemInterface () {}

      virtual bool hasWeakSource () const { abort();  return false; }

      virtual bool hasSource () const { abort();  return false; }

      //! stiff source term
      virtual double source ( const DomainType &arg,
                              const double time,
                              const RangeType &u,
                              RangeType &res ) const
      {
        abort();
        res = 0;
        return 0.0;
      }


      //! non stiff source term
      virtual double weakSource ( const DomainType &arg,
                                  const double time,
                                  JacobianRangeType &res ) const
      {
        abort();
        res = 0;
        return 0.0;
      }

      /** \brief start time of problem */
      virtual double starttime () const { return 0.0; }

      virtual void boundaryValue ( const DomainType &x, const double t, const RangeType &u, RangeType &g ) const
      {
        evaluate( x, t, g );
      }

      virtual void jacobian ( const DomainType &arg, const double t, JacobianRangeType &phi ) const = 0;
      /**
       * @brief evaluate exact solution, to be implemented in derived classes
       */
      virtual void evaluate ( const DomainType &arg, const double t, RangeType &res ) const = 0;

      virtual void boundaryFlux ( const DomainType& normal, const DomainType &x, double time, RangeType &flux ) const
      {
        flux = 0;
      }

      /**
       * @brief latex output for EocOutput, default is empty
       */
      virtual std::string description () const
      {
        return std::string( "" );
      }
    };


    template< class FunctionSpace >
    class SteadyStateProblemInterface
    {
      typedef SteadyStateProblemInterface< FunctionSpace > ThisType;

    public:
      typedef FunctionSpace FunctionSpaceType;

      static const int dimDomain = FunctionSpaceType::dimDomain;
      static const int dimRange  = FunctionSpaceType::dimRange;

      typedef typename FunctionSpaceType::DomainType DomainType;
      typedef typename FunctionSpaceType::RangeType RangeType;
      typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
      typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
      typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

      /**
       * @brief define problem parameters
       */

    protected:
      SteadyStateProblemInterface () {}

    public:
      // return prefix for data loops
      virtual std::string dataPrefix () const
      {
        return std::string( "loop" );
      }

      //! destructor
      virtual ~SteadyStateProblemInterface () {}

      virtual bool hasWeakSource () const { abort();  return false; }

      virtual bool hasSource () const { abort();  return false; }

      //! stiff source term
      virtual double source ( const DomainType &arg,
                              const RangeType &u,
                              RangeType &res ) const = 0;

      double source ( const DomainType &x, const double t, const RangeType &u, RangeType &s ) const
      {
        return source( x, u, s );
      }

      //! non stiff source term
      virtual void weakSource ( const DomainType &arg, JacobianRangeType &res ) const = 0;

      void weakSource ( const DomainType &x, const double t, JacobianRangeType &jac ) const
      {
        weakSource( x, jac );
      }

      virtual void boundaryValue ( const DomainType &x, const RangeType &u, RangeType &g ) const
      {
        evaluate( x, g );
      }

      void boundaryValue ( const DomainType &x, const double t, const RangeType &u, RangeType &g ) const
      {
        boundaryValue( x, u, g );
      }

      virtual void boundaryFlux ( const DomainType &normal, const DomainType &x, const RangeType &u, RangeType &flux ) const
      {
        abort();
        flux = 0;
      }

      virtual void jacobian ( const DomainType &arg, JacobianRangeType &phi ) const = 0;

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &phi ) const
      {
        jacobian( arg, phi );
      }

      /**
       * @brief evaluate exact solution, to be implemented in derived classes
       */
      virtual void evaluate ( const DomainType &arg, RangeType &res ) const = 0;

      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const
      {
        evaluate( arg, res );
      }

      /**
       * @brief latex output for EocOutput, default is empty
       */
      virtual std::string description () const
      {
        return std::string( "" );
      }
    };

  } // namespace Fem

} // namespace Dune
#endif  /*DUNE_PROBLEMINTERFACE_HH*/
