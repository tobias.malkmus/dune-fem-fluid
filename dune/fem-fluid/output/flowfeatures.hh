#ifndef DUNE_FEM_FLOWFEATURES_HH
#define DUNE_FEM_FLOWFEATURES_HH

#include <fstream>
#include <iostream>

#include <dune/common/fvector.hh>

#include <dune/fem/io/file/iointerface.hh>
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>


namespace Dune
{
  
  namespace Fem
  {

    template< class DataImp, class Model >
    class FlowFeatureWriter
    : public IOInterface
    {
      typedef FlowFeatureWriter< DataImp, Model > ThisType;
      typedef IOInterface BaseType;

      typedef Model ModelType;

      typedef typename DataImp :: GridPartType GridPartType;
      typedef typename DataImp :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
      typedef typename IteratorType :: Entity EntityType;
      typedef typename EntityType :: Geometry GeometryTpye;
      typedef typename GeometryTpye :: LocalCoordinate LocalCoordinateType;
      static const int dimLocal = LocalCoordinateType::dimension;

      typedef typename GridPartType :: IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;
      typedef typename IntersectionType :: Geometry IntersectionGeometryType;

      typedef typename DataImp :: LocalFunctionType LocalFunctionType;
      typedef typename LocalFunctionType :: RangeType RangeType;
      typedef typename LocalFunctionType :: JacobianRangeType JacobianRangeType;
      typedef typename LocalFunctionType :: DomainType DomainType;
      typedef typename LocalFunctionType :: DomainFieldType DomainFieldType;

      static const int dimDomain = LocalFunctionType::dimDomain;

      typedef CachingQuadrature< GridPartType, 1 > FaceQuadratureType;
      typedef FieldVector< double, 3 > DataTupleType;

    public:
      FlowFeatureWriter ( DataImp &data, ModelType &model, const DataOutputParameters &parameter = DataOutputParameters() )
      : data_( data ),
        model_( model ),
        name_()
      {

        std::string path;
        // create data path
        IOInterface :: createGlobalPath( data_.gridPart().grid().comm(), Parameter::commonOutputPath() );
        path = Parameter::commonOutputPath() + "/";
        std::string paramPath = parameter.path();
        if( paramPath != "./" )
           path += paramPath;
        IOInterface :: createGlobalPath ( data_.gridPart().grid().comm(), path );

        // add path
        name_ = path + "/";
        name_ += parameter.prefix();

        // final name
        name_ += "flowfeature.dat";

        const int rank = data_.gridPart().grid().comm().rank();
        if( rank <= 0 )
        {
          std::ofstream file;
          file.open( name_.c_str(), std::ofstream::out );
          file.close();
        }
      }

      void write ( const TimeProviderBase &tp ) const
      {
        writeData( tp.time() );
      }

      void write () const
      {
        double sequence = 0.00;
        writeData( sequence );
      }

      void writeData ( double time ) const
      {
        DataTupleType dat = computeData( time );
        // communicate data
        data_.gridPart().grid().comm().sum( dat );
        const int rank = data_.gridPart().grid().comm().rank();

        if( rank <= 0 )
        {
          std::ofstream file;
          file.open( name_.c_str(), std::ios::app );
          file<< time <<"\t"<< dat  <<"\n";
          file.close();
        }
      }
      
      DataTupleType computeData ( double time ) const
      {
        DataTupleType ret( 0 );
        RangeType u, normalStress;
        JacobianRangeType jac(0), stress;

        // for the pressure drop
        DomainType left(0.2), right(0.2);
        left[ 0 ] -= 0.05;
        right[ 0 ] += 0.05;

        double pLeft(0), pRight(0);
        int nLeft = 0;
        int nRight = 0;


        const DiscreteFunctionSpaceType &space = data_.space(); 
        const int polOrd = 2 * space.order() + 2;

        // compute lift and drag
        const IteratorType end = space.end();
        for( IteratorType it = space.begin(); it != end; ++it )
        {
          const EntityType &entity = *it;
          {
            GeometryTpye geo = entity.geometry();

            const Dune::ReferenceElement< DomainFieldType, dimLocal > &refElement
              = Dune::ReferenceElements< DomainFieldType, dimLocal >::general( geo.type() );

            const LocalCoordinateType xLeft = geo.local( left );
            if( refElement.checkInside( xLeft ) )
            {
              DomainType xGlb = entity.geometry().local( left );
              LocalFunctionType dataLocal = data_.localFunction( entity );
              dataLocal.evaluate( xGlb, u );
              pLeft += u[ dimDomain ];
              ++nLeft;
            }

            const LocalCoordinateType xRight = geo.local( right );
            if( refElement.checkInside( xRight ) )
            {
              DomainType xGlb = entity.geometry().local( right );
              LocalFunctionType dataLocal = data_.localFunction( entity );
              dataLocal.evaluate( xGlb, u );
              pRight += u[ dimDomain ];
              ++nRight;
            }
          }


          if( !entity.hasBoundaryIntersections() )
            continue;

          // we have a boundary intersection
          const IntersectionIteratorType endIt = space.gridPart().iend( entity );
          for( IntersectionIteratorType iIt = space.gridPart().ibegin( entity ); iIt != endIt; ++ iIt )
          {
            if( !iIt->boundary() )
              continue;

            if( iIt->boundaryId() != 1 )
              continue;

            const IntersectionType &intersection = *iIt;
            IntersectionGeometryType geo = intersection.geometry();

            DomainType center = geo.center();
            center -= 0.2;

            if( center.two_norm() > 0.05 + 1e-3 )
              continue;

            //else
            LocalFunctionType dataLocal = data_.localFunction( entity );
            FaceQuadratureType faceQuad( space.gridPart(), intersection, polOrd, FaceQuadratureType::INSIDE );

            const size_t numFaceQuadPoints = faceQuad.nop();
            for( size_t pt = 0; pt < numFaceQuadPoints; ++pt )
            {
              const DomainType normal = intersection.integrationOuterNormal( faceQuad.localPoint( pt ) );
              const double weight = faceQuad.weight( pt );
              dataLocal.evaluate( faceQuad[ pt ], u );
              dataLocal.jacobian( faceQuad[ pt ], jac );
              model_.stress( entity, time, faceQuad.point( pt ), u, jac, stress );

              stress.mv( normal, normalStress );
              normalStress[ dimDomain ] = 0.; 

              ret.axpy( weight, normalStress ); 
            }
          }
        }

        ret[ dimDomain ] = 0.;

        if( nLeft > 0 ) 
          ret[ dimDomain ] = pLeft / nLeft;
        if( nRight >0 )
          ret[ dimDomain ] -= pRight / nRight; 
        return ret;
      }

    private:
      DataImp &data_;
      ModelType &model_;
      std::string name_;
    };


  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_FLOWFEATURES_HH
