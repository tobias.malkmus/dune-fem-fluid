set(HEADERS
  flowfeatures.hh
)

install(FILES ${output_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fem-fluid/output)
