#ifndef DUNE_DIVERROR_HH
#define DUNE_DIVERROR_HH

// where the quadratures are defined
#include <dune/fem/quadrature/cachingquadrature.hh>


namespace Dune
{

  namespace Fem
  {

    template< class DiscreteFunctionType >
    class DivError
    {
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionType::RangeType RangeType;
      typedef typename DiscreteFunctionType::DomainType DomainType;
      typedef typename DiscreteFunctionType::JacobianRangeType JacobianRangeType;
      typedef typename DiscreteFunctionType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename DiscreteFunctionSpaceType::GridType GridType;
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
      typedef typename GridPartType::template Codim< 0 >::GeometryType EnGeometryType;
      typedef typename EnGeometryType::ctype coordType;

      typedef CachingQuadrature< GridPartType, 0 > QuadratureType;
      typedef CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;


      typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;

      enum { dim = GridType::dimension};
      enum { spacePolOrd = DiscreteFunctionSpaceType::polynomialOrder };

    public:
      template< class ModelType >
      RangeType norm ( const ModelType &model, DiscreteFunctionType &discFunc,
                       const double time )
      {
        const int order = 2*discFunc.space().order()+2;
        return norm( model, discFunc, order, time );
      }

      template< class ModelType >
      RangeType norm ( const ModelType &model, DiscreteFunctionType &discFunc,
                       int polOrd = (2 * spacePolOrd + 2),
                       double time = 0.0 )
      {
        // get function space
        const DiscreteFunctionSpaceType &space = discFunc.space();

        JacobianRangeType ret( 0.0 );
        RangeType error( 0.0 );

        IteratorType endit = space.end();
        for( IteratorType it = space.begin(); it != endit; ++it )
        {
          // entity
          const EntityType &en = *it;

          // create quadrature for given geometry type
          QuadratureType quad( en, polOrd );

          // get local function
          LocalFunctionType lf = discFunc.localFunction( en );

          // get geoemetry of entity
          const EnGeometryType &geo = en.geometry();

          // integrate
          const int quadNop = quad.nop();
          for( int qP = 0; qP < quadNop; ++qP )
          {
            const double det = quad.weight( qP ) *
                               geo.integrationElement( quad.point( qP ));

            lf.jacobian( quad[ qP ], ret );

            double local = 0;
            for( int k = 0; k < dim; ++k )
              local += det * ret[ k ][ k ];

            error[ 0 ] += local * local;
          }

          double jump = 0;

          const IntersectionIteratorType &iend = space.gridPart().iend( en );
          for( IntersectionIteratorType iit = space.gridPart().ibegin( en ); iit != iend; ++iit )
          {
            const IntersectionType &intersection = *iit;

            if( intersection.neighbor() )
            {
              EntityType neighbor = intersection.outside();

              LocalFunctionType lfNeighbor = discFunc.localFunction( neighbor );

              FaceQuadratureType faceQuadInside( space.gridPart(), intersection, polOrd, FaceQuadratureType::INSIDE );
              FaceQuadratureType faceQuadOutside( space.gridPart(), intersection, polOrd, FaceQuadratureType::OUTSIDE );

              const size_t numFaceQuadPoints = faceQuadInside.nop();
              for( size_t pt = 0; pt < numFaceQuadPoints; ++pt )
              {
                RangeType uIn, uOut;
                DomainType normal = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

                double weight = faceQuadInside.weight( pt ) * normal.two_norm() / 2.0;

                lf.evaluate( faceQuadInside[ pt ], uIn );
                lfNeighbor.evaluate( faceQuadOutside[ pt ], uOut );
                uIn -= uOut;

                uIn[ 2 ] = 0.;
                jump += uIn.two_norm2() * weight;

              }
            }

            else if( intersection.boundary() && model.hasBoundaryValue( intersection )  )
            {
              FaceQuadratureType faceQuadInside( space.gridPart(), intersection, polOrd, FaceQuadratureType::INSIDE );

              const size_t numFaceQuadPoints = faceQuadInside.nop();
              for( size_t pt = 0; pt < numFaceQuadPoints; ++pt )
              {
                RangeType uIn, uOut;
                JacobianRangeType jac;
                DomainType normal = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

                double weight = faceQuadInside.weight( pt ) * normal.two_norm();

                lf.evaluate( faceQuadInside[ pt ], uIn );
                lf.jacobian( faceQuadInside[ pt ], jac );

                model.boundaryValue( intersection, time, faceQuadInside.localPoint( pt ), uIn, jac, uOut );
//                model.boundaryValue( intersection, time, faceQuadInside.localPoint( pt ), DomainType( 0.0 ), uIn, jac, uOut );

                uIn -= uOut;
                uIn[ 2 ] = 0.;
                jump += uIn.two_norm2() * weight;
              }
            }

          }
          error[ 0 ] += jump;
        }

        error[ 0 ] = std::pow( (error[ 0 ]), 1./2. );
        return error;
      }
    };

  } // namespace Fem

} // namespace Dune

#endif //#ifndef DUNE_DIVERROR_HH
