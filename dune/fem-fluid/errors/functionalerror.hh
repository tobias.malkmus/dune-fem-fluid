#ifndef FUNCTIONALERROR_HH
#define FUNCTIONALERROR_HH

// where the quadratures are defined
#include <dune/fem/quadrature/cachingquadrature.hh>

namespace Dune
{

  namespace Fem
  {

    template< class DiscreteFunctionType >
    class FunctionalError
    {
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionType::RangeType RangeType;
      typedef typename DiscreteFunctionType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionType::JacobianRangeType JacobianRangeType;
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry EnGeometryType;

      typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;

    public:
      template< class FunctionType, class Functor >
      static RangeFieldType norm ( const FunctionType &f, DiscreteFunctionType &discFunc, Functor functor,
                                   double time, int polOrd = 2 )
      {
        // get function space
        const DiscreteFunctionSpaceType &space = discFunc.space();

        JacobianRangeType Du, Duh;

        RangeFieldType error( 0.0 );

        for( const EntityType &en : space )
        {
          // create quadrature for given geometry type
          CachingQuadrature< GridPartType, 0 > quad( en, polOrd );

          // get local function
          LocalFunctionType lf = discFunc.localFunction( en );

          // get geoemetry of entity
          const EnGeometryType &geo = en.geometry();

          // integrate
          const int quadNop = quad.nop();
          for( int qP = 0; qP < quadNop; ++qP )
          {
            const double det = quad.weight( qP ) *
                               geo.integrationElement( quad.point( qP ));

            f.jacobian( geo.global( quad.point( qP )), time, Du );
            lf.jacobian( quad[ qP ], Duh );

            error += det * functor( Du, Duh );
          }
        }

        error = space.gridPart().comm().sum( error );
        error = std::sqrt( error );

        return error;
      } // end of method

    }; // end of class W1PError

  } // namespace Fem

} // namespace Dune
#endif
