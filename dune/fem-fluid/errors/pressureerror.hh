#ifndef DUNE_PRESSUREERROR_HH
#define DUNE_PRESSUREERROR_HH


#include <dune/grid/common/rangegenerators.hh>

// where the quadratures are defined
#include <dune/fem/quadrature/cachingquadrature.hh>

namespace Dune
{

  namespace Fem
  {

    // PressureError
    // -------------

    template< class DiscreteFunctionType >
    class PressureError
    {
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionType::RangeType RangeType;
      typedef typename DiscreteFunctionType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename DiscreteFunctionSpaceType::GridType GridType;
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
      typedef typename GridPartType::template Codim< 0 >::GeometryType EnGeometryType;
      typedef typename EnGeometryType::ctype coordType;

      typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;

      enum { dim = GridType::dimension};
      enum { spacePolOrd = DiscreteFunctionSpaceType::polynomialOrder };
      enum { dimRange = RangeType::dimension };

    public:
      template< class FunctionType >
      double norm ( const FunctionType &f, DiscreteFunctionType &discFunc,
                    const double time,
                    const double p )
      {
        const int order = 2*discFunc.space().order()+2;
        return norm( f, discFunc, order, time, p );
      }

      template< class FunctionType >
      double norm ( const FunctionType &f, DiscreteFunctionType &discFunc,
                    int polOrd = (2 * spacePolOrd + 2),
                    double time = 0.0,
                    double p = 2. )
      {
        // get function space
        const DiscreteFunctionSpaceType &space = discFunc.space();

        const GridPartType &gridPart = space.gridPart();
        typedef typename GridPartType::GridType::Traits::
          CollectiveCommunication
          CommunicatorType;

        const CommunicatorType &comm = gridPart.grid().comm();

        RangeType ret( 0.0 );
        RangeType phi( 0.0 );

        double avgU = 0;
        double avgUh = 0;

        for( const EntityType &entity : space )
        {
          CachingQuadrature< GridPartType, 0 > quad( entity, polOrd );
          LocalFunctionType lf = discFunc.localFunction( entity );
          const EnGeometryType &geo = entity.geometry();

          // integrate
          const int quadNop = quad.nop();
          for( int qP = 0; qP < quadNop; ++qP )
          {
            const double det = quad.weight( qP ) * geo.integrationElement( quad.point( qP ));

            f.evaluate( geo.global( quad.point( qP )), time, ret );
            lf.evaluate( quad[ qP ], phi );
            avgU += det * ret[ 2 ];
            avgUh += det * phi[ 2 ];
          }
        }

        double error( 0.0 );

        for( const EntityType &entity : space )
        {
          // create quadrature for given geometry type
          CachingQuadrature< GridPartType, 0 > quad( entity, polOrd );

          // get local function
          LocalFunctionType lf = discFunc.localFunction( entity );

          // get geoemetry of entity
          const EnGeometryType &geo = entity.geometry();

          // integrate
          const int quadNop = quad.nop();
          for( int qP = 0; qP < quadNop; ++qP )
          {
            const double det = quad.weight( qP ) * geo.integrationElement( quad.point( qP ));

            f.evaluate( geo.global( quad.point( qP )), time, ret );
            lf.evaluate( quad[ qP ], phi );

            error += det * std::pow( std::fabs( ret[ 2 ] - avgU - phi[ 2 ] + avgUh ), p );
          }
        }

        comm.sum( error );
        return std::pow( error, 1. / p );
      } // end of method

    }; // end of class L2Error

  } //namespace Fem

} // namespace Dune
#endif
