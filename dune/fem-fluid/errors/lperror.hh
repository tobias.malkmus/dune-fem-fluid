/**************************************************************************
**       Title: LPError class
**    $RCSfile$
**   $Revision: 6469 $$Name$
**       $Date: 2011-11-01 17:56:00 +0100 (Tue, 01 Nov 2011) $
**   Copyright: GPL $Author: robertk $
** Description: L2 error class, which computes the error between a function
**              and a discrete function. Extracted class from
**              Roberts poisson-example 
**
**************************************************************************/

#ifndef DUNE_LPERROR_HH
#define DUNE_LPERROR_HH

// where the quadratures are defined 
#include <dune/fem/quadrature/cachingquadrature.hh>

namespace Dune 
{
  
namespace Fem 
{
/*======================================================================*/
/*!
 *  \class L2Error
 *  \brief The L2Error class provides methods for error computation
 *
 *  The class calculates || u-u_h ||_L2
 *
 *  Currently only error between a Function and a DiscreteFunction can be
 *  computed or the error between two DiscreteFunctions. 
 */
/*======================================================================*/
  
  template <class DiscreteFunctionType> 
  class LPError
  {
    typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
    typedef typename DiscreteFunctionType::RangeType         RangeType;
    typedef typename DiscreteFunctionType::RangeFieldType    RangeFieldType;
    typedef typename DiscreteFunctionSpaceType::IteratorType         IteratorType;
    typedef typename DiscreteFunctionSpaceType::GridType             GridType;
    typedef typename DiscreteFunctionSpaceType::GridPartType         GridPartType;
    typedef typename GridPartType::template Codim<0>::EntityType     EntityType; 
    typedef typename GridPartType::template Codim<0>::GeometryType   EnGeometryType; 
    typedef typename EnGeometryType::ctype                       coordType;
    
    typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
      
    enum { dim = GridType::dimension};
    enum { spacePolOrd = DiscreteFunctionSpaceType :: polynomialOrder }; 
    enum { dimRange = RangeType :: dimension };

  public:  
/*======================================================================*/
/*! 
 *   norm: computation of the error norm
 *
 *   this method initiates the grid-walkthrough for error computation
 *   The polynomial degree for numerical integration and the 
 *   Type of continuous function are passed as template arguments
 *
 *   \param f the continuous function
 *
 *   \param discFunc the discrete function
 *
 *   \param time the time, at which the functions should 
 *          be evaluated
 *
 *   \return the norm of the L2-Error as RangeType of DiscreteFunction 
 */
/*======================================================================*/
    
    template <class FunctionType> 
    RangeType norm (const FunctionType &f, DiscreteFunctionType &discFunc,
                    const double time,
                    const double p )
    {
      const int order = 2*discFunc.space().order()+2;
      return norm(f,discFunc, order, time, p); 
    }

    template <class FunctionType> 
    RangeType norm (const FunctionType &f, DiscreteFunctionType &discFunc,
                    int polOrd = (2 * spacePolOrd + 2), 
                    double time = 0.0,
                    double p = 2. )
    {
      // get function space
      const DiscreteFunctionSpaceType & space = discFunc.space();  
      
      const GridPartType & gridPart = space.gridPart();
      typedef typename GridPartType :: GridType :: Traits :: 
          CollectiveCommunication
          CommunicatorType; 
      
      const CommunicatorType & comm = gridPart.grid().comm();
      
      RangeType ret (0.0);
      RangeType phi (0.0);
      
      RangeType error(0.0);

      IteratorType endit = space.end();
      for(IteratorType it = space.begin(); it != endit ; ++it)
      {
        // entity
        const EntityType& en = *it;
        
        // create quadrature for given geometry type 
        CachingQuadrature <GridPartType , 0 > quad(en,polOrd); 
        
        // get local function 
        LocalFunctionType lf = discFunc.localFunction(en); 

        // get geoemetry of entity
        const EnGeometryType& geo = en.geometry();
        
        // integrate 
        const int quadNop = quad.nop();
        for(int qP = 0; qP < quadNop; ++qP)
        {
          const double det = quad.weight(qP) * 
              geo.integrationElement(quad.point(qP));

          f.evaluate(geo.global(quad.point(qP)),time,ret);
          lf.evaluate(quad[qP],phi);
          for(int k=0; k<dimRange; ++k) 
          {
            error[k] += det * std::pow( std::fabs(ret[k] - phi[k]), p );
          }
        }
      }
      
      for(int k=0; k<dimRange; ++k)
      {
        error[k] = comm.sum( error[k] );
        error[k] = std::pow( (error[k]), 1./p );
      }
      
      return error;
    } // end of method

}; // end of class L2Error
}
} // end namespace 
#endif
