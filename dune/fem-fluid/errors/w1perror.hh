/**************************************************************************
**       Title: W1PError class
**    $RCSfile$
**       $Date: 2009-06-03 11:01:44 +0200 (Wed, 03 Jun 2009) $
**   Copyright: GPL $Author: tomalk $
** Description: W1p error class, which computes the error between a function
**              and a discrete function.
**
**************************************************************************/

#ifndef W1PERROR_HH
#define W1PERROR_HH

// where the quadratures are defined 
#include <dune/fem/quadrature/cachingquadrature.hh>

namespace Dune 
{
  
namespace Fem 
{
/*======================================================================*/
/*!
 *  \class W1PError
 *  \brief The W1PError class provides methods for error computation
 *
 *  The class calculates || u-u_h ||_W^(1,P)
 *
 *  Currently only error between a Function and a DiscreteFunction can be
 *  computed or the error between two DiscreteFunctions. 
 */
/*======================================================================*/
  
  template <class DiscreteFunctionType> 
  class W1PError
  {
    typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
    typedef typename DiscreteFunctionType::RangeType                 RangeType;
    typedef typename DiscreteFunctionType::RangeFieldType            RangeFieldType;
    typedef typename DiscreteFunctionType::JacobianRangeType         JacobianRangeType;
    typedef typename DiscreteFunctionSpaceType::IteratorType         IteratorType;
    typedef typename DiscreteFunctionSpaceType::GridType             GridType;
    typedef typename DiscreteFunctionSpaceType::GridPartType         GridPartType;
    typedef typename IteratorType :: Entity                          EntityType;
    typedef typename EntityType :: Geometry                          EnGeometryType; 
    typedef typename EnGeometryType::ctype                       coordType;
    typedef typename DiscreteFunctionType::LocalFunctionType         LocalFunctionType;
      
    enum { dimDomain = GridType::dimension};
    enum { spacePolOrd = DiscreteFunctionSpaceType :: polynomialOrder }; 
    enum { dimRange = RangeType :: dimension };

  public:  
/*======================================================================*/
/*! 
 *   norm: computation of the error norm
 *
 *   this method initiates the grid-walkthrough for error computation
 *   The polynomial degree for numerical integration and the 
 *   Type of continuous function are passed as template arguments
 *
 *   \param f the continuous function
 *
 *   \param discFunc the discrete function
 *
 *   \param time the time, at which the functions should 
 *          be evaluated
 *
 *   \return the norm of the L2-Error as RangeType of DiscreteFunction 
 */
/*======================================================================*/
    
    template <class FunctionType> 
    RangeType norm ( const FunctionType &f, DiscreteFunctionType &discFunc,
                     const double time, double p )
    {
      return norm( f, discFunc, p*discFunc.space().order()+1, time, p ); 
    }


    template <class FunctionType> 
    RangeType norm( const FunctionType &f, DiscreteFunctionType &discFunc,
                    int polOrd = (2 * spacePolOrd + 2), 
                    double time = 0.0,
                    double p = 2. ) 
    {
      // get function space
      const DiscreteFunctionSpaceType & space = discFunc.space();  
      const GridPartType & gridPart = space.gridPart();
      typedef typename GridPartType :: GridType :: Traits :: CollectiveCommunication  CommunicatorType; 

      const CommunicatorType & comm = gridPart.grid().comm();
      
      RangeType ret (0.0);
      RangeType phi (0.0);
      
      JacobianRangeType dphi (0.0);
      JacobianRangeType dret (0.0);
      
      RangeType error(0.0);

      IteratorType endit = space.end();
      
      for(IteratorType it = space.begin(); it != endit ; ++it)
      {
        // entity
        const EntityType& en = *it;
        
        // create quadrature for given geometry type 
        CachingQuadrature <GridPartType , 0 > quad(en,polOrd); 
        
        // get local function 
        LocalFunctionType lf = discFunc.localFunction(en); 

        // get geoemetry of entity
        const EnGeometryType& geo = en.geometry();
        
        // integrate 
        const int quadNop = quad.nop();
	 
        RangeType error_Elm_Lp(0.0);
        for(int qP = 0; qP < quadNop; ++qP)
        {
          const double det = quad.weight(qP) * 
            geo.integrationElement(quad.point(qP));  //:= det*weight

          f.evaluate( geo.global(quad.point(qP)), time, ret);
          lf.evaluate( quad[qP], phi);

          f.jacobian( geo.global(quad.point(qP)), time, dret);
          lf.jacobian( quad[qP], dphi);

          for(int r=0; r<dimRange; ++r)
          {
            error[r] += std::pow( std::fabs(ret[r] - phi[r]) , p) * det;
	   
            for( int i=0; i<dimDomain; ++i)
            {
              error[r] += std::pow( std::fabs( dret[r][i] - dphi[r][i] ), p) * det;
            }
          }	 
        } // end loop qp
      } // loop over all elements
      
      for(int k=0; k<dimRange; ++k)
      {
        error[k] = comm.sum( error[k] );
        error[k] = std::pow( error[k], 1./ p );
      }

      return error;
    } // end of method

  }; // end of class W1PError
}
} // end namespace 
#endif
