#ifndef DUNE_FEM_SPACE_DISCONTINUOUSGALERKIN_MINIELEMENT_HH
#define DUNE_FEM_SPACE_DISCONTINUOUSGALERKIN_MINIELEMENT_HH

// dune-fem includes
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/common/defaultcommhandler.hh>
#include <dune/fem/space/shapefunctionset/selectcaching.hh>

#include <dune/fem/space/discontinuousgalerkin/generic.hh>
#include <dune/fem/space/discontinuousgalerkin/basisfunctionsets.hh>
#include <dune/fem/space/discontinuousgalerkin/shapefunctionsets.hh>

#include "shapefunctions.hh"
#include "localrestrictprolong.hh"


namespace Dune
{

  namespace Fem
  {
    // polorder is a dummy
    template< class FunctionSpace, class GridPart, int polorder, template< class > class Storage >
    class MiniElementDiscontinuousGalerkinSpace;

    // LegendreDiscontinuousGalerkinSpaceTraits
    // ----------------------------------------

    template< class FunctionSpace, class GridPart, int polorder, template< class > class Storage >
    struct MiniElementDiscontinuousGalerkinSpaceTraits
    {
      typedef MiniElementDiscontinuousGalerkinSpace< FunctionSpace, GridPart, polorder, Storage > DiscreteFunctionSpaceType;

      typedef FunctionSpace FunctionSpaceType;
      typedef GridPart GridPartType;
      
      static const int codimension = 0;

    private:
      static const int dimLocal = GridPartType::dimension;

    public:
      typedef typename FunctionSpaceType :: ScalarFunctionSpaceType ScalarFunctionSpaceType;
      typedef SimplexMiniElementShapeFunctionSet< ScalarFunctionSpaceType > MiniShapeFunctionSetType;
      typedef SelectCachingShapeFunctionSets< GridPart, MiniShapeFunctionSetType, Storage > ScalarShapeFunctionSetsType;
      typedef VectorialShapeFunctionSets< ScalarShapeFunctionSetsType, typename FunctionSpaceType::RangeType > ShapeFunctionSetsType;

      typedef DefaultBasisFunctionSets< GridPartType, ShapeFunctionSetsType > BasisFunctionSetsType;
      typedef typename BasisFunctionSetsType::BasisFunctionSetType BasisFunctionSetType;

      static const int localBlockSize = FunctionSpaceType::dimRange * MiniShapeFunctionSetType :: numShapeFunctions;
      static const int polynomialOrder = MiniShapeFunctionSetType :: polynomialOrder;

      typedef CodimensionMapper< GridPartType, codimension > BlockMapperType;

      template <class DiscreteFunction, class Operation = DFCommunicationOperation::Copy >
      struct CommDataHandle
      {
        typedef Operation OperationType;
        typedef DefaultCommunicationHandler< DiscreteFunction, Operation > Type;
      };
    };



    // LegendreDiscontinuousGalerkinSpace
    // ----------------------------------

    template< class FunctionSpace, class GridPart, int polorder, template< class > class Storage = CachingStorage >
    class MiniElementDiscontinuousGalerkinSpace
    : public GenericDiscontinuousGalerkinSpace< MiniElementDiscontinuousGalerkinSpaceTraits< FunctionSpace, GridPart, polorder, Storage > >
    {
      typedef MiniElementDiscontinuousGalerkinSpace< FunctionSpace, GridPart, polorder, Storage > ThisType;
      typedef GenericDiscontinuousGalerkinSpace< MiniElementDiscontinuousGalerkinSpaceTraits< FunctionSpace, GridPart, polorder, Storage > > BaseType;

    public:
      using BaseType::basisFunctionSet;

      static const int polynomialOrder = MiniElementDiscontinuousGalerkinSpaceTraits< FunctionSpace, GridPart, polorder, Storage > :: polynomialOrder;

      typedef typename BaseType::GridPartType GridPartType;

      typedef typename BaseType::BasisFunctionSetsType BasisFunctionSetsType;
      typedef typename BaseType::BasisFunctionSetType BasisFunctionSetType;

//      typedef DiscontinuousGalerkinLocalL2Projection< GridPartType, BasisFunctionSetType > InterpolationType;

      MiniElementDiscontinuousGalerkinSpace ( GridPartType &gridPart,
                                              const InterfaceType commInterface = InteriorBorder_All_Interface,
                                              const CommunicationDirection commDirection = ForwardCommunication )
      : BaseType( gridPart, basisFunctionSets( gridPart ), commInterface, commDirection )
      {}

    private:
      static BasisFunctionSetsType basisFunctionSets ( const GridPartType &gridPart )
      {
        typedef typename BasisFunctionSetsType::ShapeFunctionSetsType ShapeFunctionSetsType;
        ShapeFunctionSetsType shapeFunctionSets( gridPart );
        return BasisFunctionSetsType( std::move( shapeFunctionSets ) );
      }

    };


    template< class FunctionSpace,
              class GridPart,
              int polorder,
              template<class> class Storage,  
              class NewFunctionSpace >
    struct DifferentDiscreteFunctionSpace< MiniElementDiscontinuousGalerkinSpace < FunctionSpace, GridPart, polorder, Storage >, NewFunctionSpace >
    {
      typedef MiniElementDiscontinuousGalerkinSpace< NewFunctionSpace, GridPart, polorder, Storage > Type;
    };


  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_DISCONTINUOUSGALERKIN_MINIELEMENT_HH
