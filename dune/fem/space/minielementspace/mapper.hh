#ifndef DUNE_FEM_EDGESPACE_MAPPER_HH
#define DUNE_FEM_EDGESPACE_MAPPER_HH

#include <dune/fem/space/mapper/dofmapper.hh>

namespace Dune
{

  namespace Fem
  {

    // MiniElementSpaceMapper
    // ---------------
    template<class GridPart >
    class MiniElementSpaceMapper;


    template<class GridPartImp >
    struct MiniElementSpaceMapperTraits
    {
      typedef GridPartImp GridPartType;

      // we still need entities of codimension 0 here 
      typedef typename GridPartType :: template Codim< 0 > :: EntityType EntityType;

      typedef typename GridPartType :: IndexSetType IndexSetType;

      typedef MiniElementSpaceMapper< GridPartType >  DofMapperType;

    };


    template< class GridPart >
    class MiniElementSpaceMapper
    : public AdaptiveDofMapper< MiniElementSpaceMapperTraits< GridPart > >
    {
      typedef MiniElementSpaceMapper< GridPart > ThisType;
      typedef AdaptiveDofMapper< MiniElementSpaceMapperTraits< GridPart > > BaseType;

    public:
      typedef  MiniElementSpaceMapperTraits< GridPart > Traits;
      typedef typename Traits :: EntityType  EntityType;
      typedef typename Traits :: IndexSetType   IndexSetType;


      typedef GridPart  GridPartType;
      static const int dimension = GridPartType :: dimension;
      
      MiniElementSpaceMapper ( const GridPartType &gridPart )
      : 
        indexSet_( gridPart.indexSet() ),
        offSet_ ( maxNumDofs() )
      {}

      bool consecutive () const
      {
        return indexSet_.consecutive();
      }

      template< class Entity >
      size_t numEntityDofs ( const Entity &entity ) const
      {
        return ( Entity :: codimension == 0 ) * maxNumDofs();
      }

      template< class Entity >
      int mapEntityDofToGlobal ( const Entity &entity, const int localDof ) const
      {
        // should only be called for codim = 0 entitys
        assert( Entity :: codimension == 0 );
        assert( 0<= localDof && localDof <= maxNumDofs() );
        return indexSet_.index( entity ) * offSet_ + localDof;
      }

      int numberOfHoles(const int block) const
      {
        return indexSet_.numberOfHoles( 0 );
      }

      /** \copydoc DofMapper::oldIndex */
      int oldIndex ( const int hole, int ) const
      {
        // forward to index set 
        return indexSet_.oldIndex( hole, 0 ) ;
      }

      /** \copydoc DofMapper::newIndex */
      int newIndex ( const int hole, int ) const
      {
        // forward to index set 
        return indexSet_.newIndex( hole, 0 );
      }

      template< class Functor >
      void mapEach ( const EntityType &element, Functor f ) const
      {
      }

      template< class Entity, class Functor >
      void mapEachEntityDof ( const Entity &entity, Functor f ) const
      {
      }

      int numDofs ( const EntityType &entity ) const
      {
        return maxNumDofs();
      }

      int maxNumDofs() const
      {
        return dimension + 2;
      }

      bool contains ( const int codim ) const
      {
        return ( codim == 0 );
      }

      int size () const
      {
        return  indexSet_.size( 0 ) * offSet_;
      }

      int mapToGlobal ( const EntityType &entity, const int localDof ) const
      {
        // we only have one local dof
        assert( localDof < maxNumDofs() );
        return indexSet_.index( entity ) * offSet_ + localDof;
      }


      bool fixedDataSize ( int codim ) const { return true; }
      int numBlocks() const { return 1; }
    protected:
      const IndexSetType &indexSet_;
      const int offSet_;
    };

  } // namespace Fem

} // namespace Dune 

#endif // #ifndef DUNE_FEM_EDGESPACE_MAPPER_HH
