#ifndef DUNE_FEM_SPACE_MINIELEMENTSPACE_LOCALRESTRICTPROLONG_HH
#define DUNE_FEM_SPACE_MINIELEMENTSPACE_LOCALRESTRICTPROLONG_HH

// dune-fem includes
#include <dune/fem/space/discontinuousgalerkin/localrestrictprolong.hh>
#include <dune/fem/space/minielementspace.hh>

namespace Dune
{

  namespace Fem
  {
    // external forward declaration
    template< class FunctionSpace, class GridPart, int polord, template< class > class Storage >
    class MiniElementDiscontinuousGalerkinSpace;

    /** @ingroup RestrictProlongImpl
        @{
    **/


    // DefaultLocalRestrictProlong for DiscontinuousGalerkinSpace
    // ----------------------------------------------------------

    template< class FunctionSpaceImp, class GridPartImp, int polord, template< class > class StorageImp >
    struct DefaultLocalRestrictProlong< MiniElementDiscontinuousGalerkinSpace< FunctionSpaceImp, GridPartImp, polord, StorageImp > >
    : public DiscontinuousGalerkinLocalRestrictProlong< MiniElementDiscontinuousGalerkinSpace< FunctionSpaceImp, GridPartImp, polord, StorageImp >, false >
    {
      typedef DiscontinuousGalerkinLocalRestrictProlong< MiniElementDiscontinuousGalerkinSpace<
        FunctionSpaceImp, GridPartImp, polord, StorageImp >, false  >  BaseType;
      DefaultLocalRestrictProlong ( const MiniElementDiscontinuousGalerkinSpace< FunctionSpaceImp, GridPartImp, polord, StorageImp > & space )
        : BaseType( space )
      {}
    };

    ///@}

  } // namespace Fem

} // namespace Dune

#endif //_#ifndef DUNE_FEM_SPACE_MINIELEMENTSPACE_LOCALRESTRICTPROLONG_HH
