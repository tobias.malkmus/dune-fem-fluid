#ifndef DUNE_FEM_MINIELEMENTADAPTMANAGER_HH
#define DUNE_FEM_MINIELEMENTADAPTMANAGER_HH

#include <dune/common/exceptions.hh>
//- local includes  
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>




namespace Dune
{

//***********************************************************************
/** \brief This is a restriction/prolongation operator for DG data. 
 */

  namespace Fem
  {

    template< class, class, template<class> class >
      class MiniElementDiscreteFunctionSpace;
  

    template< class FunctionSpace, class GridPart, template<class> class BaseFunctionStorage >
    struct DefaultLocalRestrictProlong<
      MiniElementDiscreteFunctionSpace< FunctionSpace, GridPart, BaseFunctionStorage > >
    : public EmptyLocalRestrictProlong< Fem::MiniElementDiscreteFunctionSpace<
    FunctionSpace, GridPart, BaseFunctionStorage > >
    {
      DefaultLocalRestrictProlong ( const Fem:: MiniElementDiscreteFunctionSpace<
          FunctionSpace, GridPart, BaseFunctionStorage >  & )
      {}
    };

  } // end namespace Fem

} // end namespace Dune 

#endif // #ifndef DUNE_FEM_MINIELEMENTADAPTMANAGER_HH
