#ifndef DUNE_FEM_SPACE_TAYLORHOOD_SPACE_HH
#define DUNE_FEM_SPACE_TAYLORHOOD_SPACE_HH

// dune-fem includes
#include <dune/fem/space/common/defaultcommhandler.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/discontinuousgalerkin/generic.hh>
#include <dune/fem/space/discontinuousgalerkin/lagrange.hh>
#include <dune/fem/space/lagrange/shapefunctionset.hh>
#include <dune/fem/space/shapefunctionset/selectcaching.hh>
#include <dune/fem/space/shapefunctionset/tuple.hh>

#include <dune/fem/operator/projection/local/l2projection.hh>
#include <dune/fem/operator/projection/local/riesz.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>

#include <dune/fem/space/taylorhood/tupleshapefunctions.hh>


namespace Dune
{

  namespace Fem
  {

    // internal forward declaration

    // TaylorHoodDiscontinuousGalerkinSpace
    // ------------------------------------

    template< class VelocityFunctionSpace, class GridPart, int polorder, template< class > class Storage >
    class TaylorHoodDiscontinuousGalerkinSpace;



    // TaylorHoodDiscontinuousGalerkinSpaceTraits
    // ------------------------------------------

    template< class VelocityFunctionSpace, class GridPart, int polorder, template< class > class Storage >
    struct TaylorHoodDiscontinuousGalerkinSpaceTraits
    {
      typedef TaylorHoodDiscontinuousGalerkinSpace< VelocityFunctionSpace, GridPart, polorder, Storage > DiscreteFunctionSpaceType;
      typedef GridPart GridPartType;

      static const int codimension = 0;

    protected:
      typedef FunctionSpace< typename GridPart::ctype, typename GridPart::ctype, GridPart::dimension, 1 > ScalarFunctionSpace;

      static const int velocityDimRange = VelocityFunctionSpace::dimRange;
      static const int pressureDimRange = velocityDimRange / GridPart::dimension;

      static_assert( velocityDimRange % GridPart::dimension == 0,
                     "dimension of VelocityFunctionSpace needs to be a multiple of the Grid::dimension!" );

    public:
      typedef VelocityFunctionSpace VelocityFunctionSpaceType;
      typedef typename ToNewDimRangeFunctionSpace< VelocityFunctionSpace, pressureDimRange >::Type PressureFunctionSpaceType;

      static_assert( pressureDimRange >= 1, "wrong pressureDimRange !" );

      typedef typename ToNewDimRangeFunctionSpace< VelocityFunctionSpace, velocityDimRange + pressureDimRange >::Type FunctionSpaceType;

      static_assert( polorder >= 2, "TaylorHoodDiscontinuousGalerkinSpace works only for p >=2 !" );


      typedef SelectCachingShapeFunctionSets< GridPartType, LagrangeShapeFunctionSet< ScalarFunctionSpace, polorder >, Storage >
      ScalarVelocityShapeFunctionSetType;
      typedef VectorialShapeFunctionSets< ScalarVelocityShapeFunctionSetType, typename VelocityFunctionSpace::RangeType > VelocityShapeFunctionSetType;

      typedef SelectCachingShapeFunctionSets< GridPartType, LagrangeShapeFunctionSet< ScalarFunctionSpace, polorder-1 >, Storage >
      ScalarPressureShapeFunctionSetType;
      typedef VectorialShapeFunctionSets< ScalarPressureShapeFunctionSetType, typename PressureFunctionSpaceType::RangeType > PressureShapeFunctionSetType;

      typedef TupleShapeFunctionSets< VelocityShapeFunctionSetType, PressureShapeFunctionSetType > ShapeFunctionSetsType;

      typedef DefaultBasisFunctionSets< GridPartType, ShapeFunctionSetsType > BasisFunctionSetsType;

      typedef typename BasisFunctionSetsType::BasisFunctionSetType BasisFunctionSetType;

      typedef typename GeometryWrapper<
        Dune::Fem::GridPartCapabilities::hasSingleGeometryType< GridPartType >::topologyId, GridPartType::dimension
        >::ImplType GenericGeometryType;

      static const int numVelocityShapeFunctions = GenericLagrangeBaseFunction< ScalarFunctionSpace, GenericGeometryType, polorder >::numBaseFunctions * velocityDimRange;
      static const int numPressureShapeFunctions = GenericLagrangeBaseFunction< ScalarFunctionSpace, GenericGeometryType, polorder - 1 >::numBaseFunctions * pressureDimRange;

      static const int localBlockSize = numVelocityShapeFunctions + numPressureShapeFunctions;
      static const int polynomialOrder = polorder;

      typedef CodimensionMapper< GridPartType, codimension > BlockMapperType;

      template< class DiscreteFunction, class Operation = DFCommunicationOperation::Copy >
      struct CommDataHandle
      {
        typedef Operation OperationType;
        typedef DefaultCommunicationHandler< DiscreteFunction, Operation > Type;
      };
    };



    // TaylorHoodDiscontinuousGalerkinSpace
    // ------------------------------------

    template< class VelocityFunctionSpace, class GridPart, int polorder, template< class > class Storage = CachingStorage >
    class TaylorHoodDiscontinuousGalerkinSpace
      : public GenericDiscontinuousGalerkinSpace< TaylorHoodDiscontinuousGalerkinSpaceTraits< VelocityFunctionSpace, GridPart, polorder, Storage > >
    {
      typedef TaylorHoodDiscontinuousGalerkinSpace< VelocityFunctionSpace, GridPart, polorder, Storage > ThisType;
      typedef GenericDiscontinuousGalerkinSpace< TaylorHoodDiscontinuousGalerkinSpaceTraits< VelocityFunctionSpace, GridPart, polorder, Storage > > BaseType;

    public:
      typedef TaylorHoodDiscontinuousGalerkinSpaceTraits< VelocityFunctionSpace, GridPart, polorder, Storage > Traits;
      using BaseType::basisFunctionSet;

      static const int polynomialOrder = Traits::polynomialOrder;

      static const int numVelocityShapeFunctions = Traits::numVelocityShapeFunctions;
      static const int numPressureShapeFunctions = Traits::numPressureShapeFunctions;

      typedef typename Traits::VelocityFunctionSpaceType VelocityFunctionSpaceType;
      typedef typename Traits::PressureFunctionSpaceType PressureFunctionSpaceType;

      typedef typename BaseType::GridPartType GridPartType;

      typedef typename BaseType::BasisFunctionSetsType BasisFunctionSetsType;
      typedef typename BaseType::BasisFunctionSetType BasisFunctionSetType;

      typedef typename BaseType::EntityType EntityType;

    private:
      typedef CachingQuadrature< GridPart, EntityType::codimension > QuadratureType;
      typedef DenseLocalRieszProjection< BasisFunctionSetType, QuadratureType > LocalRieszProjectionType;

    public:
      typedef DefaultLocalL2Projection< LocalRieszProjectionType, QuadratureType > InterpolationType;

      TaylorHoodDiscontinuousGalerkinSpace ( GridPartType &gridPart,
                                             const InterfaceType commInterface = InteriorBorder_All_Interface,
                                             const CommunicationDirection commDirection = ForwardCommunication )
        : BaseType( gridPart, basisFunctionSets( gridPart ), commInterface, commDirection )
      {}

      InterpolationType interpolation ( const EntityType &entity ) const
      {
        return InterpolationType( basisFunctionSet( entity ) );
      }

    private:
      static BasisFunctionSetsType basisFunctionSets ( const GridPartType &gridPart )
      {
        typedef typename BasisFunctionSetsType::ShapeFunctionSetsType ShapeFunctionSetsType;
        ShapeFunctionSetsType shapeFunctionSets( gridPart );
        return BasisFunctionSetsType( std::move( shapeFunctionSets ) );
      }
    };


    // DifferentDiscreteFunctionSpace
    // ------------------------------

    template< class VelocityFunctionSpace, class GridPart, int polorder, template< class > class Storage, class NewFunctionSpace >
    struct DifferentDiscreteFunctionSpace< TaylorHoodDiscontinuousGalerkinSpace< VelocityFunctionSpace, GridPart, polorder, Storage >, NewFunctionSpace >
    {
    private:
      static const int gridDim = GridPart::dimension;
      static const int newDimRange = NewFunctionSpace::dimRange;
      static const bool exists = ( newDimRange % ( gridDim + 1 ) == 0 );
      static const int newVelocityDimRange = newDimRange * gridDim / ( gridDim + 1 );

      typedef typename ToNewDimRangeFunctionSpace< VelocityFunctionSpace, newVelocityDimRange >::Type NewVelocityFunctionSpace;

    public:
      // we assume that a taylor hood space needs to have dimRange = alpha * ( d + 1 ), if not we return a LagrangeDGSpace
      typedef typename std::conditional< exists,
                                         TaylorHoodDiscontinuousGalerkinSpace< NewVelocityFunctionSpace, GridPart, polorder, Storage >,
                                         LagrangeDiscontinuousGalerkinSpace< NewFunctionSpace, GridPart, polorder, Storage >
                                         >::type Type;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_TAYLORHOOD_SPACE_HH
