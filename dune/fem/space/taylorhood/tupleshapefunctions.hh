#ifndef DUNE_FEM_SPACE_TAYLORHOOD_TUPLESHAPEFUNCTIONS_HH
#define DUNE_FEM_SPACE_TAYLORHOOD_TUPLESHAPEFUNCTIONS_HH

#include <tuple>

#include <dune/fem/common/utility.hh>
#include <dune/fem/space/discontinuousgalerkin/shapefunctionsets.hh>
#include <dune/fem/space/shapefunctionset/tuple.hh>

namespace Dune
{

  namespace Fem
  {


    // TupleShapeFunctionSets
    // ----------------------

    template< class ... Implementations >
    class TupleShapeFunctionSets
    {
      typedef TupleShapeFunctionSets< Implementations ... > ThisType;

      typedef decltype ( std::index_sequence_for< Implementations ... >() ) Sequence;

    public:
      /** \brief shape function set type */
      typedef TupleShapeFunctionSet< typename Implementations::ShapeFunctionSetType ... > ShapeFunctionSetType;

      /** \name Construction
       *  \{
       */

      /*
         template< class... Args >
         explicit TupleShapeFunctionSets ( Args &&...args )
         : impl_( std::forward< Args >( args )... )
         {}
       */

      explicit TupleShapeFunctionSets ( Implementations &&... impls )
        : impl_( impls ... )
      {}

      template< class ... Args >
      explicit TupleShapeFunctionSets ( Args &&... args )
        : impl_( Implementations( std::forward< Args >( args ) ... ) ... )
      {}

      TupleShapeFunctionSets ( ThisType &&other )
        : impl_( std::move( other.impl_ ) )
      {}

      /** \} */

      /** \name Copying and assignment
       *  \{
       */

      TupleShapeFunctionSets ( const ThisType & ) = delete;

      TupleShapeFunctionSets ( ThisType & ) = delete;

      TupleShapeFunctionSets &operator= ( const ThisType & ) = delete;

      /** \} */

      /** \name Public member methods
       *  \{
       */

      /** \copydoc Dune::Fem::ShapeFunctionSets::types */
      const std::vector< Dune::GeometryType > &types () const { return std::get< 0 >( impl_ ).types(); }

      /** \copydoc Dune::Fem::ShapeFunctionSets::order */
      int order () const { return order( Sequence() ); }

      /** \copydoc Dune::Fem::ShapeFunctionSets::order */
      int order ( Dune::GeometryType type ) const { return order( type, Sequence() ); }

      /** \copydoc Dune::Fem::ShapeFunctionSets::shapeFunctionSet */
      ShapeFunctionSetType shapeFunctionSet ( const Dune::GeometryType &type ) const
      {
        return shapeFunctionSet( type, Sequence() );
      }

      /** \} */

    protected:

      template< std::size_t ... I >
      int order ( std::index_sequence< I ... > ) const
      {
        return Std::max( std::get< I >( impl_ ).order() ... );
      }

      template< std::size_t ... I >
      int order ( Dune::GeometryType type, std::index_sequence< I ... > ) const
      {
        return Std::max( std::get< I >( impl_ ).order( type ) ... );
      }

      template< std::size_t ... I >
      ShapeFunctionSetType shapeFunctionSet ( const Dune::GeometryType &type, std::index_sequence< I ... > ) const
      {
        return ShapeFunctionSetType( std::get< I >( impl_ ).shapeFunctionSet( type ) ... );
      }

    private:
      std::tuple< Implementations ... > impl_;
    };
  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_TAYLORHOOD_TUPLESHAPEFUNCTIONS_HH
