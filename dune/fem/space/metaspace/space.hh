#ifndef DUNE_FEM_SPACE_METASPACE_SPACE_HH
#define DUNE_FEM_SPACE_METASPACE_SPACE_HH

#include <dune/fem/space/common/discretefunctionspace.hh>

namespace Dune
{

  namespace Fem
  {

    // internal forward declaration

    // MetaDiscreteFunctionSpace
    // -------------------------

    template< class DiscreteFunctionSpace, class BlockMapper, int blockSize >
    class MetaDiscreteFunctionSpace;


    // MetaDiscreteFunctionSpaceTraits
    // -------------------------------

    template< class DiscreteFunctionSpace, class BlockMapper, int blockSize >
    struct MetaDiscreteFunctionSpaceTraits
      : public DiscreteFunctionSpace::Traits
    {
      typedef MetaDiscreteFunctionSpace< DiscreteFunctionSpace, BlockMapper, blockSize > DiscreteFunctionSpaceType;

      static const int localBlockSize = blockSize;

      typedef BlockMapper BlockMapperType;
    };


    // MetaDiscreteFunctionSpace
    // -------------------------

    template< class DiscreteFunctionSpace, class BlockMapper, int blockSize >
    class MetaDiscreteFunctionSpace
      : public DiscreteFunctionSpace
    {
      typedef MetaDiscreteFunctionSpace< DiscreteFunctionSpace, BlockMapper, blockSize > ThisType;
      typedef DiscreteFunctionSpace BaseType;

    public:
      typedef MetaDiscreteFunctionSpaceTraits< DiscreteFunctionSpace, BlockMapper, blockSize > Traits;

    private:
      // here we need to declare the BaseType::{blockMapper(), communicator(), slaveDofs()} private
      using BaseType::blockMapper;
      using BaseType::communicator;
      using BaseType::slaveDofs;

      typedef CommunicationManager< ThisType > CommunicationManagerType;
    public:

      typedef typename BaseType::GridPartType GridPartType;
      typedef typename Traits::BlockMapperType BlockMapperType;

      typedef SlaveDofs< ThisType, BlockMapperType > SlaveDofsType;
    protected:
      typedef typename SlaveDofsType::SingletonKey SlaveDofsKeyType;
      typedef SingletonList< SlaveDofsKeyType, SlaveDofsType > SlaveDofsProviderType;

      struct SlaveDofsDeleter
      {
        void operator() ( SlaveDofsType *slaveDofs )
        {
          SlaveDofsProviderType::removeObject( *slaveDofs );
        }
      };

    public:
      static const int localBlockSize = Traits::localBlockSize;

      MetaDiscreteFunctionSpace ( GridPartType &gridPart,
                                  const InterfaceType commInterface = InteriorBorder_All_Interface,
                                  const CommunicationDirection commDirection = ForwardCommunication )
        : BaseType( gridPart, commInterface, commDirection ), blockMapper_( BaseType::blockMapper() )
      {}

      BlockMapperType &blockMapper () const
      {
        return blockMapper_;
      }

      const SlaveDofsType &slaveDofs () const
      {
        if( !slaveDofs_ )
          slaveDofs_.reset( &( SlaveDofsProviderType :: getObject( SlaveDofsKeyType( BaseType::gridPart(), blockMapper() ) ) ),
               SlaveDofsDeleter() );
        slaveDofs_->rebuild( *this );
        return *slaveDofs_;
      }

      const CommunicationManagerType &communicator () const
      {
        if( !comm_ )
          comm_.reset( new CommunicationManagerType( *this, BaseType::communicationInterface(), BaseType::communicationDirection() ) );
        return *comm_;
      }

      template< class DiscreteFunction >
      void communicate ( DiscreteFunction &df ) const
      {
        typename Traits::template CommDataHandle< DiscreteFunction >::OperationType op;
        communicate( df, op );
      }

      template< class DiscreteFunction, class Op >
      void communicate ( DiscreteFunction &df, const Op &op ) const
      {
        communicator().exchange( df, op );
      }

    private:
      mutable BlockMapperType blockMapper_;
      mutable std::unique_ptr< CommunicationManagerType > comm_;
      mutable std::shared_ptr< SlaveDofsType > slaveDofs_;
    };


    // DifferentDiscreteFunctionSpace
    // ------------------------------

    template< class DiscreteFunctionSpace, class BlockMapper, int blockSize, class NewFunctionSpace >
    struct DifferentDiscreteFunctionSpace< MetaDiscreteFunctionSpace< DiscreteFunctionSpace, BlockMapper, blockSize >, NewFunctionSpace >
    {
      typedef typename DifferentDiscreteFunctionSpace< DiscreteFunctionSpace, NewFunctionSpace >::Type NewSpaceType;

      static const int newBlockSize = blockSize * NewSpaceType::localBlockSize / DiscreteFunctionSpace::localBlockSize;
      typedef MetaDiscreteFunctionSpace< NewSpaceType, BlockMapper, newBlockSize > Type;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_METASPACE_SPACE_HH
