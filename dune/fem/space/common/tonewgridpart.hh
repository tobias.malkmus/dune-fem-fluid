#ifndef DUNE_FEM_SPACE_TONEWGRIDPART_HH
#define DUNE_FEM_SPACE_TONEWGRIDPART_HH

#include <dune/fem/space/common/discretefunctionspace.hh>
#include <dune/fem/function/common/discretefunction.hh>

namespace Dune
{
  
  namespace Fem
  {

    // ToNewGridPart
    // -------------

    template< class OldFunctionType, class NewGridPart >
    struct ToNewGridPart;

    template< class FunctionSpaceImp,
              class OldGridPart,
              int polOrd,
              template <class> class StorageImp,
              template <class,class,int,template <class> class> class DiscreteFunctionSpaceImp,
              template <class> class DiscreteFunctionImp,
              class NewGridPart >
    struct ToNewGridPart< DiscreteFunctionImp< DiscreteFunctionSpaceImp< FunctionSpaceImp, OldGridPart, polOrd, StorageImp > >,
                          NewGridPart >
    {
      typedef  DiscreteFunctionImp< DiscreteFunctionSpaceImp< FunctionSpaceImp, NewGridPart, polOrd, StorageImp > > Type;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_TONEWGRIDPART_HH
