#ifndef DUNE_FEM_SPACE_METASPACE_TUPLENONBLOCKMAPPER_HH
#define DUNE_FEM_SPACE_METASPACE_TUPLENONBLOCKMAPPER_HH

#include <tuple>
#include <utility>

#include <dune/fem/common/forloop.hh>

#include <dune/common/std/utility.hh>
#include <dune/fem/misc/functor.hh>
#include <dune/fem/common/utility.hh>
#include <dune/fem/space/mapper/dofmapper.hh>
#include <dune/fem/space/mapper/capabilities.hh>
#include <dune/fem/gridpart/common/indexset.hh>

namespace Dune
{

  namespace Fem
  {

    template< class BlockMapper, int ... blockSizes >
    class TupleNonBlockMapper;

    template< class BlockMapper, int ... blockSizes >
    struct TupleNonBlockMapperTraits
    {
      typedef TupleNonBlockMapper< BlockMapper, blockSizes ... > DofMapperType;

      typedef typename BlockMapper::ElementType ElementType;
      typedef typename BlockMapper::SizeType SizeType;
      typedef typename BlockMapper::GlobalKeyType GlobalKeyType;
    };


    template< class BlockMapper, int ... blockSizes >
    class TupleNonBlockMapper
      : public DofMapper< TupleNonBlockMapperTraits< BlockMapper, blockSizes ... > >
    {
      typedef TupleNonBlockMapper< BlockMapper, blockSizes ... > ThisType;
      typedef DofMapper< TupleNonBlockMapperTraits< BlockMapper, blockSizes ... > > BaseType;

      typedef TupleNonBlockMapperTraits< BlockMapper, blockSizes ... > Traits;

    public:
      typedef typename Traits::ElementType ElementType;
      typedef typename Traits::SizeType SizeType;
      typedef typename Traits::GlobalKeyType GlobalKeyType;

    private:
      static const int blockSize = Std::sum( blockSizes ... );

      template< int ... I >
      struct BlockSizes
      {
        template< int j >
        static constexpr int size () { return std::get< j >( std::make_tuple( blockSizes ... ) ); }

        template< int ... j >
        static constexpr Std::integer_sequence< int, size< j >() ... > sizes ( Std::integer_sequence< int, j ... > )
        {
          return Std::integer_sequence< int, size< j >() ... >();
        }

        template< int i >
        static constexpr int offset ()
        {
          return sum( sizes( Std::make_integer_sequence< int, i >() ) );
        }

      private:
        template< int ... j >
        static constexpr int sum ( Std::integer_sequence< int, j ... > )
        {
          return Std::sum( j ... );
        }

        static constexpr int sum ( Std::integer_sequence< int > ) { return 0; }
      };


      template< class Functor >
      struct BlockFunctor
      {
        template< int I >
        struct EachBlock
        {
          static const int localBlockSize = std::get< I >( std::make_tuple( blockSizes ... ) );
          static const int offset = BlockSizes< blockSizes ... >::template offset< I >();

          template< class GlobalKey >
          static void apply ( int localKey, const GlobalKey &globalKey, Functor &functor, SizeType localSize, SizeType globalSize )
          {
            assert( localKey == 0 );
            // !!! here we assume localBlock == 0 !!!
            int localDof = localBlockSize * localKey + offset * localSize;
            const int localEnd = localDof + localBlockSize;

            SizeType globalDof = localBlockSize * globalKey + offset * globalSize;

            while( localDof != localEnd )
              functor( localDof++, globalDof++ );
          }
        };

        explicit BlockFunctor ( Functor functor, SizeType localSize, SizeType globalSize )
          : functor_( functor ), localSize_( localSize ), globalSize_( globalSize )
        {}

        template< class GlobalKey >
        void operator() ( int localKey, const GlobalKey &globalKey )
        {
          Fem::ForLoop< EachBlock, 0, sizeof ... ( blockSizes ) -1 >::apply( localKey, globalKey, functor_, localSize_, globalSize_ );
        }

      private:
        Functor functor_;
        SizeType localSize_, globalSize_;
      };

    public:

      explicit TupleNonBlockMapper ( BlockMapper &blockMapper ) : blockMapper_( blockMapper ) {}

      int size () const { return blockSize * blockMapper_.size(); }

      template< class Functor >
      void mapEach ( const ElementType &element, Functor f ) const
      {
        blockMapper_.mapEach( element, BlockFunctor< Functor >( f, blockMapper_.numDofs( element ), blockMapper_.size() ) );
      }

      void map ( const ElementType &element, std::vector< std::size_t > &indices ) const
      {
        indices.resize( numDofs( element ) );
        mapEach( element, AssignFunctor< std::vector< std::size_t > >( indices ) );
      }

      template< class Entity, class Functor >
      void mapEachEntityDof ( const Entity &entity, Functor f ) const
      {
        blockMapper_.mapEachEntityDof( entity, BlockFunctor< Functor >( f, blockMapper_.numEntityDofs( entity ), blockMapper_.size() ) );
      }

      int maxNumDofs () const
      {
        return blockSize * blockMapper_.maxNumDofs();
      }

      int numDofs ( const ElementType &element ) const
      {
        return blockSize * blockMapper_.numDofs( element );
      }

      template< class Entity >
      int numEntityDofs ( const Entity &entity ) const
      {
        return blockSize * blockMapper_.numEntityDofs( entity );
      }

      int numberOfHoles ( const int block ) const
      {
        return blockSize * blockMapper_.numberOfHoles( block );
      }

      int oldIndex ( const int hole, const int block ) const
      {
        const int i = hole % blockSize;
        const int blockHole = hole / blockSize;
        return blockMapper_.oldIndex( blockHole, block ) * blockSize + i;
      }

      int newIndex ( const int hole, const int block ) const
      {
        const int i = hole % blockSize;
        const int blockHole = hole / blockSize;
        return blockMapper_.newIndex( blockHole, block ) * blockSize + i;
      }

      bool consecutive () const
      {
        return blockMapper_.consecutive();
      }

      int oldOffSet ( const int block ) const
      {
        return blockMapper_.oldOffSet( block ) * blockSize;
      }

      int offSet ( const int block ) const
      {
        return blockMapper_.offSet( block ) * blockSize;
      }

      int numBlocks () const
      {
        return blockMapper_.numBlocks();
      }

      bool contains ( const int codim ) const
      {
        return blockMapper_.contains( codim );
      }

    private:
      BlockMapper &blockMapper_;
    };

    namespace Capabilities
    {

      template< class BlockMapper, int ... blockSizes >
      struct isAdaptiveDofMapper< TupleNonBlockMapper< BlockMapper, blockSizes ... > >
      {
        static const bool v = isAdaptiveDofMapper< BlockMapper >::v;
      };

      template< class BlockMapper, int ... blockSizes >
      struct isConsecutiveIndexSet< TupleNonBlockMapper< BlockMapper, blockSizes ... > >
      {
        static const bool v = isConsecutiveIndexSet< BlockMapper >::v;
      };

    }

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_METASPACE_TUPLENONBLOCKMAPPER_HH
