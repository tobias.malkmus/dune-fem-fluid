set(HEADERS
  mapper.hh
  tuplenonblockmapper.hh
)

install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fem/space/mapper)
