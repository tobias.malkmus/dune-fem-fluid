#ifndef DUNE_FEM_SPACE_METASPACE_MAPPER_HH
#define DUNE_FEM_SPACE_METASPACE_MAPPER_HH

#include <vector>

#include <dune/fem/space/mapper/dofmapper.hh>
#include <dune/fem/misc/functor.hh>

namespace Dune
{

  namespace Fem
  {

    template< class BlockMapper, int localBlockSize >
    class GlobalUnBlockMapper;

    template< class BlockMapper, int localBlockSize >
    struct GlobalUnBlockMapperTraits
    {
      typedef GlobalUnBlockMapper< BlockMapper, localBlockSize > DofMapperType;

      typedef typename BlockMapper::ElementType ElementType;
      typedef typename BlockMapper::SizeType SizeType;
      typedef typename BlockMapper::GlobalKeyType GlobalKeyType;
    };


    template< class BlockMapper, int blockSize >
    class GlobalUnBlockMapper
      : public DofMapper< GlobalUnBlockMapperTraits< BlockMapper, blockSize > >
    {
      typedef GlobalUnBlockMapper< BlockMapper, blockSize > ThisType;
      typedef DofMapper< GlobalUnBlockMapperTraits< BlockMapper, blockSize > > BaseType;

      typedef GlobalUnBlockMapperTraits< BlockMapper, blockSize > Traits;

    public:
      typedef typename Traits::ElementType ElementType;
      typedef typename Traits::SizeType SizeType;
      typedef typename Traits::GlobalKeyType GlobalKeyType;

    private:
      template< class Functor >
      struct BlockFunctor
      {
        explicit BlockFunctor ( Functor functor, SizeType size )
          : functor_( functor ), size_( size )
        {}

        template< class GlobalKey >
        void operator() ( int localBlock, const GlobalKey globalKey )
        {
          int localDof = blockSize*localBlock;
          SizeType globalDof = globalKey;
          const int localEnd = localDof + blockSize;

          while( localDof != localEnd )
          {
            functor_( localDof++, globalDof );
            globalDof += size_;
          }
        }

      private:
        Functor functor_;
        SizeType size_;
      };

    public:

      explicit GlobalUnBlockMapper ( BlockMapper &blockMapper ) : blockMapper_( blockMapper ) {}

      int size () const { return blockSize * blockMapper_.size(); }

      template< class Functor >
      void mapEach ( const ElementType &element, Functor f ) const
      {
        blockMapper_.mapEach( element, BlockFunctor< Functor >( f, blockMapper_.size() ) );
      }

      void map ( const ElementType &element, std::vector< std::size_t > &indices ) const
      {
        indices.resize( numDofs( element ) );
        mapEach( element, AssignFunctor< std::vector< std::size_t > >( indices ) );
      }

      template< class Entity, class Functor >
      void mapEachEntityDof ( const Entity &entity, Functor f ) const
      {
        blockMapper_.mapEachEntityDof( entity, BlockFunctor< Functor >( f, blockMapper_.size() ) );
      }

      int maxNumDofs () const
      {
        return blockSize * blockMapper_.maxNumDofs();
      }

      int numDofs ( const ElementType &element ) const
      {
        return blockSize * blockMapper_.numDofs( element );
      }

      template< class Entity >
      int numEntityDofs ( const Entity &entity ) const
      {
        return blockSize * blockMapper_.numEntityDofs( entity );
      }

      int numberOfHoles ( const int block ) const
      {
        return blockSize * blockMapper_.numberOfHoles( block );
      }

      int oldIndex ( const int hole, const int block ) const
      {
        const int i = hole % blockSize;
        const int blockHole = hole / blockSize;
        return blockMapper_.oldIndex( blockHole, block ) * blockSize + i;
      }

      int newIndex ( const int hole, const int block ) const
      {
        const int i = hole % blockSize;
        const int blockHole = hole / blockSize;
        return blockMapper_.newIndex( blockHole, block ) * blockSize + i;
      }

      bool consecutive () const
      {
        return blockMapper_.consecutive();
      }

      int oldOffSet ( const int block ) const
      {
        return blockMapper_.oldOffSet( block ) * blockSize;
      }

      int offSet ( const int block ) const
      {
        return blockMapper_.offSet( block ) * blockSize;
      }

      int numBlocks () const
      {
        return blockMapper_.numBlocks();
      }

      bool contains ( const int codim ) const
      {
        return blockMapper_.contains( codim );
      }

    private:
      BlockMapper &blockMapper_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_METASPACE_MAPPER_HH
