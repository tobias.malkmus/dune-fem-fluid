#ifndef DUNE_FEM_SPACE_BREZZIDOUGLASMARINI_DECLARATION_HH
#define DUNE_FEM_SPACE_BREZZIDOUGLASMARINI_DECLARATION_HH

namespace Dune
{

  namespace Fem
  {

    // BDMDiscreteFunctionSpace
    // ------------------------

    template< class FunctionSpace, class GridPart, int order, template< class > class Storage >
    struct BDMDiscreteFunctionSpace;

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_BREZZIDOUGLASMARINI_DECLARATION_HH
