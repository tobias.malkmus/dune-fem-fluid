#ifndef DUNE_FEM_MISC_PAIRRANGE_HH
#define DUNE_FEM_MISC_PAIRRANGE_HH

#include <iterator>
#include <utility>


namespace Dune
{

  namespace Fem
  {


    // PairRange
    // ---------

    template< class Iterator >
    struct PairRange
    {
      typedef typename std::iterator_traits< Iterator >::reference reference;
      typedef std::pair< reference, reference > PairReference;

      struct InnerIterator
      {
        InnerIterator () {}

        InnerIterator ( Iterator first, Iterator end )
          : first_( first ), second_( first ), end_( end )
        {
          if( (first_ != end_) && (++second_ == end_) )
            first_ = end_;
        }

        InnerIterator ( Iterator end )
          : first_( end ), second_( end ), end_( end ) {}

        InnerIterator ( const InnerIterator &other ) = default;
        InnerIterator &operator= ( const InnerIterator &other ) = default;

        PairReference operator* () const { return PairReference( *first_, *second_ ); }

        bool operator!= ( const InnerIterator &other ) const { return ( first_ != other.first_ ) || ( second_ != other.second_ ); }
        bool operator== ( const InnerIterator &other ) const { return ( first_ == other.first_ ) && ( second_ == other.second_ ); }

        InnerIterator &operator++ ()
        {
          if( (++second_) == end_ )
          {
            second_ = ++first_;
            if( ++second_ == end_ )
              first_ = end_;
          }
          return *this;
        }

        InnerIterator operator++ ( int ) { InnerIterator copy( *this ); ++(*this); return copy; }

      private:
        Iterator first_, second_, end_;
      };

      PairRange ( const PairRange & ) = default;
      PairRange ( Iterator begin, Iterator end ) : begin_( begin ), end_( end ) {}

      InnerIterator begin () const { return InnerIterator( begin_, end_ ); }
      InnerIterator end () const { return InnerIterator( end_ ); }

    private:
      Iterator begin_, end_;
    };


    // pairRange
    // ---------

    template< class Iterator >
    PairRange< Iterator > pairRange ( Iterator begin, Iterator end )
    {
      return PairRange< Iterator >( begin, end );
    }


    // IteratorRange
    // -------------

    template< class Iterator >
    struct IteratorRange
    {
      IteratorRange ( Iterator begin, Iterator end ) : begin_( begin ), end_( end ) {}

      Iterator begin () { return begin_; }
      Iterator end () { return end_; }
    private:
      Iterator begin_, end_;
    };


    // iteratorRange
    // -------------

    template< class Iterator >
    IteratorRange< Iterator > iteratorRange ( Iterator begin, Iterator end )
    {
      return IteratorRange< Iterator >( begin, end );
    }

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_MISC_PAIRRANGE_HH
