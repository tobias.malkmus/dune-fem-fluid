#ifndef DUNE_FEM_SOLVER_ODEALGORITHMS_HH
#define DUNE_FEM_SOLVER_ODEALGORITHMS_HH

//- system includes
#include <cmath>
#include <vector>
#include <memory>

//- dune-common includes
#include <dune/common/dynmatrix.hh>
#include <dune/common/dynvector.hh>

//- dune-fem includes
#include <dune/fem/solver/rungekutta/butchertable.hh>

namespace FemODE
{

  // OdeAlgorithm
  // ------------

  template< class Operator >
  class OdeAlgorithm
  {
  protected:
    typedef typename Operator::DestinationType Destination;

  public:
    OdeAlgorithm ( Operator &op ) : op_( op ) {}

    virtual ~OdeAlgorithm () {}

    virtual int order () const = 0;

    virtual int stages () const = 0;

    virtual double stageTime ( double time, int stage, double dt ) const = 0;

    virtual void rhs ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages, Destination &rhs ) const = 0;

    virtual double lambda ( int stage, double dt ) const = 0;

    virtual void extrapolate ( int stage, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages ) const {}

    Operator &spaceOperator () { return op_; }
    const Operator &spaceOperator () const { return op_; }

  private:
    Operator &op_;
  };


  // BackwardEulerAlgorithm
  // ----------------------

  template< class Operator >
  class BackwardEulerAlgorithm
    : public OdeAlgorithm< Operator >
  {
    typedef BackwardEulerAlgorithm< Operator > This;
    typedef OdeAlgorithm< Operator > Base;
    typedef typename Base::Destination Destination;

  public:
    BackwardEulerAlgorithm ( Operator &op ) : Base( op ) {}

    int order () const { return 1; }

    int stages () const { return 1; }

    double stageTime ( double time, int stage, double dt ) const { return time + dt; }

    void rhs ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages, Destination &rhs ) const
    {
      assert( stages.size() == 1 );
      Destination &stage = *stages[ s ];
      spaceOperator().copy( dt, u, stage );
      spaceOperator().rhs( stage, rhs, 0.0 );
    }

    double lambda ( int stage, double dt ) const { return dt; }

    void extrapolate ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages ) const
    {
      assert( stages.size() == 1 );
      Destination &stage = *stages[ s ];
      const double lambdas = lambda( s, dt );
      spaceOperator().rescale( 1.0, 1.0/lambdas, stage );
    }

    using Base::spaceOperator;
  };


  // CrankNicolsonAlgorithm
  // ----------------------

  template< class Operator >
  class CrankNicolsonAlgorithm
    : public OdeAlgorithm< Operator >
  {
    typedef CrankNicolsonAlgorithm< Operator > This;
    typedef OdeAlgorithm< Operator > Base;
    typedef typename Base::Destination Destination;

  public:
    CrankNicolsonAlgorithm ( Operator &op ) : Base( op ) {}

    int order () const { return 2; }

    int stages () const { return 1; }

    double stageTime ( double time, int stage, double dt ) const { return time + dt; }

    void rhs ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages, Destination &rhs ) const
    {
      assert( stages.size() == 1 );
      Destination &stage = *stages[ s ];
      spaceOperator().copy( 0.5 * dt, u, stage );
      spaceOperator().rhs( stage, rhs, 0.5 * dt );
    }

    double lambda ( int stage, double dt ) const { return 0.5 * dt; }

    void extrapolate ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages ) const
    {
      assert( stages.size() == 1 );
      Destination &stage = *stages[ s ];
      spaceOperator().rescale( 1.0, 1.0/dt, stage );
    }

    using Base::spaceOperator;
  };


  // ClassicalFractionalThetaAlgorithm
  // ---------------------------------

  template< class Operator >
  class ClassicalFractionalThetaAlgorithm
    : public OdeAlgorithm< Operator >
  {
    typedef ClassicalFractionalThetaAlgorithm< Operator > This;
    typedef OdeAlgorithm< Operator > Base;
    typedef typename Base::Destination Destination;

    const double theta_, thetaPrime_, alpha_, beta_;
    std::vector< double > c_;

  public:
    ClassicalFractionalThetaAlgorithm ( Operator &op )
      : Base( op ),
        theta_( 1.0 - std::sqrt( 2.0 ) / 2.0 ),
        thetaPrime_( 1.0 - 2.0 * theta_ ),
        alpha_( ( 1.0 - 2. * theta_ ) / ( 1.0 - theta_ ) ),
        beta_( 1.0 - alpha_ ),
        c_ {{ theta_, 1.0 - theta_, 1.0 }}
    {}

    int order () const { return 2; }

    int stages () const { return 3; }

    double stageTime ( double time, int stage, double dt ) const
    {
      return time + dt * c_[ stage ];
    }

    void rhs ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages, Destination &rhs ) const
    {
      assert( stages.size() == 3 );

      Destination &stage = *stages[ s ];
      const Destination &prev = ( s > 0 ) ? *( stages[ s -1 ] ) : u;

      spaceOperator().copy( lambda( s, dt ), prev, stage );

      double factor = ( s == 1 ) ? alpha_ * thetaPrime_ * dt : beta_ *theta_ *dt;
      spaceOperator().rhs( stage, rhs, factor );
    }

    double lambda ( int stage, double dt ) const
    {
      return ( stage == 1 ) ? beta_ * thetaPrime_ * dt : alpha_ *theta_ *dt;
    }

    void extrapolate ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages ) const
    {
      assert( stages.size() == 3 );
      Destination &stage = *stages[ s ];
      const double lambdas = theta_ * dt;
      spaceOperator().rescale( 1.0, 1.0/lambdas, stage );
    }

    using Base::spaceOperator;
  };


  // FractionalThetaAlgorithm
  // ------------------------

  template< class Operator >
  class FractionalThetaAlgorithm
    : public OdeAlgorithm< Operator >
  {
    typedef FractionalThetaAlgorithm< Operator > This;
    typedef OdeAlgorithm< Operator > Base;
    typedef typename Base::Destination Destination;

    const double theta_, alpha_, beta_;
    std::vector< double > c_;

  public:
    FractionalThetaAlgorithm ( Operator &op )
      : Base( op ),
        theta_( 1.0 - std::sqrt( 2.0 ) / 2.0 ),
        alpha_( ( 1.0 - theta_ ) / theta_ ),
        beta_( 1.0 - alpha_ ),
        c_ {{ theta_, 1.0 }}
    {}

    int order () const { return 2; }

    int stages () const { return 2; }

    double stageTime ( double time, int stage, double dt ) const
    {
      return time + dt * c_[ stage ];
    }

    void rhs ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages, Destination &rhs ) const
    {
      assert( stages.size() == 2 );

      Destination &stage = *stages[ s ];
      const Destination &prev = ( s > 0 ) ? *( stages[ s - 1 ] ) : u;
      spaceOperator().copy( lambda( s, dt ), prev, stage );

      spaceOperator().rhs( stage, rhs, 0.0 );
    }

    double lambda ( int stage, double dt ) const { return theta_ * dt; }

    void extrapolate ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages ) const
    {
      assert( stages.size() == 2 );
      Destination &stage = *stages[ s ];
      const double lambdas = lambda( s, dt );
      if( s == 0 )
      {
        // extrapolate velocity
        spaceOperator().rescale( alpha_, 1.0/lambdas, stage );
        spaceOperator().velocityAxpy( stage, beta_, u );
      }
      else
      {
        spaceOperator().rescale( 1.0, theta_ / lambdas, stage );
        spaceOperator().pressureAxpy( stage, 1.0 - theta_, *stages[ s-1] );
      }
    }

    using Base::spaceOperator;
  };

  // ConstraintRungeKuttaAlgorithm
  // -----------------------------

  template< class Operator >
  class ConstraintRungeKuttaAlgorithm
    : public OdeAlgorithm< Operator >
  {
    typedef ConstraintRungeKuttaAlgorithm< Operator > This;
    typedef OdeAlgorithm< Operator > Base;
    typedef typename Base::Destination Destination;

    unsigned int order_;
    unsigned int stages_;
    double delta_;
    Dune::DynamicMatrix< double > alpha_;
    Dune::DynamicVector< double > gamma_, beta_, c_;

  public:
    ConstraintRungeKuttaAlgorithm ( Operator &op, int order )
      : Base( op ), order_( order )
    {
      DuneODE::SimpleButcherTable< double > table = butcherTable( order );
      std::cout <<"created butcher table of order: "<< table.order() <<std::endl;

      stages_ = table.stages();
      alpha_ = table.A();
      c_ = table.c();

      gamma_.resize( stages_ );
      beta_.resize( stages_ );

      // compute coefficients
      Dune::DynamicMatrix< double > AL( alpha_ );
      for( int i = 0; i < stages(); ++i )
      {
        gamma_[ i ] = AL[ i ][ i ];
        AL[ i ][ i ] = 0.0;
      }

      alpha_.invert();
      alpha_.mtv( table.b(), beta_ );

      alpha_.leftmultiply( AL );
      for( int i = 0; i < stages(); ++i )
        alpha_[ i ][ i ] = gamma_[ i ];

      for( int i = 0; i < stages(); ++i )
      {
        gamma_[ i ] = 1.0;
        for( int j = 0; j < i; ++j )
          gamma_[ i ] -= alpha_[ i ][ j ];
      }

      delta_ = 1.0;
      for( int i = 0; i < stages(); ++i )
        delta_ -= beta_[ i ];
    }

    int order () const { return order_; }

    int stages () const { return stages_; }

    double stageTime ( double time, int stage, double dt ) const
    {
      return time + dt * c_[ stage ];
    }

    void rhs ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages, Destination &rhs ) const
    {
      assert( stages.size() == stages_ );

      Destination &stage = *stages[ s ];
      stage.assign( u );
      stage *= gamma_[ s ];
      for( int k = 0; k < s; ++k )
        stage.axpy( alpha_[ s ][ k ], *stages[ k ] );
      spaceOperator().rhs( stage, rhs, 0.0 );

      spaceOperator().copy( lambda( s, dt ), u, stage );
    }

    double lambda ( int stage, double dt ) const { return alpha_[ stage ][ stage ] * dt; }

    void extrapolate ( int s, double dt, const Destination &u, std::vector< std::unique_ptr< Destination > > &stages ) const
    {
      assert( stages.size() == stages_ );
      Destination &stage = *stages[ s ];

      // rescale pressure
      const double l = lambda( s, dt );
      spaceOperator().rescale( 1.0, 1.0 / l, stage );

      const int lastStage = stages_ - 1;
      if( s == lastStage )
      {
        stage *= beta_[ lastStage ];
        for( int k = lastStage-1; k >= 0; k-- )
          stage.axpy( beta_[ k ], *stages[ k ] );
        stage.axpy( delta_, u );
      }
    }

    using Base::spaceOperator;

  protected:
    static DuneODE::SimpleButcherTable< double > butcherTable ( int order )
    {
      switch( order )
      {
      case 1:
        return DuneODE::implicitEulerButcherTable();
      case 2:
        return DuneODE::gauss2ButcherTable();
      case 3:
        return DuneODE::implicit3ButcherTable();
      case 4:
        return DuneODE::implicit34ButcherTable();
      default:
        DUNE_THROW( Dune::NotImplemented, "Implicit Runge-Kutta method of order " << order << " not implemented." );
      }
    }
  };

} // namespace FemODE

#endif // #ifndef DUNE_FEM_SOLVER_ODEALGORITHMS_HH
