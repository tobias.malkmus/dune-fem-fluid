#ifndef DUNE_FEM_SOLVER_BASICIMPLICT_HH
#define DUNE_FEM_SOLVER_BASICIMPLICT_HH

//- system includes
#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>

//- Dune includes
#include <dune/fem/solver/odesolverinterface.hh>
#include <dune/fem/solver/timeprovider.hh>

#include <dune/fem/solver/odealgorithms.hh>


namespace FemODE
{

  struct ImODEParameters
    : public Dune::Fem::LocalParameter< ImODEParameters, ImODEParameters >
  {
    enum { noVerbosity = 0,  noConvergenceVerbosity = 1,
           iterativeSteps = 2, cflVerbosity = 3, fullVerbosity = 4 };

    ImODEParameters ( Dune::Fem::ParameterReader parameter = Dune::Fem::Parameter::container() )
      : // number of minimal iterations that the linear solver should do
        // if the number of iterations done is smaller then the cfl number is increased
        min_it( parameter.getValue< int >( "fem.ode.miniterations", 14 ) ),
        // number of maximal iterations that the linear solver should do
        // if the number of iterations larger then the cfl number is decreased
        max_it( parameter.getValue< int >( "fem.ode.maxiterations", 16 ) ),
        // factor for cfl number on increase (decrease is 0.5)
        sigma( parameter.getValue< double >( "fem.ode.cflincrease", 1.1 ) ),
        cfl_( parameter.getValue< double >( "fem.ode.cflStart", 1 ) ),
        parameter_( parameter )
    {}

    int obtainOdeSolverType () const
    {
      // we need this choice of explicit or implicit ode solver
      // defined here, so that it can be used later in two different
      // methods
      static const std::string odeSolver[]  = { "BWE", "CN", "FSTN", "FSTC", "RKO"  };
      return parameter_.getEnum( "fem.ode.odesolver", odeSolver, 3 );
    }

    /** \brief tolerance for the non-linear solver (should be larger than the tolerance for
               the linear solver */
    virtual double tolerance () const { return parameter_.getValue< double >( "fem.ode.tolerance", 1e-6 ); }

    virtual int iterations () const { return parameter_.getValue< int >( "fem.ode.iterations", 1000 ); }
    /** \brief verbosity level ( none, noconv, cfl, full )  */
    virtual int verbose () const
    {
      static const std::string verboseTypeTable[]
        = { "none", "noconv", "iters", "cfl", "full" };
      return parameter_.getEnum( "fem.ode.verbose", verboseTypeTable, 0 );
    }

    virtual double cflMax () const { return parameter_.getValue< double >( "fem.ode.cflMax", std::numeric_limits< double >::max() ); }

    virtual double cflMin () const { return parameter_.getValue< double >( "fem.ode.cflMin", std::numeric_limits< double >::min() ); }

    double initialDeltaT ( double dt ) const { return std::min( parameter_.getValue< double >( "fem.ode.initialdt", 987654321 ), 987654321. ); }

    /** \brief return multiplication factor for the current cfl number
     *  \param[in] imOpTimeStepEstimate Time step estimate of the first ode solver
     *  \param[in] exOpTimeStepEstimate Time step estimate of the second ode solver
     *  \param[in] solver Iterative linear solver (ILS)
     *  \param[in] converged Convergence of the ILS
     *  \param[out] factor Multiplication factor for the current cfl number
     *
     *  \note Do not increase the cfl number of the implicit solver if its time step
     *    estimate is already larger than the one of the explicit solver
     */
    virtual std::pair< double, bool >
    cflFactor ( const double imOpTimeStepEstimate,
                const double exOpTimeStepEstimate,
                const int numberOfLinearIterations,
                bool converged ) const
    {
      const int iter = numberOfLinearIterations;
      double factor = 1.0;
      bool changed = false;
      if( converged )
      {
        if( iter < min_it )
        {
          if( imOpTimeStepEstimate <= exOpTimeStepEstimate )
          {
            factor = sigma;
            changed = true;
          }
        }
        else if( iter > max_it )
        {
          factor = (double)max_it/(sigma*(double)iter);
          changed = true;
        }
      }
      else
      {
        factor = 0.5;
        changed = true;
      }

      if( (factor >= std::numeric_limits< double >::min()) &&
          (factor <= std::numeric_limits< double >::max()) )
      {
        if( factor < 1 || cfl_ <= cflMax() || cfl_ >= cflMin() )
          cfl_ *= factor;
        else 
          changed = false;
      }
      else
        DUNE_THROW( Dune::InvalidStateException, "invalid cfl factor: " << factor );

      return std::make_pair( cfl_, changed );
    }

    virtual void initTimeStepEstimate ( const double dtEstExpl, const double dtEstImpl, double &dtEst, double &cfl ) const
    {
      // initial time step already set to explicit time step
      dtEst = dtEstExpl;

      // heuristics for initial CFL number
      cfl = 1.0;
      if( (dtEstImpl > 0) && (dtEstExpl > dtEstImpl) )
        cfl = dtEstExpl / dtEstImpl;
    }

    const int min_it, max_it;
    const double sigma;
    mutable double cfl_;
  private:
    Dune::Fem::ParameterReader parameter_;
  };


  /** \brief First order Implicit RungeKutta ODE solver. */
  template< class OperatorImp, class NonLinearSolver, class SolverMonitor >
  class ImplicitOdeSolver
//    : public DuneODE::OdeSolverInterface< typename OperatorImp::DestinationType >
  {
    typedef ImplicitOdeSolver< OperatorImp, NonLinearSolver, SolverMonitor > ThisType;
//    typedef DuneODE::OdeSolverInterface< typename OperatorImp::DestinationType > BaseType;

  public:
    typedef OperatorImp OperatorType;
    typedef NonLinearSolver NonLinearSolverType;

    typedef typename OperatorType::DestinationType DestinationType;

    typedef typename DestinationType::DiscreteFunctionSpaceType SpaceType;
    typedef SolverMonitor MonitorType;

//    using BaseType::solve;

  protected:
    typedef OdeAlgorithm< OperatorType > OdeAlgorithmType;

    typedef BackwardEulerAlgorithm< OperatorType > BackwardEulerAlgorithmType;
    typedef CrankNicolsonAlgorithm< OperatorType > CrankNicolsonAlgorithmType;
    typedef FractionalThetaAlgorithm< OperatorType > FractionalThetaAlgorithmType;
    typedef ClassicalFractionalThetaAlgorithm< OperatorType > ClassicalFractionThetaAlgorithmType;
    typedef ConstraintRungeKuttaAlgorithm< OperatorType > ConstraintRungeKuttaAlgorithmType;


  public:
    /** \brief constructor
       \param[in] op Operator \f$L\f$
       \param[in] tp TimeProvider
       \param[in] pord polynomial order
       \param[in] verbose verbosity
     */
    ImplicitOdeSolver ( OperatorType &op,
                        Dune::Fem::TimeProviderBase &tp,
                        const int pord = 1,
                        Dune::Fem::ParameterReader parameter = Dune::Fem::Parameter::container() )
      : op_( op ),
        nLsolver_( op_ ),
        tp_( tp ),
        initialized_( false ),
        param_( new ImODEParameters( parameter ) ),
        algorithm_( getAlgorithm( op, pord ) ),
        rhs_( "RK rhs", op.space() )
    {
      const int stages = algorithm().stages();
      upd.resize( stages );
      for( int i = 0; i < stages; ++i )
        upd[ i ].reset( new DestinationType( std::string( "stage" ) + std::to_string( i ), op_.space() ) );
    }

    ImplicitOdeSolver ( OperatorType &op, Dune::Fem::TimeProviderBase &tp, OdeAlgorithmType *odeAlgorithm,
                        Dune::Fem::ParameterReader parameter = Dune::Fem::Parameter::container() )
      : op_( op ),
        nLsolver_( op_ ),
        tp_( tp ),
        initialized_( false ),
        param_( new ImODEParameters( parameter ) ),
        algorithm_( odeAlgorithm ),
        rhs_( "RK rhs", op.space() )
    {
      const int stages = algorithm().stages();
      upd.resize( stages );
      for( int i = 0; i < stages; ++i )
        upd[ i ].reset( new DestinationType( std::string( "stage" ) + std::to_string( i ), op_.space() ) );
    }

    //! apply operator once to get dt estimate
    void initialize ( const DestinationType &U0 )
    {
      if( !initialized_ )
      {
        // Compute Steps
        op_.initializeTimeStepSize( U0 );
        initialized_ = true;

        // provide operators time step estimate
        tp_.provideTimeStepEstimate( parameter().initialDeltaT( op_.timeStepEstimate() ) );
      }
    }

    //! solve the system
    void solve ( DestinationType &U, MonitorType &monitor )
    {
      // no information here
      monitor.reset();

      // get cfl * timeStepEstimate
      double deltaT = tp_.deltaT();
      assert( deltaT > 0 );

      // get time
      const double t = tp_.time();

      // initialize
      if( !initialized_ )
        DUNE_THROW( Dune::InvalidStateException, "ImplicitRungeKuttaSolver wasn't initialized before first call!" );

      bool converged = true;
      {
        op_.setTime( t );

        const int stages = algorithm().stages();
        for( int s = 0; s < stages; ++s )
        {

          // get rhs and intialize new solution
          algorithm().rhs( s, deltaT, U, upd, rhs_ );

          // set time to operator
          op_.setTime( algorithm().stageTime( t, s, deltaT ) );
          op_.setLambda( algorithm().lambda( s, deltaT ) );

          DestinationType &stage = *(upd[ s ]);
          nLsolver_( rhs_, stage );

          converged &= nLsolver_.converged();
          monitor.newtonIterations_ += nLsolver_.iterations();
          monitor.linearSolverIterations_ += nLsolver_.linearIterations();

          if( !converged )
            break;

          algorithm().extrapolate( s, deltaT, U, upd );
        }

        if( converged )
          U.assign( *(upd.back() ) );
      }

      if( verbose( ImODEParameters::iterativeSteps ) )
        std::cout<< tp_.timeStep()<<"\t"<< monitor.linearSolverIterations_ <<"\t" << monitor.newtonIterations_ <<std::endl;

      double dtCan = op_.timeStepEstimate();

      std::pair< double, bool > cflPair =
        parameter().cflFactor( dtCan, dtCan, nLsolver_.iterations(), converged );


      if( converged )
      {
        tp_.provideTimeStepEstimate( cflPair.first * dtCan );

        if( cflPair.second && verbose( ImODEParameters::cflVerbosity ) )
          Dune::derr << " New cfl number is: "<< cflPair.first << ", iterations per time step("
                     << "ILS: " << monitor.linearSolverIterations_
                     << ", Newton: " << monitor.newtonIterations_
                     << ")"
                     << std::endl;
      }
      else
      {
        tp_.provideTimeStepEstimate( cflPair.first * deltaT );
        tp_.invalidateTimeStep();

        // output only in verbose mode
        if( verbose( ImODEParameters::noConvergenceVerbosity ) )
          Dune::derr << "No convergence: New cfl number is " << cflPair.first <<", iterations per time step("
                     << "ILS: " << monitor.linearSolverIterations_
                     << ", Newton: " << monitor.newtonIterations_
                     << ")"
                     << std::endl;
      }

    }

    void description ( std::ostream &out ) const
    {
      out << "ImplRungeKutta, steps: " << algorithm().order()
        //<< ", cfl: " << this->tp_.factor()
          << "\\\\" <<std::endl;
    }

  protected:

    bool verbose ( int level ) const 
    {
      return ( level <= parameter().verbose() ) && ( Dune::Fem::MPIManager::rank() == 0 ); 
    }

    OdeAlgorithmType* getAlgorithm ( OperatorType &op, int order )
    {
      switch( parameter().obtainOdeSolverType() )
      {
        // bwe
        case 0:
          return new BackwardEulerAlgorithmType( op );
        // crank nicolson
        case 1:
          return new CrankNicolsonAlgorithmType( op );
        // new style fractional-step theta scheme
        case 2:
          return new FractionalThetaAlgorithmType( op );
        // old style fractional-step theta scheme
        case 3:
          return new ClassicalFractionThetaAlgorithmType( op );
        // general constraint Runge Kutta Method
        case 4:
          return new ConstraintRungeKuttaAlgorithmType( op, order );
        default:
          std::cerr << "The desired solver type is not implemented !" << std::endl;
          abort();
      }

      return nullptr;
    }


    //! return reference to algorithm
    const OdeAlgorithmType &algorithm () const { return *algorithm_; }

    //! return reference to parameter class
    const ImODEParameters &parameter () const { assert( param_ ); return *param_; }

  private:
    // non linear operator to solve for
    OperatorType &op_;

    // non linear solver
    NonLinearSolverType nLsolver_;

    // time provider
    Dune::Fem::TimeProviderBase &tp_;

    // init flag
    bool initialized_;

    // parameter class
    const std::unique_ptr< ImODEParameters > param_;

    // ode solver algorithm, !!! uses param class !!!
    std::unique_ptr< OdeAlgorithmType > algorithm_;

    DestinationType rhs_;
    std::vector< std::unique_ptr< DestinationType > > upd;

    double cfl_;
  };

} // namespace FemODE

#endif // #ifndef DUNE_FEM_SOLVER_BASICIMPLICT_HH
