#ifndef DUNE_FEM_SOLVER_UZAWA_HH
#define DUNE_FEM_SOLVER_UZAWA_HH

#include <tuple>

#include <dune/fem/common/utility.hh>
#include <dune/fem/operator/common/operator.hh>


namespace Dune
{

  namespace Fem
  {

    // TupleArgs
    // ---------

    template< class ... D >
    struct TupleArgs
      : public std::tuple< D ... >
    {
      static_assert( Std::are_all_same< typename D::DomainFieldType ... >::value,
                     "TupleArgs needs a common DomainFieldType to be defined." );
      static_assert( Std::are_all_same< typename D::RangeFieldType ... >::value,
                     "TupleArgs needs a common RangeFieldType to be defined." );

      typedef typename std::tuple_element< 0, std::tuple< D ... > >::type::DomainFieldType DomainFieldType;
      typedef typename std::tuple_element< 0, std::tuple< D ... > >::type::RangeFieldType RangeFieldType;

      template< class ... Args >
      TupleArgs ( Args&& ... args ) : std::tuple< D ... >( std::forward< Args >( args ) ... ) {}
    };


    // Uzawa Inverse Operator, solving:
    //
    //  A B^T  u = f
    //  B 0    p = g
    //

    template< class ARange, class BRange >
    class UzawaInverseOperator
      : public Operator< TupleArgs< ARange, BRange > >
    {
      typedef UzawaInverseOperator< ARange, BRange > ThisType;
      typedef Operator< TupleArgs< ARange, BRange > > BaseType;

    public:

      typedef typename BaseType::DomainFunctionType DomainType;
      typedef typename BaseType::RangeFunctionType RangeType;
      typedef typename BaseType::RangeFieldType RangeFieldType;

      typedef Operator< ARange, ARange > AOperatorType;
      typedef Operator< ARange, BRange > BOperatorType;
      typedef Operator< BRange, ARange > BTOperatorType;
      typedef Operator< BRange, BRange > Preconder;

      UzawaInverseOperator ( const Preconder &precon, const AOperatorType &aInv, const BOperatorType &b, const BTOperatorType &bt,
                             RangeFieldType eps, std::size_t maxIter, bool verbose )
        : precon_( precon ), aInv_( aInv ), b_( b ), bt_( bt ), eps_( eps ), maxIter_( maxIter ), verbose_( verbose ) {}

      void operator() ( const DomainType &arg, RangeType &dest ) const
      {
        (*this)( std::get< 0 >( arg ), std::get< 1 >( arg ), std::get< 0 >( dest ), std::get< 1 >( dest ) );
      }

      void operator() ( const ARange &b, const BRange &g, ARange &x, BRange &lambda ) const
      {
        // reset iteration counter
        iter_ = 0;

        ARange p( b );
        bt_( lambda, p );
        p -= b;
        p *= -1.0;

        ARange h( b );
        aInv_( p, x );

        BRange d( g ), k( g );
        b_( x, d );
        d -= g;
        precon_( d, k );

        BRange r( k ), rOld( k );

        RangeFieldType res = r*k;
        RangeFieldType preRes = res;

        while( res > eps_ && iter_ < maxIter_ )
        {
          if( verbose_ )
            std::cout<< "Uzawa-Iteration: " << iter_ << ", Residuum: " << res << std::endl;

          if( iter_ > 0 )
          {
            d *= ( res / preRes );
            d += k;
          }

          bt_( d, p );
          aInv_( p, h );

          RangeFieldType alpha = res / ( p * h );
          lambda.axpy( alpha, d );
          x.axpy( -1.0 * alpha, h );

          b_( x, r );
          r -= g;
          precon_( r, k );

          preRes = res;
          res = r*k;
          iter_++;
        }

        if( verbose_ )
          std::cout<< "Uzawa-Iteration: " << iter_ << ", Residuum: " << res << std::endl;
      }

      std::size_t iterations () const { return iter_; }

    protected:
      const Preconder &precon_;
      const AOperatorType &aInv_;
      const BOperatorType &b_;
      const BTOperatorType &bt_;

      const RangeFieldType eps_;
      const std::size_t maxIter_;
      const bool verbose_;
      mutable std::size_t iter_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SOLVER_UZAWA_HH
