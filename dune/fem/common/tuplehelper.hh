#ifndef DUNE_FEM_COMMON_TUPLEHELPER_HH
#define DUNE_FEM_COMMON_TUPLEHELPER_HH

#include <utility>

namespace Dune
{

  template< class D, class I, template< class... > class TT >
  struct SameTypeTupleImpl;

  template< class D, std::size_t... i, template< class... > class TT >
  struct SameTypeTupleImpl< D, std::index_sequence< i... >, TT >
  {
    template< std::size_t j >
    using T = D;

    typedef TT< T< i >... > Type;

    template< class ... Args >
    static Type construct ( Args && ... args )
    {
      return Type( T< i >( args... )... );
    }
  };

  template< class D, std::size_t N, template< class... > class TT >
  using SameTypeTuple = SameTypeTupleImpl< D, std::make_index_sequence< N >, TT >;

} // namespace Dune

#endif // #ifndef DUNE_FEM_COMMON_TUPLEHELPER_HH
