#ifndef DUNE_FEM_OPERATOR_COMMON_LOCALMATRIXADAPTER_HH
#define DUNE_FEM_OPERATOR_COMMON_LOCALMATRIXADAPTER_HH

#include <fstream>

#include <dune/fem/operator/common/localmatrix.hh>
#include <dune/fem/function/localfunction/localfunctionset.hh>

namespace Dune
{

  namespace Fem
  {
    
    template< class LocalFunctionSet >
    class LocalMatrixAdapter;


    template< class LocalFunctionSet >
    struct LocalMatrixAdapterTraits
    {
      typedef typename LocalFunctionSet::DiscreteFunctionSpaceType DomainSpaceType;
      typedef typename LocalFunctionSet::DiscreteFunctionSpaceType RangeSpaceType;

      typedef typename LocalFunctionSet::RangeFieldType RangeFieldType;
      typedef typename LocalFunctionSet::RangeFieldType LittleBlockType;

      typedef LocalMatrixAdapter< LocalFunctionSet > LocalMatrixType;
    };


    template< class LocalFunctionSet >
    class LocalMatrixAdapter
    : public LocalMatrixDefault< LocalMatrixAdapterTraits< LocalFunctionSet > > 
    {
      typedef LocalMatrixDefault< LocalMatrixAdapterTraits< LocalFunctionSet > > BaseType;

      typedef LocalMatrixAdapter< LocalFunctionSet > ThisType;

      typedef typename LocalFunctionSet::EntityType EntityType;

      typedef typename LocalFunctionSet::DofType DofType;

    public:
      LocalMatrixAdapter ( LocalFunctionSet &set )
      : BaseType( set.discreteFunctionSpace(), set.discreteFunctionSpace() ),
        set_( set )
      {
        init( set.entity(), set.entity() );
      }

      void init ( const EntityType &domainEntity, const EntityType &rangeEntity )
      {
        BaseType::init( domainEntity, rangeEntity );
      }

      int rows () const { return set_.size(); }

      int columns () const { return set_.numDofs(); }

       //! add value to matrix entry
      void add( int localRow, int localCol, const DofType value )
      {
        assert( localRow < rows() );
        assert( localCol < columns() );
        set_[ localRow ][ localCol ] += value;
      }

      //! get matrix entry 
      DofType get( int localRow, int localCol ) const
      {
        assert( localRow < rows() );
        assert( localCol < columns() );
        return set_[ localRow ][ localCol ];
      }

      //! set matrix entry to value 
      void set( int localRow, int localCol, const DofType value )
      {
        assert( localRow < rows() );
        assert( localCol < columns() );
        set_[ localRow ][ localCol ] = value;
      }
 
      //! set matrix row to zero except diagonla entry 
      void unitRow( const int localRow )
      {
        DUNE_THROW( NotImplemented, "On LocalMatrixAdapter unitRow() is not implemented!" );
      }

      //! set matrix row to zero
      void clearRow( const int localRow )
      {
        DUNE_THROW( NotImplemented, "On LocalMatrixAdapter clearRow() is not implemented!" );
      }

      //! set matrix column to zero
      void clearCol ( const int localCol )
      {
        DUNE_THROW( NotImplemented, "On LocalMatrixAdapter clearCol() is not implemented!" );
      }

      //! clear all entries belonging to local matrix 
      void clear ()
      {
        set_.clear();
      }

      //! scale local matrix with a certain value 
      void scale ( const DofType& value )
      {
        const int row = rows();
        const int cols = columns();
        for( int i = 0; i < row; ++i )
          for( int j = 0; j< cols; ++j )
            set_[ i ][ j ] *= value;
      }

      //! resort all global rows of matrix to have ascending numbering 
      void resort ()
      {
      }

    protected:
      LocalFunctionSet &set_;
    };


    template< class Traits >
    inline std :: ostream & 
      operator<< ( std::ostream &out, 
                   const LocalMatrixInterface< Traits > &matrix )
    {
      out<<std::endl;
      const int cols = matrix.columns();
      const int rows = matrix.rows();
      for( int c = 0; c< cols; ++c ) 
      {
        for( int r =0; r < rows; ++r )
          out<< " " <<matrix.get( r,c );
        out<<std::endl;
      }
      out<<std::endl;

      return out;
    }



  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_COMMON_LOCALMATRIXADAPTER_HH
