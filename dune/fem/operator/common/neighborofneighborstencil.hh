#ifndef DUNE_FEM_OPERATOR_COMMON_NEIGHBOROFNEIGHBORSTENCIL_HH
#define DUNE_FEM_OPERATOR_COMMON_NEIGHBOROFNEIGHBORSTENCIL_HH

#include <dune/fem/operator/common/stencil.hh>

namespace Dune
{

  namespace Fem
  {

    /** \class DiagonalAndNeighborOfNeighborStencil
     *  \brief Stencil contaning the entries (en,en) and (en,nb) for all entities en in the space
     *         and neighbors nb of en.
     *         Defailt for an operator over a DG space.
     *
     *  \tparam  DomainSpace  type of discrete function space for the domain
     *  \tparam  RangeSpace   type of discrete function space for the range
     *
     */
    template <class DomainSpace, class RangeSpace>
    struct DiagonalAndNeighborOfNeighborStencil : public Stencil<DomainSpace,RangeSpace>
    {
      typedef Stencil<DomainSpace,RangeSpace> BaseType;
    public:
      typedef typename BaseType::DomainEntityType       DomainEntityType;
      typedef typename BaseType::RangeEntityType        RangeEntityType;
      typedef typename BaseType::DomainGlobalKeyType    DomainGlobalKeyType;
      typedef typename BaseType::RangeGlobalKeyType     RangeGlobalKeyType;
      typedef typename BaseType::LocalStencilType       LocalStencilType;
      typedef typename BaseType::GlobalStencilType      GlobalStencilType;

      DiagonalAndNeighborOfNeighborStencil(const DomainSpace &dSpace, const RangeSpace &rSpace,
                                           bool onlyNonContinuousNeighbors = false)
        : BaseType( dSpace, rSpace )
      {
        for( const DomainEntityType &entity : dSpace )
        {
          BaseType::fill(entity,entity);
          typedef typename DomainSpace::GridPartType::IntersectionType IntersectionType;

          for( const IntersectionType &intersection : intersections( dSpace.gridPart(), entity ) )
          {
            if ( onlyNonContinuousNeighbors 
                && rSpace.continuous(intersection) && dSpace.continuous(intersection) )
              continue;
            if( intersection.neighbor() ) 
            {
              DomainEntityType neighbor = intersection.outside();
              BaseType::fill(neighbor,entity);

              if( neighbor.partitionType() != Dune::InteriorEntity )
              {
                BaseType::fill(entity, neighbor);
                BaseType::fill(neighbor,neighbor);
              }

              for( const IntersectionType &neighborIntersection : intersections( dSpace.gridPart(), neighbor ) )
              {
                if( neighborIntersection.neighbor() )
                {
                  DomainEntityType neighborNeighbor = neighborIntersection.outside();

                  BaseType::fill(neighborNeighbor, entity);
                  BaseType::fill(entity, neighborNeighbor);
                }
              }
            }
          }
        }
      }
    };

  } // namespace Fem

} // namespace Dune

#endif //#ifndef DUNE_FEM_OPERATOR_COMMON_NEIGHBOROFNEIGHBORSTENCIL_HH
