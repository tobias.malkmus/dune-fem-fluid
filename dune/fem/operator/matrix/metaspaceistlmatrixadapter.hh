#ifndef DUNE_FEM_OPERATOR_MATRIX_METADFISTALMATRIXADAPTER_HH
#define DUNE_FEM_OPERATOR_MATRIX_METADFISTALMATRIXADAPTER_HH

#if HAVE_DUNE_ISTL

#include <dune/fem/operator/matrix/istlmatrixadapter.hh>
#include <dune/fem/space/metaspace.hh>

namespace Dune
{

  namespace Fem
  {

    template< class Matrix, class DiscreteFunctionSpace, class BlockMapper, int blockSize >
    struct ISTLParallelMatrixAdapter< Matrix, MetaDiscreteFunctionSpace< DiscreteFunctionSpace, BlockMapper, blockSize > >
    {
      typedef typename ISTLParallelMatrixAdapter< Matrix, DiscreteFunctionSpace >::Type Type;
    };

  } // namespace Fem

} // namespace Dune

#endif // #if HAVE_DUNE_ISTL

#endif //#ifndef DUNE_FEM_OPERATOR_MATRIX_METADFISTALMATRIXADAPTER_HH
