#ifndef DUNE_FEM_FLUID_ISTLMATRIXADAPTER_HH
#define DUNE_FEM_FLUID_ISTLMATRIXADAPTER_HH

#if HAVE_DUNE_ISTL

#include <dune/fem/operator/matrix/istlmatrixadapter.hh>
#include <dune/fem/space/taylorhood.hh>

namespace Dune
{

  namespace Fem
  {

    template< class MatrixImp, class VelocityFunctionSpace, class GridPart, int polOrder, template< class > class Storage>
    struct ISTLParallelMatrixAdapter< MatrixImp, TaylorHoodDiscontinuousGalerkinSpace
    < VelocityFunctionSpace, GridPart, polOrder, Storage > >
    {
      typedef DGParallelMatrixAdapter<MatrixImp> Type ;
    };

  } // namespace Fem

} // namespace Dune

#endif // #if HAVE_DUNE_ISTL

#endif // #ifndef DUNE_FEM_ISTLMATRIXADAPTER_HH
