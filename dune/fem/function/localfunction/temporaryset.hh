#ifndef DUNE_FEM_FUNCTION_LOCALFUNCTION_TEMPORARYSET_HH
#define DUNE_FEM_FUNCTION_LOCALFUNCTION_TEMPORARYSET_HH

// dune-fem includes
#include <dune/common/dynmatrix.hh>

#include <dune/fem/function/localfunction/evaluate.hh>
#include <dune/fem/function/localfunction/localfunctionset.hh>


namespace Dune
{

  namespace Fem
  {

    // TemporaryLocalFunctionSet
    // -------------------------

    template< class DiscreteFunctionSpace,
              class Dof = typename DiscreteFunctionSpace::RangeFieldType >
    class TemporaryLocalFunctionSet
    {

      typedef TemporaryLocalFunctionSet< DiscreteFunctionSpace, Dof > ThisType;

    public:
      //! type of the discrete function space
      typedef DiscreteFunctionSpace DiscreteFunctionSpaceType;

    protected:
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::GridType GridType;

    public:
      //! type of the base function set
      typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType
        BasisFunctionSetType;

      //! type of DoF used
      typedef Dof DofType;

      //! type of the entity, this local function is associated with
      typedef typename DiscreteFunctionSpaceType::EntityType EntityType;

      //! type of analytical function space
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      //! type of domain vectors
      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      //! field type of domain vectors
      typedef typename DiscreteFunctionSpaceType::DomainFieldType DomainFieldType;
      //! type of range vectors
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      //! field type of range vectors
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      //! type of the jacobian
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;
      //! type of the hessian
      typedef typename DiscreteFunctionSpaceType::HessianRangeType HessianRangeType;

      enum { dimDomain = DiscreteFunctionSpaceType::dimDomain };
      enum { dimRange = DiscreteFunctionSpaceType::dimRange };

    protected:
      typedef DynamicMatrix< DofType > DofMatrixType;
      typedef typename DofMatrixType::row_type DofVectorType;

    public:

      TemporaryLocalFunctionSet ( const DiscreteFunctionSpaceType &dfSpace )
        : discreteFunctionSpace_( dfSpace ),
          entity_( nullptr ),
          dofs_( DiscreteFunctionSpace::localBlockSize * discreteFunctionSpace_.blockMapper().maxNumDofs(),
                 DiscreteFunctionSpace::localBlockSize * discreteFunctionSpace_.blockMapper().maxNumDofs(), DofType( 0.0 ) )
      {}

      /** \brief constructor creating a local function without binding it to an
       *         entity
       *
       *  Creates the local function without initializing the fields depending on
       *  the current entity.
       *
       *  \note Before using the local function it must be initilized by
       *  \code
       *  localFunction.init( entity );
       *  \endcode
       *
       *  \param[in] dfSpace discrete function space the local function shall
       *                     belong to
       */
      TemporaryLocalFunctionSet ( const DiscreteFunctionSpaceType &dfSpace, const int maxSize )
        : discreteFunctionSpace_( dfSpace ),
          entity_( nullptr ),
          dofs_( maxSize, DiscreteFunctionSpace::localBlockSize * discreteFunctionSpace_.blockMapper().maxNumDofs(), RangeFieldType( 0 ) )
      {}

      /** \brief constructor creating a local function and binding it to an
       *         entity
       *
       *  Creates the local function and initilizes the fields depending on the
       *  current entity. It is not necessary, though allowed, to call init
       *  before using the discrete function.
       *
       *  \note The degrees of freedom are not initialized by this function.
       *
       *  \param[in] dfSpace discrete function space the local function shall
       *                     belong to
       *  \param[in] entity  entity for initialize the local function to
       */
      TemporaryLocalFunctionSet ( const DiscreteFunctionSpaceType &dfSpace, const int maxSize,
                                  const EntityType &entity )
        : discreteFunctionSpace_( dfSpace ),
          entity_( &entity ),
          dofs_( maxSize, DiscreteFunctionSpace::localBlockSize * discreteFunctionSpace_.blockMapper().maxNumDofs(), RangeFieldType( 0 ) )
      {
        init( entity, maxSize );
      }

      DofVectorType &operator[] ( std::size_t i )
      {
        assert( i < size() );
        return dofs_[ i ];
      }

      const DofVectorType &operator[] ( std::size_t i ) const
      {
        assert( i < size() );
        return dofs_[ i ];
      }

      void clear ()
      {
        dofs_ = 0.0;
      }

      ThisType &operator+= ( const ThisType &other )
      {
        assert( size() ==  other.size() );
        dofs_ += other.dofs_;
        return *this;
      }

      ThisType &axpy ( const RangeFieldType &s, const ThisType &other )
      {
        assert( size() ==  other.size() );
        dofs_.axpy( s, other.dofs_ );
        return *this;
      }

      template< class PointType >
      void axpy ( const PointType &x, const std::vector< RangeType > &factor )
      {
        assert( factor.size() == size() );
        for( std::size_t i = 0; i < size(); ++i )
          basisFunctionSet().axpy( x, factor[ i ], dofs_[ i ] );
      }

      template< class PointType >
      void axpy ( const PointType &x, const std::vector< JacobianRangeType > &factor )
      {
        assert( factor.size() == size() );
        for( std::size_t i = 0; i < size(); ++i )
          basisFunctionSet().axpy( x, factor[ i ], dofs_[ i ] );
      }

      template< class PointType >
      void axpy ( const PointType &x, const std::vector< RangeType > &factor1, const std::vector< JacobianRangeType > &factor2 )
      {
        assert( factor1.size() == size() );
        assert( factor1.size() == factor2.size() );
        for( std::size_t i = 0; i < size(); ++i )
          basisFunctionSet().axpy( x, factor1[ i ], factor2[ i ], dofs_[ i ] );
      }

/*
      template< class QuadratureType, class VectorType >
      void axpyQuadrature ( const QuadratureType &quad, const VectorType &values )
      {
        asImp().basisFunctionSet().axpy( quad, values, asImp() );
      }

      template< class QuadratureType, class RangeVectorType, class JacobianRangeVectorType >
      void axpyQuadrature ( const QuadratureType &quad,
                            const RangeVectorType& rangeVector,
                            const JacobianRangeVectorType& jacobianVector )
      {
        asImp().basisFunctionSet().axpy( quad, rangeVector, jacobianVector, asImp() );
      }

      template< class QuadratureType, class VectorType  >
      void evaluateQuadrature( const QuadratureType &quad, VectorType &result ) const
      {
        assert( result.size() > 0 );
        evaluateQuadrature( quad, result, result[ 0 ] );
      }
 */


      const BasisFunctionSetType &basisFunctionSet () const
      {
        assert( entity_ );
        return basisFunctionSet_;
      }

      const EntityType &entity () const
      {
        assert( entity_ );
        return *entity_;
      }

      void init ( const EntityType &entity, const int size )
      {
        entity_ = &entity;
        basisFunctionSet_ = discreteFunctionSpace_.basisFunctionSet( entity );
        dofs_.resize( size, DiscreteFunctionSpaceType::localBlockSize * discreteFunctionSpace_.blockMapper().maxNumDofs() );
        assert( basisFunctionSet().size() <= dofs_[ 0 ].size() );
      }

      void init ( const EntityType &entity )
      {
        entity_ = &entity;
        basisFunctionSet_ = discreteFunctionSpace_.basisFunctionSet( entity );
        assert( basisFunctionSet().size() <= dofs_[ 0 ].size() );
      }

      /*
         template< class DiscreteFunction >
         void init ( const EntityType &entity, const DiscreteFunction& discreteFunction )
         {
         // initialize
         init( entity );

         // copy dofs to local storage (modifying not allowed)
         assert( numDofs() <= (int) dofs_.size() );
         assert( &discreteFunctionSpace_ == &discreteFunction.space() );
         AssignDofs< DiscreteFunction > assignDofs( discreteFunction, dofs_ );
         discreteFunctionSpace_.blockMapper().mapEach( entity, assignDofs );
         }
       */

      int numDofs () const
      {
        return basisFunctionSet().size();
      }

      std::size_t size () const
      {
        return dofs_.size();
      }

      int order () const
      {
        return basisFunctionSet().order();
      }

      template< class Point, class Functor >
      void evaluateEach ( const Point &x, Functor functor ) const
      {
        for( std::size_t i = 0; i < size(); ++i )
        {
          RangeType value( 0.0 );
          basisFunctionSet().evaluateAll( x, dofs_[ i ], value );
          functor( i, value );
        }
      }

      template< class Point, class Functor >
      void jacobianEach ( const Point &x, Functor functor ) const
      {
        for( std::size_t i = 0; i < size(); ++i )
        {
          JacobianRangeType value( RangeFieldType( 0 ) );
          basisFunctionSet().jacobianAll( x, dofs_[ i ], value );
          functor( i, value );
        }
      }

      template< class Point, class Functor >
      void hessianEach ( const Point &x, Functor functor ) const
      {
        for( std::size_t i = 0; i < size(); ++i )
        {
          HessianRangeType value( RangeFieldType( 0 ) );
          basisFunctionSet().hessianAll( x, dofs_[ i ], value );
          functor( i, value );
        }
      }

      const DiscreteFunctionSpaceType &discreteFunctionSpace  () const
      {
        return discreteFunctionSpace_;
      }

    protected:
      const DiscreteFunctionSpaceType &discreteFunctionSpace_;
      const EntityType *entity_;
      BasisFunctionSetType basisFunctionSet_;

      DofMatrixType dofs_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_FUNCTION_LOCALFUNCTION_TEMPORARY_HH
