#ifndef DUNE_FEM_SPACE_LOCALFUNCTIONSET_EVALUATE_HH
#define DUNE_FEM_SPACE_LOCALFUNCTIONSET_EVALUATE_HH

#include <cassert>

#include <dune/fem/space/basisfunctionset/functor.hh>

namespace Dune
{

  namespace Fem
  {

    // Evaluation of Local Function Sets
    // ---------------------------------

    template< class LocalFunctionSet, class Point, class Array >
    inline void evaluateAll ( const LocalFunctionSet &localFunctionSet, const Point &point, Array &values )
    {
      assert( values.size() >= localFunctionSet.size() );
      AssignFunctor< Array > f( values );
      localFunctionSet.evaluateEach( point, f );
    }


    template< class LocalFunctionSet, class Point, class Array >
    inline void jacobianAll ( const LocalFunctionSet &localFunctionSet, const Point &point, Array &jacobians )
    {
      assert( jacobians.size() >= localFunctionSet.size() );
      AssignFunctor< Array > f( jacobians );
      localFunctionSet.jacobianEach( point, f );
    }


    template< class LocalFunctionSet, class Point, class Array >
    inline void hessianAll ( const LocalFunctionSet &localFunctionSet, const Point &point, Array &hessians )
    {
      assert( hessians.size() >= localFunctionSet.size() );
      AssignFunctor< Array > f( hessians );
      localFunctionSet.hessianEach( point, f );
    }



    // Evaluation of Linear Combinations
    // ---------------------------------

    template< class LocalFunctionSet, class Point, class Coefficients >
    inline void evaluateLinearCombination ( const LocalFunctionSet &localFunctionSet,
                                            const Point &x,
                                            const Coefficients &coefficients,
                                            typename LocalFunctionSet::RangeType &value )
    {
      typedef typename LocalFunctionSet::RangeFieldType RangeFieldType;
      typedef typename LocalFunctionSet::RangeType RangeType;
      value = RangeType( RangeFieldType( 0 ) );
      AxpyFunctor< Coefficients, RangeType > f( coefficients, value );
      localFunctionSet.evaluateEach( x, f );
    }


    template< class LocalFunctionSet, class Point, class Coefficients >
    inline void jacobianLinearCombination ( const LocalFunctionSet &localFunctionSet,
                                            const Point &x,
                                            const Coefficients &coefficients,
                                            typename LocalFunctionSet::JacobianRangeType &jacobian )
    {
      typedef typename LocalFunctionSet::RangeFieldType RangeFieldType;
      typedef typename LocalFunctionSet::JacobianRangeType JacobianRangeType;
      jacobian = JacobianRangeType( RangeFieldType( 0 ) );
      AxpyFunctor< Coefficients, JacobianRangeType > f( coefficients, jacobian );
      localFunctionSet.jacobianEach( x, f );
    }


    template< class LocalFunctionSet, class Point, class Coefficients >
    inline void hessianLinearCombination ( const LocalFunctionSet &localFunctionSet,
                                           const Point &x,
                                           const Coefficients &coefficients,
                                           typename LocalFunctionSet::HessianRangeType &hessian )
    {
      typedef typename LocalFunctionSet::RangeFieldType RangeFieldType;
      typedef typename LocalFunctionSet::HessianRangeType HessianRangeType;
      hessian = HessianRangeType( typename HessianRangeType::value_type( RangeFieldType( 0 ) ) );
      AxpyFunctor< Coefficients, HessianRangeType > f( coefficients, hessian );
      localFunctionSet.hessianEach( x, f );
    }

  } // namespace Fem

} // namespace Dune

#endif // #define DUNE_FEM_SPACE_LOCALFUNCTIONSET_EVALUATE_HH
