#include <config.h>

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/fem/gridpart/leafgridpart.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>

#include <dune/fem/space/brezzidouglasmarini/localfiniteelement.hh>
#include <dune/fem/space/shapefunctionset/lagrange.hh>
#include <dune/fem/space/shapefunctionset/localfunctions.hh>
#include <dune/fem/space/shapefunctionset/tuple.hh>
#include <dune/fem/space/shapefunctionset/vectorial.hh>

#include <dune/fem/common/tuplehelper.hh>
#include <dune/fem/space/basisfunctionset/transformed.hh>
#include <dune/fem/space/basisfunctionset/piolatransformation.hh>

#include <dune/fem/space/basisfunctionset/test/checkbasisfunctionset.hh>
#include <dune/fem/test/testgrid.hh>


template< class GridPartType, int polorder >
void traverse ( GridPartType &gridPart )
{
  static const int dimDomain = GridPartType::dimensionworld;

  typedef typename GridPartType::ctype field_type;

  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
  auto iterator = gridPart.template begin< 0 >();
  iterator++;
  const EntityType &entity = *iterator;

  // create quadrature
  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;
  QuadratureType quadrature( entity, polorder );

#if 0 //HAVE_DUNE_LOCALFUNCTIONS
  static const unsigned int topologyId = Dune::Fem::GridPartCapabilities::hasSingleGeometryType< GridPartType >::topologyId;

  // needs a geometry type to construct
  typedef Dune::Fem::BDMLocalFiniteElement< topologyId, field_type, field_type, dimDomain, polorder >
    LocalFiniteElementType;

  typedef Dune::Fem::LocalFunctionsShapeFunctionSet< typename LocalFiniteElementType::Traits::LocalBasisType >
    BasicShapeFunctionSetType;

  static const int bS = 3;

  typedef typename Dune::SameTypeTuple< BasicShapeFunctionSetType, bS, Dune::Fem::TupleShapeFunctionSet >::Type
    ShapeFunctionSetType;

  typename LocalFiniteElementType::Traits::LocalBasisType localBasis( 0 );
  BasicShapeFunctionSetType basicShapeFunctionSet( localBasis );
  ShapeFunctionSetType shapeFunctionSet =
    Dune::SameTypeTuple< BasicShapeFunctionSetType, bS, Dune::Fem::TupleShapeFunctionSet >::construct( basicShapeFunctionSet );

  typedef typename ShapeFunctionSetType::RangeType RangeType;

#else

  typedef Dune::Fem::FunctionSpace< field_type, field_type, dimDomain, 1 >ScalarSpaceType;
  typedef Dune::Fem::LagrangeShapeFunctionSet< ScalarSpaceType, polorder > ScalarShapeFunctionSetType;
  typedef Dune::FieldVector< field_type, dimDomain *3 > RangeType;
  typedef Dune::Fem::VectorialShapeFunctionSet< ScalarShapeFunctionSetType, RangeType > ShapeFunctionSetType;

  ShapeFunctionSetType shapeFunctionSet( entity.type() );

#endif

  // type of error
  typedef Dune::FieldVector< field_type, 6 > ErrorType;

  double eps = 1e-8;

  ErrorType error( 0 );
  // default basis function set
  typedef Dune::Fem::PiolaTransformation< typename EntityType::Geometry, RangeType::dimension > TransformationType;
  Dune::Fem::TransformedBasisFunctionSet< EntityType, ShapeFunctionSetType, TransformationType >
  basisSet1( entity, shapeFunctionSet );
  error = Dune::Fem::checkQuadratureConsistency( basisSet1, quadrature, false );
  if( error.two_norm() > eps )
  {
    std::cerr<<"Errors( evaluate, jacobian, hessian, value axpy, jacobian axpy ): "<< error <<std::endl;
    DUNE_THROW( Dune::InvalidStateException, " DefaultBasisFunctionSet< LagrangeShapeFunctionSet > test failed." );
  }
}


int main ( int argc, char **argv )
{
  Dune::Fem::MPIManager::initialize( argc, argv );

  Dune::Fem::Parameter::append( argc, argv );
  Dune::Fem::Parameter::append( argc >= 2 ? argv[ 1 ] : "parameter" );

  typedef Dune::GridSelector::GridType GridType;
  GridType &grid = Dune::Fem::TestGrid::grid();

  grid.globalRefine( 1 );

  typedef Dune::Fem::LeafGridPart< GridType > GridPartType;
  GridPartType gridPart( grid );

  if( gridPart.begin< 0 >() == gridPart.end< 0 >() )
    return 1;

  traverse< GridPartType, 1 >( gridPart );
  traverse< GridPartType, 2 >( gridPart );

  return 0;
}
