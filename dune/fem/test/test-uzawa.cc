#include <config.h>

// Includes from the IOStream Library
// ----------------------------------

#include <iostream>
#include <sstream>


// Includes from DUNE-FEM
// ----------------------

#include <dune/common/fvector.hh>

#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/operator/common/operator.hh>

#include <dune/fem/solver/uzawa.hh>


// Global Type Definitions
// -----------------------
//

template< class V >
struct ArgAdapter
  : public V
{
  typedef typename V::field_type RangeFieldType;
  typedef typename V::field_type DomainFieldType;

  template< class ... Args >
  ArgAdapter ( Args && ... args ) : V( std::forward< Args >( args ) ... ) {}
};

template< class ARange >
struct A : public Dune::Fem::Operator< ARange, ARange >
{
  void operator() ( const ARange &arg, ARange &dest ) const
  {
    for( std::size_t i = 0; i < dest.size(); ++i )
      dest[ i ] = arg[ i ] * ( 1 + i );
  }
};

template< class ARange >
struct AInv : public Dune::Fem::Operator< ARange, ARange >
{
  void operator() ( const ARange &arg, ARange &dest ) const
  {
    for( std::size_t i = 0; i < dest.size(); ++i )
      dest[ i ] = arg[ i ] / ( 1 + i );
  }
};

template< class ARange, class BRange >
struct B
  : public Dune::Fem::Operator< ARange, BRange >
{
  void operator() ( const ARange &arg, BRange &dest ) const
  {
    for( std::size_t i = 0; i < dest.size(); ++i )
      dest[ i ] = arg[ i * 3 ];
  }
};

template< class BRange, class ARange >
struct BT
  : public Dune::Fem::Operator< BRange, ARange >
{
  void operator() ( const BRange &arg, ARange &dest ) const
  {
    for( std::size_t i = 0; i < arg.size(); ++i )
      dest[ i * 3 ] = arg[ i ];
  }
};


template< class BRange, class ARange >
struct PreConder
  : public Dune::Fem::Operator< BRange, BRange >
{

  PreConder () {}

  void operator() ( const BRange &arg, BRange &dest ) const
  {
    dest = arg;
    /*
       bt_( arg, ai_ );
       b_( ai_, dest );
     */
  }

protected:
  A< ARange > a_;
  B< ARange, BRange > b_;
  BT< BRange, ARange > bt_;
  mutable ARange ai_, ui_;
};

template< class FullRange, class ARange, class BRange >
struct FullOperator
  : public Dune::Fem::Operator< FullRange >
{
  typedef Dune::Fem::Operator< FullRange > BaseType;

  typedef typename BaseType::DomainFunctionType Domain;
  typedef typename BaseType::RangeFunctionType Range;

  // todo define ARange and BRange


  FullOperator () {}

  void operator() ( const Domain &arg, Range &dest ) const
  {}

  const A< ARange > &a () const { return a_; }
  const AInv< ARange > &aInv () const { return aInv_; }
  const B< ARange, BRange > &b () const { return b_; }
  const BT< BRange, ARange > &bt () const { return bt_; }

protected:
  A< ARange > a_;
  AInv< ARange > aInv_;
  B< ARange, BRange > b_;
  BT< BRange, ARange > bt_;
};


template< int d, int k >
void testUzawa ()
{
  typedef ArgAdapter< Dune::FieldVector< double, d + k > > FullRange;
  typedef ArgAdapter< Dune::FieldVector< double, d > > ARange;
  typedef ArgAdapter< Dune::FieldVector< double, k > > BRange;

  FullOperator< FullRange, ARange, BRange > op;

  typedef Dune::Fem::UzawaInverseOperator< ARange, BRange > InverseOperator;
  PreConder< BRange, ARange > preconder;

  bool verbose = true;
  InverseOperator uzawa( preconder, op.aInv(), op.b(), op.bt(), 1e-8, 1000, verbose );

  ARange f, u;
  for( std::size_t i = 0; i < d; ++i )
    f[ i ] = i;

  BRange g, p;
  for( std::size_t i = 0; i < k; ++i )
    g[ i ] = -1.0 * i;

  uzawa( f, g, u, p );

  std::cout << k <<"\t" << uzawa.iterations() << std::endl;
}




// Main Program
// ------------

int main ( int argc, char **argv )
try
{
  // initialize MPI manager and PETSc
  Dune::Fem::MPIManager::initialize( argc, argv );

  // add command line parameters to global parameter table
  Dune::Fem::Parameter::append( argc, argv );
  // append parameters from the parameter file

  Dune::Fem::Parameter::append( (argc < 2) ? "parameter" : argv[ 1 ] );

  testUzawa< 6, 2 >();
  testUzawa< 24, 8 >();
  testUzawa< 36, 12 >();
  testUzawa< 60, 20 >();

  Dune::Fem::Parameter::write( "parameter.log" );

  return 0;
}
catch( const Dune::Exception &exception )
{
  // display the exception message on the console
  std::cerr << exception << std::endl;
  return 1;
}
