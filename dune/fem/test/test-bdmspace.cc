#include <config.h>

// Includes from the IOStream Library
// ----------------------------------

#include <iostream>
#include <sstream>


// Includes from DUNE-FEM
// ----------------------

// include Lagrange discrete function space
#include <dune/fem/gridpart/leafgridpart.hh>
#include <dune/fem/space/brezzidouglasmarini.hh>
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/common/interpolate.hh>

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/cginverseoperator.hh>

#include <dune/fem/misc/h1norm.hh>
#include <dune/fem/misc/l2norm.hh>

#include <dune/fem/io/file/datawriter.hh>

#include <dune/fem/test/massoperator.hh>

// Global Type Definitions
// -----------------------


typedef Dune::GridSelector::GridType GridType;

typedef Dune::Fem::LeafGridPart< GridType > GridPartType;
typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, GridType::dimensionworld > Space1Type;
#if HAVE_DUNE_LOCALFUNCTIONS
typedef Dune::Fem::BDMDiscreteFunctionSpace< Space1Type, GridPartType, 2 > DiscreteSpaceType;
#else
typedef Dune::Fem::LagrangeDiscreteFunctionSpace< Space1Type, GridPartType, 2 > DiscreteSpaceType;
#endif
typedef typename DiscreteSpaceType::FunctionSpaceType SpaceType;
typedef Dune::Fem::AdaptiveDiscreteFunction< DiscreteSpaceType > DiscreteFunctionType;
typedef Dune::Fem::SparseRowLinearOperator< DiscreteFunctionType, DiscreteFunctionType > LinearOperatorType;
typedef Dune::Fem::CGInverseOperator< DiscreteFunctionType > InverseOperatorType;

typedef MassOperator< DiscreteFunctionType, LinearOperatorType > MassOperatorType;

// Function to Project
// -------------------

template< class FunctionSpace >
struct Function
{
  typedef FunctionSpace FunctionSpaceType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  void evaluate ( const DomainType &x, RangeType &value ) const
  {
    value = 1;
    value[0] = x[1]*x[1]*x[1]+2*x[0]*x[1];
    value[1] = x[0]*x[0]*x[0]-x[1]*x[1];

//    value *= x.two_norm2();
    /*
    for( int k = 0; k < FunctionSpace::dimDomain; ++k )
      value[ k ] *= x[ (k+1) % FunctionSpace::dimDomain ];
//      value *= sin( M_PI * x[ k ] );
      */
  }

  void jacobian ( const DomainType &x, JacobianRangeType &jacobian ) const
  {
    jacobian = 0;
    jacobian[ 0 ][ 0 ] = 2*x[1];
    jacobian[ 0 ][ 1 ] = 3.*x[1]*x[1] + 2.*x[0];
    jacobian[ 1 ][ 0 ] = 3*x[0]*x[0];
    jacobian[ 1 ][ 1 ] = -2*x[1];

    /*
    jacobian[ 0 ][ 0 ] = x.two_norm2() + 2 * x[0 ] * x[ 0];
    jacobian[ 0 ][ 1 ] = 2 * x[ 0 ] * x[ 1 ];
    jacobian[ 1 ][ 0 ] = 2 * x[ 0 ] * x[ 1 ];
    jacobian[ 1 ][ 1 ] = x.two_norm2() + 2 * x[1] * x[1];
    */

    /*
       for( int j = 0; j < FunctionSpace::dimDomain; ++j )
       {
       // jacobian has only one row, calc j-th column
       jacobian[ 0 ][ j ] = M_PI;
       for( int k = 0; k < FunctionSpace::dimDomain; ++k )
        jacobian[ 0 ][ j ] *= (j == k ? cos( M_PI*x[ k ] ) : sin( M_PI*x[ k ] ));

       for( int r = 1; r < FunctionSpace::dimRange; ++r )
        jacobian[ r ] = jacobian[ 0 ];
       }
     */
  }
};


template< class BasisSet, class FunctionSpace >
struct LocalBasis
{
  typedef typename FunctionSpace::RangeType RangeType;
  typedef typename FunctionSpace::RangeFieldType RangeFieldType;

  typedef typename FunctionSpace::DomainType DomainType;

  LocalBasis ( const BasisSet &bSet, int i )
    : bSet_( bSet ),
      comp_( bSet.size(), 0 )
  {
    comp_[ i ] = 1.0;
  }

  void evaluate ( const DomainType &arg, RangeType &dest ) const
  {
    dest = 0;
    bSet_.evaluateAll( arg, comp_, dest );
  }

  const typename BasisSet::EntityType &entity () const { return bSet_.entity(); }

private:
  const BasisSet &bSet_;
  std::vector< double > comp_;
};


// Algorithm
// ---------

struct Algorithm
{
  typedef Dune::FieldVector< double, 2 > ErrorType;
  typedef Function< SpaceType > FunctionType;

  explicit Algorithm ( GridType &grid );
  ErrorType operator() ( int step );
  ErrorType finalize ( DiscreteFunctionType &u );
  DiscreteSpaceType &space ();
  void nextMesh ();

private:
  GridType &grid_;
  FunctionType function_;
};

template<class Point, class Geo, class Jac >
void piola ( const Point &p, const Geo &geo, Jac &jac )
{
  double det = 1.0 / geo.integrationElement( p );
  Jac help( jac );
  jac = 0;
  auto gjit = geo.jacobianTransposed( p );
  for( std::size_t r = 0; r < Jac::rows; ++r )
    gjit.mtv( help[ r ], jac[ r ] );

  jac *= det;
}

template<class Point, class Geo, class Jac >
void invPiola ( const Point &p, const Geo &geo, Jac &jac )
{
  double det = geo.integrationElement( p );
  Jac help( jac );
  jac = 0;
  auto gjit = geo.jacobianInverseTransposed( p );
  for( std::size_t r = 0; r < Jac::rows; ++r )
    gjit.mtv( help[ r ], jac[ r ] );

  jac *= det;
}

inline Algorithm::Algorithm ( GridType &grid )
  : grid_( grid )
{}

inline void Algorithm::nextMesh ()
{
  grid_.globalRefine( Dune::DGFGridInfo< GridType >::refineStepsForHalf() );
}

inline typename Algorithm::ErrorType Algorithm::operator() ( int step )
{
#if 1 
  GridPartType gridPart( grid_ );
  DiscreteSpaceType space( gridPart );
  DiscreteFunctionType solution( "solution", space );

  // get operator
  MassOperatorType massOperator( space );

  // assemble RHS
  DiscreteFunctionType rhs( "rhs", space );
  massOperator.assembleRHS( function_, rhs );

  unsigned long maxIter = space.size();
  maxIter = space.gridPart().comm().sum( maxIter );

  // clear result
  solution.clear();

  // apply solver
  InverseOperatorType inverseOperator( massOperator, 1e-10, 1e-10, maxIter );
  inverseOperator( rhs, solution );

  std::cout << inverseOperator.iterations() << std::endl;

  std::tuple< DiscreteFunctionType * > ioTuple( &solution );
  Dune::Fem::DataWriter< GridType, std::tuple< DiscreteFunctionType * > > writer( gridPart.grid(), ioTuple );

  writer.writeData( step );
  return finalize( solution );
#else
  GridPartType gridPart( grid_ );
  DiscreteSpaceType space( gridPart );
  DiscreteFunctionType solution( "solution", space );

  Dune::Fem::GridFunctionAdapter< FunctionType, GridPartType > adapter( "adapter", function_, gridPart, space.order() );
  Dune::Fem::interpolate( adapter, solution );

  typedef LocalBasis< DiscreteSpaceType::BasisFunctionSetType, SpaceType > LocalBasisType;

  for( auto entity : space )
  {
    auto bSet = space.basisFunctionSet( entity );

    for( std::size_t i = 0; i < bSet.size(); ++i )
    {
      LocalBasisType local( bSet, i );

      std::vector< double > phii( bSet.size(), 0 );

      space.interpolation( entity ) ( local, phii );
      for( std::size_t j = 0; j < phii.size(); ++j )
        if( std::abs(  phii[ j ] -( i == j ))  > 1e-8 )
          std::cout << "Error:" <<  phii[ j ] <<  std::endl;
    }
  }


  std::tuple< DiscreteFunctionType * > ioTuple( &solution );
  Dune::Fem::DataWriter< GridType, std::tuple< DiscreteFunctionType * > > writer( gridPart.grid(), ioTuple );

  writer.writeData( step );

  return finalize( solution );
#endif
}


inline typename Algorithm::ErrorType Algorithm::finalize ( DiscreteFunctionType &solution )
{
  const GridPartType &gridPart = solution.space().gridPart();
  ErrorType error;
  typedef Dune::Fem::GridFunctionAdapter< FunctionType, GridPartType > GridFunctionType;

  const int order = DiscreteSpaceType::polynomialOrder+1;
  GridFunctionType gridFunction( "exact solution", function_, gridPart, order );

  Dune::Fem::L2Norm< GridPartType > l2norm( gridPart );
  Dune::Fem::H1Norm< GridPartType > h1norm( gridPart );

  error[ 0 ] = l2norm.distance( gridFunction, solution );
  error[ 1 ] = h1norm.distance( gridFunction, solution );

  std::cout << error << std::endl;
  return error;
}


// Main Program
// ------------

int main ( int argc, char **argv )
try
{
  // initialize MPI manager and PETSc
  Dune::Fem::MPIManager::initialize( argc, argv );

  // add command line parameters to global parameter table
  Dune::Fem::Parameter::append( argc, argv );
  // append parameters from the parameter file
  Dune::Fem::Parameter::append( (argc < 2) ? "parameter" : argv[ 1 ] );

  std::string gridFile( "2dgrid.dgf" );

  Dune::GridPtr< GridType > gridPtr( gridFile );
  GridType &grid = *gridPtr;
//  grid.globalRefine( 2 );

  const int nrSteps = 4;

  std::cout<< "testing bdm space" <<std::endl;
  Algorithm algorithm( grid );
  typename Algorithm::ErrorType *error;
  error = new  typename Algorithm::ErrorType[ nrSteps ];
  for( int step = 0; step < nrSteps; ++step )
  {
    error[ step ] = algorithm( step );
    algorithm.nextMesh();
  }

  for( int step = 1; step < nrSteps; ++step )
  {
    double l2eoc = log( error[ step ][ 0 ] / error[ step -1 ][ 0 ] ) / log( 0.5 );
    double h1eoc = log( error[ step ][ 1 ] / error[ step -1 ][ 1 ] ) / log( 0.5 );

    std::cout<< "L2 Eoc: " << l2eoc << std::endl;
    std::cout<< "H1 Eoc: " << h1eoc << std::endl;
    /*
       if( std::abs( l2eoc - 2 )  > 0.2 )
       DUNE_THROW( Dune::InvalidStateException, "EOC check of refinement failed" );
       if( std::abs( h1eoc - 1 )  > 0.2 )
       //note: This will fail with Yaspgrid, bug in Geometry JacobianInverse
       DUNE_THROW( Dune::InvalidStateException, "EOC check of refinement failed" );
     */
  }

  delete error;
  Dune::Fem::Parameter::write( "parameter.log" );

  return 0;
}
catch( const Dune::Exception &exception )
{
  // display the exception message on the console
  std::cerr << exception << std::endl;
  return 1;
}
