#include <config.h>

#include <iostream>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/mapper/codimensionmapper.hh>
#include <dune/fem/space/mapper/tuplenonblockmapper.hh>


typedef Dune::GridSelector::GridType GridType;
typedef Dune::Fem::AdaptiveLeafGridPart< GridType > GridPartType;

typedef Dune::Fem::CodimensionMapper< GridPartType, 0 > BlockMapperType;
typedef Dune::Fem::TupleNonBlockMapper< BlockMapperType, 4, 15, 2, 7 > MapperType;

static std::array< std::size_t, 4 > A = { 4, 15, 2, 7};

template< class Functor >
void checkBlocking ( const std::vector< std::size_t > &indices, const std::vector< std::size_t > &blockIndices, Functor f )
{
  bool ret = true;
  for( std::size_t i = 0; i < blockIndices.size(); ++i )
    ret &= f( i, blockIndices[ i ], indices );

  if( !ret )
    DUNE_THROW( Dune::InvalidStateException, "Local To Global dof blocking failed." );
}

template< class BlockMapper, class Mapper >
void checkMapper ( const GridPartType &gridPart, const BlockMapper &blockMapper, const Mapper &mapper )
{
  // compare sizes
  std::size_t numDofs =  (4 + 15 + 2 + 7) * gridPart.indexSet().size( 0 );
  if( numDofs != (std::size_t) mapper.size() )
    DUNE_THROW( Dune::InvalidStateException, "Sizes of Mapper are missmatching." );

  // store all dof indices
  for( const auto &entity : Dune::elements( gridPart ) )
  {
    // get global dof indices of Mapper
    std::vector< std::size_t > indices( mapper.numDofs( entity ) );
    mapper.mapEach( entity, [ &indices ]( std::size_t i, const std::size_t dof ) { indices[ i ] = dof; } );

    // get global dof indices of block Mapper
    std::vector< std::size_t > blockIndices( blockMapper.numDofs( entity ) );
    blockMapper.mapEach( entity, [ &blockIndices ]( std::size_t i, const std::size_t dof ) { blockIndices[ i ] = dof; } );

    if( indices.size() != ( 4 + 15 +2 + 7) * blockIndices.size() )
      DUNE_THROW( Dune::InvalidStateException, "Local NumDofs are missmatching." );

    checkBlocking( indices, blockIndices,
        [ s = blockMapper.size() ] ( std::size_t localBlock, const std::size_t &blockDof, const std::vector< std::size_t > &indices ) -> bool
        {
          bool ret = true;
          std::size_t offset = 0;
          for( std::size_t j = 0; j< 4; ++j )
          {
            ret &= ( A[ j ] * blockDof + offset * s == indices[ localBlock + offset ] );
            offset += A[ j ];
          }
          return ret;
        });
  }
}


int main ( int argc, char **argv )
{
  try
  {
    Dune::Fem::MPIManager::initialize( argc, argv );

    Dune::Fem::Parameter::append( argc, argv );
    Dune::Fem::Parameter::append( (argc < 2) ? "parameter" : argv[ 1 ] );

    std::string gridFile( "2dgrid.dgf" );

    Dune::GridPtr< GridType > gridPtr( gridFile );
    GridType &grid = *gridPtr;

    GridPartType gridPart( grid );

    BlockMapperType blockMapper( gridPart );
    MapperType mapper( blockMapper );

    checkMapper( gridPart, blockMapper, mapper );
    return 0;
  }
  catch ( const Dune::Exception &e )
  {
    std::cerr<< e <<std::endl;
    return 1;
  }

  return 0;
}
