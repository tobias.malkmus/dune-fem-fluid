#ifndef DUNE_FEM_IO_FILE_TUPLEDATAWRITER_HH
#define DUNE_FEM_IO_FILE_TUPLEDATAWRITER_HH

#include <memory>
#include <string>
#include <tuple>
#include <utility>

#include <dune/common/std/utility.hh>
#include <dune/fem/io/file/datawriter.hh>

namespace Dune
{

  namespace Fem
  {

    // forward declaration

    template< class GridPtrTuple, class IOTupleTuple >
    class TupleDataWriter;


    // TupleDataOutputParameter
    // ------------------------

    struct TupleDataOutputParameter
      : public LocalParameter< DataWriterParameters, TupleDataOutputParameter >
    {
      int comp_;
      TupleDataOutputParameter ( int comp ) : comp_( comp ) {}
      TupleDataOutputParameter ( const TupleDataOutputParameter & ) = default;
      std::string path () const { return std::to_string( comp_ ); }
    };


    // TupleDataWriter
    // ---------------

    template< class ... Grids, class ... IOTuples >
    class TupleDataWriter< std::tuple< Grids ... >, std::tuple< IOTuples ... > >
    {
      typedef TupleDataWriter< std::tuple< Grids ... >, std::tuple< IOTuples ... > >  ThisType;

      static_assert( sizeof ... ( Grids ) == sizeof ... ( IOTuples ),
                     "TupleDataWriter needs same size of GridTuple and IoTupleTuple" );

      typedef decltype ( Std::index_sequence_for< Grids ... >() ) Sequence;

      typedef std::tuple< Grids ... > GridTuple;
      typedef std::tuple< IOTuples ... > IOTupleTuple;

      template< class S >
      struct Tupler;

      template< std::size_t ... I >
      struct Tupler< Std::index_sequence< I ... > >
      {
        typedef std::tuple<
          DataWriter<
            typename std::tuple_element< I, GridTuple >::type,
            typename std::tuple_element< I, IOTupleTuple >::type
            > * ... > type;
      };

      typedef typename Tupler< Sequence >::type DataWriterTuple;

    public:

      TupleDataWriter ( const GridTuple &grid, IOTupleTuple &ioTuple )
        : writerTuple_( creatTuple( grid, ioTuple, Sequence() ) )
      {}

      template< class ... Args >
      void write ( Args && ... args ) const
      {
        writeOwn( Sequence(), std::forward< Args >( args ) ... );
      }

      template< class ... Args >
      void writeData ( Args && ... args ) const
      {
        writeDataOwn( Sequence(), std::forward< Args >( args ) ... );
      }

    protected:
      template< class ... Args, std::size_t ... I >
      void writeOwn ( Std::index_sequence< I ... >, Args&& ... args )  const
      {
        std::ignore = std::make_tuple( ( std::get< I >( writerTuple_ )->write( std::forward< Args >( args ) ... ), I ) ... );
      }

      template< class ... Args, std::size_t ... I >
      void writeDataOwn ( Std::index_sequence< I ... >, Args&& ... args )  const
      {
        std::ignore = std::make_tuple( ( std::get< I >( writerTuple_ )->writeData( std::forward< Args >( args ) ... ), I ) ... );
      }

      template< std::size_t ... I >
      static DataWriterTuple creatTuple ( const GridTuple &grid, IOTupleTuple &ioTuple, Std::index_sequence< I ... > )
      {
        return std::make_tuple(
                 new typename
                 std::remove_pointer< typename std::tuple_element< I, DataWriterTuple >::type >::type
                   ( std::get< I >( grid ), std::get< I >( ioTuple ), TupleDataOutputParameter( I ) ) ... );
      }

      DataWriterTuple writerTuple_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #infdef DUNE_FEM_IO_FILE_TUPLEDATAWRITER_HH
