#ifndef DUNE_FEM_SPACE_BASISFUNCTIONSET_LIFTED_HH
#define DUNE_FEM_SPACE_BASISFUNCTIONSET_LIFTED_HH

#include <iterator>
#include <vector>
#include <tuple>

#include <dune/fem/function/localfunction/evaluate.hh>
#include <dune/fem/function/localfunction/temporaryset.hh>
#include <dune/fem/operator/1order/localmassmatrix.hh>
#include <dune/fem/operator/common/localmatrixadapter.hh>
#include <dune/fem/space/common/discretefunctionspace.hh>


namespace Dune
{

  namespace Fem
  {



    // LiftedBasisFunctionSet
    // ----------------------

    template< class HostDiscreteFunctionSpace >
    class LiftedBasisFunctionSet
    {
      typedef LiftedBasisFunctionSet< HostDiscreteFunctionSpace > ThisType;

    public:
      typedef HostDiscreteFunctionSpace HostDiscreteFunctionSpaceType;

      typedef typename HostDiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename HostDiscreteFunctionSpaceType::FunctionSpaceType HostFunctionSpaceType;
      typedef typename HostDiscreteFunctionSpaceType::BasisFunctionSetType HostBasisFunctionSetType;

      typedef typename HostDiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      static const int dimDomain = HostFunctionSpaceType::dimDomain;
      static const int dimHostRange = HostFunctionSpaceType::dimRange;

      static const int dimRange = dimDomain * dimHostRange;

      typedef typename ToNewDimRangeFunctionSpace< typename HostDiscreteFunctionSpace::FunctionSpaceType, dimRange >::Type FunctionSpaceType;

      typedef typename DifferentDiscreteFunctionSpace< HostDiscreteFunctionSpaceType, FunctionSpaceType >::Type DiscreteFunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

    protected:

      struct ScaledAssign
      {
        ScaledAssign ( double s ) : s_( s )
        {}

        template< class T, class U >
        void operator() ( const T &a, U &b ) const
        {
          b = a;
          b *= s_;
        }

      private:
        double s_;
      };

      typedef typename HostDiscreteFunctionSpaceType::RangeType HostRangeType;
      typedef typename HostDiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

    public:
      // the sets will need a stacking allocator for the real performance
      typedef TemporaryLocalFunctionSet< DiscreteFunctionSpaceType > SingleEntityLiftedBasisSetType;

    protected:
      typedef std::vector< std::tuple< SingleEntityLiftedBasisSetType, EntityType, int > > LiftedBasisFunctionSetStorageType;

      typedef CachingQuadrature< GridPartType, 0 > QuadratureType;
      typedef CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

      struct OnEntityLiftedSet
      {
        OnEntityLiftedSet ( const std::tuple< SingleEntityLiftedBasisSetType, EntityType, int > &key )
          : single_( std::get< 0 >( key ) ),
            entity_( std::get< 1 >( key ) ),
            index_( std::get< 2 >( key ) )
        {}

        const SingleEntityLiftedBasisSetType &set () const { return single_; }

        const EntityType &entity () const { return entity_; }

        template< class Point, class RangeVector >
        void evaluateAll ( const Point &point, RangeVector &ranges ) const
        {
          AssignFunctor< RangeVector > f( ranges );
          single_.evaluateEach( point, f );
        }

        template< class Point, class RangeVector >
        void evaluateAllScaled ( const Point &point, RangeVector &ranges, const double scale ) const
        {
          ScaledAssign assign( scale );
          AssignFunctor< RangeVector, ScaledAssign > f( ranges, assign );
          single_.evaluateEach( point, f );
        }

        // umv
        template< class Vector1, class Vector2 >
        void usmv ( double s, const Vector1 &vec1, Vector2 &vec2 ) const
        {
          assert( vec1.size() == size() );
          assert( std::size_t( numDofs() ) == vec2.size() );

          // Note column vise multiplication
          for( std::size_t i = 0; i < size(); ++i )
            for( int j = 0; j < numDofs(); ++j )
              vec2[ j ] += s * vec1[ i ] * single_[ i ][ j ];
        }

        // umv
        template< class Vector1, class Vector2 >
        void umv ( const Vector1 &vec1, Vector2 &vec2 ) const
        {
          assert( vec1.size() == size() );
          assert( std::size_t( numDofs() ) == vec2.size() );

          // Note column vise multiplication
          for( std::size_t i = 0; i < size(); ++i )
            for( int j = 0; j < numDofs(); ++j )
              vec2[ j ] += vec1[ i ] * single_[ i ][ j ];
        }

        void clear () { single_.clear(); }
        std::size_t size () const { return single_.size(); }
        int numDofs () const { return single_.numDofs(); }

        int index () const { return index_; }

      protected:
        const SingleEntityLiftedBasisSetType &single_;
        EntityType entity_;
        const int index_;
      };

    public:
      typedef typename LiftedBasisFunctionSetStorageType::iterator iterator;
      typedef typename LiftedBasisFunctionSetStorageType::const_iterator const_iterator;

      typedef OnEntityLiftedSet OnEntityLiftedSetType;

      LiftedBasisFunctionSet ( const HostDiscreteFunctionSpaceType &hostSpace )
        : hostSpace_( hostSpace ),
          space_( hostSpace.gridPart() ),
          localMassMatrix_( space_ )
      {
        liftedSetStorage_.reserve( 8 );
      }


      const DiscreteFunctionSpaceType &space () const { return space_; }

      void initEntity ( const EntityType &entity )
      {
        // clean up
        liftedSetStorage_.clear();

        const GridPartType &gridPart = space_.gridPart();
        const BasisFunctionSetType basisSet = hostSpace_.basisFunctionSet( entity );
        const std::size_t numDofs = basisSet.size();
        phi_.resize( numDofs );

        neighborOffSet_ = entity.subEntities( 1 );

        for( int i = 0; i < neighborOffSet_; ++i )
        {
          liftedSetStorage_.emplace_back( space_, entity, i );
          std::get< 0 >( liftedSetStorage_.back() ).init( entity, numDofs );
          std::get< 0 >( liftedSetStorage_.back() ).clear();
        }

        // rebuild
        const int quadOrder = space_.order() * 2;
        for( const IntersectionType &intersection : intersections( gridPart, entity ) )
        {
          SingleEntityLiftedBasisSetType &liftedSet = std::get< 0 >( liftedSetStorage_[ getInsideIndex( intersection ) ] );
          bLifts_.resize( liftedSet.size() );

          FaceQuadratureType faceQuadInside( gridPart, intersection, quadOrder, FaceQuadratureType::INSIDE );

          if( intersection.neighbor() )
          {
            EntityType neighbor = intersection.outside ();
            liftedSetStorage_.emplace_back( space_, neighbor, getOutsideIndex( intersection ) );

            FaceQuadratureType faceQuadOutside( gridPart, intersection, quadOrder, FaceQuadratureType::OUTSIDE );

            SingleEntityLiftedBasisSetType &liftedSetNb = std::get< 0 >( liftedSetStorage_.back() );
            liftedSetNb.init( entity, numDofs );
            liftedSetNb.clear();
            assert( liftedSetNb.size() == numDofs );

            bLiftsNb_.resize( liftedSetNb.size() );

            const BasisFunctionSetType &basisSetNb = hostSpace_.basisFunctionSet( neighbor );
            phiNb_.resize( basisSetNb.size() );

            const size_t numQuadraturePoints = faceQuadInside.nop();
            for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
            {
              const double weight = faceQuadInside.weight( pt );
              // integration element is contained in the normal
              DomainType normal
                = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

              normal *= weight;

              basisSet.evaluateAll( faceQuadInside[ pt ], phi_ );
              basisSetNb.evaluateAll( faceQuadOutside[ pt ], phiNb_ );

              for( std::size_t i = 0; i < numDofs; ++i )
                for( int r = 0; r < dimHostRange; ++r )
                  for( int d = 0; d < dimDomain; ++d )
                  {
                    bLifts_[ i ][ r * dimDomain + d ] = phi_[ i ][ r ] * normal[ d ];
                    bLiftsNb_[ i ][ r * dimDomain + d ] = phiNb_[ i ][ r ] * normal[ d ];
                  }

              liftedSet.axpy( faceQuadInside[ pt ], bLifts_ );
              liftedSetNb.axpy( faceQuadInside[ pt ], bLiftsNb_ );
            }
          }
          else
          {
            const size_t numQuadraturePoints = faceQuadInside.nop();
            for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
            {
              const double weight = faceQuadInside.weight( pt );
              // integration element is contained in the normal
              DomainType normal
                = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

              normal *= weight;

              basisSet.evaluateAll( faceQuadInside[ pt ], phi_ );

              for( std::size_t i = 0; i < numDofs; ++i )
                for( int r = 0; r < dimHostRange; ++r )
                  for( int d = 0; d < dimDomain; ++d )
                    bLifts_[ i ][ r * dimDomain + d ] = phi_[ i ][ r ] * normal[ d ];

              liftedSet.axpy( faceQuadInside[ pt ], bLifts_ );
            }
          }
        }

        for( auto &singleSet : liftedSetStorage_ )
          applyInverseMass( std::get< 0 >( singleSet ) );
      }

      template< class LocalFunction >
      void applyInverseMass ( LocalFunction &local ) const
      {
        localMassMatrix_.applyInverse( local );
      }

      void applyInverseMass ( SingleEntityLiftedBasisSetType &local ) const
      {
        LocalMatrixAdapter< SingleEntityLiftedBasisSetType > adapter( local );
        localMassMatrix_.leftMultiplyInverse( adapter );
      }

      int getInsideIndex ( const IntersectionType &intersection )  const
      {
        return intersection.indexInInside();
      }

      int getOutsideIndex ( const IntersectionType &intersection ) const
      {
        return neighborOffSet_ + intersection.indexInInside();
      }

      // make it range based for compatible
      const_iterator begin () const { return liftedSetStorage_.begin(); }
      const_iterator end () const { return liftedSetStorage_.end(); }
      iterator begin () { return liftedSetStorage_.begin(); }
      iterator end () { return liftedSetStorage_.end(); }

      std::size_t size () const { return liftedSetStorage_.size(); }

      int maxSize () const { return 2 * neighborOffSet_; }

    protected:
      const HostDiscreteFunctionSpaceType &hostSpace_;
      DiscreteFunctionSpaceType space_;
      LocalMassMatrix< DiscreteFunctionSpaceType, QuadratureType > localMassMatrix_;

      LiftedBasisFunctionSetStorageType liftedSetStorage_;

      std::vector< RangeType > bLifts_, bLiftsNb_;
      std::vector< HostRangeType > phi_, phiNb_;

      mutable int neighborOffSet_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_BASISFUNCTIONSET_LIFTED_HH
