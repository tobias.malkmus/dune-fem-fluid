set(HEADERS
  lifted.hh
)

install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fem-dg/space/basisfunctionset)
