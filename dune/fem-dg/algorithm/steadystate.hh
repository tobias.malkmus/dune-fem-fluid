#ifndef DUNE_FEM_ALGORITHM_STEADYSTATEALGORITHM_HH
#define DUNE_FEM_ALGORITHM_STEADYSTATEALGORITHM_HH

// include std libs
#include <iostream>
#include <string>

// dune-fem includes
#include <dune/fem/misc/gridwidth.hh>
#include <dune/fem/solver/newtoninverseoperator.hh>
#include <dune/fem/space/common/adaptmanager.hh>


namespace Dune
{

  namespace Fem
  {

    // SteadyStateTraits
    // -----------------

    template< class Grid, class ProblemTraits, int polOrd >
    struct SteadyStateTraits
    {
      enum { polynomialOrder = polOrd };

      // type of Grid
      typedef Grid GridType;

      typedef ProblemTraits ProblemTraitsType;

      typedef typename ProblemTraitsType::HostGridPartType HostGridPartType;
      typedef typename ProblemTraitsType::GridPartType GridPartType;

      typedef typename ProblemTraitsType::AnalyticalTraits AnalyticalTraits;
      typedef typename ProblemTraitsType::template DiscreteTraits< polOrd > DiscreteTraits;

      // obtain the problem dependent types, analytical context
      typedef typename AnalyticalTraits::ModelType ModelType;
      typedef typename AnalyticalTraits::ProblemType ProblemType;

      // type of discrete function space and discrete function
      typedef typename DiscreteTraits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteTraits::DiscreteFunctionType DiscreteFunctionType;

      // type of dg operator
      typedef typename DiscreteTraits::OperatorType OperatorType;

      // tpye of jacobian operator used in the nested newton loops
      typedef typename DiscreteTraits::JacobianOperatorType JacobianOperatorType;

      // type of solver
      typedef typename DiscreteTraits::Solver::Type InverseOperatorType;

      // type of IOTuple
      typedef typename DiscreteTraits::IOTupleType IOTupleType;

      // type of solver monitor
      typedef typename DiscreteTraits::SolverMonitorType SolverMonitorType;

    };


    // NestedNewtonParameter
    // ---------------------

    struct NestedNewtonParameter
      : public LocalParameter< NewtonParameter, NestedNewtonParameter >
    {
      NestedNewtonParameter ( ParameterReader parameter = Parameter::container() )
        : parameter_( parameter )
      {}

      double toleranceParameter () const
      {
        return parameter_.getValue< double >( "fem.ode.tolerance", 1e-6 );
      }

      double linAbsTolParameter ( const double &tolerance )  const
      {
        return parameter_.getValue< double >( "fem.ode.solver.tolerance", tolerance / 8 );
      }

      double linReductionParameter ( const double &tolerance ) const
      {
        return parameter_.getValue< double >( "fem.ode.solver.tolerance", tolerance / 8 );
      }

      bool verbose () const
      {
        static const std::string verboseTypeTable[]
          = { "none", "noconv", "cfl", "full" };
        return ( parameter_.getEnum( "fem.ode.verbose", verboseTypeTable, 0 ) > 2 );
      }

      bool linearSolverVerbose () const
      {
        return parameter_.getValue< bool >( "fem.ode.solver.verbose", false );
      }

      int maxIterationsParameter () const
      {
        return parameter_.getValue< int >( "fem.ode.iterations", 100 );
      }

      int maxLinearIterationsParameter () const
      {
        return parameter_.getValue< int >( "fem.ode.solver.iterations", 10000 );
      }

    protected:
      ParameterReader parameter_;
    };



    // SteadyStateAlgorithm
    // --------------------

    template< class Grid, class ProblemTraits, int polynomialOrder >
    class SteadyStateAlgorithm
    {
      // my traits class
      typedef SteadyStateTraits< Grid, ProblemTraits, polynomialOrder > Traits;
      typedef SteadyStateAlgorithm< Grid, ProblemTraits, polynomialOrder > This;

    public:
      typedef typename Traits::GridType GridType;
      typedef typename Traits::IOTupleType IOTupleType;
      typedef typename Traits::SolverMonitorType SolverMonitorType;

      // initial data type
      typedef typename Traits::ProblemType ProblemType;

      // An analytical version of our model
      typedef typename Traits::ModelType ModelType;

      typedef typename Traits::GridPartType GridPartType;
      typedef typename Traits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;

      // The DG space operator
      typedef typename Traits::OperatorType OperatorType;

      // type of steady state solver
      typedef typename Traits::InverseOperatorType InverseOperatorType;

      // type of analytical traits
      typedef typename Traits::AnalyticalTraits AnalyticalTraits;

      // type of discrete traits
      typedef typename Traits::DiscreteTraits DiscreteTraits;

      SteadyStateAlgorithm ( GridType &grid, ParameterReader parameter = Parameter::container() )
        : grid_( grid ),
          problem_( AnalyticalTraits::problem() ),
          model_( problem() ),
          gridPart_( grid ),
          space_( gridPart_ ),
          solution_( "solution", space_ ),
          operator_( space_, model() ),
          errorHandler_(),
          description_( "Steady state scheme,\nOperator: " + operator_.description() +"\nProblem: " + problem().description() ),
          parameter_( std::move( parameter ) )
      {}

      //! return reference to discrete space
      DiscreteFunctionSpaceType &space () { return space_; }

      //! returns data prefix for EOC loops ( default is loop )
      std::string dataPrefix () const { return problem().dataPrefix(); }

      //! return default data tuple for data output
      IOTupleType dataTuple () { return IOTupleType( &solution_ ); }

      //! return reference to grid
      GridType &grid () { return grid_; }

      // solve the problem for eoc loop 'step'
      void solve ( int step )
      {
        DiscreteFunctionType rhs( "rhs", space_ );

        rhs.clear();
        solution_.clear();

        InverseOperatorType invOp( operator_, NestedNewtonParameter( parameter_ ) );
        invOp( rhs, solution_ );

        monitor_.gridwidth_ = GridWidth::calcGridWidth( gridPart_ );
        monitor_.numberOfElements_ = gridSize();
        monitor_.totalNewtonIterations_ = invOp.iterations();
        monitor_.totalLinearSolverIterations_ = invOp.linearIterations();

        monitor_.avgTimeStep_ = 0;
        monitor_.minTimeStep_ = 0;
        monitor_.maxTimeStep_ = 0;
        monitor_.timeSteps_ = 0;

        monitor_.finalize();
      }

      //! return size of grid
      std::size_t gridSize () const
      {
        std::size_t grSize = grid_.size( 0 );
        return grid_.comm().sum( grSize );
      }

      //! finalize computation by calculating errors and EOCs
      void finalize ( const int eocloop )
      {
        errorHandler_.finalize( eocloop, model(), solution_, monitor_, operator_.assambleTime() );
      }

      //! name of the scheme
      std::string description () const { return description_; }

      const ProblemType &problem () const
      {
        assert( problem_ );
        return *problem_;
      }

      ProblemType &problem ()
      {
        assert( problem_ );
        return *problem_;
      }

      ModelType &model () { return model_; }
      const ModelType &model () const { return model_; }

      SolverMonitorType monitor () { return monitor_; }

      void globalRefine ()
      {
        GlobalRefine::apply( grid_, Dune::DGFGridInfo< GridType >::refineStepsForHalf() );
      }

    protected:
      GridType &grid_;
      ProblemType *problem_;
      ModelType model_;

      GridPartType gridPart_;      // reference to grid part, i.e. the leaf grid
      DiscreteFunctionSpaceType space_;    // the discrete function space
      DiscreteFunctionType solution_;

      OperatorType operator_;
      typename AnalyticalTraits::ErrorHandler errorHandler_;

      std::string description_;
      SolverMonitorType monitor_;
      ParameterReader parameter_;
    };

  }  // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_ALGORITHM_STEADYSTATEALGORITHM_HH
