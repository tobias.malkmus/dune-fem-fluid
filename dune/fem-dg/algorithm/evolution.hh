#ifndef DUNE_FEM_ALGORITHM_EVOLUTION_HH
#define DUNE_FEM_ALGORITHM_EVOLUTION_HH

#include <utility>

// system includes
#include <sstream>

// dune-fem includes
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/femtimer.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/space/common/adaptmanager.hh>

#include <dune/fem-dg/algorithm/basic/evolution.hh>
#include <dune/fem-dg/algorithm/basic/movingdomain.hh>


namespace Dune
{

  namespace Fem
  {


    // EvolutionAlgorithmBase
    // ----------------------

    template< class Algorithm >
    class EvolutionAlgorithmBase
    {
      typedef EvolutionAlgorithmBase< Algorithm > ThisType;

    public:
      // type of Grid
      typedef typename Algorithm::GridType GridType;

      // type of IOTuple
      typedef typename Algorithm::IOTupleType IOTupleType;

      // type of statistics monitor
      typedef typename Algorithm::SolverMonitorType SolverMonitorType;

      // type of time provider organizing time for time loops
      typedef TimeProvider< > TimeProviderType;

      //! constructor
      EvolutionAlgorithmBase ( GridType &grid, ParameterReader parameter = Parameter::container() )
        : algorithm_( grid, parameter ),
          parameter_( std::move( parameter ) ),
          // Initialize Timer for CPU time measurements
          timeStepTimer_( Dune::FemTimer::addTo( "max time/timestep" ) ),
          fixedTimeStep_( parameter_.getValue< double >( "timeloop.step.fixed", 0 ) ),
          fixedTimeStepEocLoopFactor_( parameter_.getValue< double >( "fixedTimeStepEocLoopFactor", 1. ) )
      {}

      //! return reference to grid
      GridType &grid () { return algorithm_.grid(); }

      //! return description of the algorithm
      std::string description () { return algorithm_.description(); }

      //! return default data tuple for data output
      IOTupleType dataTuple ()
      {
        return algorithm_.dataTuple();
      }

      //! default time loop implementation, overload for changes in derived classes !!!
      void solve ( const int loop )
      {
        // get grid reference
        GridType &grid = algorithm_.grid();

        // print info on each printCount step
        const int printCount = parameter_.getValue< int >( "timeloop.printcount", -1 );

        double maxTimeStep = parameter_.getValue( "timeloop.step.max", std::numeric_limits< double >::max());
        const double starttime = parameter_.getValue< double >( "timeloop.start", 0.0 );

        // get end time from parameter_.ile
        const double endtime   = parameter_.getValue< double >( "timeloop.end" );

        // if this variable is set then only maximalTimeSteps timesteps will be computed
        const int maximalTimeSteps = parameter_.getValue( "timeloop.step.end", std::numeric_limits< int >::max());

        SolverMonitorType monitor;

        // Initialize TimeProvider
        TimeProviderType tp( starttime );

        // set initial data (and create ode solver)
        algorithm_.initializeStep( tp, loop, monitor );

        // start first time step with prescribed fixed time step
        // if it is not 0 otherwise use the internal estimate
        tp.provideTimeStepEstimate( maxTimeStep );

        // adjust fixed time step with timeprovider.factor()
        const double fixedTimeStep = fixedTimeStep_/tp.factor();

        if( fixedTimeStep > 1e-20 )
          tp.init( fixedTimeStep );
        else
          tp.init();

        //******************************
        //*  Time Loop                 *
        //******************************
        for(; tp.time() < endtime; )
        {
          // write data for current time
          algorithm_.writeData( tp );

          // reset time step estimate
          tp.provideTimeStepEstimate( maxTimeStep );

          // current time step number
          const int timeStep  = tp.timeStep();

          //************************************************
          //* Compute an ODE timestep                      *
          //************************************************
          Dune::FemTimer::start( timeStepTimer_ );

          // perform the solve for one time step, i.e. solve ODE
          algorithm_.step( tp, monitor );

          // stop FemTimer for this time step
          Dune::FemTimer::stop( timeStepTimer_, Dune::FemTimer::max );

          if( (printCount > 0) && (timeStep % printCount == 0))
            if( grid.comm().rank() == 0 )
              monitor.dump( tp, std::cout );

          // next time step is prescribed by fixedTimeStep
          // it fixedTimeStep is not 0
          if( fixedTimeStep > 1e-20 )
            tp.next( fixedTimeStep );
          else
            tp.next();

          // for debugging and codegen only
          if( tp.timeStep() >= maximalTimeSteps )
          {
            if( Parameter::verbose() )
              std::cerr << "ABORT: time step count reached max limit of " << maximalTimeSteps << std::endl;
            break;
          }
        }

        // write last time step
        algorithm_.writeData( tp, true );

        // finalize eoc step
        algorithm_.finalizeStep( tp );

        // prepare the fixed time step for the next eoc loop
        fixedTimeStep_ /= fixedTimeStepEocLoopFactor_;

        // adjust average time step size
        monitor.finalize();

        monitor_ = monitor;
      }

      //! unused, for symetry to steadystate algorithm
      void finalize ( const int eocloop ) {}

      //! return copy of solver monitor, used in eoc statistics
      SolverMonitorType monitor () { return monitor_; }

      //! perform a global grid refinement
      void globalRefine ()
      {
        GlobalRefine::apply( algorithm_.grid(), Dune::DGFGridInfo< GridType >::refineStepsForHalf() );
      }

    protected:
      Algorithm algorithm_;
      ParameterReader parameter_;
      unsigned int timeStepTimer_;

      // use fixed time step if fixedTimeStep>0
      double fixedTimeStep_;
      double fixedTimeStepEocLoopFactor_;
      SolverMonitorType monitor_;
    };


    // EvolutionAlgorithm
    // ------------------

    template< class Grid, class Problem, int polOrder >
    using EvolutionAlgorithm = EvolutionAlgorithmBase< BasicEvolution< Grid, Problem, polOrder > >;


    // MovingDomainEvolutionDomain
    // ---------------------------

    template< class Grid, class Problem, int polOrder >
    using MovingDomainEvolutionAlgorithm = EvolutionAlgorithmBase< BasicMovingDomainEvolution< Grid, Problem, polOrder > >;

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_ALGORITHM_EVOLUTION_HH
