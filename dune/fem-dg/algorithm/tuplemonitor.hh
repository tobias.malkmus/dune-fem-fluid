#ifndef DUNE_FEM_FLUID_ALGORITHM_TUPLEMONITOR_HH
#define DUNE_FEM_FLUID_ALGORITHM_TUPLEMONITOR_HH

#include <tuple>
#include <utility>

#include <dune/common/std/utility.hh>
#include <dune/fem/misc/femeoc.hh>


namespace Dune
{

  namespace Fem
  {


    // TupleSolverMonitor
    // ------------------

    template< class ... Monitors >
    struct TupleSolverMonitor
    {
      typedef std::tuple< Monitors ... >  MonitorTuple;

      typedef decltype ( Std::index_sequence_for< Monitors ... >() ) IndexSequence;

      TupleSolverMonitor () {}

      TupleSolverMonitor ( const TupleSolverMonitor & ) = default;
      TupleSolverMonitor ( TupleSolverMonitor && ) = default;

      TupleSolverMonitor ( std::tuple< Monitors ... > tuple )
        : tuple_( tuple ) {}

      TupleSolverMonitor &operator= ( const TupleSolverMonitor & ) = default;

      template< class TimeProvider >
      void dump ( const TimeProvider &tp, std::ostream &out ) const
      {
        dump( tp, out, IndexSequence() );
      }

      void reset () { reset( IndexSequence() ); }

      template< class TimeProvider >
      void step ( const TimeProvider &tp ) { step( tp, IndexSequence() ); }

      void finalize () { finalize( IndexSequence() ); }

      template< int i >
      typename std::tuple_element< i, MonitorTuple >::type& subMonitor()
      {
        return std::get< i >( tuple_ );
      }

      unsigned int numberOfElements_;

    protected:
      template< class TimeProvider, std::size_t ... I >
      void dump ( const TimeProvider &tp, std::ostream &out, Std::index_sequence< I ... > ) const
      {
        std::ignore = std::make_tuple( ( std::get< I >( tuple_ ).dump( tp, out ), I ) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void step ( const TimeProvider &tp, Std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( std::get< I >( tuple_ ).step( tp ), I ) ... );
      }

      template< std::size_t ... I >
      void reset ( Std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( std::get< I >( tuple_ ).reset(), I ) ... );
      }

      template< std::size_t ... I >
      void finalize ( Std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( std::get< I >( tuple_ ).finalize(), I ) ... );
      }

      MonitorTuple tuple_;
    };


    // writeFemEoc
    // -----------

    template< class ... Monitors >
    void writeFemEoc ( const TupleSolverMonitor< Monitors ... > &monitor, std::size_t gridSize, double runTime, std::stringstream &eocInfo )
    {}

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_FLUID_ALGORITHM_TUPLEMONITOR_HH
