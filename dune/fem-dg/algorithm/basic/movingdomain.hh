#ifndef DUNE_FEM_FLUID_ALGORITHM_MOVINGDOMAIN_HH
#define DUNE_FEM_FLUID_ALGORITHM_MOVINGDOMAIN_HH

#include <dune/fem/io/parameter.hh>

#include <dune/fem-dg/algorithm/basic/evolution.hh>


namespace Dune
{

  namespace Fem
  {

    // BasicMovingDomainEvolution
    // --------------------------

    template< class Grid, class ProblemTraits, int polynomialOrder >
    class BasicMovingDomainEvolution
    {
      typedef BasicMovingDomainEvolution< Grid, ProblemTraits, polynomialOrder > ThisType;
      typedef BasicEvolutionTraits< Grid, ProblemTraits, polynomialOrder > Traits;

    public:
      typedef typename Traits::GridType GridType;
      typedef typename Traits::IOTupleType IOTupleType;
      typedef typename Traits::SolverMonitorType SolverMonitorType;

      // An analytical version of our model
      typedef typename Traits::ModelType ModelType;
      typedef typename Traits::ProblemType ProblemType;

      // type of coordfunction space
      typedef typename ProblemTraits::HostGridPartType HostGridPartType;
      typedef typename ProblemTraits::DomainDescriptorType DomainDescriptorType;

      typedef typename Traits::GridPartType GridPartType;
      typedef typename Traits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;

      // The DG space operator
      typedef typename Traits::OperatorType OperatorType;

      // The ODE Solvers
      typedef typename Traits::SolverType SolverType;

      // type of DataWriter
      typedef typename Traits::DataWriterType DataWriterType;

      // analytical Tratis
      typedef typename Traits::AnalyticalTraits AnalyticalTraits;

      // discrete Traits
      typedef typename Traits::DiscreteTraits DiscreteTraits;

      BasicMovingDomainEvolution ( GridType &grid, ParameterReader parameter = Parameter::container() )
        : grid_( grid ),
          problem_( AnalyticalTraits::problem() ),
          model_( problem() ),
          hostGridPart_( grid ),
          domainDescriptor_( hostGridPart_ ),
          gridPart_( domainDescriptor_.coordFunction() ),
          space_( gridPart_ ),
          solution_( "solution", space() ),
          dgOperator_( space_, model(), domainDescriptor_.domainVelocity() ),
          solverOrder_( parameter.getValue< int >( "fem.ode.order", 1 ) ),
          domainVeloSpace_( gridPart_ ),
          domainVelocity_( "domain velocity", domainVeloSpace_, domainDescriptor_.domainVelocity().dofVector() ),
          dataTuple_( &solution_, &domainVelocity_ ),
          description_( "Evolution scheme\nOperator: " + dgOperator_.description() +"\nProblem: " +problem().description() ),
          parameter_( parameter )
      {
        dgOperator_.setFunctor( [ this ]( double t ){ this->domainDescriptor_.setTime( t ); } );
      }

      BasicMovingDomainEvolution ( const ThisType &other )
        : grid_( other.grid_ ),
          problem_( other.problem_ ),
          model_( problem() ),
          hostGridPart_( grid_ ),
          domainDescriptor_( hostGridPart_ ),
          gridPart_( domainDescriptor_.coordFunction() ),
          space_( gridPart_ ),
          solution_( "solution", space() ),
          dgOperator_( space_, model(), domainDescriptor_.domainVelocity() ),
          solverOrder_( other.solverOrder_ ),
          domainVeloSpace_( gridPart_ ),
          domainVelocity_( "domain velocity", domainVeloSpace_, domainDescriptor_.domainVelocity().dofVector() ),
          dataTuple_( &solution_, &domainVelocity_ ),
          description_( other.description_ )
      {
        dgOperator_.setFunctor( [ this ]( double t ){ this->domainDescriptor_.setTime( t ); } );
      }

      BasicMovingDomainEvolution ( ThisType &&other )
        : grid_( other.grid_ ),
          problem_( std::move( other.problem_ ) ),
          model_( problem() ),
          hostGridPart_( grid_ ),
          domainDescriptor_( hostGridPart_ ),
          gridPart_( domainDescriptor_.coordFunction() ),
          space_( gridPart_ ),
          solution_( "solution", space() ),
          dgOperator_( space_, model(), domainDescriptor_.domainVelocity() ),
          solverOrder_( std::move( other.solverOrder_ ) ),
          domainVeloSpace_( gridPart_ ),
          domainVelocity_( "domain velocity", domainVeloSpace_, domainDescriptor_.domainVelocity().dofVector() ),
          dataTuple_( &solution_, &domainVelocity_ ),
          description_( std::move( other.description_ ) )
      {
        dgOperator_.setFunctor( [ this ]( double t ){ this->domainDescriptor_.setTime( t ); } );
      }

      GridType &grid () { return grid_; }

      //! return overal number of grid elements
      std::size_t gridSize () const
      {
        std::size_t grSize = dgOperator_.numberOfElements();
        double minMax[ 2 ] = { double(grSize), 1.0/double(grSize) };
        grid_.comm().max( &minMax[ 0 ], 2 );
        if( Parameter::verbose() )
          std::cout << "grid size (min,max) = ( " << size_t( 1.0/minMax[ 1 ] ) << " , " << size_t( minMax[ 0 ] ) << ")" << std::endl;
        return grid_.comm().sum( grSize );
      }


      IOTupleType dataTuple () { return dataTuple_; }

      template< class TimeProvider >
      void writeData ( TimeProvider &tp, const bool writeAnyway = false )
      {
        if( dataWriter_ && dataWriter_->willWrite( tp ) )
          dataWriter_->write( tp );
      }

      //! returns data prefix for EOC loops ( default is loop )
      virtual std::string dataPrefix () const
      {
        return problem().dataPrefix();
      }

      // before first step, do data initialization
      template< class TimeProvider >
      void initializeStep ( TimeProvider &tp, int loop, SolverMonitorType &monitor )
      {
        monitor.reset();

        domainDescriptor_.initialize( tp, loop );

        DiscreteFunctionType &U = solution();

        solver_.reset( new SolverType( dgOperator_, tp, solverOrder_, parameter_ ) );

        // create a data writer if not existing
        dataWriter_.reset( new DataWriterType( grid_, dataTuple_, tp, EocLoopDataOutputParameter( loop, problem().dataPrefix() ) ) );

        interpolate( gridFunctionAdapter( problem().fixedTimeFunction( tp.time() ), gridPart_, 2 * U.space().order() ), U );

        // ode.initialize applies the DG Operator once to get an initial
        // estimate on the time step. This does not change the initial data u.
        solver().initialize( U );

        // set grid width
        monitor.gridwidth_ = GridWidth::calcGridWidth( gridPart_ );
        monitor.numberOfElements_ = gridSize();
      }


      template< class TimeProvider >
      void step ( TimeProvider &tp, SolverMonitorType &monitor )
      {
        DiscreteFunctionType &U = solution();

        // reset overall timer
        overallTimer_.reset();

        solver().solve( U, monitor );

        if( tp.timeStepValid() )
          monitor.step( tp );
      }


      template< class TimeProvider >
      void finalizeStep ( TimeProvider &tp )
      {
        if( true )
        {
          std::cout<<"Time spend during Matrix assamble: " << dgOperator_.assambleTime() <<std::endl;
          std::cout<<"Time spend during Operator evaluation: "<< dgOperator_.computeTime() <<std::endl;
        }
      }

      std::string description () const { return description_; }

      // return reference to the discrete function space
      const DiscreteFunctionSpaceType &space () const { return space_; }

      // return reference to discrete function holding solution
      DiscreteFunctionType &solution () { return solution_; }
      const DiscreteFunctionType &solution () const { return solution_; }

      ProblemType &problem ()
      {
        assert( problem_ );
        return *problem_;
      }

      const ProblemType &problem () const
      {
        assert( problem_ );
        return *problem_;
      }

      const ModelType &model () const { return model_; }
      ModelType &model () { return model_; }

      GridPartType &gridPart () { return gridPart_; }

      DomainDescriptorType &domainDescriptor () { return domainDescriptor_; }

    protected:
      SolverType &solver ()
      {
        assert( solver_ );
        return *solver_;
      }

      GridType &grid_;
      std::unique_ptr< ProblemType > problem_;
      ModelType model_;

      HostGridPartType hostGridPart_;

      DomainDescriptorType domainDescriptor_;

      GridPartType gridPart_;

      DiscreteFunctionSpaceType space_;
      DiscreteFunctionType solution_;

      OperatorType dgOperator_;

      Dune::Timer overallTimer_;
      std::unique_ptr< SolverType > solver_;
      const int solverOrder_;

      typename DiscreteTraits::DomainVelocitySpaceType domainVeloSpace_;
      typename DiscreteTraits::DomainVelocityType domainVelocity_;

      IOTupleType dataTuple_;
      std::unique_ptr< DataWriterType > dataWriter_;
      std::string description_;

      ParameterReader parameter_;
    };

  } // namespace Fem

} // namespace Dune

#endif //#ifndef DUNE_FEM_FLUID_ALGORITHM_MOVINGDOMAIN_HH
