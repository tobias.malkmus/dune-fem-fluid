#ifndef DUNE_FEM_DG_ALGORITHM_BASIC_EVOLUTION_HH
#define DUNE_FEM_DG_ALGORITHM_BASIC_EVOLUTION_HH

#include <dune/fem/function/common/gridfunctionadapter.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/gridwidth.hh>
#include <dune/fem/space/common/interpolate.hh>


namespace Dune
{

  namespace Fem
  {

    // EocLoopDataOutputParameter
    // --------------------------

    struct EocLoopDataOutputParameter
      : public LocalParameter< DataWriterParameters, EocLoopDataOutputParameter >
    {
      std::string loop_;
      EocLoopDataOutputParameter ( int loop, const std::string &name ) : loop_( name + std::to_string( loop ) ) {}
      EocLoopDataOutputParameter ( const EocLoopDataOutputParameter & ) = default;
      std::string path () const { return loop_; }
    };


    // BasicEvolutionTraits
    // --------------------

    template< class Grid, class ProblemTraits, int polOrder >
    struct BasicEvolutionTraits
    {
      // type of Grid
      typedef Grid GridType;

      typedef ProblemTraits ProblemTraitsType;

      typedef typename ProblemTraitsType::GridPartType GridPartType;

      typedef typename ProblemTraitsType::AnalyticalTraits AnalyticalTraits;
      typedef typename ProblemTraitsType::template DiscreteTraits< polOrder > DiscreteTraits;

      // obtain the problem dependent types, analytical context
      typedef typename AnalyticalTraits::ModelType ModelType;
      typedef typename AnalyticalTraits::ProblemType ProblemType;

      // type of discrete function space and discrete function
      typedef typename DiscreteTraits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteTraits::DiscreteFunctionType DiscreteFunctionType;

      // type of dg operator
      typedef typename DiscreteTraits::OperatorType OperatorType;

      // type of dg jacobian operator
      typedef typename DiscreteTraits::JacobianOperatorType JacobianOperatorType;

      // type of ode solver
      typedef typename DiscreteTraits::Solver::Type SolverType;

      // type of IOTuple
      typedef typename DiscreteTraits::IOTupleType IOTupleType;

      typedef typename DiscreteTraits::SolverMonitorType SolverMonitorType;

      // type of Data io
      typedef DataWriter< GridType, IOTupleType > DataWriterType;
    };


    // EvolutionAlgorithm
    // ------------------

    template< class Grid, class ProblemTraits, int polynomialOrder >
    class BasicEvolution
    {
      typedef BasicEvolution< Grid, ProblemTraits, polynomialOrder > ThisType;
      typedef BasicEvolutionTraits< Grid, ProblemTraits, polynomialOrder > Traits;

    public:
      typedef typename Traits::GridType GridType;
      typedef typename Traits::IOTupleType IOTupleType;
      typedef typename Traits::SolverMonitorType SolverMonitorType;

      // An analytical version of our model
      typedef typename Traits::ModelType ModelType;
      typedef typename Traits::ProblemType ProblemType;

      typedef typename Traits::GridPartType GridPartType;
      typedef typename Traits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;

      // The space operator
      typedef typename Traits::OperatorType OperatorType;

      // The ODE Solvers
      typedef typename Traits::SolverType SolverType;

      // type of DataWriter
      typedef typename Traits::DataWriterType DataWriterType;

      // analytical Tratis
      typedef typename Traits::AnalyticalTraits AnalyticalTraits;

      // discrete Traits
      typedef typename Traits::DiscreteTraits DiscreteTraits;

      BasicEvolution ( GridType &grid, ParameterReader parameter = Parameter::container() )
        : grid_( grid ),
          problem_( AnalyticalTraits::problem() ),
          model_( problem() ),
          gridPart_( grid ),
          space_( gridPart_ ),
          solution_( "solution", space() ),
          operator_( space_, model() ),
          solverOrder_( parameter.getValue< int >( "fem.ode.order", 1 ) ),
          dataTuple_( &solution_ ),
          description_( "Evolution scheme\nOperator: " + operator_.description() +"\nProblem: " +problem().description() ),
          parameter_( parameter )
      {}

      BasicEvolution ( const ThisType & other )
        : grid_( other.grid_ ),
          problem_( other.problem_ ),
          model_( problem() ),
          gridPart_( grid_ ),
          space_( gridPart_ ),
          solution_( "solution", space() ),
          operator_( space_, model() ),
          solverOrder_( other.solverOrder_ ),
          dataTuple_( other.dataTuple_ ),
          description_( other.description_ ),
          parameter_( other.parameter_ )
      {}

      BasicEvolution ( ThisType && other )
        : grid_( other.grid_ ),
          problem_( std::move( other.problem_ ) ),
          model_( problem() ),
          gridPart_( grid_ ),
          space_( gridPart_ ),
          solution_( "solution", space() ),
          operator_( space_, model() ),
          solverOrder_( std::move( other.solverOrder_ ) ),
          dataTuple_( std::move( other.dataTuple_ ) ),
          description_( std::move( other.description_ ) ),
          parameter_( std::move( other.parameter_ ) )
      {}


      GridType &grid () { return grid_; }

      IOTupleType dataTuple () { return dataTuple_; }

      template< class TimeProvider >
      void writeData ( TimeProvider &tp, const bool writeAnyway = false )
      {
        if( dataWriter_ && dataWriter_->willWrite( tp ) )
          dataWriter_->write( tp );
      }


      // before first step, do data initialization
      template< class TimeProvider >
      void initializeStep ( TimeProvider &tp, int loop, SolverMonitorType &monitor )
      {
        monitor.reset();

        DiscreteFunctionType &U = solution();

        solver_.reset( new SolverType( operator_, tp, solverOrder_, parameter_ ) );

        // create a data writer if not existing
        dataWriter_.reset( new DataWriterType( grid_, dataTuple_, tp, EocLoopDataOutputParameter( loop, problem().dataPrefix() ) ) );

        interpolate( gridFunctionAdapter( problem().fixedTimeFunction( tp.time() ), gridPart_, 2 * U.space().order() ), U );

        // ode.initialize applies the DG Operator once to get an initial
        // estimate on the time step. This does not change the initial data u.
        solver().initialize( U );

        // set grid width
        monitor.gridwidth_ = GridWidth::calcGridWidth( gridPart_ );
        monitor.numberOfElements_ = gridSize();

        errorHandler_.initializeStep( tp, loop, model(), U );
      }


      template< class TimeProvider >
      void step ( TimeProvider &tp, SolverMonitorType &monitor )
      {
        DiscreteFunctionType &U = solution();

        // reset overall timer
        overallTimer_.reset();

        solver().solve( U, monitor );

        if( tp.timeStepValid() )
          monitor.step( tp );

        errorHandler_.step( tp, model(), U );
      }


      template< class TimeProvider >
      void finalizeStep ( TimeProvider &tp )
      {
        errorHandler_.finalizeStep( tp, model(), solution() );

        if( Parameter::verbose() )
        {
          std::cout<<"Time spend during Matrix assamble: " << operator_.assambleTime() <<std::endl;
          std::cout<<"Time spend during Operator evaluation: "<< operator_.computeTime() <<std::endl;
        }
      }

      std::string description () const { return description_; }


      // non interface methods:

      //! returns data prefix for EOC loops ( default is loop )
      std::string dataPrefix () const { return problem().dataPrefix(); }

      std::size_t gridSize () const { return grid_.size( 0 ); }

      // return reference to the discrete function space
      const DiscreteFunctionSpaceType &space () const { return space_; }

      // return reference to discrete function holding solution
      DiscreteFunctionType &solution () { return solution_; }

      ProblemType &problem ()
      {
        assert( problem_ );
        return *problem_;
      }

      const ProblemType &problem () const
      {
        assert( problem_ );
        return *problem_;
      }

      const ModelType &model () const { return model_; }
      ModelType &model () { return model_; }

      GridPartType &gridPart () { return gridPart_; }

    protected:
      SolverType &solver ()
      {
        assert( solver_ );
        return *solver_;
      }

      GridType &grid_;
      std::unique_ptr< ProblemType > problem_;
      ModelType model_;

      GridPartType gridPart_;

      DiscreteFunctionSpaceType space_;
      DiscreteFunctionType solution_;

      OperatorType operator_;

      Dune::Timer overallTimer_;
      std::unique_ptr< SolverType > solver_;
      const int solverOrder_;

      IOTupleType dataTuple_;
      typename AnalyticalTraits::ErrorHandler errorHandler_;

      std::unique_ptr< DataWriterType > dataWriter_;
      std::string description_;

      ParameterReader parameter_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_ALGORITHM_BASIC_EVOLUTION_HH
