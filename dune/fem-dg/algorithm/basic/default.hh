#ifndef DUNE_FEM_DG_ALGORITHM_BASIC_DEFAULT_HH
#define DUNE_FEM_DG_ALGORITHM_BASIC_DEFAULT_HH

#include <string>

#include <dune/fem/io/parameter.hh>

namespace Dune
{

  namespace Fem
  {

    // BasicEvolutionAlgorithm (documentation)
    // ---------------------------------------

    template< class Traits >
    class BasicEvolutionAlgorithm
    {
      typedef BasicEvolutionAlgorithm< Traits > ThisType;

    public:
      // type of Grid
      typedef typename Traits::GridType GridType;

      // type of IOTuple
      typedef typename Traits::IOTupleType IOTupleType;

      // type of statistics monitor
      typedef typename Traits::SolverMonitorType SolverMonitorType;

      BasicEvolutionAlgorithm ( GridType &grid, ParameterReader = Parameter::container() ) {}
      BasicEvolutionAlgorithm ( const ThisType & ) {}
      BasicEvolutionAlgorithm ( ThisType && ) {}

      //! return reference to hierarchical grid
      GridType &grid ();

      //! return decsription of algorithm
      std::string description () const {}

      //! return default data tuple for data output
      IOTupleType dataTuple () {}

      //! initialize method for time loop, i.e. L2-project initial data
      template< class TimeProvider >
      void initializeStep ( TimeProvider &tp, const int loop, SolverMonitorType &monitor ) {}

      //! solve one time step
      template< class TimeProvider >
      void step ( TimeProvider &tp, SolverMonitorType &monitor ) {}

      //! finalize this EOC loop and possibly calculate EOC ...
      template< class TimeProvider >
      void finalizeStep ( TimeProvider &tp ) {}

      //! write data
      template< class TimeProvider >
      void writeData ( TimeProvider &tp, const bool reallyWrite ) {}

    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_ALGORITHM_BASIC_DEFAULT_HH
