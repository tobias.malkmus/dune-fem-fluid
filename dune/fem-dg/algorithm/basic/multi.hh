#ifndef DUNE_FEM_DG_ALGORITHM_BASIC_MULTI_HH
#define DUNE_FEM_DG_ALGORITHM_BASIC_MULTI_HH

#include <tuple>
#include <utility>
#include <type_traits>

#include <dune/fem/common/utility.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem-dg/algorithm/tuplemonitor.hh>


namespace Dune
{

  namespace Fem
  {


    // BasicMultiAlgorithmTraits
    // -------------------------

    template< class ... Algorithms >
    struct BasicMultiAlgorithmTraits
    {
      typedef decltype ( std::index_sequence_for< Algorithms ... >() ) Sequence;
      static_assert( Std::are_all_same< typename Algorithms::GridType ... > ::value,
          "BasicMultiProblemEvolution needs to take Algorithms defined over a common Grid" );

      typedef typename std::tuple_element< 0, std::tuple< Algorithms ... > >::type::GridType GridType;
      typedef TupleSolverMonitor< typename Algorithms::SolverMonitorType ... > SolverMonitorType;

      typedef std::tuple< Algorithms * ... > AlgorithmTuple;

      typedef decltype( std::tuple_cat( Algorithms::dataTuple() ... ) ) IOTupleType;

      static IOTupleType ioTuple ( AlgorithmTuple &tuple ) { return ioTuple( tuple, Sequence() ); }
      static AlgorithmTuple algorithms ( GridType &grid ) { return algorithms( grid, Sequence() ); }

    protected:
      template< std::size_t ... I >
      static IOTupleType ioTuple ( AlgorithmTuple &tuple, std::index_sequence< I ... > )
      {
        return std::tuple_cat( std::get< I >( tuple )->dataTuple() ... );
      }

      template< std::size_t ... I >
      static AlgorithmTuple algorithms ( GridType &grid, std::index_sequence< I ... > )
      {
        return std::make_tuple( (new typename std::tuple_element< I, std::tuple< Algorithms ... > >::type( std::get< I >( grid ) ) ) ... );
      }
    };



    // BasicMultiAlgorithm
    // -------------------

    template< class ... Algorithms >
    class BasicMultiAlgorithm
    {
      typedef BasicMultiAlgorithm< Algorithms... > ThisType;
      typedef BasicMultiAlgorithmTraits< Algorithms ... > Traits;

      typedef decltype ( std::index_sequence_for< Algorithms ... >() ) IndexSequence;

    public:
      typedef typename Traits::GridType GridType;
      typedef typename Traits::IOTupleType IOTupleType;
      typedef typename Traits::SolverMonitorType SolverMonitorType;

      BasicMultiAlgorithm ( GridType &grid, ParameterReader parameter = Parameter::container() )
        : grid_( grid ),
          algorithms_( Traits::algorithms( grid_ ), parameter ),
          ioTuple_( Traits::ioTuple( algorithms_ ) )
      {}

      GridType &grid () { return grid_; }

      std::string description () { return "BasicMultiAlgorithm"; }

      IOTupleType dataTuple () { return ioTuple_; }

      template< class TimeProvider >
      void initializeStep ( TimeProvider &tp, const int loop, SolverMonitorType &monitor )
      {
        initializeStep( tp, loop, monitor, IndexSequence() );
      }

      template< class TimeProvider >
      void step ( TimeProvider &tp, SolverMonitorType &monitor )
      {
        step( tp, monitor, IndexSequence() );
      }

      template< class TimeProvider >
      void finalizeStep ( TimeProvider &tp )
      {
        finalizeStep( tp, IndexSequence() );
      }

      template< class TimeProvider >
      void writeData ( TimeProvider &tp, const bool reallyWrite = false )
      {
        writeData( tp, reallyWrite, IndexSequence() );
      }

    protected:

      template< class TimeProvider, std::size_t ... I >
      void initializeStep ( TimeProvider &tp, int loop, SolverMonitorType &monitor, std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( (std::get< I >( algorithms_ )->initializeStep( tp, loop, monitor.template subMonitor< I >() ), I ) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void step ( TimeProvider &tp, SolverMonitorType &monitor, std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( std::get< I >( algorithms_ )->step( tp, monitor.template subMonitor< I >() ), I ) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void finalizeStep ( TimeProvider &tp, std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( std::get< I >( algorithms_ )->finalizeStep( tp ), I ) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void writeData ( TimeProvider &tp, bool willWrite, std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( std::get< I >( algorithms_ )->writeData( tp, willWrite ), I ) ... );
      }

      GridType &grid_;
      // only one member
      std::tuple< Algorithms * ... > algorithms_;
      IOTupleType ioTuple_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_FLUID_ALGORITHM_MULTI_HH
