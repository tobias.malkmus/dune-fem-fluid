#ifndef DUNE_FEM_FLUID_ALGORITHM_MONITOR_HH
#define DUNE_FEM_FLUID_ALGORITHM_MONITOR_HH

#include <iostream>

#include <dune/fem/solver/odesolverinterface.hh>
#include <dune/fem/solver/timeprovider.hh>

namespace Dune
{

  namespace Fem
  {

    //! solver statistics and further info
    //! such as newton iterations and iterations of the linear solver
    template< class D >
    struct SolverMonitor
      : public DuneODE::OdeSolverInterface< D >::MonitorType
    {
      typedef typename DuneODE::OdeSolverInterface< D >::MonitorType BaseType;

      // grid parameters
      double gridwidth_;
      std::size_t numberOfElements_;

      // time step statistics
      std::size_t timeSteps_;
      double avgTimeStep_;
      double minTimeStep_;
      double maxTimeStep_;

      // solver statistics
      int totalNewtonIterations_;
      int totalLinearSolverIterations_;
      int newtonIterations_;
      int linearSolverIterations_;

      SolverMonitor ()
        : timeSteps_( 0 ), avgTimeStep_( 0.0 ),
          minTimeStep_( std::numeric_limits< double >::max() ), maxTimeStep_( 0.0 ),
          totalNewtonIterations_( 0 ), totalLinearSolverIterations_( 0 )
      {
        reset();
      }

      SolverMonitor ( const SolverMonitor & ) = default;

      void reset ()
      {
        BaseType::reset();
        // int quantities
        linearSolverIterations_ = 0;
        newtonIterations_ = 0;
      }

      void step ( const Dune::Fem::TimeProviderBase &tp )
      {
        const double ldt = tp.deltaT();
        // calculate time step info
        minTimeStep_ = std::min( ldt, minTimeStep_ );
        maxTimeStep_ = std::max( ldt, maxTimeStep_ );
        avgTimeStep_ += ldt;

        ++timeSteps_;
        totalNewtonIterations_ += newtonIterations_;
        totalLinearSolverIterations_ += linearSolverIterations_;
      }

      void finalize ()
      {
        avgTimeStep_ /= double( timeSteps_ );
      }

      void dump ( const TimeProviderBase &tp, std::ostream &out ) const
      {
        out << "step: " << timeSteps_ << "  time = " << tp.time() << ", dt = " << tp.deltaT()
            <<",  grid size: " << numberOfElements_ << ",  Newton: " << newtonIterations_
            << "  ILS: " << linearSolverIterations_ << std::endl;
      }
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_FLUID_ALGORITHM_MONITOR_HH
