#ifndef DUNE_FEM_FLUID_ALGORITHM_MULTI_HH
#define DUNE_FEM_FLUID_ALGORITHM_MULTI_HH

#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/fem-dg/algorithm/tuplemonitor.hh>
#include <dune/fem/common/utility.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/femtimer.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/space/common/adaptmanager.hh>



namespace Dune
{

  namespace Fem
  {

    // MultiEvolutionAlgorithmBase
    // ---------------------------

    template< class ... Algorithms >
    class MultiEvolutionAlgorithm
    {
      typedef MultiEvolutionAlgorithm< Algorithms ... > ThisType;
      typedef decltype ( std::index_sequence_for< Algorithms ... >() ) Sequence;

    public:
      typedef std::tuple< typename Algorithms::GridType & ... > GridType;
      typedef std::tuple< typename Algorithms::IOTupleType ... > IOTupleType;
      typedef TupleSolverMonitor< typename Algorithms::SolverMonitorType ... > SolverMonitorType;

      // type of time provider organizing time for time loops
      typedef TimeProvider< > TimeProviderType;

      // This ctor is different to the normal one, we expect a serios of grids here
      MultiEvolutionAlgorithm ( GridType &grid, ParameterReader parameter = Parameter::container() ) : MultiEvolutionAlgorithm ( grid, parameter, Sequence() ) {}

      //! return reference to grid
      GridType grid () { return grid( Sequence() ); }

      //! return description of the algorithm
      std::string description () { return description( Sequence() ); }

      //! return default data tuple for data output
      IOTupleType dataTuple () { return dataTuple( Sequence() ); }

      //! default time loop implementation, overload for changes in derived classes !!!
      void solve ( const int loop )
      {
        // print info on each printCount step
        const int printCount = parameter_.getValue< int >( "timeloop.printcount", -1 );

        double maxTimeStep = parameter_.getValue( "timeloop.step.max", std::numeric_limits< double >::max());
        const double starttime = parameter_.getValue< double >( "timeloop.start", 0.0 );

        // get end time from parameter file
        const double endtime   = parameter_.getValue< double >( "timeloop.end" );

        // if this variable is set then only maximalTimeSteps timesteps will be computed
        const int maximalTimeSteps = parameter_.getValue( "timeloop.step.end", std::numeric_limits< int >::max());

        SolverMonitorType monitor;

        // Initialize TimeProvider
        TimeProviderType tp( starttime );

        // set initial data (and create ode solver)
        initializeStep( tp, loop, monitor, Sequence() );

        // start first time step with prescribed fixed time step
        // if it is not 0 otherwise use the internal estimate
        tp.provideTimeStepEstimate( maxTimeStep );

        // adjust fixed time step with timeprovider.factor()
        const double fixedTimeStep = fixedTimeStep_/tp.factor();

        if( fixedTimeStep > 1e-20 )
          tp.init( fixedTimeStep );
        else
          tp.init();

        //******************************
        //*  Time Loop                 *
        //******************************
        for(; tp.time() < endtime; )
        {
          // write data for current time
          writeData( tp, Sequence() );

          // reset time step estimate
          tp.provideTimeStepEstimate( maxTimeStep );

          // current time step number
          const int timeStep  = tp.timeStep();

          //************************************************
          //* Compute an ODE timestep                      *
          //************************************************
          Dune::FemTimer::start( timeStepTimer_ );

          // perform the solve for one time step, i.e. solve ODE
          step( tp, monitor, Sequence() );

          // stop FemTimer for this time step
          Dune::FemTimer::stop( timeStepTimer_, Dune::FemTimer::max );

          if( (printCount > 0) && (timeStep % printCount == 0))
            monitor.dump( tp, std::cout );

          // next time step is prescribed by fixedTimeStep
          // it fixedTimeStep is not 0
          if( fixedTimeStep > 1e-20 )
            tp.next( fixedTimeStep );
          else
            tp.next();

          // for debugging and codegen only
          if( tp.timeStep() >= maximalTimeSteps )
          {
            if( Parameter::verbose() )
              std::cerr << "ABORT: time step count reached max limit of " << maximalTimeSteps << std::endl;
            break;
          }
        }

        // write last time step
        writeData( tp, true, Sequence() );

        // finalize eoc step
        finalizeStep( tp, Sequence() );

        // prepare the fixed time step for the next eoc loop
        fixedTimeStep_ /= fixedTimeStepEocLoopFactor_;

        // adjust average time step size
        monitor.finalize();

        monitor_ = monitor;
      }

      //! unused, for symetry to steadystate algorithm
      void finalize ( const int eocloop ) {}

      //! return copy of solver monitor, used in eoc statistics
      SolverMonitorType monitor () { return monitor_; }

      //! this needs to be user defined, as the grids might interfere
      void globalRefine () { globalRefine( Sequence() ); }

    protected:
      template< std::size_t ... I >
      MultiEvolutionAlgorithm ( GridType &grid, ParameterReader parameter, std::index_sequence< I ... > )
        : algorithms_( std::get< I >( grid, parameter ) ... ),
          parameter_( std::move( parameter ) ),
          // Initialize Timer for CPU time measurements
          timeStepTimer_( Dune::FemTimer::addTo( "max time/timestep" ) ),
          fixedTimeStep_( parameter_.getValue< double >( "timeloop.step.fixed", 0 ) ),
          fixedTimeStepEocLoopFactor_( parameter_.getValue< double >( "fixedTimeStepEocLoopFactor", 1. ) )
      {}

      template< std::size_t ... I >
      GridType grid ( std::index_sequence< I ... > )
      {
        return std::tie( std::get< I >( algorithms_ ).grid() ... );
      }

      template< std::size_t ... I >
      std::string description ( std::index_sequence< I ... > )
      {
        return Std::sum( ( std::get< I >( algorithms_ ).description() + " " ) ... );
      }

      template< std::size_t ... I >
      IOTupleType dataTuple ( std::index_sequence< I ... > )
      {
        return std::make_tuple( std::get< I >( algorithms_ ).dataTuple() ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void initializeStep ( TimeProvider &tp, const int loop, SolverMonitorType &monitor, std::index_sequence< I ... > )
      {
        std::ignore
          = std::make_tuple( ( std::get< I >( algorithms_ ).initializeStep( tp, loop,  monitor.template subMonitor< I >() ), I) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void step ( TimeProvider &tp, SolverMonitorType &monitor, std::index_sequence< I ... > )
      {
        std::ignore
          = std::make_tuple( ( std::get< I >( algorithms_ ).step( tp, monitor.template subMonitor< I >() ), I) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void finalizeStep ( TimeProvider &tp, std::index_sequence< I ... > )
      {
        std::ignore
          = std::make_tuple( ( std::get< I >( algorithms_ ).finalizeStep( tp ), I) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void writeData ( TimeProvider &tp, std::index_sequence< I ... > )
      {
        std::ignore
          = std::make_tuple( ( std::get< I >( algorithms_ ).writeData( tp ), I) ... );
      }

      template< class TimeProvider, std::size_t ... I >
      void writeData ( TimeProvider &tp, bool write, std::index_sequence< I ... > )
      {
        std::ignore
          = std::make_tuple( ( std::get< I >( algorithms_ ).writeData( tp, write ), I) ... );
      }

      template< std::size_t ... I >
      void globalRefine ( std::index_sequence< I ... > )
      {
        std::ignore = std::make_tuple( ( GlobalRefine::apply( std::get< I >( algorithms_ ).grid(),
                                                              Dune::DGFGridInfo< typename std::tuple_element< I, std::tuple< Algorithms ... > >::type::GridType
                                                                                 >::refineStepsForHalf() ), I ) ... );
      }


      std::tuple< Algorithms ... > algorithms_;
      ParameterReader parameter_;
      unsigned int timeStepTimer_;

      // use fixed time step if fixedTimeStep>0
      double fixedTimeStep_;
      double fixedTimeStepEocLoopFactor_;
      SolverMonitorType monitor_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_FLUID_ALGORITHM_MULTI_HH
