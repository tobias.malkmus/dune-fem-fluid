#ifndef DUNE_FEM_DG_MISC_SIMULATE_HH
#define DUNE_FEM_DG_MISC_SIMULATE_HH

#include <dune/common/timer.hh>

#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/femeoc.hh>
#include <dune/fem/misc/femtimer.hh>


namespace Dune
{

  namespace Fem
  {


    // writeFemEoc
    // -----------

    template< class SolverMonitor >
    void writeFemEoc ( const SolverMonitor &monitor, std::size_t gridSize, double runTime, std::stringstream &eocInfo )
    {
      // generate EOC information
      FemEoc::write( monitor.gridwidth_, gridSize, runTime, monitor.timeSteps_,
                     // double pairs
                     {
                       { std::string( "avg dt" ), monitor.avgTimeStep_ },
                       { std::string( "min dt" ), monitor.minTimeStep_ },
                       { std::string( "max dt" ), monitor.maxTimeStep_ }
                     },
                     // int pairs
                     {
                       { std::string( "Linear Iterations" ), monitor.totalLinearSolverIterations_ },
                       { std::string( "Newton Iterations" ), monitor.totalNewtonIterations_ }
                     }, eocInfo );
    }



    // eoc loop
    template< class Algorithm, template< class, class > class Writer = DataWriter >
    void simulate ( Algorithm &algorithm )
    {

      // initialize FemEOC writer
      FemEoc::clear();
      FemEoc::initialize( Parameter::getValue< std::string >( "eoc.filename", std::string( "eoc" )), algorithm.description() );

      // get number of eoc steps
      const int eocSteps = Parameter::getValue< int >( "eoc.steps", 1 );

      // data output for eoc loop runs
      typedef typename Algorithm::IOTupleType IOTupleType;
      typedef typename Algorithm::GridType GridType;
      typedef Writer< GridType, IOTupleType > DataOutputType;

      IOTupleType ioTuple = algorithm.dataTuple();
      DataOutputType dataOutput( algorithm.grid(), ioTuple );

      const unsigned int femTimerId = FemTimer::addTo( "timestep" );
      for( int eocloop = 0; eocloop < eocSteps; ++eocloop )
      {
        // start fem timer (enable with -DFEMTIMER)
        FemTimer::start( femTimerId );

        // additional timer
        Dune::Timer timer;

        // call algorithm and return solver statistics and some info
        algorithm.solve( eocloop );
        
        // get run time
        const double runTime = timer.elapsed();

        // also get times for FemTimer if enabled
        FemTimer::stop( femTimerId );
        FemTimer::printFile( "./timer.out" );
        FemTimer::reset( femTimerId );

        // finalize the algorithm
        algorithm.finalize( eocloop );

        // only do this if we have more than 1 eoc step
        //if( eocSteps > 1 )
        {
          std::stringstream eocInfo;

          auto monitor = algorithm.monitor();
          writeFemEoc( monitor, monitor.numberOfElements_, runTime, eocInfo );

          // in verbose mode write EOC info to std::cout
          if( Parameter::verbose() )
            std::cout << std::endl << "EOC info: " << std::endl << eocInfo.str() << std::endl;

          // write eoc step
          dataOutput.writeData( eocloop );

          // Refine the grid for the next EOC Step. If the scheme uses adaptation,
          // the refinement level needs to be set in the algorithms' initialize method.
          if( eocSteps > 1 )
            algorithm.globalRefine();
        }

      } /***** END of EOC Loop *****/

      // FemTimer cleanup
      FemTimer::removeAll();
    }

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_MISC_SIMULATE_HH
