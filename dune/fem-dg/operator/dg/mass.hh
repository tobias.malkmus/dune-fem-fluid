#ifndef DUNE_FEM_OPERATOR_COMMON_MASS_HH
#define DUNE_FEM_OPERATOR_COMMON_MASS_HH

#include <dune/fem/function/localfunction/temporary.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>


namespace Dune
{

  namespace Fem
  {

    // MassOperator
    // ------------

    template< class DiscreteFunction >
    class MassOperator
      : public Operator< DiscreteFunction, DiscreteFunction >
    {
      typedef MassOperator< DiscreteFunction > ThisType;
      typedef Operator< DiscreteFunction, DiscreteFunction > BaseType;

    public:
      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;

      typedef typename DomainFunctionType::DiscreteFunctionSpaceType DomainDiscreteFunctionSpaceType;
      typedef typename RangeFunctionType::DiscreteFunctionSpaceType RangeDiscreteFunctionSpaceType;

    protected:

      typedef typename DomainDiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DomainDiscreteFunctionSpaceType::EntityType EntityType;
      typedef typename EntityType::Geometry GeometryType;
      typedef CachingQuadrature< GridPartType, 0 > QuadratureType;

    public:
      MassOperator () {}

      void operator() ( const DomainFunctionType &arg, RangeFunctionType &dest ) const
      {
        this->operator()( arg, dest, [] ( typename DomainFunctionType::RangeType & ) {} );
      }

      template< class Mass >
      void operator() ( const DomainFunctionType &arg, RangeFunctionType &dest, Mass mass ) const
      {
        int quadOrder = Parameter::getValue< int >( "quad.order", -1 );
        quadOrder = ( quadOrder > -1 ) ? quadOrder : dest.space().order() * 2;

        dest.clear();
        TemporaryLocalFunction< DomainDiscreteFunctionSpaceType > local( dest.space() );
        typename DomainFunctionType::RangeType vu;

        for( const EntityType &entity : elements( arg.space().gridPart() ) )
        {
          GeometryType geometry = entity.geometry();
          auto argLocal = arg.localFunction( entity );
          local.init( entity );
          local.clear();

          QuadratureType quadrature( entity, quadOrder );
          for( auto point : quadrature )
          {
            double weight = point.weight() * geometry.integrationElement( point.position() );
            argLocal.evaluate( point, vu );
            mass( vu );
            vu *= weight;
            local.axpy( point, vu );
          }

          dest.addLocalDofs( entity, local.localDofVector() );
        }
        dest.communicate();
      }
    };

  } // namespace Fem

} //  namespace Dune

#endif //#ifndef DUNE_FEM_OPERATOR_COMMON_MASS_HH
