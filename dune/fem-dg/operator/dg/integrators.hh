#ifndef DUNE_FEM_OPERATOR_DG_INTEGRATORS_HH
#define DUNE_FEM_OPERATOR_DG_INTEGRATORS_HH

#include <type_traits>
#include <vector>

#include <dune/fem-dg/operator/dg/integratorsbase.hh>


namespace Dune
{

  namespace Fem
  {

    // FunctionIntegrator
    // ------------------

    template< class DiscreteFunction, class DiscreteModel, bool needsLiftings >
    class FunctionIntegrator
      : public std::conditional< needsLiftings,
                                 LiftedFunctionIntegrator< DiscreteFunction, DiscreteModel >,
                                 FunctionIntegratorBase< DiscreteFunction, DiscreteModel > >::type
    {
      typedef FunctionIntegrator< DiscreteFunction, DiscreteModel, needsLiftings > ThisType;
      typedef typename std::conditional< needsLiftings,
                                         LiftedFunctionIntegrator< DiscreteFunction, DiscreteModel >,
                                         FunctionIntegratorBase< DiscreteFunction, DiscreteModel > >::type BaseType;

    public:
      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteModelType DiscreteModelType;
      typedef typename BaseType::EntityType EntityType;
      typedef typename BaseType::GridPartType GridPartType;
      typedef typename BaseType::IntersectionType IntersectionType;

      typedef CachingQuadrature< GridPartType, 0 > QuadratureType;
      typedef CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

      using BaseType::space;

      FunctionIntegrator ( const DiscreteFunctionSpaceType &space, const DiscreteModelType &discreteModel )
        : BaseType( space, discreteModel ),
          quadOrder_( Parameter::getValue< int >( "quad.order", -1 ) )
      {
        quadOrder_ = ( quadOrder_ > -1 ) ? quadOrder_ : space.order() *2 + 2;
      }

      void volumenIntegral ( const EntityType &entity ) const
      {
        QuadratureType quad( entity, quadOrder_ );
        BaseType::volumenIntegral( entity, quad );
      }

      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity, const EntityType &outside ) const
      {
        FaceQuadratureType quadInside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::INSIDE );
        FaceQuadratureType quadOutside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::OUTSIDE );
        BaseType::surfaceIntegral( intersection, entity, outside, quadInside, quadOutside );
      }

      void boundaryIntegral ( const IntersectionType &intersection, const EntityType &entity ) const
      {
        FaceQuadratureType quadInside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::INSIDE );
        BaseType::boundaryIntegral( intersection, entity, quadInside );
      }

    protected:
      int quadOrder_;
    };



    // JacobianIntegrator
    // ------------------

    template< class JacobianOperator, class DiscreteModel, bool needsLiftings >
    class JacobianIntegrator
      : public std::conditional< needsLiftings,
                                 LiftedJacobianIntegrator< JacobianOperator, DiscreteModel >,
                                 JacobianIntegratorBase< JacobianOperator, DiscreteModel > >::type
    {
      typedef JacobianIntegrator< JacobianOperator, DiscreteModel, needsLiftings > ThisType;
      typedef typename std::conditional< needsLiftings,
                                         LiftedJacobianIntegrator< JacobianOperator, DiscreteModel >,
                                         JacobianIntegratorBase< JacobianOperator, DiscreteModel > >::type BaseType;

    public:
      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteModelType DiscreteModelType;
      typedef typename BaseType::EntityType EntityType;
      typedef typename BaseType::GridPartType GridPartType;
      typedef typename BaseType::IntersectionType IntersectionType;

      typedef CachingQuadrature< GridPartType, 0 > QuadratureType;
      typedef CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

      using BaseType::space;

      JacobianIntegrator ( const DiscreteFunctionSpaceType &space, const DiscreteModelType &discreteModel )
        : BaseType( space, discreteModel ),
          quadOrder_( Parameter::getValue< int >( "quad.order", -1 ) )
      {
        quadOrder_ = ( quadOrder_ > -1 ) ? quadOrder_ : space.order() *2 + 2;
      }

      void volumenIntegral ( const EntityType &entity ) const
      {
        QuadratureType quad( entity, quadOrder_ );
        BaseType::volumenIntegral( entity, quad );
      }

      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity, const EntityType &outside ) const
      {
        FaceQuadratureType quadInside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::INSIDE );
        FaceQuadratureType quadOutside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::OUTSIDE );
        BaseType::surfaceIntegral( intersection, entity, outside, quadInside, quadOutside );
      }

      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity ) const
      {
        FaceQuadratureType quadInside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::INSIDE );
        BaseType::surfaceIntegral( intersection, entity, quadInside );
      }

      void boundaryIntegral ( const IntersectionType &intersection, const EntityType &entity ) const
      {
        FaceQuadratureType quadInside( space().gridPart(), intersection, quadOrder_ + 1, FaceQuadratureType::INSIDE );
        BaseType::boundaryIntegral( intersection, entity, quadInside );
      }

    protected:
      int quadOrder_;
    };

  } // namespace Fem

} // namespace  Dune

#endif // #ifndef DUNE_FEM_OPERATOR_DG_INTEGRATORS_HH
