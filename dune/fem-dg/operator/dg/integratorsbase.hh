#ifndef DUNE_FEM_OPERATOR_DG_INTEGRATORSBASE_HH
#define DUNE_FEM_OPERATOR_DG_INTEGRATORSBASE_HH

#include <vector>

#include <dune/common/iteratorrange.hh>
#include <dune/fem/function/localfunction/temporary.hh>
#include <dune/fem/function/localfunction/temporaryset.hh>
#include <dune/fem/misc/fmatrixconverter.hh>
#include <dune/fem/operator/common/temporarylocalmatrix.hh>
#include <dune/fem/pairrange.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/storage/dynamicarray.hh>


namespace Dune
{

  namespace Fem
  {

    // Function Integrator Base Class
    // ------------------------------

    template< class DiscreteFunction, class DiscreteModel >
    class FunctionIntegratorBase
    {
      typedef FunctionIntegratorBase< DiscreteFunction, DiscreteModel > ThisType;

    public:
      typedef DiscreteFunction DiscreteFunctionType;
      typedef DiscreteModel DiscreteModelType;

      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename FunctionSpaceType::DomainType DomainType;
      typedef typename FunctionSpaceType::RangeType RangeType;
      typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;

    protected:
      typedef TemporaryLocalFunction< DiscreteFunctionSpaceType > TemporaryLocalFunctionType;

      static const bool isEvolutionProblem = DiscreteModelType::isEvolutionProblem;

    public:
      FunctionIntegratorBase ( const DiscreteFunctionSpaceType &space, const DiscreteModelType &discreteModel )
        : space_( space ),
          discreteModel_( discreteModel ),
          localFunction_( space ),
          values_( 20 ),
          fluxes_( 20 ),
          lambda_( 1.0 ),
          lambdaInv_( 0.0 )
      {
        values_.setMemoryFactor( 1.1 );
        fluxes_.setMemoryFactor( 1.1 );
      }

      void setLambda ( const double lambda ) const
      {
        if( lambda == 0.0 )
        {
          lambda_ = 1.0;
          lambdaInv_ = 0.0;
        }
        else
        {
          lambda_ = lambda;
          lambdaInv_ = 1. / lambda;
        }
      }

      void prepare ( const DiscreteFunctionType &arg, DiscreteFunctionType &dest ) const
      {
        discreteModel().prepare( arg, dest );
        dest_ = &dest;
      }

      void finalize ( DiscreteFunctionType &dest ) const
      {
        discreteModel().finalize( dest );
        dest_ = nullptr;
      }

      void init ( const EntityType &entity ) const
      {
        discreteModel().init( entity );

        // initialize and clear local function
        localFunction_.init( entity );
        localFunction_.clear();
      }

      template< class FaceQuadIn, class FaceQuadOut >
      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity, const EntityType &neighbor,
                             const FaceQuadIn &faceQuadInside, const FaceQuadOut &faceQuadOutside ) const
      {
        discreteModel().initIntersection( intersection, entity, neighbor, faceQuadInside, faceQuadOutside );

        const std::size_t numQuadraturePoints = faceQuadInside.nop();
        values_.resize( numQuadraturePoints );
        fluxes_.resize( numQuadraturePoints );

        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          RangeType &value = values_[ pt ];
          JacobianRangeType &flux = fluxes_[ pt ];

          const double weight = faceQuadInside.weight( pt );
          // integration element is contained in the normal
          DomainType normal
            = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

          // call numerical flux from the in and out side
          discreteModel().numericalFlux( intersection, entity, neighbor,
                                         faceQuadInside, faceQuadOutside, pt, normal, value, flux );

          // multiply weights
          value *= weight;
          flux *= weight;
        }

        localFunction_.axpyQuadrature( faceQuadInside, values_, fluxes_ );
      }


      template< class FaceQuadrature >
      void boundaryIntegral ( const IntersectionType &intersection, const EntityType &entity,
                              const FaceQuadrature &faceQuadInside ) const
      {
        discreteModel().initIntersection( intersection, entity, faceQuadInside );

        const std::size_t numQuadraturePoints = faceQuadInside.nop();
        values_.resize( numQuadraturePoints );
        fluxes_.resize( numQuadraturePoints );

        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          RangeType &value = values_[ pt ];
          JacobianRangeType &flux = fluxes_[ pt ];

          const double weight = faceQuadInside.weight( pt );
          // integration element is contained in the normal
          DomainType normal
            = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

          // call numerical flux from the in and out side
          discreteModel().boundaryFlux( intersection, entity, faceQuadInside, pt, normal, value, flux );

          // multiply weights
          value *= weight;
          flux *= weight;
        }

        localFunction_.axpyQuadrature( faceQuadInside, values_, fluxes_ );
      }

      template< class Quadrature >
      void volumenIntegral ( const EntityType &entity, const Quadrature &quadrature ) const
      {
        const Geometry geometry = entity.geometry();
        discreteModel().initEntity( entity, quadrature, true );

        const std::size_t numQuadraturePoints = quadrature.nop();
        values_.resize( numQuadraturePoints );
        fluxes_.resize( numQuadraturePoints );

        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          RangeType &source = values_[ pt ];
          JacobianRangeType &fluxes = fluxes_[ pt ];

          // call discrete model to compute fluxes and sources
          discreteModel().fluxAndSource( entity, quadrature, pt, source, fluxes );

          // add mass term if problem is an evolution problem
          if( isEvolutionProblem )
            discreteModel().addScaledMassTerm( entity, quadrature, pt, lambdaInv_, source );

          const double weight = quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );
          // multiply weights
          source *= weight;
          fluxes *= -weight;
        }

        localFunction_.axpyQuadrature( quadrature, values_, fluxes_ );
      }

      void finalizeEntity ( const EntityType &entity ) const
      {
        updateLocalFunction( entity, localFunction_ );
        discreteModel().finalizeEntity( entity );
      }

    protected:
      void updateLocalFunction ( const EntityType &entity, const TemporaryLocalFunctionType &local ) const
      {
        if( isEvolutionProblem )
          dest_->addScaledLocalDofs( entity, lambda_, local.localDofVector() );
        else
          dest_->addLocalDofs( entity, local.localDofVector() );
      }

      const DiscreteModelType &discreteModel () const { return discreteModel_; }
      const DiscreteFunctionSpaceType &space () const { return space_; }

      const DiscreteFunctionSpaceType &space_;
      const DiscreteModelType &discreteModel_;
      mutable DiscreteFunctionType *dest_;


      mutable TemporaryLocalFunctionType localFunction_;
      mutable DynamicArray< RangeType > values_;
      mutable DynamicArray< JacobianRangeType > fluxes_;
      mutable double lambda_;
      mutable double lambdaInv_;
    };



    // Jacobian Integrator Base Class
    // ------------------------------

    template< class JacobianOperator, class DiscreteModel >
    class JacobianIntegratorBase
    {
      typedef JacobianIntegratorBase< JacobianOperator, DiscreteModel > ThisType;

    public:
      typedef JacobianOperator JacobianOperatorType;
      typedef DiscreteModel DiscreteModelType;

      typedef typename JacobianOperatorType::DomainSpaceType DiscreteFunctionSpaceType;
      typedef typename JacobianOperatorType::DomainFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

      typedef typename FunctionSpaceType::DomainType DomainType;
      typedef typename FunctionSpaceType::RangeType RangeType;
      typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      typedef typename JacobianOperatorType::LocalMatrixType LocalMatrixType;

    protected:
      static const bool isEvolutionProblem = DiscreteModelType::isEvolutionProblem;

      static const int blockSize = DiscreteFunctionSpaceType::localBlockSize;

      typedef TemporaryLocalMatrix< DiscreteFunctionSpaceType, DiscreteFunctionSpaceType > TemporaryLocalMatrixType;

    public:
      JacobianIntegratorBase ( const DiscreteFunctionSpaceType &space, const DiscreteModelType &discreteModel )
        : space_( space ),
          discreteModel_( discreteModel ),
          matrixStorage_( 2, TemporaryLocalMatrixType( space, space ) ),
          lambda_( 1.0 ),
          lambdaInv_( 0.0 )
      {
        const int maxNumDofs = space_.blockMapper().maxNumDofs() * blockSize;
        phi_.reserve( maxNumDofs );
        phiNb_.reserve( maxNumDofs );
        dphi_.reserve( maxNumDofs );
        dphiNb_.reserve( maxNumDofs );
      }

      void setLambda ( const double lambda ) const
      {
        if( lambda == 0.0 )
        {
          lambda_ = 1.0;
          lambdaInv_ = 1.0;
        }
        else
        {
          lambda_ = lambda;
          lambdaInv_ = 1. / lambda;
        }
      }

      void prepare ( const DiscreteFunctionType &arg, JacobianOperatorType &jac ) const
      {
        discreteModel().prepare( arg, jac );
        jac_ = &jac;
      }

      void finalize ( JacobianOperatorType &jac ) const
      {
        discreteModel().finalize( jac );
        jac_ = nullptr;
      }

      void init ( const EntityType &entity ) const
      {
        discreteModel().init( entity );
        matrixStorage_[ 0 ].init( entity, entity );
        matrixStorage_[ 0 ].clear();
      }


      template< class FaceQuadIn, class FaceQuadOut >
      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity,
                             const EntityType &neighbor, const FaceQuadIn &faceQuadInside,
                             const FaceQuadOut &faceQuadOutside ) const
      {
        discreteModel().initIntersection( intersection, entity, neighbor, faceQuadInside, faceQuadOutside );

        TemporaryLocalMatrixType &diag = matrixStorage_[ 0 ];
        TemporaryLocalMatrixType &offDiag = matrixStorage_[ 1 ];

        offDiag.init( neighbor, entity );
        offDiag.clear();

        const BasisFunctionSetType &basisSet = diag.domainBasisFunctionSet();
        const BasisFunctionSetType &basisSetNb = offDiag.domainBasisFunctionSet();

        const std::size_t numDofs = diag.rows();
        assert( numDofs == basisSet.size() );
        resize( numDofs );

        const std::size_t numQuadraturePoints = faceQuadInside.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = faceQuadInside.weight( pt );
          // integration element is contained in the normal
          DomainType normal
            = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

          basisSet.evaluateAll( faceQuadInside[ pt ], phi_ );
          basisSet.jacobianAll( faceQuadInside[ pt ], dphi_ );

          basisSetNb.evaluateAll( faceQuadOutside[ pt ], phiNb_ );
          basisSetNb.jacobianAll( faceQuadOutside[ pt ], dphiNb_ );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            RangeType value( 0 ), valueNb( 0 );
            JacobianRangeType flux( 0 ), fluxNb( 0 );

            // call discrete model to compute fluxes and sources
            discreteModel().linearNumericalFlux( intersection, entity, neighbor, faceQuadInside, faceQuadOutside,
                                                 pt, normal, phi_[ i ], phiNb_[ i ], dphi_[ i ], dphiNb_[ i ],
                                                 value, valueNb, flux, fluxNb );

            for( auto &e : vals_ )
              e = 0;
            basisSet.axpy( faceQuadInside[ pt ], value, flux, vals_ );

            // multiply weights
            for( std::size_t j = 0; j < numDofs; ++j )
              diag.add( j, i, vals_[ j ] * weight );

            for( auto &e : vals_ )
              e = 0;
            basisSet.axpy( faceQuadInside[ pt ], valueNb, fluxNb, vals_ );

            // multiply weights
            for( std::size_t j = 0; j < numDofs; ++j )
              offDiag.add( j, i, vals_[ j ] * weight );
          }
        }
        updateLocalMatrix( offDiag );
      }

      template< class FaceQuadrature >
      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity,
                             const FaceQuadrature &faceQuadInside ) const
      {
        discreteModel().initIntersection( intersection, entity, entity, faceQuadInside, faceQuadInside );

        TemporaryLocalMatrixType &diag = matrixStorage_[ 0 ];

        const BasisFunctionSetType &basisSet = diag.domainBasisFunctionSet();

        const std::size_t numDofs = diag.rows();
        assert( numDofs == basisSet.size() );
        resize( numDofs );

        const std::size_t numQuadraturePoints = faceQuadInside.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = faceQuadInside.weight( pt );
          // integration element is contained in the normal
          DomainType normal
            = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

          basisSet.evaluateAll( faceQuadInside[ pt ], phi_ );
          basisSet.jacobianAll( faceQuadInside[ pt ], dphi_ );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            RangeType value( 0 ), valueNb( 0 );
            JacobianRangeType flux( 0 ), fluxNb( 0 );

            // call discrete model to compute fluxes and sources
            discreteModel().linearNumericalFlux( intersection, entity, entity, faceQuadInside, faceQuadInside,
                                                 pt, normal, phi_[ i ], phi_[ i ], dphi_[ i ], dphi_[ i ],
                                                 value, valueNb, flux, fluxNb );

            for( auto &e : vals_ )
              e = 0;
            basisSet.axpy( faceQuadInside[ pt ], value, flux, vals_ );

            // multiply weights
            for( std::size_t j = 0; j < numDofs; ++j )
              diag.add( j, i, vals_[ j ] * weight );
          }
        }
      }

      template< class FaceQuadrature >
      void boundaryIntegral ( const IntersectionType &intersection, const EntityType &entity,
                              const FaceQuadrature &faceQuadInside ) const
      {
        discreteModel().initIntersection( intersection, entity, faceQuadInside );

        TemporaryLocalMatrixType &diag = matrixStorage_[ 0 ];
        const BasisFunctionSetType &basisSet = diag.domainBasisFunctionSet();

        const std::size_t numDofs = diag.rows();
        resize( numDofs );
        const std::size_t numQuadraturePoints = faceQuadInside.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = faceQuadInside.weight( pt );
          // integration element is contained in the normal
          DomainType normal
            = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

          basisSet.evaluateAll( faceQuadInside[ pt ], phi_ );
          basisSet.jacobianAll( faceQuadInside[ pt ], dphi_ );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            RangeType value( 0 );
            JacobianRangeType flux( 0 );

            // call discrete model to compute fluxes and sources
            discreteModel().linearBoundaryFlux( intersection, entity, faceQuadInside,
                                                pt, normal, phi_[ i ], dphi_[ i ], value, flux );

            for( auto &e : vals_ )
              e = 0;
            basisSet.axpy( faceQuadInside[ pt ], value, flux, vals_ );

            // multiply weights
            for( std::size_t j = 0; j < numDofs; ++j )
              diag.add( j, i, vals_[ j ] * weight );
          }
        }
      }

      template< class Quadrature >
      void volumenIntegral ( const EntityType &entity, const Quadrature &quadrature ) const
      {
        const Geometry geometry = entity.geometry();
        discreteModel().initEntity( entity, quadrature, false );

        TemporaryLocalMatrixType &diag = matrixStorage_[ 0 ];
        const BasisFunctionSetType &basisSet = diag.domainBasisFunctionSet();

        const std::size_t numDofs = diag.rows();
        resize( numDofs );

        const std::size_t numQuadraturePoints = quadrature.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );

          basisSet.evaluateAll( quadrature[ pt ], phi_ );
          basisSet.jacobianAll( quadrature[ pt ], dphi_ );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            RangeType source( 0 );
            JacobianRangeType flux( 0 );

            // call discrete model to compute fluxes and sources
            discreteModel().linearFluxAndSource( entity, quadrature, pt, phi_[ i ], dphi_[ i ], source, flux );
            flux *= -1.;

            // add linearized mass term
            if( isEvolutionProblem )
              discreteModel().addScaledMassTerm( entity, quadrature, pt, lambdaInv_, phi_[ i ], source );

            for( auto &e : vals_ )
              e = 0;
            basisSet.axpy( quadrature[ pt ], source, flux, vals_ );

            // multiply weights
            for( std::size_t j = 0; j < numDofs; ++j )
              diag.add( j, i, vals_[ j ] * weight );
          }
        }
      }

      void finalizeEntity ( const EntityType &entity ) const
      {
        updateLocalMatrix( matrixStorage_[ 0 ] );
      }

    protected:
      void updateLocalMatrix ( const TemporaryLocalMatrixType &local ) const
      {
        if( isEvolutionProblem )
          jac_->addScaledLocalMatrix( local.domainEntity(), local.rangeEntity(), local, lambda_ );
        else
          jac_->addLocalMatrix( local.domainEntity(), local.rangeEntity(), local );
      }

      void resize ( const int newsize ) const
      {
        phi_.resize( newsize );
        phiNb_.resize( newsize );
        dphi_.resize( newsize );
        dphiNb_.resize( newsize );
        vals_.resize( newsize );
      }

      const DiscreteModelType &discreteModel () const { return discreteModel_; }
      const DiscreteFunctionSpaceType &space () const { return space_; }

    private:
      const DiscreteFunctionSpaceType &space_;
      const DiscreteModelType &discreteModel_;

      mutable JacobianOperatorType *jac_;

    protected:
      mutable std::vector< TemporaryLocalMatrixType > matrixStorage_;

      mutable std::vector< RangeType > phi_, phiNb_;
      mutable std::vector< JacobianRangeType > dphi_, dphiNb_;

      mutable std::vector< double > vals_;

    private:
      mutable double lambda_;
      mutable double lambdaInv_;
    };



    /////////////////////////////////////////////////
    //      Integrators with liftings              //
    /////////////////////////////////////////////////


    // Function Integrator for methods with liftings
    // ---------------------------------------------

    template< class DiscreteFunction, class DiscreteModel >
    class LiftedFunctionIntegrator
      : public FunctionIntegratorBase< DiscreteFunction, DiscreteModel >
    {
      typedef LiftedFunctionIntegrator< DiscreteFunction, DiscreteModel > ThisType;
      typedef FunctionIntegratorBase< DiscreteFunction, DiscreteModel > BaseType;

      static_assert( DiscreteModel::needsLiftings, "This integrator only works with discretemodels which needs lifiting terms" );

    public:
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
      typedef typename BaseType::DiscreteModelType DiscreteModelType;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

      typedef typename FunctionSpaceType::DomainType DomainType;
      typedef typename FunctionSpaceType::RangeType RangeType;
      typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename BaseType::EntityType EntityType;
      typedef typename BaseType::Geometry Geometry;

      typedef typename BaseType::IntersectionType IntersectionType;

    protected:
      typedef typename BaseType::TemporaryLocalFunctionType TemporaryLocalFunctionType;

      typedef typename DiscreteModelType::LiftedBasisFunctionSetType LiftedBasisFunctionSetType;
      typedef typename DiscreteModelType::LiftingRangeType LiftingRangeType;
      typedef typename DiscreteModelType::OnEntityLiftedSetType OnEntityLiftedSetType;

      typedef typename LiftedBasisFunctionSetType::const_iterator LiftedSetIteratorType;

      using BaseType::updateLocalFunction;
      using BaseType::volumenIntegral;
      using BaseType::discreteModel;
      using BaseType::space;

    public:
      LiftedFunctionIntegrator ( const DiscreteFunctionSpaceType &space, const DiscreteModelType &discreteModel )
        : BaseType( space, discreteModel ),
          localOutsideFunction_( space )
      {}

      template< class FaceQuadIn, class FaceQuadOut >
      void surfaceIntegral ( const IntersectionType &intersection, const EntityType &entity,
                             const EntityType &neighbor, const FaceQuadIn &faceQuadInside,
                             const FaceQuadOut &faceQuadOutside ) const
      {
        BaseType::surfaceIntegral( intersection, entity, neighbor, faceQuadInside, faceQuadOutside );
      }


      template< class Quadrature >
      void volumenIntegral ( const EntityType &entity, const Quadrature &quadrature ) const
      {
        BaseType::volumenIntegral( entity, quadrature );
        Geometry geometry = entity.geometry();

        const int numDofs = space().basisFunctionSet( entity ).size();
        const LiftedBasisFunctionSetType &liftedBasisFunctionSet = discreteModel().liftedBasisFunctionSet();

        TemporaryLocalFunctionSet< typename LiftedBasisFunctionSetType::DiscreteFunctionSpaceType >
        insideLiftings( liftedBasisFunctionSet.space(), numDofs, entity );

        LiftedSetIteratorType it = getInsideLifiting( insideLiftings );

        // inside lifting contributions
        computeLifiting( entity, quadrature, geometry, insideLiftings );

        // outside lifting contributions
        for( const OnEntityLiftedSetType &liftedSet : iteratorRange( it, liftedBasisFunctionSet.end() ) )
          computeLifiting( entity, quadrature, geometry, liftedSet );
      }

    protected:

      template< class LocalFunctionSet >
      LiftedSetIteratorType getInsideLifiting ( LocalFunctionSet &lfSet ) const
      {
        lfSet.clear();
        LiftedSetIteratorType it = discreteModel().liftedBasisFunctionSet().begin();
        const LiftedSetIteratorType end = discreteModel().liftedBasisFunctionSet().end();

        for(; it != end; ++it )
        {
          OnEntityLiftedSetType liftedSetInside( *it );
          const int insideIndex = liftedSetInside.index();
          const double factorInside = discreteModel().liftingFactor( insideIndex );

          if( !discreteModel().isInsideLifting( insideIndex ) )
            return it;

          if( factorInside == 0.0 )
            continue;

          lfSet.axpy( factorInside, liftedSetInside.set() );
        }
        return it;
      }


      // computeLifiting for inside lifiting

      template< class Quadrature, class LiftingSet >
      void computeLifiting ( const EntityType &entity, const Quadrature &quadrature,
                             const Geometry &geometry, const LiftingSet &liftedSet ) const
      {
        const std::size_t setSize = liftedSet.size();
        bLifts_.resize( setSize );

        const EntityType &other = liftedSet.entity();
        localOutsideFunction_.init( other );
        localOutsideFunction_.clear();

        JacobianRangeType fluxes;
        LiftingRangeType liftingFlux;

        const std::size_t numQuadraturePoints = quadrature.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );

          // call discrete model to compute fluxes and sources
          discreteModel().diffusion( entity, quadrature, pt, fluxes );
          discreteModel().diffusion( entity, quadrature, pt, liftingFlux );

          FieldMatrixConverter< LiftingRangeType, JacobianRangeType > converter( liftingFlux );
          converter += fluxes;
          liftingFlux *= weight;

          evaluateAll( liftedSet, quadrature[ pt ], bLifts_ );

          for( std::size_t k = 0; k < setSize; ++k )
            localOutsideFunction_[ k ] -= liftingFlux * bLifts_[ k ];
        }

        updateLocalFunction( other, localOutsideFunction_ );
      }


      // computeLifiting for outside liftings

      template< class Quadrature >
      void computeLifiting ( const EntityType &entity, const Quadrature &quadrature,
                             const Geometry &geometry, const OnEntityLiftedSetType &liftedSet ) const
      {
        const double liftingFactor = discreteModel().liftingFactor( liftedSet.index() );

        const std::size_t setSize = liftedSet.size();
        bLifts_.resize( setSize );

        const EntityType &other = liftedSet.entity();
        localOutsideFunction_.init( other );
        localOutsideFunction_.clear();

        JacobianRangeType fluxes;
        LiftingRangeType liftingFlux;

        const std::size_t numQuadraturePoints = quadrature.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );

          // call discrete model to compute fluxes and sources
          discreteModel().diffusion( entity, quadrature, pt, fluxes );
          discreteModel().diffusion( entity, quadrature, pt, liftingFlux );

          FieldMatrixConverter< LiftingRangeType, JacobianRangeType > converter( liftingFlux );
          converter += fluxes;
          liftingFlux *= weight;

          liftedSet.evaluateAllScaled( quadrature[ pt ], bLifts_, liftingFactor );

          for( std::size_t k = 0; k < setSize; ++k )
            localOutsideFunction_[ k ] -= liftingFlux * bLifts_[ k ];
        }

        updateLocalFunction( other, localOutsideFunction_ );
      }

    private:
      mutable std::vector< LiftingRangeType > bLifts_, bLiftsNb_;
      mutable TemporaryLocalFunctionType localOutsideFunction_;
    };


    // Jacobian Integrator for methods with liftings
    // ---------------------------------------------

    template< class JacobianOperator, class DiscreteModel >
    class LiftedJacobianIntegrator
      : public JacobianIntegratorBase< JacobianOperator, DiscreteModel >
    {
      typedef LiftedJacobianIntegrator< JacobianOperator, DiscreteModel > ThisType;
      typedef JacobianIntegratorBase< JacobianOperator, DiscreteModel > BaseType;

      static_assert( DiscreteModel::needsLiftings, "This integrator only works with discretemodels which needs lifiting terms" );

    public:

      typedef JacobianOperator JacobianOperatorType;
      typedef DiscreteModel DiscreteModelType;

      typedef typename JacobianOperatorType::DomainSpaceType DiscreteFunctionSpaceType;
      typedef typename JacobianOperatorType::DomainFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

      typedef typename FunctionSpaceType::DomainType DomainType;
      typedef typename FunctionSpaceType::RangeType RangeType;
      typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename BaseType::EntityType EntityType;
      typedef typename BaseType::Geometry Geometry;

      typedef typename BaseType::IntersectionType IntersectionType;

      typedef typename JacobianOperatorType::LocalMatrixType LocalMatrixType;

    protected:
      static const int blockSize = DiscreteFunctionSpaceType::localBlockSize;
      static const int dimRange = FunctionSpaceType::dimRange;
      static const int dimDomain = FunctionSpaceType::dimDomain;

      typedef TemporaryLocalMatrix< DiscreteFunctionSpaceType, DiscreteFunctionSpaceType > TemporaryLocalMatrixType;

      typedef typename DiscreteModelType::LiftingDiscreteFunctionSpaceType LiftingDiscreteFunctionSpaceType;
      typedef typename DiscreteModelType::LiftingRangeType LiftingRangeType;
      typedef typename DiscreteModelType::LiftedBasisFunctionSetType LiftedBasisFunctionSetType;
      typedef typename DiscreteModelType::OnEntityLiftedSetType OnEntityLiftedSetType;

      typedef typename LiftedBasisFunctionSetType::const_iterator LiftedSetIteratorType;

      using BaseType::updateLocalMatrix;
      using BaseType::volumenIntegral;
      using BaseType::discreteModel;
      using BaseType::space;

      using BaseType::matrixStorage_;
      using BaseType::phi_;
      using BaseType::phiNb_;
      using BaseType::dphi_;
      using BaseType::dphiNb_;
      using BaseType::resize;

    public:
      LiftedJacobianIntegrator ( const DiscreteFunctionSpaceType &space, const DiscreteModelType &discreteModel )
        : BaseType( space, discreteModel ),
          otherLocalMatrix_( 3, TemporaryLocalMatrixType( space, space ) ),
          insideLiftings_( discreteModel.liftedBasisFunctionSet().space(),
                           space.blockMapper().maxNumDofs() * DiscreteFunctionSpaceType::localBlockSize )
      {}

      template< class Quadrature >
      void volumenIntegral ( const EntityType &entity, const Quadrature &quadrature ) const
      {
        BaseType::volumenIntegral( entity, quadrature );

        const Geometry geometry = entity.geometry();
        const BasisFunctionSetType &basisSet = space().basisFunctionSet( entity );

        LiftedSetIteratorType it = initInsideLifiting( entity, basisSet.size() );
        const LiftedSetIteratorType end = discreteModel().liftedBasisFunctionSet().end();

        // if entity has dirichlet type boundary intersections we have to apply the linearized boundary values
        if( discreteModel().hasBoundaryFunctor( entity ) )
        {
          auto boundaryFunctor = discreteModel().boundaryFunctor();

          // inside/inside terms
          computeInsideInsideLiftings( entity, quadrature, geometry, basisSet, insideLiftings_, boundaryFunctor );

          // inside/outside and outside/inside terms
          for( const OnEntityLiftedSetType &outsideLifting : iteratorRange( it, end ) )
            computeInsideOutsideLiftings( entity, quadrature, geometry, basisSet, insideLiftings_, outsideLifting, boundaryFunctor );
        }
        else
        {
          auto boundaryFunctor = [] ( auto quad, int pt, int i, const LiftingRangeType &lift ) -> LiftingRangeType { return lift; };

          // inside/inside terms
          computeInsideInsideLiftings( entity, quadrature, geometry, basisSet, insideLiftings_, boundaryFunctor );

          // inside/outside and outside/inside terms
          for( const OnEntityLiftedSetType &outsideLifting : iteratorRange( it, end ) )
            computeInsideOutsideLiftings( entity, quadrature, geometry, basisSet, insideLiftings_, outsideLifting, boundaryFunctor );
        }

        // outside/outside terms
        for( const auto &pair : pairRange( it, end ) )
          computeOutsideOutsideLifitings( entity, quadrature, geometry, pair );
      }

    protected:
      LiftedSetIteratorType initInsideLifiting ( const EntityType &entity, std::size_t size ) const
      {
        insideLiftings_.init( entity, size );
        insideLiftings_.clear();

        LiftedSetIteratorType it = discreteModel().liftedBasisFunctionSet().begin();
        const LiftedSetIteratorType end = discreteModel().liftedBasisFunctionSet().end();
        for(; it != end; ++it )
        {
          OnEntityLiftedSetType liftedSetInside( *it );
          const int insideIndex = liftedSetInside.index();
          const double factorInside = discreteModel().liftingFactor( insideIndex );

          if( !discreteModel().isInsideLifting( insideIndex ) )
            return it;

          if( factorInside == 0.0 )
            continue;

          insideLiftings_.axpy( factorInside, liftedSetInside.set() );
        }
        return it;
      }


      // inside Lifting contribution

      template< class Quadrature, class LiftedBasisSet, class BoundaryFunctor >
      void computeInsideInsideLiftings ( const EntityType &entity, const Quadrature &quadrature, const Geometry &geometry,
                                         const BasisFunctionSetType &basisSet, const LiftedBasisSet &liftedSet,
                                         const BoundaryFunctor &bFunctor ) const
      {
        TemporaryLocalMatrixType &diag = matrixStorage_[ 0 ];
        bLifts_.resize( liftedSet.size() );

        const std::size_t numDofs = diag.rows();
        resize( numDofs );

        assert( liftedSet.size() == numDofs );

        JacobianRangeType diffFlux, flux;
        LiftingRangeType liftingFlux;

        const std::size_t numQuadraturePoints = quadrature.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );

          basisSet.evaluateAll( quadrature[ pt ], phi_ );
          basisSet.jacobianAll( quadrature[ pt ], dphi_ );

          evaluateAll( liftedSet, quadrature[ pt ], bLifts_ );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            const RangeType &u = phi_[ i ];
            const JacobianRangeType &jac = dphi_[ i ];
            // apply linearized boundary treatment if existant
            LiftingRangeType lift = bFunctor( quadrature, pt, i, bLifts_[ i ] );

            // compute diffusions: A'( phi, dphi + R( phi ) ), A'( phi, R( phi ) )
            discreteModel().linearDiffusion( entity, quadrature, pt, u, jac, lift, diffFlux );
            discreteModel().linearDiffusion( entity, quadrature, pt, u, jac, lift, liftingFlux );

            FieldMatrixConverter< LiftingRangeType, JacobianRangeType > entityConverter( liftingFlux );
            entityConverter += diffFlux;

            for( std::size_t j = 0; j < numDofs; ++j )
            {
              RangeFieldType value = 0.0;

              // A'( phi, dphi +  R( phi ) ) * dphi_
              for( int r = 0; r < dimRange; ++r )
                value += diffFlux[ r ] * dphi_[ j ][ r ];

              // [ A'( phi, dphi + R( phi ) ) + A'( phi, R( phi ) ) ] * R( phi )
              value -= liftingFlux * bLifts_[ j ];

              diag.add( j, i, weight * value );
            }
          }
        }
      }

      template< class Quadrature, class LiftedBasisSet, class BoundaryFunctor >
      void computeInsideOutsideLiftings ( const EntityType &entity, const Quadrature &quadrature, const Geometry &geometry,
                                          const BasisFunctionSetType &basisSet, const LiftedBasisSet &liftedSet,
                                          const OnEntityLiftedSetType &liftedSetNb, const BoundaryFunctor &bFunctor ) const
      {
        const double liftingFactor = discreteModel().liftingFactor( liftedSetNb.index() );

        const EntityType &domainEntity = liftedSet.entity();
        const EntityType &rangeEntity = liftedSetNb.entity();

        TemporaryLocalMatrixType &neighborEntity = otherLocalMatrix_[ 0 ];
        neighborEntity.init( rangeEntity, domainEntity );
        neighborEntity.clear();

        TemporaryLocalMatrixType &entityNeighbor = otherLocalMatrix_[ 1 ];
        entityNeighbor.init( domainEntity, rangeEntity );
        entityNeighbor.clear();

        TemporaryLocalMatrixType &neighborNeighbor = otherLocalMatrix_[ 2 ];
        neighborNeighbor.init( rangeEntity, rangeEntity );
        neighborNeighbor.clear();

        bLifts_.resize( liftedSet.size() );
        bLiftsNb_.resize( liftedSetNb.size() );

        const std::size_t numDofs = neighborEntity.rows();
        resize( numDofs );
        JacobianRangeType null( 0 );

        assert( liftedSet.size() == numDofs );
        assert( liftedSetNb.size() == numDofs );

        JacobianRangeType entityFlux, neighborFlux;
        LiftingRangeType entityLiftingFlux, neighborLiftingFlux;

        const std::size_t numQuadraturePoints = quadrature.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );

          basisSet.evaluateAll( quadrature[ pt ], phi_ );
          basisSet.jacobianAll( quadrature[ pt ], dphi_ );

          evaluateAll( liftedSet, quadrature[ pt ], bLifts_ );
          liftedSetNb.evaluateAllScaled( quadrature[ pt ], bLiftsNb_, liftingFactor );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            const RangeType &u = phi_[ i ];
            const JacobianRangeType &jac = dphi_[ i ];
            const LiftingRangeType &liftNb = bLiftsNb_[ i ];

            // apply linearized boundary treatment if existant
            LiftingRangeType lift = bFunctor( quadrature, pt, i, bLifts_[ i ] );

            // compute diffusions: A'(phi, dphi_ + R( phi ) ), A'( phi, R( phi ) )
            discreteModel().linearDiffusion( entity, quadrature, pt, u, jac, lift, entityFlux );
            discreteModel().linearDiffusion( entity, quadrature, pt, u, jac, lift, entityLiftingFlux );
            FieldMatrixConverter< LiftingRangeType, JacobianRangeType > entityConverter( entityLiftingFlux );
            entityConverter += entityFlux;

            // compute diffusions: A'( 0, 0 + R( psi ) ), A'( 0, R( psi ) ) (! psi is neighbor basis set!)
            discreteModel().linearDiffusion( entity, quadrature, pt, RangeType( 0 ), null, liftNb, neighborFlux );
            discreteModel().linearDiffusion( entity, quadrature, pt, RangeType( 0 ), null, liftNb, neighborLiftingFlux );
            FieldMatrixConverter< LiftingRangeType, JacobianRangeType > neighborConverter( neighborLiftingFlux );
            neighborConverter += neighborFlux;

            for( std::size_t j = 0; j < numDofs; ++j )
            {
              RangeFieldType neighborEntityValue = 0.0;
              RangeFieldType entityNeighborValue = 0.0;
              RangeFieldType neighborNeighborValue = 0.0;

              // A'( psi, 0 + R( psi ) ) * dphi_
              for( int r = 0; r < dimRange; ++r )
                neighborEntityValue += neighborFlux[ r ] * dphi_[ j ][ r ];

              // [ A'( 0, 0 + R( psi ) ) + A'( 0, R( psi ) ) ] * R( phi )
              neighborEntityValue -= neighborLiftingFlux * bLifts_[ j ];
              // [ A'( phi, dphi + R( phi ) ) + A'( phi, R( phi ) ) ] * R( psi )
              entityNeighborValue -= entityLiftingFlux * bLiftsNb_[ j ];
              // [ A'( 0, 0 + R( psi ) ) + A'( 0, R( psi ) ) ] * R( psi )
              neighborNeighborValue -= neighborLiftingFlux * bLiftsNb_[ j ];

              neighborEntity.add( j, i, weight * neighborEntityValue );
              entityNeighbor.add( j, i, weight * entityNeighborValue );
              neighborNeighbor.add( j, i, weight * neighborNeighborValue );
            }
          }
        }

        updateLocalMatrix( entityNeighbor );
        updateLocalMatrix( neighborEntity );
        updateLocalMatrix( neighborNeighbor );
      }


      template< class Quadrature, class LiftingPair >
      void computeOutsideOutsideLifitings ( const EntityType &entity, const Quadrature &quadrature, const Geometry &geometry, const LiftingPair &liftingPair ) const
      {
        OnEntityLiftedSetType liftedSet( liftingPair.first );
        OnEntityLiftedSetType liftedSetNb( liftingPair.second );

        const double factorInside = discreteModel().liftingFactor( liftedSet.index() );
        const double factorOutside = discreteModel().liftingFactor( liftedSetNb.index() );

        // get involved entities
        const EntityType &domainEntity = liftedSet.entity();
        const EntityType &rangeEntity = liftedSetNb.entity();

        bLifts_.resize( liftedSet.size() );
        bLiftsNb_.resize( liftedSetNb.size() );

        TemporaryLocalMatrixType &neighborEntity = otherLocalMatrix_[ 0 ];
        neighborEntity.init( rangeEntity, domainEntity );
        neighborEntity.clear();

        TemporaryLocalMatrixType &entityNeighbor = otherLocalMatrix_[ 1 ];
        entityNeighbor.init( domainEntity, rangeEntity );
        entityNeighbor.clear();

        const JacobianRangeType null( 0 );
        JacobianRangeType entityFlux, neighborFlux;
        LiftingRangeType entityLiftingFlux, neighborLiftingFlux;

        const std::size_t numDofs = neighborEntity.rows();
        resize( numDofs );

        const std::size_t numQuadraturePoints = quadrature.nop();
        for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
        {
          const double weight = -1.0 * quadrature.weight( pt ) * geometry.integrationElement( quadrature.point( pt ) );

          liftedSet.evaluateAllScaled( quadrature[ pt ], bLifts_, factorInside );
          liftedSetNb.evaluateAllScaled( quadrature[ pt ], bLiftsNb_, factorOutside );

          for( std::size_t i = 0; i < numDofs; ++i )
          {
            const LiftingRangeType &lift = bLifts_[ i ];
            const LiftingRangeType &liftNb = bLiftsNb_[ i ];

            // compute diffusions: A'( 0, 0 + R( phi ) ), A'( 0, R( phi ) ) (! phi is neighbor basis set!)
            discreteModel().linearDiffusion( entity, quadrature, pt, RangeType( 0 ), null, lift, entityFlux );
            discreteModel().linearDiffusion( entity, quadrature, pt, RangeType( 0 ), null, lift, entityLiftingFlux );
            FieldMatrixConverter< LiftingRangeType, JacobianRangeType > entityConverter( entityLiftingFlux );
            entityConverter += entityFlux;

            // compute diffusions: A'( 0, 0 + R( psi ) ), A'( 0, R( psi ) ) (! psi is neighbor basis set!)
            discreteModel().linearDiffusion( entity, quadrature, pt, RangeType( 0 ), null, liftNb, neighborFlux );
            discreteModel().linearDiffusion( entity, quadrature, pt, RangeType( 0 ), null, liftNb, neighborLiftingFlux );
            FieldMatrixConverter< LiftingRangeType, JacobianRangeType > neighborConverter( neighborLiftingFlux );
            neighborConverter += neighborFlux;

            for( std::size_t j = 0; j < numDofs; ++j )
            {
              // [ A'( 0, 0 + R( phi ) ) + A'( 0, R( phi ) ) ] * R( psi )
              RangeFieldType entityNeighborValue = entityLiftingFlux * bLiftsNb_[ j ];
              // [ A'( 0, 0 + R( psi ) ) + A'( 0, R( psi ) ) ] * R( phi )
              RangeFieldType neighborEntityValue = neighborLiftingFlux * bLifts_[ j ];

              entityNeighbor.add( j, i, weight * entityNeighborValue );
              neighborEntity.add( j, i, weight * neighborEntityValue );
            }
          }
        }

        updateLocalMatrix( neighborEntity );
        updateLocalMatrix( entityNeighbor );
      }

    private:
      mutable std::vector< TemporaryLocalMatrixType > otherLocalMatrix_;
      mutable std::vector< LiftingRangeType > bLifts_, bLiftsNb_;
      mutable TemporaryLocalFunctionSet< typename LiftedBasisFunctionSetType::DiscreteFunctionSpaceType > insideLiftings_;
    };

  } // namespace Fem

} // namespace  Dune

#endif // #ifndef DUNE_FEM_OPERATOR_DG_INTEGRATORSBASE_HH
