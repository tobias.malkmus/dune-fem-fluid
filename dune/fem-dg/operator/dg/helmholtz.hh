#ifndef DUNE_FEM_OPERATOR_DG_HELMHOLTZ_HH
#define DUNE_FEM_OPERATOR_DG_HELMHOLTZ_HH

#include <dune/fem/space/combinedspace.hh>
#include <dune/fem/space/taylorhood.hh>

#include <dune/fem-dg/operator/dg/mass.hh>
#include <dune/fem/operator/common/differentiableoperator.hh>


namespace Dune
{

  namespace Fem
  {

    // DGHelmholtzOperator
    // -------------------

    template< class SpaceOperator >
    class DGHelmholtzOperator
      : public DifferentiableOperator< typename SpaceOperator::JacobianOperatorType >
    {
      typedef DGHelmholtzOperator< SpaceOperator > ThisType;
      typedef DifferentiableOperator< typename SpaceOperator::JacobianOperatorType > BaseType;

    public:
      typedef SpaceOperator SpaceOperatorType;

      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;

      typedef typename BaseType::JacobianOperatorType JacobianOperatorType;

      typedef typename DomainFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::EntityType EntityType;

      typedef MassOperator< DomainFunctionType > MassOperatorType;

      typedef typename DomainFunctionType::LocalFunctionType LocalFunctionType;
      typedef RangeFunctionType DestinationType;

      template< class ... Args >
      DGHelmholtzOperator ( Args && ... args )
        : spaceOp_( std::forward< Args >( args ) ... ),
          lambda_( 0 ),
          functor_( [] ( double ){} )
      {}

      std::size_t numberOfElements () const { return spaceOp_.numberOfElements(); }
      std::string description () const { return spaceOp_.description(); }

      void operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const
      {
        if( lambda_ != 0.0 )
          spaceOp_( u, w );
        else
          mass_( u, w );
      }

      void jacobian ( const DomainFunctionType &u, JacobianOperatorType &jOp ) const
      {
        spaceOp_.jacobian( u, jOp );
      }

      const double lambda () const { return lambda_; }
      void setLambda ( double lambda ) { lambda_ = lambda; spaceOp_.setLambda( lambda ); }
      void setTime ( double time ) { spaceOperator().setTime( time ); functor_( time ); }
      const DiscreteFunctionSpaceType &space () const { return spaceOperator().space(); }
      double timeStepEstimate () const { return spaceOperator().timeStepEstimate(); }

      void initializeTimeStepSize ( const DomainFunctionType &u ) const
      {
        DomainFunctionType tmp( u );
        spaceOp_( u, tmp );
      }

      const SpaceOperatorType &spaceOperator () const { return spaceOp_; }
      SpaceOperatorType &spaceOperator () { return spaceOp_; }

      double computeTime () const { return spaceOp_.computeTime(); }
      double assambleTime () const { return spaceOp_.assambleTime(); }

      void setRelaxFactor ( const double f ) const { spaceOp_.setRelaxFactor( f ); }

      template< class Functor >
      void setFunctor ( Functor f ) { functor_ = f; }

    protected:
      mutable SpaceOperator spaceOp_;
      MassOperatorType mass_;
      double lambda_;
      std::function< void( double ) > functor_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_DG_HELMHOLTZ_HH
