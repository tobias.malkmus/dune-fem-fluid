#ifndef DUNE_FEM_OPERATOR_DG_BASE_HH
#define DUNE_FEM_OPERATOR_DG_BASE_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/timer.hh>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/fem/function/common/discretefunction.hh>
#include <dune/fem/operator/common/automaticdifferenceoperator.hh>
#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem/operator/common/operator.hh>

#include <dune/fem-dg/operator/dg/integrators.hh>


namespace Dune
{

  namespace Fem
  {


    // DgOperatorBase
    // --------------

    template< class Traits >
    struct DgOperatorBase
      : public Operator< typename Traits::DomainFunctionType, typename Traits::RangeFunctionType >
    {
      typedef DgOperatorBase< Traits > ThisType;
      typedef Operator< typename Traits::DomainFunctionType, typename Traits::RangeFunctionType > BaseType;

    public:
      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;

      typedef typename Traits::ModelType ModelType;
      typedef typename Traits::DiscreteModelType DiscreteModelType;

      typedef DomainFunctionType DiscreteFunctionType;
      typedef DomainFunctionType DestinationType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType::Intersection IntersectionType;

      static const int dimDomain = FunctionSpaceType::dimDomain;
      static const int dimRange = FunctionSpaceType::dimRange;

      typedef typename Traits::FunctionIntegratorType FunctionIntegratorType;

      template< class ... Args >
      DgOperatorBase ( const DiscreteFunctionSpaceType &space, const ModelType &model, Args&& ... args )
        : space_( space ),
          discreteModel_( space_, model, std::forward< Args >( args ) ... ),
          gridPart_( space_.gridPart() ),
          functionIntegrator_( space_, discreteModel_ ),
          description_( "with discrete Model: " + discreteModel_.description() )
      {}

      std::string description () const { return description_; }


      void setLambda ( const double lambda )
      {
        discreteModel().setLambda( lambda );
        functionIntegrator_.setLambda( lambda );
      }

      void setRelaxFactor ( const double f ) const { discreteModel().setRelaxFactor( f ); }

      double timeStepEstimate () const { return discreteModel().timeStepEstimate(); }

      void setTime ( const double time ) { discreteModel().setTime( time ); }
      double time () const { return discreteModel().time(); }

      // prepare operator evaluation
      void prepare ( const DiscreteFunctionType &arg, DiscreteFunctionType &dest ) const
      {
        functionIntegrator_.prepare( arg, dest );
      }

      // finalize operator evaluation
      void finalize ( DiscreteFunctionType &dest ) const
      {
        functionIntegrator_.finalize( dest );
      }

      // operator evaluation
      void operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const
      {
        Dune::Timer timer;

        prepare( u, w );

        for( const EntityType &entity : elements( gridPart_, Partitions::all ) )
          applyLocal( entity, functionIntegrator_ );

        finalize( w );
        computeTime_ += timer.elapsed();
      }

      const DiscreteFunctionSpaceType &space () const { return space_; }

      const ModelType &model ()  const { return discreteModel().model(); }

      const DiscreteModelType &discreteModel () const { return discreteModel_; }
      DiscreteModelType &discreteModel () { return discreteModel_; }

      // statistics functions
      double computeTime () const { return computeTime_; }
      std::size_t numberOfElements () const { return discreteModel().numberOfElements(); }

    protected:
      template< class Integrator >
      void applyLocal ( const EntityType &entity, Integrator &integrator ) const
      {
        // init entity
        integrator.init( entity );

        // we have to start with the surface intergrals to compute the lifitings
        for( const IntersectionType &intersection : intersections( gridPart_, entity ) )
        {
          if( intersection.neighbor() ) // && !space_.continuous() )
            integrator.surfaceIntegral( intersection, entity, intersection.outside() );
          else if( intersection.boundary() )
            integrator.boundaryIntegral( intersection, entity );
        }

        // volume integrals
        integrator.volumenIntegral( entity );

        // finalize entity
        integrator.finalizeEntity( entity );
      }

    protected:
      const DiscreteFunctionSpaceType &space_;
      DiscreteModelType discreteModel_;
      GridPartType &gridPart_;

      FunctionIntegratorType functionIntegrator_;

      mutable double computeTime_;

      std::string description_;
    };


    // !!!!!!!
    // we have to forward the () operator directly otherwise casting into Dune::Fem::operator will fail, due to missing
    // implementation of operator ().
    // !!!!!!


    // AutomaticDifferenceDgOpaterorBase
    // ---------------------------------

    template< class Traits >
    class AutomaticDifferenceDgOperatorBase
      : public DgOperatorBase< Traits >,
        public AutomaticDifferenceOperator< typename Traits::DomainFunctionType, typename Traits::RangeFunctionType, typename Traits::JacobianOperatorType >
    {
      typedef AutomaticDifferenceDgOperatorBase< Traits > ThisType;
      typedef DgOperatorBase< Traits > BaseType;

    public:
      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;


      template< class ... Args >
      AutomaticDifferenceDgOperatorBase ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... )
      {}

      void operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const
      {
        BaseType::operator()( u, w );
      }

      double assambleTime () const { return 0.0; }
    };



    // DifferentiableDgOperatorBase
    // ----------------------------

    template< class Traits >
    class DifferentiableDgOperatorBase
      : public DgOperatorBase< Traits >,
        public DifferentiableOperator< typename Traits::JacobianOperatorType >
    {
      typedef DifferentiableDgOperatorBase< Traits > ThisType;
      typedef DgOperatorBase< Traits > BaseType;

    public:

      typedef typename DifferentiableOperator< typename Traits::JacobianOperatorType >::JacobianOperatorType JacobianOperatorType;
      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
      typedef typename BaseType::DiscreteModelType DiscreteModelType;
      typedef typename BaseType::EntityType EntityType;
      typedef typename BaseType::IntersectionType IntersectionType;

      typedef typename Traits::JacobianIntegratorType JacobianIntegratorType;

      static_assert( !std::is_same< JacobianOperatorType, DiscreteFunctionType >::value, "" );

      using BaseType::space;
      using BaseType::discreteModel;

      template< class ... Args >
      DifferentiableDgOperatorBase ( Args&& ... args )
        : BaseType( std::forward< Args >( args ) ... ),
          jacobianIntegrator_( space(), discreteModel() ),
          assambleTime_( 0.0 )
      {}

      void setLambda ( const double lambda )
      {
        BaseType::setLambda( lambda );
        jacobianIntegrator_.setLambda( lambda );
      }

      using BaseType::prepare;
      using BaseType::finalize;

      void operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const
      {
        BaseType::operator()( u, w );
      }

      // prepare operator evaluation
      void prepare ( const DiscreteFunctionType &arg, JacobianOperatorType &jac ) const
      {
        jacobianIntegrator_.prepare( arg, jac );
      }

      // finalize operator evaluation
      void finalize ( JacobianOperatorType &jac ) const
      {
        jacobianIntegrator_.finalize( jac );
      }

      void jacobian ( const DiscreteFunctionType &u, JacobianOperatorType &jac ) const
      {
        Dune::Timer timer;
        prepare( u, jac );

        for( const EntityType &entity : elements( gridPart_, Partitions::all ) )
          applyLocal( entity, jacobianIntegrator_ );

        finalize( jac );

        assambleTime_ += timer.elapsed();
      }

      double assambleTime () const { return assambleTime_; }

    protected:
      using BaseType::gridPart_;
      using BaseType::discreteModel_;

      void applyLocal ( const EntityType &entity, const JacobianIntegratorType &integrator ) const
      {
        // init entity
        integrator.init( entity );

        // we have to start with the surface intergrals to compute the lifitings
        for( const IntersectionType &intersection : intersections( gridPart_, entity ) )
        {
          if( intersection.neighbor() && ( entity.partitionType() == Dune::InteriorEntity ) )
          {
            integrator.surfaceIntegral( intersection, entity, intersection.outside() );

            if( intersection.outside().partitionType() != Dune::InteriorEntity )
            {
              // save acctual state
              integrator.finalizeEntity( entity );

              // compute ghost/overlap contributions
              applyLocal( intersection.outside(), integrator );

              // re-init to original entity
              integrator.init( entity );
            }
          }
          else if( intersection.boundary() )
            integrator.boundaryIntegral( intersection, entity );
          else if( entity.partitionType() != Dune::InteriorEntity )
            integrator.surfaceIntegral( intersection, entity );
        }

        // volume integrals
        integrator.volumenIntegral( entity );

        // finalize entity
        integrator.finalizeEntity( entity );
      }

    private:
      JacobianIntegratorType jacobianIntegrator_;
      mutable double assambleTime_;
    };


    // DgOperatorTraits
    // ----------------

    template< class JacobianOperator, class DiscreteModel >
    struct DgOperatorTraits
    {
      typedef JacobianOperator JacobianOperatorType;

      typedef typename JacobianOperatorType::DomainFunctionType DomainFunctionType;
      typedef typename JacobianOperatorType::RangeFunctionType RangeFunctionType;

      typedef DiscreteModel DiscreteModelType;
      typedef typename DiscreteModelType::ModelType ModelType;

      typedef DomainFunctionType DiscreteFunctionType;
      typedef typename DomainFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      static const bool needsLiftings = DiscreteModelType::needsLiftings;

      typedef FunctionIntegrator< DiscreteFunctionType, DiscreteModelType, needsLiftings > FunctionIntegratorType;
    };


    // DifferentiableDgOperatorTraits
    // ------------------------------

    template< class JacobianOperator, class DiscreteModel >
    struct DifferentiableDgOperatorTraits
      : public DgOperatorTraits< JacobianOperator, DiscreteModel >
    {
      typedef DgOperatorTraits< JacobianOperator, DiscreteModel > BaseType;
      typedef JacobianIntegrator< JacobianOperator, DiscreteModel, BaseType::needsLiftings > JacobianIntegratorType;
    };


    // implementational stuff

    namespace __Operator
    {

      template< class JOp, class Dm, bool isAssmebledOperator
                  = std::is_base_of< AssembledOperator< typename JOp::DomainFunctionType, typename JOp::RangeFunctionType >, JOp >::value >
      struct Switch;

      template< class JOp, class Dm >
      struct Switch< JOp, Dm, false >
      {
        typedef DgOperatorTraits< JOp, Dm > Traits;
        typedef AutomaticDifferenceDgOperatorBase< Traits > Type;
      };

      template< class JOp, class Dm >
      struct Switch< JOp, Dm, true >
      {
        typedef DifferentiableDgOperatorTraits< JOp, Dm > Traits;
        typedef DifferentiableDgOperatorBase< Traits > Type;
      };

    }


    // DgOperator
    // ----------

    template< class JacobianOperator, class DiscreteModel >
    using DgOperator = typename __Operator::Switch< JacobianOperator, DiscreteModel >::Type;


  } // namespace Fem

} // namespace Dune
#endif // #ifndef DUNE_FEM_OPERATOR_DG_BASE_HH
