#ifndef DUNE_FEM_OPERATOR_DG_NSEHELMHOLTZ_HH
#define DUNE_FEM_OPERATOR_DG_NSEHELMHOLTZ_HH

#include <dune/fem/space/combinedspace.hh>
#include <dune/fem/space/taylorhood.hh>

#include <dune/fem-dg/operator/dg/mass.hh>
#include <dune/fem/operator/common/differentiableoperator.hh>


namespace Dune
{

  namespace Fem
  {

    // NSEDGHelmholtzOperator
    // ----------------------

    template< class SpaceOperator >
    class NSEDGHelmholtzOperator
      : public DifferentiableOperator< typename SpaceOperator::JacobianOperatorType >
    {
      typedef NSEDGHelmholtzOperator< SpaceOperator > ThisType;
      typedef DifferentiableOperator< typename SpaceOperator::JacobianOperatorType > BaseType;

    public:
      typedef SpaceOperator SpaceOperatorType;

      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;

      typedef typename BaseType::JacobianOperatorType JacobianOperatorType;

      typedef typename DomainFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::EntityType EntityType;

      typedef MassOperator< DomainFunctionType > MassOperatorType;

      typedef typename DomainFunctionType::LocalFunctionType LocalFunctionType;

      static const int dimDomain = DiscreteFunctionSpaceType::dimDomain;

      typedef RangeFunctionType DestinationType;

      template< class ... Args >
      NSEDGHelmholtzOperator ( Args && ... args )
        : spaceOp_( std::forward< Args >( args ) ... ),
          lambda_( 0 ),
          functor_( [] ( double ){} )
      {}

      std::size_t numberOfElements () const { return spaceOp_.numberOfElements(); }
      std::string description () const { return spaceOp_.description(); }

      void operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const
      {
        if( lambda_ != 0.0 )
          spaceOp_( u, w );
        else
          mass_( u, w );
      }

      void jacobian ( const DomainFunctionType &u, JacobianOperatorType &jOp ) const
      {
        spaceOp_.jacobian( u, jOp );
      }

      void rhs ( const DomainFunctionType &arg, RangeFunctionType &dest, const double lambda ) const
      {
        // ( u , 0 )
        // apply weighted mass
        mass_( arg, dest, [] ( typename DomainFunctionType::RangeType & u ) { u[ dimDomain ] = 0.; } );

        if( lambda > 1e-14 )
        {
          // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          // -( F( u ), 0 )
          spaceOp_.setLambda( 0.0 );
          RangeFunctionType fDest( dest );
          spaceOp_( arg, fDest );
          spaceOp_.setLambda( lambda_ );
          // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          // ( u, 0 ) - lambda( F( u ), 0 ) -lambda( 0 + B'(p), 0 )
          dest.axpy( -lambda, fDest );
        }
      }

      void rescale ( double velocityFactor, double pressureFactor, DomainFunctionType &dest ) const
      {
        axpy( velocityFactor, pressureFactor, dest );
      }

      void copy ( double dt, const DomainFunctionType &arg, DomainFunctionType &dest ) const
      {
        dest.clear();
        axpy( 1.0, dt, arg, dest );
      }

      void velocityAxpy ( DomainFunctionType &x, double a, const DomainFunctionType &y ) const
      {
        axpy( a, 0., y, x );
      }

      void pressureAxpy ( DomainFunctionType &x, double a, const DomainFunctionType &y ) const
      {
        axpy( 0., a, y, x );
      }

      const double lambda () const { return lambda_; }
      void setLambda ( double lambda ) { lambda_ = lambda; spaceOp_.setLambda( lambda ); }
      void setTime ( double time ) { spaceOperator().setTime( time ); functor_( time ); }
      const DiscreteFunctionSpaceType &space () const { return spaceOperator().space(); }
      double timeStepEstimate () const { return spaceOperator().timeStepEstimate(); }

      void initializeTimeStepSize ( const DomainFunctionType &u ) const
      {
        DomainFunctionType tmp( u );
        spaceOp_( u, tmp );
      }

      const SpaceOperatorType &spaceOperator () const { return spaceOp_; }
      SpaceOperatorType &spaceOperator () { return spaceOp_; }


      double computeTime () const { return spaceOp_.computeTime(); }
      double assambleTime () const { return spaceOp_.assambleTime(); }

      void setRelaxFactor ( const double f ) const { spaceOp_.setRelaxFactor( f ); }

      template< class Functor >
      void setFunctor ( Functor f ) { functor_ = f; }

    protected:
      template< class Space1, class Space2 >
      void scale ( const EntityType &entity, const TupleDiscreteFunctionSpace< Space1, Space2 > &space,
                   double velocityFactor, double pressureFactor, std::vector< typename DomainFunctionType::DofType > &values ) const
      {
        const unsigned int numDofsVelocity = std::get< 0 >( space.spaceTuple() ).blockMapper().numDofs( entity )
          * DiscreteFunctionSpaceType::template SubDiscreteFunctionSpace< 0 >::Type::localBlockSize;
        std::size_t k = 0;
        for(; k < numDofsVelocity; ++k )
          values[ k ] *= velocityFactor;
        for(; k < values.size(); ++k )
          values[ k ] *= pressureFactor;
      }

      template< class VelocityFunctionSpace, class GridPart, int polOrder, template< class > class Storage >
      void scale ( const EntityType &entity, const TaylorHoodDiscontinuousGalerkinSpace< VelocityFunctionSpace, GridPart, polOrder, Storage > &space,
                   double velocityFactor, double pressureFactor, std::vector< typename DomainFunctionType::DofType > &values ) const
      {
        const unsigned int numDofsVelocity = TaylorHoodDiscontinuousGalerkinSpace< VelocityFunctionSpace, GridPart, polOrder, Storage >::numVelocityShapeFunctions;

        std::size_t k = 0;
        for(; k < numDofsVelocity; ++k )
          values[ k ] *= velocityFactor;
        for(; k < values.size(); ++k )
          values[ k ] *= pressureFactor;
      }

      void axpy ( double a1, double a2, const DomainFunctionType &arg, DomainFunctionType &dest ) const
      {
        std::vector< typename DomainFunctionType::DofType > values;
        for( const EntityType &entity : elements( dest.space().gridPart(), Partitions::all ) )
        {
          values.resize( dest.space().blockMapper().numDofs( entity ) );
          arg.getLocalDofs( entity, values );
          scale( entity, dest.space(), a1, a2, values );
          dest.addLocalDofs( entity, values );
        }
      }

      void axpy ( double a1, double a2, DomainFunctionType &dest ) const
      {
        std::vector< typename DomainFunctionType::DofType > values;
        for( const EntityType &entity : elements( dest.space().gridPart(), Partitions::all ) )
        {
          values.resize( dest.space().blockMapper().numDofs( entity ) );
          dest.getLocalDofs( entity, values );
          scale( entity, dest.space(), a1, a2, values );
          dest.setLocalDofs( entity, values );
        }
      }


      mutable SpaceOperator spaceOp_;
      MassOperatorType mass_;
      double lambda_;
      std::function< void( double ) > functor_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_DG_NSEHELMHOLTZ_HH
