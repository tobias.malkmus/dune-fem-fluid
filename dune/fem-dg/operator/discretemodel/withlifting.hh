#ifndef DUNE_FEM_OPERATOR_DG_DISCRETEMODEL_HH
#define DUNE_FEM_OPERATOR_DG_DISCRETEMODEL_HH

#include <dune/common/typetraits.hh>

#include <dune/fem-dg/operator/discretemodel/base.hh>
#include <dune/fem-dg/operator/discretemodel/differentiablebase.hh>

#include <dune/fem-dg/space/basisfunctionset/lifted.hh>


namespace Dune
{

  namespace Fem
  {

    // DiscreteModelWithLiftings
    // -------------------------

    template< class Traits, class B = DiscreteModelBase< Traits > >
    class DiscreteModelWithLiftings
      : public B
    {
      typedef DiscreteModelWithLiftings< Traits > ThisType;
      typedef B BaseType;

    public:
      typedef typename BaseType::ModelType ModelType;
      typedef typename BaseType::NumFluxType NumFluxType;
      typedef typename BaseType::DiffusionFluxType DiffusionFluxType;

      static_assert( DiffusionFluxType::needsLiftings, "This DiscreteModelWithLiftings only works for methods which needs liftings" );
      static const bool needsLiftings = true;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      typedef LiftedBasisFunctionSet< DiscreteFunctionSpaceType > LiftedBasisFunctionSetType;
      typedef typename LiftedBasisFunctionSetType::OnEntityLiftedSetType OnEntityLiftedSetType;

      typedef typename LiftedBasisFunctionSetType::DiscreteFunctionSpaceType LiftingDiscreteFunctionSpaceType;
      typedef typename LiftingDiscreteFunctionSpaceType::RangeType LiftingRangeType;

      typedef TemporaryLocalFunction< LiftingDiscreteFunctionSpaceType > LiftingLocalFunctionType;

    protected:
      static const int dimRange = DiscreteFunctionSpaceType::dimRange;
      static const int dimDomain = DiscreteFunctionSpaceType::dimDomain;

      using BaseType::diffFlux_;
      using BaseType::values_;
      using BaseType::valuesNb_;
      using BaseType::jacobians_;
      using BaseType::jacobiansNb_;
      using BaseType::hasBoundaryValues_;
      using BaseType::uBnd_;
      using BaseType::time;
      using BaseType::uOutside_;

    public:
      using BaseType::model;
      using BaseType::space;

      template< class ... Args >
      DiscreteModelWithLiftings ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... ),
          liftedBasisFunctionSet_( space() ),
          uLifting_( liftedBasisFunctionSet_.space() )
      {}

      const LiftedBasisFunctionSetType &liftedBasisFunctionSet () const { return liftedBasisFunctionSet_; }

      void init ( const EntityType &entity ) const
      {
        hasBoundaryLiftings_ = false;
        wasCalled_ = false;
        BaseType::init( entity );

        liftedBasisFunctionSet_.initEntity( entity );

        uLifting_.init( entity );
        uLifting_.clear();

        liftingFactor_.clear();
        liftingFactor_.resize( liftedBasisFunctionSet().maxSize(), std::make_pair( 0.0, false )  );
      }


      // compute u on each quadrature point
      template< class Quadrature >
      void initEntity ( const EntityType &entity, const Quadrature &quad, bool evalType ) const
      {
        BaseType::initEntity( entity, quad, evalType );

        if( !wasCalled_ )
        {
          computeLifting( entity );
          wasCalled_ = true;
        }

        BaseType::initValues( uLifting_, quad, liftings_ );
        diffFlux_.init( entity, time(), quad, values_, jacobians_, liftings_, evalType );
      }


      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside, const
                              FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside ) const
      {
        BaseType::initIntersection( intersection, inside, outside, faceQuadInside, faceQuadOutside );
        setLiftingFactor( intersection );
      }


      // compute u on each quadrature point
      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const FaceQuadrature &faceQuadInside ) const
      {
        BaseType::initIntersection( intersection, inside, faceQuadInside );

        // in case of dirichlet boundary values we need to compute the lifting of the dirichlet trace
        if( hasBoundaryValues_ )
        {
          const std::size_t numQuadraturePoints = faceQuadInside.nop();
          for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
          {
            const double weight = faceQuadInside.weight( pt );
            DomainType normal
              = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

            LiftingRangeType lift( 0 );
            for( std::size_t r = 0; r < dimRange; ++r )
              for( std::size_t d = 0; d < dimDomain; ++d )
                lift[ r * dimDomain + d ] = -valuesNb_[ pt ][ r ] * normal[ d ] * weight;

            uLifting_.axpy( faceQuadInside[ pt ], lift );
          }
          hasBoundaryLiftings_ = true;
        }

        setBoundaryLiftingFactor( intersection );
      }


      // numericalFlux
      // -------------

      template< class FaceQuadrature >
      void numericalFlux ( const IntersectionType &intersection,
                           const EntityType &insideEntity,
                           const EntityType &outsideEntity,
                           const FaceQuadrature &faceQuadInside,
                           const FaceQuadrature &faceQuadOutside,
                           const int pt,
                           const DomainType &normal,
                           RangeType &value,
                           JacobianRangeType &flux ) const
      {
        BaseType::numericalFlux( intersection, insideEntity, outsideEntity, faceQuadInside, faceQuadOutside, pt, normal, value, flux );

        if( ModelType::hasDiffusion )
        {
          RangeType diffValue( 0.0 );
          diffFlux_.numericalFlux( intersection, insideEntity, outsideEntity, time(), faceQuadInside, faceQuadOutside, pt,
                                   normal, values_[ pt ], valuesNb_[ pt ], diffValue );
          value += diffValue;
        }
      }


      // boundaryFlux
      // ------------

      template< class FaceQuadrature >
      void boundaryFlux ( const IntersectionType &intersection,
                          const EntityType &insideEntity,
                          const FaceQuadrature &faceQuadInside,
                          const int pt,
                          const DomainType &normal,
                          RangeType &value,
                          JacobianRangeType &flux ) const
      {
        BaseType::boundaryFlux( intersection, insideEntity, faceQuadInside, pt, normal, value, flux );

        if( ModelType::hasDiffusion && hasBoundaryValues_ )
        {
          RangeType diffValue;
          diffFlux_.boundaryFlux( intersection, insideEntity, time(), faceQuadInside, pt,
                                  normal, values_[ pt ], valuesNb_[ pt ], diffValue );
          value += diffValue;
        }
      }


      // fluxAndSource
      // -------------

      template< class Quadrature >
      void fluxAndSource ( const EntityType &entity,
                           const Quadrature &quad,
                           const int pt,
                           RangeType &source,
                           JacobianRangeType &flux ) const
      {
        BaseType::fluxAndSource( entity, quad, pt, source, flux );

        // diffusive part
        if( ModelType::hasDiffusion )
        {
          JacobianRangeType diffFlux( 0 );
          diffusion( entity, quad, pt, diffFlux );
          flux -= diffFlux;
        }
      }

      template< class Quadrature, class JacobianVector >
      void diffusion ( const EntityType &entity,
                       const Quadrature &quad,
                       const int pt,
                       JacobianVector &diffusion ) const
      {
        diffFlux_.diffusion( entity, time(), quad.point( pt ), values_[ pt ], jacobians_[ pt ], liftings_[ pt ], diffusion );
      }


      double liftingFactor ( int index ) const
      {
        assert( ( index >= 0 ) && ( (unsigned int ) index < liftingFactor_.size() ) );
        return liftingFactor_[ index ].first;
      }

      bool isInsideLifting ( int index ) const
      {
        assert( ( index >= 0 ) && ( (unsigned int ) index < liftingFactor_.size() ) );
        return liftingFactor_[ index ].second;
      }

    protected:
      LiftedBasisFunctionSetType &liftedBasisFunctionSet () { return liftedBasisFunctionSet_; }

      void computeLifting ( const EntityType &entity ) const
      {
        // apply inverse mass matrix to bnd lifting
        if( hasBoundaryLiftings_ )
          liftedBasisFunctionSet().applyInverseMass( uLifting_ );

        for( auto &key : liftedBasisFunctionSet() )
        {
          OnEntityLiftedSetType liftedSet( key );

          if( liftingFactor( liftedSet.index() ) != 0.0 )
          {
            BaseType::initLocalFunction( liftedSet.entity(), uOutside_ );
            liftedSet.usmv( liftingFactor( liftedSet.index() ), uOutside_.localDofVector(), uLifting_.localDofVector() );
          }
        }
      }

      void setLiftingFactor ( const IntersectionType &intersection ) const
      {
        liftingFactor_[ liftedBasisFunctionSet().getInsideIndex( intersection ) ]
          = std::make_pair( diffFlux_.insideLiftingFactor( intersection ), true );
        liftingFactor_[ liftedBasisFunctionSet().getOutsideIndex( intersection ) ]
          = std::make_pair( diffFlux_.outsideLiftingFactor( intersection ), false );
      }

      void setBoundaryLiftingFactor ( const IntersectionType &intersection ) const
      {
        liftingFactor_[ liftedBasisFunctionSet().getInsideIndex( intersection ) ]
          = std::make_pair( diffFlux_.boundaryLiftingFactor( intersection ) * hasBoundaryValues_, true );
      }

      mutable LiftedBasisFunctionSetType liftedBasisFunctionSet_;
      mutable LiftingLocalFunctionType uLifting_;
      mutable std::vector< LiftingRangeType > liftings_;
      mutable std::vector< std::pair< double, bool > > liftingFactor_;
      mutable bool hasBoundaryLiftings_, wasCalled_;
    };


    // DifferentiableDiscreteModelWithLiftings
    // ---------------------------------------

    template< class Traits, class B = DifferentiableDiscreteModelBase< Traits > >
    class DifferentiableDiscreteModelWithLiftings
      : public DiscreteModelWithLiftings< Traits, B >
    {
      typedef DifferentiableDiscreteModelWithLiftings ThisType;
      typedef DiscreteModelWithLiftings< Traits, B > BaseType;

    public:
      typedef typename BaseType::ModelType ModelType;
      typedef typename BaseType::NumFluxType NumFluxType;
      typedef typename BaseType::DiffusionFluxType DiffusionFluxType;

      static const bool needsLiftings = BaseType::needsLiftings;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      typedef typename BaseType::LiftingRangeType LiftingRangeType;

    protected:
      static const int dimRange = DiscreteFunctionSpaceType::dimRange;
      static const int dimDomain = DiscreteFunctionSpaceType::dimDomain;

      using BaseType::diffFlux_;
      using BaseType::values_;
      using BaseType::valuesNb_;
      using BaseType::jacobians_;
      using BaseType::jacobiansNb_;
      using BaseType::hasBoundaryValues_;
      using BaseType::uBnd_;
      using BaseType::time;
      using BaseType::liftings_;

    public:
      template< class ... Args >
      DifferentiableDiscreteModelWithLiftings ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... ),
          boundaryLiftings_( BaseType::liftedBasisFunctionSet().space(),
              BaseType::space().blockMapper().maxNumDofs() * DiscreteFunctionSpaceType::localBlockSize )
      {}

      using BaseType::liftedBasisFunctionSet;
      using BaseType::model;

      void init ( const EntityType & entity ) const
      {
        BaseType::init( entity );
        wasCalled_ = false;
        hasBoundaryLiftings_ = false;
      }

      bool hasBoundaryFunctor ( const EntityType &entity ) const { return hasBoundaryLiftings_; }

      auto boundaryFunctor () const
      {
        return [ this ] ( const auto &quad, int pt, int i, const LiftingRangeType &lift ) -> LiftingRangeType
        {
          return lift + bLiftsBnd_[ pt ][ i ];
        };
      }

      // compute u on each quadrature point
      template< class Quadrature >
      void initEntity ( const EntityType &entity, const Quadrature &quad, bool evalType ) const
      {
        BaseType::initEntity( entity, quad, evalType );

        if( hasBoundaryLiftings_ )
        {
          if( !wasCalled_ )
          {
            liftedBasisFunctionSet().applyInverseMass( boundaryLiftings_ );
            wasCalled_ = true;
          }

          std::size_t size = boundaryLiftings_.size();
          bLiftsBnd_.resize( quad.nop() );
          for( auto qp : quad )
          {
            bLiftsBnd_[qp.index()].resize( size );
            evaluateAll( boundaryLiftings_, qp, bLiftsBnd_[ qp.index() ] );
          }
        }
      }

      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside, const
                              FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside ) const
      {
        BaseType::initIntersection( intersection, inside, outside, faceQuadInside, faceQuadOutside );
      }


      // compute u on each quadrature point
      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const FaceQuadrature &faceQuadInside ) const
      {
        BaseType::initIntersection( intersection, inside, faceQuadInside );

        if( model().hasLinearBoundaryValue( intersection ) && hasBoundaryValues_ )
        {
          auto basisSet = BaseType::space().basisFunctionSet( inside );
          std::size_t numDofs = basisSet.size();

          if( !hasBoundaryLiftings_ )
          {
            boundaryLiftings_.init( inside, numDofs );
            boundaryLiftings_.clear();
          }

          hasBoundaryLiftings_ = true;

          std::vector< RangeType > phii( numDofs );
          std::vector< JacobianRangeType > dphii( numDofs );
          std::vector< LiftingRangeType > lift( numDofs, LiftingRangeType( 0 ) );

          const std::size_t numQuadraturePoints = faceQuadInside.nop();
          for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
          {
            const double weight = faceQuadInside.weight( pt );
            DomainType normal = intersection.integrationOuterNormal( faceQuadInside.localPoint( pt ) );

            basisSet.evaluateAll( faceQuadInside[ pt ], phii );
            basisSet.jacobianAll( faceQuadInside[ pt ], dphii );

            for( std::size_t i = 0; i < numDofs; ++i )
            {
              model().linearBoundaryValue( intersection, time(), faceQuadInside.localPoint( pt ), values_[ pt ],
                  jacobians_[ pt ], phii[ i ], dphii[ i ], uBnd_ );

              for( std::size_t r = 0; r < dimRange; ++r )
                for( std::size_t d = 0; d < dimDomain; ++d )
                  lift[ i ][ r * dimDomain + d ] = -uBnd_[ r ] * normal[ d ] * weight;
            }

            boundaryLiftings_.axpy( faceQuadInside[ pt ], lift );
          }
        }
      }

      // linearNumericalFlux
      // -------------------

      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &insideEntity,
                                 const EntityType &outsideEntity,
                                 const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside,
                                 const int pt,
                                 const DomainType &normal,
                                 const RangeType &uLeft,
                                 const RangeType &uRight,
                                 const JacobianRangeType &jacLeft,
                                 const JacobianRangeType &jacRight,
                                 RangeType &gLeft,
                                 RangeType &gRight,
                                 JacobianRangeType &gDiffLeft,
                                 JacobianRangeType &gDiffRight ) const
      {
        BaseType::linearNumericalFlux( intersection, insideEntity, outsideEntity, faceQuadInside, faceQuadOutside, pt, normal,
                                       uLeft, uRight, jacLeft, jacRight, gLeft, gRight, gDiffLeft, gDiffRight );

        if( ModelType::hasDiffusion )
        {
          RangeType diffValueLeft;
          RangeType diffValueRight;
          diffFlux_.linearNumericalFlux( intersection, insideEntity, outsideEntity, time(),
                                         faceQuadInside, faceQuadOutside, pt, normal, values_[ pt ], valuesNb_[ pt ],
                                         uLeft, uRight, diffValueLeft, diffValueRight );
          gLeft += diffValueLeft;
          gRight += diffValueRight;
        }
      }


      // linearBoundaryFlux
      // ------------------

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &insideEntity,
                                const FaceQuadrature &faceQuadInside,
                                const int pt,
                                const DomainType &normal,
                                const RangeType &u,
                                const JacobianRangeType &jac,
                                RangeType &value,
                                JacobianRangeType &flux ) const
      {
        BaseType::linearBoundaryFlux( intersection, insideEntity, faceQuadInside, pt, normal, u, jac, value, flux );

        if( ModelType::hasDiffusion && hasBoundaryValues_ )
        {
          RangeType diffValue( 0 );
          diffFlux_.linearBoundaryFlux( intersection, insideEntity, time(),
                                        faceQuadInside, pt, normal, values_[ pt ], valuesNb_[ pt ],
                                        u, uBnd_, diffValue );
          value += diffValue;
        }
      }


      // linearFluxAndSource
      // -------------------

      template< class Quadrature >
      void linearFluxAndSource ( const EntityType &entity,
                                 const Quadrature &quad,
                                 const int pt,
                                 const RangeType &u,
                                 const JacobianRangeType &jac,
                                 RangeType &source,
                                 JacobianRangeType &flux ) const
      {
        BaseType::linearFluxAndSource( entity, quad, pt, u, jac, source, flux );
      }


      template< class Quadrature, class JacobianVector >
      void linearDiffusion ( const EntityType &entity,
                             const Quadrature &quad,
                             const int pt,
                             const RangeType &u,
                             const JacobianRangeType &jac,
                             const LiftingRangeType &lifting,
                             JacobianVector &diffusion ) const
      {
        diffusion = 0.;
        if( ModelType::hasDiffusion )
          diffFlux_.linearDiffusion( entity, time(), quad, pt, values_[ pt ], jacobians_[ pt ],
                                     liftings_[ pt ], u, jac, lifting, diffusion );
      }

    protected:
      mutable typename BaseType::LiftedBasisFunctionSetType::SingleEntityLiftedBasisSetType boundaryLiftings_;
      mutable bool wasCalled_, hasBoundaryLiftings_;
      mutable std::vector< std::vector< LiftingRangeType > > bLiftsBnd_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_DG_DISCRETEMODEL_HH
