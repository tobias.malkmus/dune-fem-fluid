#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_BASE_HH
#define DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_BASE_HH

#include <vector>

#include <dune/fem-dg/operator/discretemodel/timestepestimator.hh>
#include <dune/fem/function/localfunction/temporary.hh>


namespace Dune
{

  namespace Fem
  {

    // DiscreteModelBase
    // -----------------

    template< class Traits >
    class DiscreteModelBase
    {
      typedef DiscreteModelBase< Traits > ThisType;

    public:

      typedef typename Traits::ModelType ModelType;
      typedef typename Traits::NumFluxType NumFluxType;
      typedef typename Traits::DiffusionFluxType DiffusionFluxType;

      typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;
      typedef typename Traits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      static const bool needsLiftings = DiffusionFluxType::needsLiftings;

      static const bool isEvolutionProblem = ModelType::isEvolutionProblem;

    protected:
      typedef TemporaryLocalFunction< DiscreteFunctionSpaceType > TemporaryLocalFunctionType;

      typedef LocalTimeStepEstimator< isEvolutionProblem > LocalTimeStepEstimatorType;
    public:
      template< class ... Args >
      DiscreteModelBase ( const DiscreteFunctionSpaceType &space, const ModelType &model, Args && ... args )
        : space_( space ),
          model_( model ),
          numFlux_( model_ ),
          diffFlux_( model_ ),
          time_( 0.0 ),
          dtEst_( std::numeric_limits< double >::max() ),
          uInside_( space_ ),
          uOutside_( space_ ),
          numberOfElements_( 0 ),
          description_( "Advection flux: " +numFlux_.description() + " Diffusive Flux :" + diffFlux_.description() )
      {}

      ~DiscreteModelBase ()
      {
        arg_ = nullptr;
      }

      std::string description () const { return description_; }

      void setLambda ( const double lambda ) const { model().setLambda( lambda ); }

      void setRelaxFactor ( const double f ) const { model().setRelaxFactor( f ); }

      std::size_t numberOfElements () const { return numberOfElements_; }

      double timeStepEstimate () const { return dtEst_; }
      void setTime ( const double time ) { time_ = time; }
      double time () const { return time_; }

      void prepare ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const
      {
        dtEst_ =  std::numeric_limits< double >::max();
        numberOfElements_ = 0;
        arg_ = const_cast< DiscreteFunctionType * >(&u);
        w.clear();
      }

      void finalize ( DiscreteFunctionType &w ) const
      {
        w.communicate();
        arg_ = nullptr;
      }

      // initialize local function values
      void init ( const EntityType &entity ) const
      {
        numberOfElements_++;
        initLocalFunction( entity, uInside_ );
        hasBoundaryValues_ = false;

        localTimeStepEstimator_.reset();
        localTimeStepEstimator_.setInsideVolume( entity.geometry().volume() );
      }

      // compute u on each quadrature point
      template< class Quadrature >
      void initEntity ( const EntityType &entity, const Quadrature &quad, bool evalType ) const
      {
        initValues( uInside_, quad, values_ );
        initValues( uInside_, quad, jacobians_ );
      }

      void finalizeEntity ( const EntityType &entity ) const
      {
        dtEst_ = std::min( dtEst_, localTimeStepEstimator_.estimate() );
      }

      // compute u on each quadrature point
      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const FaceQuadrature &faceQuadInside ) const
      {
        initValues( uInside_, faceQuadInside, values_ );
        initValues( uInside_, faceQuadInside, jacobians_ );

        // check wether we have dirichlet boundary values
        hasBoundaryValues_ = model().hasBoundaryValue( intersection );

        if( hasBoundaryValues_ )
        {
          // compute dirichlet boundary values and store them
          const std::size_t numQuadraturePoints = faceQuadInside.nop();
          valuesNb_.resize( numQuadraturePoints );
          for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
            model().boundaryValue( intersection, time(), faceQuadInside.localPoint( pt ), values_[ pt ], jacobians_[ pt ], valuesNb_[ pt ] );
        }

        diffFlux_.initIntersection( intersection, inside, faceQuadInside );
      }


      // compute u on each quadrature point
      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                              const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside ) const
      {
        initLocalFunction( outside, uOutside_ );
        localTimeStepEstimator_.setOutsideVolume( outside.geometry().volume() );

        initValues( uInside_, faceQuadInside, values_ );
        initValues( uOutside_, faceQuadOutside, valuesNb_ );

        if( !needsLiftings )
        {
          initValues( uInside_, faceQuadInside, jacobians_ );
          initValues( uOutside_, faceQuadOutside, jacobiansNb_ );
        }

        diffFlux_.initIntersection( intersection, inside, outside, faceQuadInside, faceQuadOutside );
      }


      // numericalFlux
      // -------------

      template< class FaceQuadrature >
      void numericalFlux ( const IntersectionType &intersection,
                           const EntityType &insideEntity,
                           const EntityType &outsideEntity,
                           const FaceQuadrature &faceQuadInside,
                           const FaceQuadrature &faceQuadOutside,
                           const int pt,
                           const DomainType &normal,
                           RangeType &value,
                           JacobianRangeType &flux ) const
      {
        value = 0;
        flux = 0;
        double advSpeed = ( ModelType::hasFlux ) ?
                          numFlux_.numericalFlux( intersection, insideEntity, outsideEntity, time(), faceQuadInside, faceQuadOutside, pt,
                                                  normal, values_[ pt ], valuesNb_[ pt ], value )
                          : 0;

        // weak source
        if( ModelType::hasWeakSource )
        {
          JacobianRangeType weakSource;
          model().weakSource( insideEntity, time(), faceQuadInside.point( pt ), weakSource );
          weakSource.usmv( -1.0, normal, value );
        }

        localTimeStepEstimator_.intersectionEstimate( advSpeed * faceQuadInside.weight( pt ) );
      }


      // boundaryFlux
      // ------------

      template< class FaceQuadrature >
      void boundaryFlux ( const IntersectionType &intersection,
                          const EntityType &insideEntity,
                          const FaceQuadrature &faceQuadInside,
                          const int pt,
                          const DomainType &normal,
                          RangeType &value,
                          JacobianRangeType &flux ) const
      {
        value = 0;
        flux = 0;

        if( hasBoundaryValues_ )
        {
          double advSpeed = ( ModelType::hasFlux ) ?
                            numFlux_.boundaryFlux( intersection, insideEntity, time(), faceQuadInside, pt,
                                                   normal, values_[ pt ], valuesNb_[ pt ], value )
                            : dtEst_;

          localTimeStepEstimator_.intersectionEstimate( advSpeed * faceQuadInside.weight( pt ) );
        }
        else
        {
          double wSpeed = model().boundaryFlux( intersection, insideEntity, time(), faceQuadInside.localPoint( pt ), normal,
                                                values_[ pt ], jacobians_[ pt ], value );

          localTimeStepEstimator_.intersectionEstimate( wSpeed * faceQuadInside.weight( pt ) );
        }

        // weak source
        if( ModelType::hasWeakSource )
        {
          JacobianRangeType weakSource;
          model().weakSource( insideEntity, time(), faceQuadInside.point( pt ), weakSource );
          weakSource.usmv( -1.0, normal, value );
        }
      }


      // fluxAndSource
      // -------------

      template< class Quadrature >
      void fluxAndSource ( const EntityType &entity,
                           const Quadrature &quad,
                           const int pt,
                           RangeType &source,
                           JacobianRangeType &flux ) const
      {
        flux = 0;
        source = 0;
        // advective part
        if( ModelType::hasFlux )
          model().advection( entity, time(), quad.point( pt ), values_[ pt ], flux );

        // weak source
        if( ModelType::hasWeakSource )
        {
          JacobianRangeType weakSource;
          model().weakSource( entity, time(), quad.point( pt ), weakSource );
          flux -= weakSource;
        }

        // source terms
        const double dtSourceEst = ( ModelType::hasSource ) ?
                                   model().source( entity, time(), quad.point( pt ), values_[ pt ], jacobians_[ pt ], source )
                                   : 0;
        localTimeStepEstimator_.sourceEstimate( dtSourceEst );
      }


      // will be called by the integrator to add the mass term in evolution problems
      template< class Quadrature >
      void addScaledMassTerm ( const EntityType &entity, const Quadrature &quad, const int pt, const double scale, RangeType &source ) const
      {
        if( ModelType::hasMass )
          model().addScaledMassTerm( entity, time(), quad.point( pt ), scale, values_[ pt ], source );
      }


      const DiscreteFunctionSpaceType &space () const { return space_; }

    protected:
      // initialize localfunction on entity
      void initLocalFunction ( const EntityType &entity, TemporaryLocalFunctionType &lf ) const
      {
        lf.init( entity );
        lf.clear();
        assert( arg_ );
        arg_->getLocalDofs( entity, lf.localDofVector() );
      }

      // compute localfunction on each quadrature point
      template< class LocalFunction, class Quadrature, class RangeVector >
      void initValues ( const LocalFunction &localFunction, const Quadrature &quad,
                        RangeVector &ranges ) const
      {
        ranges.resize( quad.nop());
        localFunction.evaluateQuadrature( quad, ranges );
      }

      const ModelType &model () const { return model_; }

      const DiscreteFunctionSpaceType &space_;
      const ModelType &model_;

      NumFluxType numFlux_;
      DiffusionFluxType diffFlux_;

      mutable bool hasBoundaryValues_;
      mutable double time_;
      mutable double dtEst_;
      mutable LocalTimeStepEstimatorType localTimeStepEstimator_;

      mutable RangeType uBnd_;
      mutable std::vector< RangeType > values_, valuesNb_;
      mutable std::vector< JacobianRangeType > jacobians_, jacobiansNb_;

      mutable DiscreteFunctionType *arg_;

      mutable TemporaryLocalFunctionType uInside_;
      mutable TemporaryLocalFunctionType uOutside_;

      mutable std::size_t numberOfElements_;
      mutable int sequence_;
      std::string description_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_BASE_HH
