#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_WITHOUTLIFTING_HH
#define DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_WITHOUTLIFTING_HH

#include <dune/common/typetraits.hh>

#include <dune/fem-dg/operator/discretemodel/base.hh>
#include <dune/fem-dg/operator/discretemodel/differentiablebase.hh>


namespace Dune
{

  namespace Fem
  {

    // DiscreteModelWithoutLiftings
    // ----------------------------

    template< class Traits, class B = DiscreteModelBase< Traits > >
    class DiscreteModelWithoutLiftings
      : public B
    {
      typedef DiscreteModelWithoutLiftings< Traits > ThisType;
      typedef B BaseType;

    public:
      typedef typename BaseType::ModelType ModelType;
      typedef typename BaseType::NumFluxType NumFluxType;
      typedef typename BaseType::DiffusionFluxType DiffusionFluxType;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

    protected:
      using BaseType::diffFlux_;
      using BaseType::values_;
      using BaseType::valuesNb_;
      using BaseType::jacobians_;
      using BaseType::jacobiansNb_;
      using BaseType::hasBoundaryValues_;
      using BaseType::uBnd_;
      using BaseType::time;

    public:
      template< class ... Args >
      DiscreteModelWithoutLiftings ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... )
      {}

      using BaseType::model;

      template< class Quadrature >
      void initEntity ( const EntityType &entity, const Quadrature &quad, bool EvalType ) const
      {
        BaseType::initEntity( entity, quad, EvalType );
        diffFlux_.init( entity, time(), quad, values_, jacobians_, EvalType );
      }

      // numericalFlux
      // -------------

      template< class FaceQuadrature >
      void numericalFlux ( const IntersectionType &intersection,
                           const EntityType &insideEntity,
                           const EntityType &outsideEntity,
                           const FaceQuadrature &faceQuadInside,
                           const FaceQuadrature &faceQuadOutside,
                           const int pt,
                           const DomainType &normal,
                           RangeType &value,
                           JacobianRangeType &flux ) const
      {
        BaseType::numericalFlux( intersection, insideEntity, outsideEntity, faceQuadInside, faceQuadOutside, pt, normal, value, flux );

        if( ModelType::hasDiffusion )
        {
          RangeType diffValue( 0.0 );
          diffFlux_.numericalFlux( intersection, insideEntity, outsideEntity, time(), faceQuadInside, faceQuadOutside, pt,
                                   normal, values_[ pt ], valuesNb_[ pt ], jacobians_[ pt ], jacobiansNb_[ pt ], diffValue, flux );
          value += diffValue;
        }
      }


      // boundaryFlux
      // ------------

      template< class FaceQuadrature >
      void boundaryFlux ( const IntersectionType &intersection,
                          const EntityType &insideEntity,
                          const FaceQuadrature &faceQuadInside,
                          const int pt,
                          const DomainType &normal,
                          RangeType &value,
                          JacobianRangeType &flux ) const
      {
        BaseType::boundaryFlux( intersection, insideEntity, faceQuadInside, pt, normal, value, flux );

        if( ModelType::hasDiffusion && hasBoundaryValues_ )
        {
          RangeType diffValue;
          diffFlux_.boundaryFlux( intersection, insideEntity, time(), faceQuadInside, pt,
                                  normal, values_[ pt ], valuesNb_[ pt ], jacobians_[ pt ], diffValue, flux );
          value += diffValue;
        }
      }


      // fluxAndSource
      // -------------

      template< class Quadrature >
      void fluxAndSource ( const EntityType &entity,
                           const Quadrature &quad,
                           const int pt,
                           RangeType &source,
                           JacobianRangeType &flux ) const
      {
        BaseType::fluxAndSource( entity, quad, pt, source, flux );

        // diffusive part
        if( ModelType::hasDiffusion )
        {
          JacobianRangeType diffFlux( 0 );
          model().diffusion( entity, time(), quad.point( pt ), values_[ pt ], jacobians_[ pt ], diffFlux );
          flux -= diffFlux;
        }
      }

    };



    // DifferentiableDiscreteModelWithoutLiftings
    // ------------------------------------------

    template< class Traits, class B = DifferentiableDiscreteModelBase< Traits > >
    class DifferentiableDiscreteModelWithoutLiftings
      : public DiscreteModelWithoutLiftings< Traits, B >
    {
      typedef DifferentiableDiscreteModelWithoutLiftings< Traits > ThisType;
      typedef DiscreteModelWithoutLiftings< Traits, B > BaseType;

    public:
      typedef typename BaseType::ModelType ModelType;
      typedef typename BaseType::NumFluxType NumFluxType;
      typedef typename BaseType::DiffusionFluxType DiffusionFluxType;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

    protected:
      using BaseType::diffFlux_;
      using BaseType::values_;
      using BaseType::valuesNb_;
      using BaseType::jacobians_;
      using BaseType::jacobiansNb_;
      using BaseType::hasBoundaryValues_;
      using BaseType::uBnd_;
      using BaseType::time;

    public:
      template< class ... Args >
      DifferentiableDiscreteModelWithoutLiftings ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... )
      {}


      // linearNumericalFlux
      // -------------------

      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &insideEntity,
                                 const EntityType &outsideEntity,
                                 const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside,
                                 const int pt,
                                 const DomainType &normal,
                                 const RangeType &uLeft,
                                 const RangeType &uRight,
                                 const JacobianRangeType &jacLeft,
                                 const JacobianRangeType &jacRight,
                                 RangeType &gLeft,
                                 RangeType &gRight,
                                 JacobianRangeType &gDiffLeft,
                                 JacobianRangeType &gDiffRight ) const
      {
        BaseType::linearNumericalFlux( intersection, insideEntity, outsideEntity, faceQuadInside, faceQuadOutside, pt, normal,
                                       uLeft, uRight, jacLeft, jacRight, gLeft, gRight, gDiffLeft, gDiffRight );

        if( ModelType::hasDiffusion )
        {
          RangeType diffValueLeft;
          RangeType diffValueRight;
          diffFlux_.linearNumericalFlux( intersection, insideEntity, outsideEntity, time(),
                                         faceQuadInside, faceQuadOutside, pt, normal, values_[ pt ], valuesNb_[ pt ],
                                         jacobians_[ pt ], jacobiansNb_[ pt ], uLeft, uRight, jacLeft, jacRight,
                                         diffValueLeft, diffValueRight, gDiffLeft, gDiffRight );
          gLeft += diffValueLeft;
          gRight += diffValueRight;
        }
      }


      // linearBoundaryFlux
      // ------------------

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &insideEntity,
                                const FaceQuadrature &faceQuadInside,
                                const int pt,
                                const DomainType &normal,
                                const RangeType &u,
                                const JacobianRangeType &jac,
                                RangeType &value,
                                JacobianRangeType &flux ) const
      {
        BaseType::linearBoundaryFlux( intersection, insideEntity, faceQuadInside, pt, normal, u, jac, value, flux );

        if( ModelType::hasDiffusion && hasBoundaryValues_ )
        {
          RangeType diffValue;
          diffFlux_.linearBoundaryFlux( intersection, insideEntity, time(),
                                        faceQuadInside, pt, normal, values_[ pt ], valuesNb_[ pt ],
                                        jacobians_[ pt ], u, uBnd_, jac, diffValue, flux );
          value += diffValue;
        }
      }


      // linearFluxAndSource
      // -------------------

      template< class Quadrature >
      void linearFluxAndSource ( const EntityType &entity,
                                 const Quadrature &quad,
                                 const int pt,
                                 const RangeType &u,
                                 const JacobianRangeType &jac,
                                 RangeType &source,
                                 JacobianRangeType &flux ) const
      {
        BaseType::linearFluxAndSource( entity, quad, pt, u, jac, source, flux );

        if( ModelType::hasDiffusion )
        {
          JacobianRangeType diffFlux;
          diffFlux_.linearDiffusion( entity, time(), quad, pt, values_[ pt ], jacobians_[ pt ], u, jac, diffFlux );
          flux -= diffFlux;
        }
      }

    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_WITHOUTLIFTING_HH
