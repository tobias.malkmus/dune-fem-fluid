#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_TIMESTEPESTIMATOR_HH
#define DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_TIMESTEPESTIMATOR_HH

#include <algorithm>
#include <limits>

namespace Dune
{

  namespace Fem
  {

    // LocalTimeStepEstimator
    // ----------------------

    template< bool >
    struct LocalTimeStepEstimator
    {
      double insideVolume_, outsideVolume_, fluxEst_, sourceEst_, minLimit_;

      LocalTimeStepEstimator () { reset(); }

      void reset ()
      {
        insideVolume_ = std::numeric_limits< double >::max();
        outsideVolume_ = std::numeric_limits< double >::max();
        sourceEst_ = std::numeric_limits< double >::max();
        minLimit_ = 2.0*std::numeric_limits< double >::min();
        fluxEst_ = std::numeric_limits< double >::max();
      }

      void setInsideVolume ( const double vol ) { insideVolume_ = vol; }
      void setOutsideVolume ( const double vol ) { outsideVolume_ = vol; }

      void intersectionEstimate ( const double est )
      {
        fluxEst_ = std::min( fluxEst_, std::min( insideVolume_, outsideVolume_ ) / est );
      }

      void sourceEstimate ( const double est )
      {
        if( est > minLimit_ )
          sourceEst_ = std::min( sourceEst_, est );
      }

      double estimate ()
      {
        return std::min( sourceEst_, fluxEst_ );
      }
    };


    template< >
    struct LocalTimeStepEstimator< false >
    {
      LocalTimeStepEstimator () {}
      void reset () {}
      void setInsideVolume ( const double ) {}
      void setOutsideVolume ( const double ) {}
      void intersectionEstimate ( const double ) {}
      void sourceEstimate ( const double ) {}
      double estimate () { return 0; }
    };

  } // namespace Fem

} // namespace Dune

#endif //#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_TIMESTEPESTIMATOR_HH
