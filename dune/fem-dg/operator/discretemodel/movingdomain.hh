#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_MOVINGBASE_HH
#define DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_MOVINGBASE_HH

#include <vector>

#include <dune/fem/function/localfunction/temporary.hh>
#include <dune/fem/function/localfunction/const.hh>

#include <dune/fem-dg/operator/discretemodel/default.hh>
#include <dune/fem-dg/operator/discretemodel/timestepestimator.hh>
#include <dune/fem-dg/operator/discretemodel/withlifting.hh>
#include <dune/fem-dg/operator/discretemodel/withoutlifting.hh>



namespace Dune
{

  namespace Fem
  {

    // MovingDomainDiscreteModelBase
    // -----------------------------

    template< class Traits, class DomainVelocityFunction >
    class MovingDomainDiscreteModelBase
    {
      typedef MovingDomainDiscreteModelBase< Traits, DomainVelocityFunction > ThisType;

    public:

      typedef typename Traits::ModelType ModelType;
      typedef typename Traits::NumFluxType NumFluxType;
      typedef typename Traits::DiffusionFluxType DiffusionFluxType;

      typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;
      typedef typename Traits::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      static const bool needsLiftings = DiffusionFluxType::needsLiftings;

      static const bool isEvolutionProblem = ModelType::isEvolutionProblem;

      typedef DomainVelocityFunction DomainVelocityFunctionType;


    protected:
      typedef TemporaryLocalFunction< DiscreteFunctionSpaceType > TemporaryLocalFunctionType;

      typedef LocalTimeStepEstimator< isEvolutionProblem > LocalTimeStepEstimatorType;

      typedef ConstLocalFunction< DomainVelocityFunctionType > LocalDomainVelocityFunctionType;

      typedef CachingQuadrature< typename DiscreteFunctionSpaceType::GridPartType, 0 > QuadratureType;
      typedef CachingQuadrature< typename DiscreteFunctionSpaceType::GridPartType, 1 > FaceQuadratureType;
    public:
      template< class ... Args >
      MovingDomainDiscreteModelBase ( const DiscreteFunctionSpaceType &space, const ModelType &model,
                                      const DomainVelocityFunctionType &domVelo, Args && ... args )
        : space_( space ),
          model_( model ),
          numFlux_( model_ ),
          diffFlux_( model_ ),
          time_( 0.0 ),
          dtEst_( std::numeric_limits< double >::max() ),
          uInside_( space_ ),
          uOutside_( space_ ),
          domainVelocityLocal_( domVelo ),
          numberOfElements_( 0 ),
          description_( "Advection flux: " +numFlux_.description() + " Diffusive Flux :" + diffFlux_.description() )
      {}

      ~MovingDomainDiscreteModelBase ()
      {
        arg_ = nullptr;
      }

      std::string description () const { return description_; }

      void setLambda ( const double lambda ) const { model().setLambda( lambda ); }

      void setRelaxFactor ( const double f ) const { model().setRelaxFactor( f ); }

      std::size_t numberOfElements () const { return numberOfElements_; }

      double timeStepEstimate () const { return dtEst_; }
      void setTime ( const double time ) { time_ = time; }
      double time () const { return time_; }

      void prepare ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const
      {
        dtEst_ =  std::numeric_limits< double >::max();
        numberOfElements_ = 0;
        arg_ = const_cast< DiscreteFunctionType * >(&u);
        w.clear();
      }

      void finalize ( DiscreteFunctionType &w ) const
      {
        w.communicate();
        arg_ = nullptr;
      }

      // initialize local function values
      void init ( const EntityType &entity ) const
      {
        numberOfElements_++;
        initLocalFunction( entity, uInside_ );
        hasBoundaryValues_ = false;

        domainVelocityLocal_.init( gridEntity( entity ) );

        localTimeStepEstimator_.reset();
        localTimeStepEstimator_.setInsideVolume( entity.geometry().volume() );
      }

      // compute u on each quadrature point
      template< class Quadrature >
      void initEntity ( const EntityType &entity, const Quadrature &quad, bool evalType ) const
      {
        initValues( uInside_, quad, values_ );
        initValues( uInside_, quad, jacobians_ );
        initValues( domainVelocityLocal_, quad, domVelo_ );
      }

      void finalizeEntity ( const EntityType &entity ) const
      {
        dtEst_ = std::min( dtEst_, localTimeStepEstimator_.estimate() );
      }

      // compute u on each quadrature point
      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const FaceQuadrature &faceQuadInside ) const
      {
        initValues( uInside_, faceQuadInside, values_ );
        initValues( domainVelocityLocal_, faceQuadInside, domVelo_ );

        // check wether we have dirichlet boundary values
        hasBoundaryValues_ = model().hasBoundaryValue( intersection );
        if( hasBoundaryValues_ )
        {
          // compute dirichlet boundary values and store them
          const std::size_t numQuadraturePoints = faceQuadInside.nop();
          valuesNb_.resize( numQuadraturePoints );
          for( std::size_t pt = 0; pt < numQuadraturePoints; ++pt )
            model().boundaryValue( intersection, time(), faceQuadInside.localPoint( pt ), domVelo_[ pt ], values_[ pt ], jacobians_[ pt ], valuesNb_[ pt ] );
        }

        if( !needsLiftings )
          initValues( uInside_, faceQuadInside, jacobians_ );

        if( needsLiftings && ( !hasBoundaryValues_ ) )
          initValues( uInside_, faceQuadInside, jacobians_ );

        diffFlux_.initIntersection( intersection, inside, faceQuadInside );
      }


      // compute u on each quadrature point
      template< class FaceQuadrature >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                              const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside ) const
      {
        //! note domain velocity is assumed to be continues
        initValues( domainVelocityLocal_, faceQuadInside, domVelo_ );

        initLocalFunction( outside, uOutside_ );
        localTimeStepEstimator_.setOutsideVolume( outside.geometry().volume() );

        initValues( uInside_, faceQuadInside, values_ );
        initValues( uOutside_, faceQuadOutside, valuesNb_ );

        if( !needsLiftings )
        {
          initValues( uInside_, faceQuadInside, jacobians_ );
          initValues( uOutside_, faceQuadOutside, jacobiansNb_ );
        }

        diffFlux_.initIntersection( intersection, inside, outside, faceQuadInside, faceQuadOutside );
      }


      // numericalFlux
      // -------------

      template< class FaceQuadrature >
      void numericalFlux ( const IntersectionType &intersection,
                           const EntityType &insideEntity,
                           const EntityType &outsideEntity,
                           const FaceQuadrature &faceQuadInside,
                           const FaceQuadrature &faceQuadOutside,
                           const int pt,
                           const DomainType &normal,
                           RangeType &value,
                           JacobianRangeType &flux ) const
      {
        value = 0;
        flux = 0;
        double advSpeed = ( ModelType::hasFlux ) ?
                          numFlux_.numericalFlux( intersection, insideEntity, outsideEntity, time(), faceQuadInside, faceQuadOutside, pt,
                                                  normal, domVelo_[ pt ], values_[ pt ], valuesNb_[ pt ], value )
                          : 0;

        // weak source
        if( ModelType::hasWeakSource )
        {
          JacobianRangeType weakSource;
          model().weakSource( insideEntity, time(), faceQuadInside.point( pt ), weakSource );
          weakSource.usmv( -1.0, normal, value );
        }

        localTimeStepEstimator_.intersectionEstimate( advSpeed * faceQuadInside.weight( pt ) );
      }


      // boundaryFlux
      // ------------

      template< class FaceQuadrature >
      void boundaryFlux ( const IntersectionType &intersection,
                          const EntityType &insideEntity,
                          const FaceQuadrature &faceQuadInside,
                          const int pt,
                          const DomainType &normal,
                          RangeType &value,
                          JacobianRangeType &flux ) const
      {
        value = 0;
        flux = 0;

        if( hasBoundaryValues_ )
        {
          double advSpeed = ( ModelType::hasFlux ) ?
                            numFlux_.boundaryFlux( intersection, insideEntity, time(), faceQuadInside, pt,
                                                   normal, domVelo_[ pt ], values_[ pt ], valuesNb_[ pt ], value )
                            : dtEst_;

          localTimeStepEstimator_.intersectionEstimate( advSpeed * faceQuadInside.weight( pt ) );
        }
        else
        {
          double wSpeed = model().boundaryFlux( intersection, insideEntity, time(), faceQuadInside.localPoint( pt ), normal,
                                                domVelo_[ pt ], values_[ pt ], jacobians_[ pt ], value );

          localTimeStepEstimator_.intersectionEstimate( wSpeed * faceQuadInside.weight( pt ) );
        }

        // weak source
        if( ModelType::hasWeakSource )
        {
          JacobianRangeType weakSource;
          model().weakSource( insideEntity, time(), faceQuadInside.point( pt ), weakSource );
          weakSource.usmv( -1.0, normal, value );
        }
      }


      // fluxAndSource
      // -------------

      template< class Quadrature >
      void fluxAndSource ( const EntityType &entity,
                           const Quadrature &quad,
                           const int pt,
                           RangeType &source,
                           JacobianRangeType &flux ) const
      {
        flux = 0;
        source = 0;
        // advective part
        if( ModelType::hasFlux )
          model().advection( entity, time(), quad.point( pt ), domVelo_[ pt ], values_[ pt ], flux );

        // weak source
        if( ModelType::hasWeakSource )
        {
          JacobianRangeType weakSource;
          model().weakSource( entity, time(), quad.point( pt ), weakSource );
          flux -= weakSource;
        }

        // source terms
        const double dtSourceEst = ( ModelType::hasSource ) ?
                                   model().source( entity, time(), quad.point( pt ), domVelo_[ pt ], values_[ pt ], jacobians_[ pt ], source )
                                   : 0;
        localTimeStepEstimator_.sourceEstimate( dtSourceEst );
      }


      // will be called by the integrator to add the mass term in evolution problems
      template< class Quadrature >
      void addScaledMassTerm ( const EntityType &entity, const Quadrature &quad, const int pt, const double scale, RangeType &source ) const
      {
        if( ModelType::hasMass )
          model().addScaledMassTerm( entity, time(), quad.point( pt ), scale, values_[ pt ], source );
      }


      const DiscreteFunctionSpaceType &space () const { return space_; }

    protected:
      // initialize localfunction on entity
      void initLocalFunction ( const EntityType &entity, TemporaryLocalFunctionType &lf ) const
      {
        lf.init( entity );
        lf.clear();
        assert( arg_ );
        arg_->getLocalDofs( entity, lf.localDofVector() );
      }

      // compute localfunction on each quadrature point
      template< class LocalFunction, class Quadrature, class RangeVector >
      void initValues ( const LocalFunction &localFunction, const Quadrature &quad,
                        RangeVector &ranges ) const
      {
        ranges.resize( quad.nop());
        localFunction.evaluateQuadrature( quad, ranges );
      }

      const ModelType &model () const { return model_; }

      const DiscreteFunctionSpaceType &space_;
      const ModelType &model_;

      NumFluxType numFlux_;
      DiffusionFluxType diffFlux_;

      mutable bool hasBoundaryValues_;
      mutable double time_;
      mutable double dtEst_;
      mutable LocalTimeStepEstimatorType localTimeStepEstimator_;

      mutable RangeType uBnd_;
      mutable std::vector< RangeType > values_, valuesNb_;
      mutable std::vector< JacobianRangeType > jacobians_, jacobiansNb_;

      mutable DiscreteFunctionType *arg_;

      mutable TemporaryLocalFunctionType uInside_;
      mutable TemporaryLocalFunctionType uOutside_;

      mutable LocalDomainVelocityFunctionType domainVelocityLocal_;
      mutable std::vector< DomainType > domVelo_;

      mutable std::size_t numberOfElements_;
      mutable int sequence_;
      std::string description_;
    };




    // DifferentiableMovingDomainDiscreteModelBase
    // -------------------------------------------

    template< class Traits, class DomainVelocityFunction, class B = MovingDomainDiscreteModelBase< Traits, DomainVelocityFunction > >
    class DifferentiableMovingDomainDiscreteModelBase
      : public B
    {
      typedef DifferentiableMovingDomainDiscreteModelBase< Traits, DomainVelocityFunction, B > ThisType;
      typedef B BaseType;

    public:

      typedef typename BaseType::ModelType ModelType;
      typedef typename BaseType::NumFluxType NumFluxType;
      typedef typename BaseType::DiffusionFluxType DiffusionFluxType;

      typedef typename Traits::StencilType StencilType;
      typedef typename Traits::JacobianOperatorType JacobianOperatorType;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      static const bool needsLiftings = DiffusionFluxType::needsLiftings;

      static const bool isEvolutionProblem = ModelType::isEvolutionProblem;

    protected:
      using BaseType::diffFlux_;
      using BaseType::numFlux_;
      using BaseType::arg_;

      using BaseType::model;
      using BaseType::uBnd_;
      using BaseType::values_;
      using BaseType::valuesNb_;
      using BaseType::jacobians_;
      using BaseType::jacobiansNb_;

      using BaseType::hasBoundaryValues_;
      using BaseType::domVelo_;

    public:

      using BaseType::space;
      using BaseType::time;

      template< class ... Args >
      DifferentiableMovingDomainDiscreteModelBase ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... ),
          stencil_( new StencilType( space(), space() ) ),
          sequence_( space().sequence() )
      {}

      using BaseType::prepare;
      using BaseType::finalize;

      void prepare ( const DiscreteFunctionType &u, JacobianOperatorType &jac ) const
      {
        arg_ = const_cast< DiscreteFunctionType * >(&u);

        jac.reserve( stencil() );
        jac.clear();
      }

      void finalize ( JacobianOperatorType &jac ) const
      {
        jac.communicate();
        arg_ = nullptr;
      }

      // linearNumericalFlux
      // -------------------

      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &insideEntity,
                                 const EntityType &outsideEntity,
                                 const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside,
                                 const int pt,
                                 const DomainType &normal,
                                 const RangeType &uLeft,
                                 const RangeType &uRight,
                                 const JacobianRangeType &jacLeft,
                                 const JacobianRangeType &jacRight,
                                 RangeType &gLeft,
                                 RangeType &gRight,
                                 JacobianRangeType &gDiffLeft,
                                 JacobianRangeType &gDiffRight ) const
      {
        gLeft = 0.; gRight = 0.; gDiffLeft = 0.; gDiffRight = 0.;

        if( ModelType::hasFlux )
          numFlux_.linearNumericalFlux( intersection, insideEntity, outsideEntity,
                                        time(), faceQuadInside, faceQuadOutside, pt, normal, domVelo_[ pt ],
                                        values_[ pt ], valuesNb_[ pt ], uLeft, uRight, gLeft, gRight );
      }


      // linearBoundaryFlux
      // ------------------

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &insideEntity,
                                const FaceQuadrature &faceQuadInside,
                                const int pt,
                                const DomainType &normal,
                                const RangeType &u,
                                const JacobianRangeType &jac,
                                RangeType &value,
                                JacobianRangeType &flux ) const
      {
        value = 0; flux = 0;

        if( hasBoundaryValues_ )
        {
          model().linearBoundaryValue( intersection, time(), faceQuadInside.localPoint( pt ), domVelo_[ pt ],
                                       values_[ pt ], jacobians_[ pt ], u, jac, uBnd_ );

          if( ModelType::hasFlux )
            numFlux_.linearBoundaryFlux( intersection, insideEntity,
                                         time(), faceQuadInside, pt, normal, domVelo_[ pt ],
                                         values_[ pt ], valuesNb_[ pt ], u, uBnd_, value );
        }
        else
          model().linearBoundaryFlux( intersection, insideEntity, time(), faceQuadInside.localPoint( pt ), normal, domVelo_[ pt ],
                                      values_[ pt ], jacobians_[ pt ], u, jac, value );
      }


      // linearFluxAndSource
      // -------------------

      template< class Quadrature >
      void linearFluxAndSource ( const EntityType &entity,
                                 const Quadrature &quad,
                                 const int pt,
                                 const RangeType &u,
                                 const JacobianRangeType &jac,
                                 RangeType &source,
                                 JacobianRangeType &flux ) const
      {
        source = 0.;
        flux = 0.;
        if( ModelType::hasFlux )
          model().linearAdvection( entity, time(), quad.point( pt ), domVelo_[ pt ], values_[ pt ], u, flux );

        if( ModelType::hasSource )
          model().linearSource( entity, time(), quad.point( pt ), domVelo_[ pt ], values_[ pt ], jacobians_[ pt ], u, jac, source );
      }

      using BaseType::addScaledMassTerm;

      template< class Quadrature >
      void addScaledMassTerm ( const EntityType &entity, const Quadrature &quad, const int pt, const double scale,
                               const RangeType &u, RangeType &source ) const
      {
        if( ModelType::hasMass )
          model().addScaledMassTerm( entity, time(), quad.point( pt ), scale, u, source );
      }

    protected:

      const StencilType &stencil () const
      {
        if( !stencil_ || sequence_ != space().sequence() )
          stencil_.reset( new StencilType( space(), space() ) );
        return *stencil_;
      }

      mutable std::unique_ptr< StencilType > stencil_;
      mutable int sequence_;
    };




    // __MovingModel
    // -------------

    namespace __MovingModel
    {

      template< bool isDiscreteFunction, class Range, class N, class D, class Dv >
      struct Switch
      {
        typedef __DiscreteModelTraits::Default< Range, N, D > Traits;
        typedef typename std::conditional< Traits::needsLiftings,
                                           DiscreteModelWithLiftings< Traits, MovingDomainDiscreteModelBase< Traits, Dv > >,
                                           DiscreteModelWithoutLiftings< Traits, MovingDomainDiscreteModelBase< Traits, Dv > > >::type Type;
      };


      template< class Op, class N, class D, class Dv >
      struct Switch< false, Op, N, D, Dv >
      {
        typedef __DiscreteModelTraits::Differentiable< Op, N, D > Traits;
        typedef typename std::conditional< Traits::needsLiftings,
                                           DifferentiableDiscreteModelWithLiftings< Traits, DifferentiableMovingDomainDiscreteModelBase< Traits, Dv > >,
                                           DifferentiableDiscreteModelWithoutLiftings< Traits, DifferentiableMovingDomainDiscreteModelBase< Traits, Dv > > >::type Type;
      };

    }



    // MovingDomainDefaultDiscreteModel
    // --------------------------------

    template< class Range, class NumFlux, class DiffFlux, class DomainVelocityFunction >
    class MovingDomainDefaultDiscreteModel
      : public __MovingModel::Switch< std::is_base_of< IsDiscreteFunction, Range >::value, Range, NumFlux, DiffFlux, DomainVelocityFunction >::Type
    {
      typedef MovingDomainDefaultDiscreteModel< Range, NumFlux, DiffFlux, DomainVelocityFunction > ThisType;
      typedef typename __MovingModel::Switch< std::is_base_of< IsDiscreteFunction, Range >::value, Range, NumFlux, DiffFlux, DomainVelocityFunction >::Type BaseType;

    public:
      template< class ... Args >
      MovingDomainDefaultDiscreteModel ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... ) {}
    };


  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_MOVINGBASE_HH
