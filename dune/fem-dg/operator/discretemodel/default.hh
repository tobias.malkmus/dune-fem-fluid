#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_DEFAULT_HH
#define DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_DEFAULT_HH

#include <dune/fem/function/common/discretefunction.hh>

#include <dune/fem-dg/operator/discretemodel/withlifting.hh>
#include <dune/fem-dg/operator/discretemodel/withoutlifting.hh>


namespace Dune
{

  namespace Fem
  {

    // DiscreteModelTraits
    // -------------------

    namespace __DiscreteModelTraits
    {

      template< class DiscreteFunction, class NumFlux, class DiffFlux >
      struct Default
      {
        typedef typename NumFlux::ModelType ModelType;
        typedef NumFlux NumFluxType;
        typedef DiffFlux DiffusionFluxType;
        typedef DiscreteFunction DiscreteFunctionType;
        typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

        static const bool needsLiftings = DiffFlux::needsLiftings;
      };

      template< class Op, class NumFlux, class DiffFlux >
      struct Differentiable
        : public Default< typename Op::RangeFunctionType, NumFlux, DiffFlux >
      {
        typedef Op JacobianOperatorType;
        typedef typename DiffFlux::template Stencil<
          typename JacobianOperatorType::DomainFunctionType::DiscreteFunctionSpaceType,
          typename JacobianOperatorType::RangeFunctionType::DiscreteFunctionSpaceType
          >::Type StencilType;
      };

    }


    // choose correct model

    namespace __Model
    {

      template< bool isDiscreteFunction, class Range, class N, class D >
      struct Switch
      {
        typedef __DiscreteModelTraits::Default< Range, N, D > Traits;

        typedef typename std::conditional< Traits::needsLiftings,
                                           DiscreteModelWithLiftings< Traits >,
                                           DiscreteModelWithoutLiftings< Traits > >::type Type;
      };


      template< class Op, class N, class D >
      struct Switch< false, Op, N, D >
      {
        typedef __DiscreteModelTraits::Differentiable< Op, N, D > Traits;

        typedef typename std::conditional< Traits::needsLiftings,
                                           DifferentiableDiscreteModelWithLiftings< Traits >,
                                           DifferentiableDiscreteModelWithoutLiftings< Traits > >::type Type;
      };

    }


    // DiscreteModelDefault
    // --------------------

    template< class Range, class NumFlux, class DiffFlux >
    class DiscreteModelDefault
      : public __Model::Switch< std::is_base_of< IsDiscreteFunction, Range >::value, Range, NumFlux, DiffFlux  >::Type
    {
      typedef DiscreteModelDefault< Range, NumFlux, DiffFlux > ThisType;
      typedef typename __Model::Switch< std::is_base_of< IsDiscreteFunction, Range >::value, Range, NumFlux, DiffFlux  >::Type BaseType;

    public:

      template< class ... Args >
      DiscreteModelDefault ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... ) {}
    };

  } // namespace Fem

} // namespace Dune
#endif //#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_DEFAULT_HH
