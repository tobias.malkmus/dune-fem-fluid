#ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_DIFFERENTIABLEBASE_HH
#define DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_DIFFERENTIABLEBASE_HH

#include <memory>

#include <dune/fem-dg/operator/discretemodel/base.hh>

namespace Dune
{

  namespace Fem
  {

    // DifferentiableDiscreteModelBase
    // -------------------------------

    template< class Traits, class B = DiscreteModelBase< Traits > >
    class DifferentiableDiscreteModelBase
      : public B
    {
      typedef DifferentiableDiscreteModelBase< Traits > ThisType;
      typedef B BaseType;

    public:

      typedef typename BaseType::ModelType ModelType;
      typedef typename BaseType::NumFluxType NumFluxType;
      typedef typename BaseType::DiffusionFluxType DiffusionFluxType;

      typedef typename Traits::StencilType StencilType;
      typedef typename Traits::JacobianOperatorType JacobianOperatorType;

      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

      typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
      typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
      typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;

      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      static const bool needsLiftings = DiffusionFluxType::needsLiftings;

      static const bool isEvolutionProblem = ModelType::isEvolutionProblem;

    protected:
      using BaseType::diffFlux_;
      using BaseType::numFlux_;
      using BaseType::arg_;

      using BaseType::model;
      using BaseType::uBnd_;
      using BaseType::values_;
      using BaseType::valuesNb_;
      using BaseType::jacobians_;
      using BaseType::jacobiansNb_;

      using BaseType::hasBoundaryValues_;

    public:

      using BaseType::space;
      using BaseType::time;

      template< class ... Args >
      DifferentiableDiscreteModelBase ( Args && ... args )
        : BaseType( std::forward< Args >( args ) ... ),
          stencil_( new StencilType( space(), space() ) ),
          sequence_( space().sequence() )
      {}

      ~DifferentiableDiscreteModelBase ()
      {
        arg_ = nullptr;
      }

      using BaseType::prepare;
      using BaseType::finalize;

      void prepare ( const DiscreteFunctionType &u, JacobianOperatorType &jac ) const
      {
        arg_ = const_cast< DiscreteFunctionType * >(&u);

        jac.reserve( stencil() );
        jac.clear();
      }

      void finalize ( JacobianOperatorType &jac ) const
      {
        jac.communicate();
        arg_ = nullptr;
      }

      // linearNumericalFlux
      // -------------------

      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &insideEntity,
                                 const EntityType &outsideEntity,
                                 const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside,
                                 const int pt,
                                 const DomainType &normal,
                                 const RangeType &uLeft,
                                 const RangeType &uRight,
                                 const JacobianRangeType &jacLeft,
                                 const JacobianRangeType &jacRight,
                                 RangeType &gLeft,
                                 RangeType &gRight,
                                 JacobianRangeType &gDiffLeft,
                                 JacobianRangeType &gDiffRight ) const
      {
        gLeft = 0.; gRight = 0.; gDiffLeft = 0.; gDiffRight = 0.;

        if( ModelType::hasFlux )
          numFlux_.linearNumericalFlux( intersection, insideEntity, outsideEntity,
                                        time(), faceQuadInside, faceQuadOutside, pt, normal,
                                        values_[ pt ], valuesNb_[ pt ], uLeft, uRight, gLeft, gRight );
      }


      // linearBoundaryFlux
      // ------------------

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &insideEntity,
                                const FaceQuadrature &faceQuadInside,
                                const int pt,
                                const DomainType &normal,
                                const RangeType &u,
                                const JacobianRangeType &jac,
                                RangeType &value,
                                JacobianRangeType &flux ) const
      {
        value = 0; flux = 0;

        if( hasBoundaryValues_ )
        {
          model().linearBoundaryValue( intersection, time(), faceQuadInside.localPoint( pt ), values_[ pt ], jacobians_[ pt ],
                                       u, jac, uBnd_ );

          if( ModelType::hasFlux )
            numFlux_.linearBoundaryFlux( intersection, insideEntity,
                                         time(), faceQuadInside, pt, normal,
                                         values_[ pt ], valuesNb_[ pt ], u, uBnd_, value );
        }
        else
          model().linearBoundaryFlux( intersection, insideEntity, time(), faceQuadInside.localPoint( pt ), normal,
                                      values_[ pt ], jacobians_[ pt ], u, jac, value );
      }


      // linearFluxAndSource
      // -------------------

      template< class Quadrature >
      void linearFluxAndSource ( const EntityType &entity,
                                 const Quadrature &quad,
                                 const int pt,
                                 const RangeType &u,
                                 const JacobianRangeType &jac,
                                 RangeType &source,
                                 JacobianRangeType &flux ) const
      {
        source = 0.;
        flux = 0.;
        if( ModelType::hasFlux )
          model().linearAdvection( entity, time(), quad.point( pt ), values_[ pt ], u, flux );

        if( ModelType::hasSource )
          model().linearSource( entity, time(), quad.point( pt ), values_[ pt ], jacobians_[ pt ], u, jac, source );
      }

      using BaseType::addScaledMassTerm;

      template< class Quadrature >
      void addScaledMassTerm ( const EntityType &entity, const Quadrature &quad, const int pt, const double scale,
                               const RangeType &u, RangeType &source ) const
      {
        if( ModelType::hasMass )
          model().addScaledMassTerm( entity, time(), quad.point( pt ), scale, u, source );
      }

    protected:

      const StencilType &stencil () const
      {
        if( !stencil_ || sequence_ != space().sequence() )
          stencil_.reset( new StencilType( space(), space() ) );
        return *stencil_;
      }

      mutable std::unique_ptr< StencilType > stencil_;
      mutable int sequence_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_OPERATOR_DISCRETEMODEL_DIFFERENTIABLEBASE_HH
