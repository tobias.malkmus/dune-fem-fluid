add_subdirectory(dg)
add_subdirectory(discretemodel)
add_subdirectory(fluxes)

add_subdirectory(test EXCLUDE_FROM_ALL)
