#ifndef DUNE_FEM_DG_OPERATOR_FLUXES_SKEWSYMMETRYNSMOVING_HH
#define DUNE_FEM_DG_OPERATOR_FLUXES_SKEWSYMMETRYNSMOVING_HH

#include <dune/common/diagonalmatrix.hh>


namespace Dune
{

  namespace Fem
  {

    template< class Model >
    class SkewSymetricNSFlux
    {
      typedef SkewSymetricNSFlux< Model >  ThisType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      typedef typename ModelType::RangeFieldType RangeFieldType;
      static const int dimRange = ModelType::dimRange;

      typedef DiagonalMatrix< RangeFieldType, dimRange > EigenValueMatrixType;

      SkewSymetricNSFlux ( const ModelType &model )
        : model_( model )
      {}

      std::string description () const { return std::string( "Mean Value Flux" ); }

      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside,
                             const int pt, const DomainType &normal, const DomainType &domainVelocity,
                             const RangeType &uLeft, const RangeType &uRight, RangeType &flux ) const
      {
        const double len = normal.two_norm();

        JacobianRangeType anaFlux;
        model_.advection( inside, time, faceQuadInside.point( pt ), domainVelocity, uLeft, anaFlux );
        anaFlux.mv( normal, flux );

        model_.advection( outside, time, faceQuadOutside.point( pt ), domainVelocity, uRight, anaFlux );
        anaFlux.umv( normal, flux );

        RangeType visc( uRight );
        visc -= uLeft;

        EigenValueMatrixType ev;
        model_.eigenValues( intersection, time, faceQuadInside.point( pt ), normal, uLeft, ev.diagonal() );
        ev.usmv( -len, visc, flux );
        double maxSpeed = ev.infinity_norm();

        model_.eigenValues( intersection, time, faceQuadOutside.point( pt ), normal, uRight, ev.diagonal() );
        ev.usmv( -len, visc, flux );
        maxSpeed = std::max( maxSpeed, ev.infinity_norm() );

        JacobianRangeType normalJump( 0 );
        for( int i = 0; i < dimRange; ++i )
          normalJump[ i ].axpy( -visc[ i ], normal );

        RangeType sourceFlux;
        model_.stiffSource( inside, time, faceQuadInside.point( pt ), domainVelocity, uLeft, normalJump, sourceFlux );
        flux -= sourceFlux;

        flux *= 0.5;
        return len * maxSpeed;
      }

      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection, const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt, const DomainType &normal, const DomainType &domainVelocity,
                            const RangeType &uLeft, const RangeType &uBnd, RangeType &flux ) const
      {
        const double len = normal.two_norm();

        JacobianRangeType anaFlux;
        model_.advection( inside, time, faceQuadInside.point( pt ), domainVelocity, uBnd, anaFlux );
        anaFlux.mv( normal, flux );

        RangeType visc( uBnd );
        visc -= uLeft;

        EigenValueMatrixType ev;
        model_.eigenValues( intersection, time, faceQuadInside.point( pt ), normal, uBnd, ev.diagonal() );
        ev.usmv( -len, visc, flux );
        double maxSpeed = ev.infinity_norm();

        JacobianRangeType normalJump( 0 );
        for( int i = 0; i < dimRange; ++i )
          normalJump[ i ].axpy( -visc[ i ], normal );

        RangeType sourceFlux;
        model_.stiffSource( inside, time, faceQuadInside.point( pt ), domainVelocity, uBnd, normalJump, sourceFlux );
        flux -= sourceFlux;

        return len * maxSpeed;
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside,
                                 const int pt, const DomainType &normal, const DomainType &domainVelocity,
                                 const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 RangeType &gLeft, RangeType &gRight ) const
      {
        const double len = normal.two_norm();

        JacobianRangeType normalJumpLeft( 0 ), normalJumpRight( 0 ), normalJumpStar( 0 );
        JacobianRangeType null( 0 );
        for( int r = 0; r < dimRange; ++r )
        {
          normalJumpStar[ r ].axpy( uStarLeft[ r ] - uStarRight[ r ], normal );
          normalJumpLeft[ r ].axpy( uLeft[ r ], normal );
          normalJumpRight[ r ].axpy( -uRight[ r ], normal );
        }

        JacobianRangeType anaFlux;
        model_.linearAdvection( inside, time, faceQuadInside.point( pt ), domainVelocity, uStarLeft, uLeft, anaFlux );
        anaFlux.mv( normal, gLeft );

        EigenValueMatrixType ev;
        model_.eigenValues( intersection, time, faceQuadInside.point( pt ), normal, uStarLeft, uLeft, ev.diagonal() );
        ev.usmv( 2.0 * len, uLeft, gLeft );

        RangeType sourceFlux;
        model_.linearStiffSource( inside, time, faceQuadInside.point( pt ), domainVelocity, uStarLeft, normalJumpStar, uLeft, normalJumpLeft, sourceFlux );
        gLeft -= sourceFlux;

        gLeft *= 0.5;

        model_.linearAdvection( outside, time, faceQuadOutside.point( pt ), domainVelocity, uStarRight, uRight, anaFlux );
        anaFlux.mv( normal, gRight );

        model_.eigenValues( intersection, time, faceQuadOutside.point( pt ), normal, uStarRight, uRight, ev.diagonal() );
        ev.usmv( -2.0 * len, uRight, gRight );

        model_.linearStiffSource( inside, time, faceQuadInside.point( pt ), domainVelocity, uStarLeft, null, uLeft, normalJumpRight, sourceFlux );
        gRight -= sourceFlux;

        gRight *= 0.5;

      }

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection, const EntityType &inside,
                                const double time, const FaceQuadrature &faceQuadInside,
                                const int pt, const DomainType &normal, const DomainType &domainVelocity,
                                const RangeType &uStarLeft, const RangeType &uStarBnd, 
                                const RangeType &uLeft, const RangeType &uBnd, RangeType &flux ) const
      {
        const double len = normal.two_norm();

        JacobianRangeType anaFlux;
        model_.linearAdvection( inside, time, faceQuadInside.point( pt ), domainVelocity, uStarBnd, uBnd, anaFlux );
        anaFlux.mv( normal, flux );

        RangeType visc( uBnd );
        visc -= uLeft;

        EigenValueMatrixType ev;
        model_.eigenValues( intersection, time, faceQuadInside.point( pt ), normal, uStarBnd, uBnd, ev.diagonal() );
        ev.usmv( -len, visc, flux );

        JacobianRangeType normalJump( 0 ), normalJumpStar( 0 );
        for( int r = 0; r < dimRange; ++r )
        {
          normalJumpStar[ r ].axpy( uStarLeft[ r ] - uStarBnd[ r ], normal );
          normalJump[ r ].axpy( -visc[ r ], normal );
        }

        RangeType sourceFlux;
        model_.linearStiffSource( inside, time, faceQuadInside.point( pt ), domainVelocity, uStarBnd, normalJumpStar, uBnd, normalJump, sourceFlux );
        flux -= sourceFlux;
      }

    private:
      const ModelType &model_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_OPERATOR_FLUXES_SKEWSYMMETRYNSMOVING_HH
