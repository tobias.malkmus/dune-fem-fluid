#ifndef DUNE_FEM_OPERATOR_FLUXES_IPDG_HH
#define DUNE_FEM_OPERATOR_FLUXES_IPDG_HH

#include <dune/fem/io/parameter.hh>
#include <dune/fem/operator/common/stencil.hh>

#include <dune/fem-dg/operator/fluxes/common.hh>

namespace Dune
{

  namespace Fem
  {

    // IpDgFlux
    // --------
    //
    // Most general IPDG implementation,
    // which assumes a fully non linear diffusion operator
    //
    // D( u, grad u )
    //

    template< class Model >
    class IpDgFlux
    {
      typedef IpDgFlux< Model > ThisType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;
      typedef typename ModelType::RangeFieldType RangeFieldType;

      static const int dimRange = ModelType::dimRange;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      static const bool needsLiftings = false;

      template< class DomainSpace, class RangeSpace >
      struct Stencil
      {
        typedef DiagonalAndNeighborStencil< DomainSpace, RangeSpace > Type;
      };

      IpDgFlux ( const ModelType &model )
        : model_( model ),
        penalty_( Parameter::getValue< double >( "ipdg.penalty", 7.5 ) )
      {}

      const ModelType &model () const { return model_; }

      template< class ... Args >
      void init ( Args && ... args ) const {}

      template< class ... Args >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside,
                              const EntityType &outside, Args && ... args ) const
      {
        Tmin = std::min( inside.geometry().volume(), outside.geometry().volume() );
      }

      template< class ... Args >
      void initIntersection ( const IntersectionType &intersection, const EntityType &inside, Args && ... args ) const
      {
        Tmin = inside.geometry().volume();
      }

      template< class Quadtrature >
      void linearDiffusion ( const EntityType &entity, const double time, const Quadtrature &quad, const int pt,
                             const RangeType &uStar, const JacobianRangeType &jacStar, const RangeType &u, const JacobianRangeType &jac,
                             JacobianRangeType &d ) const
      {
        model().linearDiffusion( entity, time, quad.point( pt ), uStar, jacStar, u, jac, d );
      }

      std::string description () const { return std::string( "IP method with penalty: " + std::to_string( penalty_ ) ); }

      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection,
                             const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside,
                             const FaceQuadrature &faceQuadOutside, const int pt,
                             const DomainType &normal,
                             const RangeType &uLeft, const RangeType &uRight,
                             const JacobianRangeType &jacLeft, const JacobianRangeType &jacRight,
                             RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        const double len = normal.two_norm();

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        model().diffusion( outside, time, faceQuadOutside.point( pt ), uRight, jacRight, diffFlux );
        diffFlux.mmv( normal, flux );

        JacobianRangeType jump = normalJump( uLeft, uRight, normal, 1.0/Tmin );

        // outside
        model().diffusion( outside, time, faceQuadOutside.point( pt ), uRight, jump, jacRight, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );

        // inside
        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jump, jacLeft, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );

        diffFlux *= -0.5 * Tmin;
        flux *= 0.5;

        return len;
      }


      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection,
                            const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt,
                            const DomainType &normal,
                            const RangeType &uLeft, const RangeType &uBnd,
                            const JacobianRangeType &jacLeft,
                            RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        const double len = normal.two_norm();

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        JacobianRangeType jump = normalJump( uLeft, uBnd, normal, 1.0/Tmin );

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jump, jacLeft, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );

        diffFlux *= -1.0 * Tmin;

        return len;
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside, const int pt,
                                 const DomainType &normal,
                                 const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const JacobianRangeType &jacStarLeft, const JacobianRangeType &jacStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 const JacobianRangeType &jacLeft, const JacobianRangeType &jacRight,
                                 RangeType &gLeft, RangeType &gRight,
                                 JacobianRangeType &gDiffLeft, JacobianRangeType &gDiffRight ) const
      {
        const RangeType null( 0 );
        const JacobianRangeType Null( 0.0 );

        gLeft = 0;
        gRight = 0;

        // left
        model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, jacStarLeft, uLeft, jacLeft, gDiffLeft );
        gDiffLeft.mmv( normal, gLeft );

        // right
        model().linearDiffusion( outside, time, faceQuadOutside.point( pt ), uStarRight, jacStarRight, uRight, jacRight, gDiffRight );
        gDiffRight.mmv( normal, gRight );

        JacobianRangeType jumpLeft = normalJump( uLeft, null, normal, 1.0 / Tmin );
        JacobianRangeType jumpRight = normalJump( null, uRight, normal, 1.0 / Tmin );
        JacobianRangeType jumpStar = normalJump( uStarLeft, uStarRight, normal, 1.0 / Tmin );

        auto linDiffOut = model().linearDiffusion( outside, time, faceQuadOutside.point( pt ), uStarRight, jumpStar, jacStarRight );
        auto linDiffIn = model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, jumpStar, jacStarLeft );

        // right
        linDiffOut( uRight, jumpRight, jacRight, gDiffRight );
        gDiffRight.usmv( penalty_, normal, gRight );

        linDiffIn( null, jumpRight, Null, gDiffRight );
        gDiffRight.usmv( penalty_, normal, gRight );

        // left
        linDiffOut( null, jumpLeft, Null, gDiffLeft );
        gDiffLeft.usmv( penalty_, normal, gLeft );

        linDiffIn( uLeft, jumpLeft, jacLeft, gDiffLeft );
        gDiffLeft.usmv( penalty_, normal, gLeft );

        gDiffLeft *= -0.5 * Tmin;
        gDiffRight *= -0.5* Tmin;
        gLeft *= 0.5;
        gRight *= 0.5;
      }


      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &inside, const double time,
                                const FaceQuadrature &faceQuadInside, const int pt, const DomainType &normal,
                                const RangeType &uStarLeft, const RangeType &uStarBnd, const JacobianRangeType &jacStarLeft,
                                const RangeType &uLeft, const RangeType &uBnd, const JacobianRangeType &jacLeft,
                                RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, jacStarLeft, uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        model().linearDiffusion( inside, time, faceQuadInside.point( pt ),
            uStarLeft, normalJump( uStarLeft, uStarBnd, normal, 1.0 / Tmin ), jacStarLeft,
            uLeft, normalJump( uLeft, uBnd, normal, 1.0 / Tmin ), jacLeft, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );
        diffFlux *= -1.0 * Tmin;
      }

    private:
      const ModelType &model_;

    protected:
      const double penalty_;
      mutable double Tmin;
    };



    // SemiLinearIpDgFlux
    // ------------------
    //
    //
    // IPDG implementation for diffusion operators D( u, grad u )
    // which are linear in grad u and non-linear in u
    //

    template< class Model >
    class SemiLinearIpDgFlux
      : public IpDgFlux< Model >
    {
      typedef SemiLinearIpDgFlux< Model > ThisType;
      typedef IpDgFlux< Model > BaseType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;
      typedef typename ModelType::RangeFieldType RangeFieldType;

      static const int dimRange = ModelType::dimRange;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      static const bool needsLiftings = false;

      using BaseType::Stencil;
      using BaseType::model;

    protected:
      using BaseType::Tmin;
      using BaseType::penalty_;

    public:

      SemiLinearIpDgFlux ( const ModelType &model ) : BaseType( model ) {}

      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection,
                             const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside,
                             const FaceQuadrature &faceQuadOutside, const int pt,
                             const DomainType &normal,
                             const RangeType &uLeft, const RangeType &uRight,
                             const JacobianRangeType &jacLeft, const JacobianRangeType &jacRight,
                             RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        const double len = normal.two_norm();

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        model().diffusion( outside, time, faceQuadOutside.point( pt ), uRight, jacRight, diffFlux );
        diffFlux.mmv( normal, flux );

        JacobianRangeType jump = normalJump( uLeft, uRight, normal, 1.0/Tmin );

        // outside
        model().diffusion( outside, time, faceQuadOutside.point( pt ), uRight, jump, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );
        // inside
        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jump, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );

        diffFlux *= -0.5 * Tmin;
        flux *= 0.5;

        return len;
      }


      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection,
                            const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt,
                            const DomainType &normal,
                            const RangeType &uLeft, const RangeType &uBnd,
                            const JacobianRangeType &jacLeft,
                            RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        const double len = normal.two_norm();

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        JacobianRangeType jump = normalJump( uLeft, uBnd, normal, 1.0/Tmin );

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jump, diffFlux );
        diffFlux.usmv( penalty_, normal, flux );

        diffFlux *= -1.0 * Tmin;

        return len;
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside, const int pt,
                                 const DomainType &normal,
                                 const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const JacobianRangeType &jacStarLeft, const JacobianRangeType &jacStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 const JacobianRangeType &jacLeft, const JacobianRangeType &jacRight,
                                 RangeType &gLeft, RangeType &gRight,
                                 JacobianRangeType &gDiffLeft, JacobianRangeType &gDiffRight ) const
      {
        const RangeType null( 0 );

        gLeft = 0;
        gRight = 0;

        // left
        model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, jacStarLeft, uLeft, jacLeft, gDiffLeft );
        gDiffLeft.mmv( normal, gLeft );

        // right
        model().linearDiffusion( outside, time, faceQuadOutside.point( pt ), uStarRight, jacStarRight, uRight, jacRight, gDiffRight );
        gDiffRight.mmv( normal, gRight );

        JacobianRangeType jumpLeft = normalJump( uLeft, null, normal, 1.0 / Tmin );
        JacobianRangeType jumpRight = normalJump( null, uRight, normal, 1.0 / Tmin );
        JacobianRangeType jumpStar = normalJump( uStarLeft, uStarRight, normal, 1.0 / Tmin );

        auto linDiffOut = model().linearDiffusion( outside, time, faceQuadOutside.point( pt ), uStarRight, jumpStar );
        auto linDiffIn = model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, jumpStar );

        // right
        linDiffOut( uRight, jumpRight, gDiffRight );
        gDiffRight.usmv( penalty_, normal, gRight );

        linDiffIn( null, jumpRight, gDiffRight );
        gDiffRight.usmv( penalty_, normal, gRight );

        // left
        linDiffOut( null, jumpLeft, gDiffLeft );
        gDiffLeft.usmv( penalty_, normal, gLeft );

        linDiffIn( uLeft, jumpLeft, gDiffLeft );
        gDiffLeft.usmv( penalty_, normal, gLeft );

        gDiffLeft *= -0.5 * Tmin;
        gDiffRight *= -0.5* Tmin;
        gLeft *= 0.5;
        gRight *= 0.5;
      }


      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &inside, const double time,
                                const FaceQuadrature &faceQuadInside, const int pt, const DomainType &normal,
                                const RangeType &uStarLeft, const RangeType &uStarBnd, const JacobianRangeType &jacStarLeft,
                                const RangeType &uLeft, const RangeType &uBnd, const JacobianRangeType &jacLeft,
                                RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, jacStarLeft, uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        model().linearDiffusion( inside, time, faceQuadInside.point( pt ), uStarLeft, normalJump( uStarLeft, uStarBnd, normal, 1.0 / Tmin ),
                                 uLeft, normalJump( uLeft, uBnd, normal, 1.0 / Tmin ), diffFlux );
        diffFlux.usmv( penalty_, normal, flux );
        diffFlux *= -1.0 * Tmin;
      }
    };


    // LinearIpDgFlux
    // --------------
    //
    // To be used for fully linear Diffusion operators
    //
    // D( u, grad u ) = D( grad u ) = A(x) grad u
    //

    template< class Model >
    class LinearIpDgFlux
      : public IpDgFlux< Model >
    {
      typedef LinearIpDgFlux< Model > ThisType;
      typedef IpDgFlux< Model > BaseType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;
      typedef typename ModelType::RangeFieldType RangeFieldType;

      static const int dimRange = ModelType::dimRange;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      static const bool needsLiftings = false;

      using BaseType::Stencil;
      using BaseType::model;

    protected:
      using BaseType::Tmin;
      using BaseType::penalty_;

    public:

      LinearIpDgFlux ( const ModelType &model ) : BaseType( model ) {}


      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection,
                             const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside,
                             const FaceQuadrature &faceQuadOutside, const int pt,
                             const DomainType &normal,
                             const RangeType &uLeft, const RangeType &uRight,
                             const JacobianRangeType &jacLeft, const JacobianRangeType &jacRight,
                             RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        const double len = normal.two_norm();

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );
        model().diffusion( outside, time, faceQuadOutside.point( pt ), uRight, jacRight, diffFlux );
        diffFlux.mmv( normal, flux );

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, normalJump( uLeft, uRight, normal, 1.0/Tmin ), diffFlux );
        diffFlux.usmv( 2.0 * penalty_, normal, flux );
        diffFlux *= -0.5 * Tmin;
        flux *= 0.5;
        return len;
      }


      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection,
                            const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt,
                            const DomainType &normal,
                            const RangeType &uLeft, const RangeType &uBnd,
                            const JacobianRangeType &jacLeft,
                            RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        flux = 0;
        const double len = normal.two_norm();

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, jacLeft, diffFlux );
        diffFlux.mmv( normal, flux );

        model().diffusion( inside, time, faceQuadInside.point( pt ), uLeft, normalJump( uLeft, uBnd, normal, 1.0/Tmin ), diffFlux );
        diffFlux.usmv( penalty_, normal, flux );
        diffFlux *= -1.0 * Tmin;
        return len;
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside, const int pt,
                                 const DomainType &normal,
                                 const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const JacobianRangeType &jacStarLeft, const JacobianRangeType &jacStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 const JacobianRangeType &jacLeft, const JacobianRangeType &jacRight,
                                 RangeType &gLeft, RangeType &gRight,
                                 JacobianRangeType &gDiffLeft, JacobianRangeType &gDiffRight ) const
      {
        JacobianRangeType jNull(0);
        RangeType null(0);

        numericalFlux( intersection, inside, outside, time, faceQuadInside, faceQuadOutside,
                       pt, normal, uLeft, null, jacLeft, jNull, gLeft, gDiffLeft );
        numericalFlux( intersection, inside, outside, time, faceQuadInside, faceQuadOutside,
                       pt, normal, null, uRight, jNull, jacRight, gRight, gDiffRight );
      }


      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &inside, const double time,
                                const FaceQuadrature &faceQuadInside, const int pt, const DomainType &normal,
                                const RangeType &uStarLeft, const RangeType &uStarBnd, const JacobianRangeType &jacStarLeft,
                                const RangeType &uLeft, const RangeType &uBnd, const JacobianRangeType &jacLeft,
                                RangeType &flux, JacobianRangeType &diffFlux ) const
      {
        boundaryFlux( intersection, inside, time, faceQuadInside, pt, normal, uLeft, uBnd, jacLeft, flux, diffFlux );
      }
    };


  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_FLUXES_IPDG_HH
