#ifndef DUNE_FEM_OPERATOR_FLUXES_LINEARIPDG_HH
#define DUNE_FEM_OPERATOR_FLUXES_LINEARIPDG_HH

#warning "Content of linearipdg.hh has moved into ipdg.hh. Include file ipdg.hh directly."
#include <dune/fem-dg/operator/fluxes/ipdg.hh>

#endif // #ifndef DUNE_FEM_OPERATOR_FLUXES_LINEARIPDG_HH
