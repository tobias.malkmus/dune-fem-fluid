#ifndef DUNE_FEM_DG_OPERATOR_FLUXES_COMMON_HH
#define DUNE_FEM_DG_OPERATOR_FLUXES_COMMON_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>


namespace Dune
{

  namespace Fem
  {


    // normalJump
    // ----------

    template< class T, int r, int k >
    inline static FieldMatrix< T, r, k > normalJump ( const FieldVector< T, r > &a, const FieldVector< T, r > &b, const FieldVector< T, k > &d, T scale ) noexcept
    {
      FieldMatrix< T, r, k > jump( 0 );
      for( std::size_t i = 0; i < r; ++i )
        jump[ i ].axpy( ( a[ i ] - b[ i ] ) * scale, d );
      return jump;
    }


    // normalJump
    // ----------

    template< class T, int r, int k >
    inline static FieldMatrix< T, r, k > normalJump ( const FieldVector< T, r > &a, const FieldVector< T, r > &b, const FieldVector< T, k > &d ) noexcept
    {
      return normalJump( a, b, d, 1.0 );
    }


    // avg
    // ---

    template< class T >
    inline static T avg ( const T &a, const T &b ) noexcept
    {
      T avg( a );
      avg += b;
      return avg *= 0.5;
    }

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_DG_OPERATOR_FLUXES_COMMON_HH
