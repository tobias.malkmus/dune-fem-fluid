#ifndef DUNE_FEM_OPERATOR_FLUXES_PRIMALLDGFLUX_HH
#define DUNE_FEM_OPERATOR_FLUXES_PRIMALLDGFLUX_HH

#include <dune/common/fvector.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/fmatrixconverter.hh>
#include <dune/fem/operator/common/neighborofneighborstencil.hh>

#include <dune/fem-dg/operator/fluxes/common.hh>

namespace Dune
{

  namespace Fem
  {

    // PrimalLdgFlux
    // -------------

    template< class Model >
    class PrimalLdgFlux
    {
      typedef PrimalLdgFlux< Model > ThisType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;
      typedef typename ModelType::RangeFieldType RangeFieldType;

      static const int dimDomain = ModelType::dimDomain;
      static const int dimRange = ModelType::dimRange;
      static const int dimGradRange = dimDomain * dimRange;

      typedef FieldVector< RangeFieldType, dimGradRange > LiftingRangeType;
      typedef FieldMatrixConverter< LiftingRangeType, JacobianRangeType > ConverterType;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      static const bool needsLiftings = true;

      template< class DomainSpace, class RangeSpace >
      struct Stencil
      {
        typedef DiagonalAndNeighborOfNeighborStencil< DomainSpace, RangeSpace > Type;
      };

      PrimalLdgFlux ( const ModelType &model )
        : model_( model ),
        penalty_( Parameter::getValue< double >( "pldg.penalty", 0.5 ) )
      {}

      template< class ... Args >
      void init ( Args && ... args ) const {}

      template< class ... Args >
      void initIntersection ( Args && ... args ) const {}

      template< class Quadtrature >
      void linearDiffusion ( const EntityType &entity, const double time, const Quadtrature &quad, const int pt,
                             const RangeType &uStar, const JacobianRangeType &jacStar, const LiftingRangeType &liftStar,
                             const RangeType &u, const JacobianRangeType &jac, const LiftingRangeType &lift,
                             JacobianRangeType &d ) const
      {
        JacobianRangeType arg( jac ), argStar( jacStar );
        arg -= ConverterType( lift );
        argStar -= ConverterType( liftStar );
        model_.linearDiffusion( entity, time, quad.point( pt ), uStar, argStar, u, arg, d );
      }

      template< class Quadtrature >
      void linearDiffusion ( const EntityType &entity, const double time, const Quadtrature &quad, const int pt,
                             const RangeType &uStar, const JacobianRangeType &jacStar, const LiftingRangeType &liftStar,
                             const RangeType &u, const JacobianRangeType &jac, const LiftingRangeType &lift,
                             LiftingRangeType &d ) const
      {
        d = 0;
      }

      std::string description () const { return std::string( "Primal LDG method with penalty: " + std::to_string(  penalty_ ) ); }

      void diffusion ( const EntityType &entity,
                       const double time,
                       const DomainType &x,
                       const RangeType &u,
                       const JacobianRangeType &jac,
                       const LiftingRangeType &lift,
                       JacobianRangeType &diffusion ) const
      {
        JacobianRangeType arg( jac );

        ConverterType con( lift );
        arg -= con;
        model_.diffusion( entity, time, x,  u, arg, diffusion );
      }


      void diffusion ( const EntityType &entity,
                       const double time,
                       const DomainType &x,
                       const RangeType &u,
                       const JacobianRangeType &jac,
                       const LiftingRangeType &lift,
                       LiftingRangeType &diffusion ) const
      {
        diffusion = 0;
      }


      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection,
                             const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside,
                             const FaceQuadrature &faceQuadOutside, const int pt,
                             const DomainType &normal,
                             const RangeType &uLeft, const RangeType &uRight,
                             RangeType &flux ) const
      {
        flux = 0;
        const double lenSqr = normal.two_norm2();
        JacobianRangeType diffFlux;
        model_.diffusion( inside, time, faceQuadInside.point( pt ), avg( uLeft, uRight ),
            normalJump( uLeft, uRight, normal, 1.0 / lenSqr ), diffFlux );
        diffFlux.usmv( penalty_, normal, flux );
        return std::sqrt( lenSqr );
      }


      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection,
                            const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt,
                            const DomainType &normal,
                            const RangeType &uLeft, const RangeType &uBnd,
                            RangeType &flux ) const
      {
        return numericalFlux( intersection, inside, inside, time, faceQuadInside, faceQuadInside, pt, normal, uLeft, uBnd, flux );
      }


      void linearDiffusion ( const EntityType &entity,
                             const double time,
                             const DomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const LiftingRangeType &liftStar,
                             const RangeType &u,
                             const JacobianRangeType &jac,
                             const LiftingRangeType &lift,
                             JacobianRangeType &diffusion ) const
      {
        JacobianRangeType argStar( jacStar );
        argStar -= ConverterType( liftStar );
        JacobianRangeType arg( jac );
        arg -= ConverterType( lift );
        model_.linearDiffusion( entity, time, x,  uStar, argStar, u, arg, diffusion );
//        diffusion = 0;
      }


      void linearDiffusion ( const EntityType &entity,
                             const double time,
                             const DomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const LiftingRangeType &liftStar,
                             const RangeType &u,
                             const JacobianRangeType &jac,
                             const LiftingRangeType &lift,
                             LiftingRangeType &diffusion ) const
      {
        diffusion = 0;
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection,
                                 const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside,
                                 const FaceQuadrature &faceQuadOutside, const int pt,
                                 const DomainType &normal,
                                 const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 RangeType &gLeft, RangeType &gRight ) const
      {
        const RangeType null(0);
        JacobianRangeType diffFlux;

        const double lenSqr = normal.two_norm2();
        JacobianRangeType jumpStar = normalJump( uStarLeft, uStarRight, normal, 1.0 / lenSqr );

        // left
        model_.linearDiffusion( inside, time, faceQuadInside.point( pt ), avg( uStarLeft, uStarRight ), jumpStar,
                                avg( uLeft, null ), normalJump( uLeft, null, normal, 1.0/ lenSqr ), diffFlux );

        gLeft = 0;
        diffFlux.usmv( penalty_, normal, gLeft );

        // right
        model_.linearDiffusion( outside, time, faceQuadOutside.point( pt ), avg( uStarLeft, uStarRight ), jumpStar,
                                avg( null, uRight ), normalJump( null, uRight, normal, 1.0/ lenSqr ), diffFlux );

        gRight = 0;
        diffFlux.usmv( penalty_, normal, gRight );
      }


      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection,
                                const EntityType &inside, const double time,
                                const FaceQuadrature &faceQuadInside, const int pt, const DomainType &normal,
                                const RangeType &uStarLeft, const RangeType &uStarBnd,
                                const RangeType &uLeft, const RangeType &uBnd,
                                RangeType &gLeft ) const
      {

        gLeft = 0;

        JacobianRangeType diffFlux;
        const double lenSqr = normal.two_norm2();

        model_.linearDiffusion( inside, time, faceQuadInside.point( pt ),
            avg( uStarLeft, uStarBnd ), normalJump( uStarLeft, uStarBnd, normal, 1.0 / lenSqr ),
            avg( uLeft, uBnd ), normalJump( uLeft, uBnd, normal, 1.0 / lenSqr ),
            diffFlux );

        diffFlux.usmv( penalty_, normal, gLeft );
      }

      double insideLiftingFactor ( const IntersectionType &itersection ) const { return 0.5; }
      double outsideLiftingFactor ( const IntersectionType &itersection ) const { return -0.5; }
      double boundaryLiftingFactor ( const IntersectionType &itersection ) const { return 1.0; }

    protected:
      const ModelType &model () const { return model_; }

      double penalty () const { return penalty_; }


    private:
      const ModelType &model_;
      const double penalty_;
    };



    // CachedPrimalLdgFlux
    // -------------------

    template< class Model >
    class CachedPrimalLdgFlux
      : public PrimalLdgFlux< Model >
    {
      typedef CachedPrimalLdgFlux< Model > ThisType;
      typedef PrimalLdgFlux< Model > BaseType;

    public:
      typedef Model ModelType;
      typedef typename ModelType::LinearDiffusion LinearDiffusion;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;
      typedef typename ModelType::RangeFieldType RangeFieldType;

      static const int dimDomain = ModelType::dimDomain;
      static const int dimRange = ModelType::dimRange;
      static const int dimGradRange = dimDomain * dimRange;

      typedef FieldVector< RangeFieldType, dimGradRange > LiftingRangeType;
      typedef FieldMatrixConverter< LiftingRangeType, JacobianRangeType > ConverterType;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      static const bool needsLiftings = true;

      template< class DomainSpace, class RangeSpace >
      struct Stencil
      {
        typedef DiagonalAndNeighborOfNeighborStencil< DomainSpace, RangeSpace > Type;
      };

      CachedPrimalLdgFlux ( const ModelType &model )
        : BaseType( model )
      {}

      template< class Quadtrature, class RangeVector, class JacobianVector, class LiftingVectors >
      void init ( const EntityType &entity, const double time, const Quadtrature &quad, const RangeVector &ranges, const JacobianVector &jacobians,
                  const LiftingVectors &liftings, bool evalType ) const
      {
        if( !evalType )
        {
          linearDiffusion_.resize( quad.nop() );
          for( std::size_t i = 0; i < quad.nop(); ++i )
          {
            JacobianRangeType arg( jacobians[ i ] );
            arg -= ConverterType( liftings[ i ] );
            linearDiffusion_[ i ] = model().linearDiffusion( entity, time, quad.point( i ), ranges[ i ], arg );
          }
        }
      }

      template< class Quadtrature >
      void linearDiffusion ( const EntityType &entity, const double time, const Quadtrature &quad, const int pt,
                             const RangeType &uStar, const JacobianRangeType &jacStar, const LiftingRangeType &liftStar,
                             const RangeType &u, const JacobianRangeType &jac, const LiftingRangeType &lift,
                             JacobianRangeType &d ) const
      {
        JacobianRangeType arg( jac );
        arg -= ConverterType( lift );
        linearDiffusion_[ pt ]( u, arg, d );
      }

      template< class Quadtrature >
      void linearDiffusion ( const EntityType &entity, const double time, const Quadtrature &quad, const int pt,
                             const RangeType &uStar, const JacobianRangeType &jacStar, const LiftingRangeType &liftStar,
                             const RangeType &u, const JacobianRangeType &jac, const LiftingRangeType &lift,
                             LiftingRangeType &d ) const
      {
        d = 0;
      }


      void linearDiffusion ( const EntityType &entity,
                             const double time,
                             const DomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const LiftingRangeType &liftStar,
                             const RangeType &u,
                             const JacobianRangeType &jac,
                             const LiftingRangeType &lift,
                             JacobianRangeType &diffusion ) const
      {
        JacobianRangeType argStar( jacStar );
        argStar -= ConverterType( liftStar );
        JacobianRangeType arg( jac );
        arg -= ConverterType( lift );
        model().linearDiffusion( entity, time, x,  uStar, argStar, u, arg, diffusion );
      }

      void linearDiffusion ( const EntityType &entity,
                             const double time,
                             const DomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const LiftingRangeType &liftStar,
                             const RangeType &u,
                             const JacobianRangeType &jac,
                             const LiftingRangeType &lift,
                             LiftingRangeType &diffusion ) const
      {
        diffusion = 0;
      }

    protected:
      using BaseType::model;

    private:
      mutable std::vector< LinearDiffusion > linearDiffusion_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_FLUXES_PRIMALLDGFLUX_HH
