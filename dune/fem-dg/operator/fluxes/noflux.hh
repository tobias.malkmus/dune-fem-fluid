#ifndef DUNE_FEM_OPERATOR_FLUXES_NOFLUX_HH
#define DUNE_FEM_OPERATOR_FLUXES_NOFLUX_HH

#include <string>

#include <dune/common/diagonalmatrix.hh>


namespace Dune
{

  namespace Fem
  {

    template< class Model >
    class NoFlux
    {
      typedef NoFlux< Model >  ThisType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      typedef typename ModelType::RangeFieldType RangeFieldType;
      static const int dimRange = ModelType::dimRange;

      typedef DiagonalMatrix< RangeFieldType, dimRange > EigenValueMatrixType;

      NoFlux ( const ModelType &model ) {}

      std::string description () const { return std::string( "No Flux" ); }

      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside,
                             const int pt, const DomainType &normal, const RangeType &uLeft, const RangeType &uRight, RangeType &flux ) const
      {
        return 0;
      }

      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection, const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt, const DomainType &normal, const RangeType &uLeft, const RangeType &uBnd, RangeType &flux ) const
      {
        return 0;
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside,
                                 const int pt, const DomainType &normal, const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 RangeType &gLeft, RangeType &gRight ) const
      {
      }

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection, const EntityType &inside,
                                const double time, const FaceQuadrature &faceQuadInside,
                                const int pt, const DomainType &normal, const RangeType &uStarLeft,
                                const RangeType &uStarBnd, const RangeType &uLeft, const RangeType &uBnd, RangeType &flux ) const
      {
      }
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_FLUXES_MEANVALUE_HH
