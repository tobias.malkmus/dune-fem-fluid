#ifndef DUNE_FEM_OPERATOR_FLUXES_MEANVALUE_HH
#define DUNE_FEM_OPERATOR_FLUXES_MEANVALUE_HH

#include <dune/fem-dg/operator/fluxes/common.hh>


namespace Dune
{

  namespace Fem
  {

    // MeanValueFlux
    // -------------

    template< class Model >
    class MeanValueFlux
    {
      typedef MeanValueFlux< Model >  ThisType;

    public:
      typedef Model ModelType;

      typedef typename ModelType::DomainType DomainType;
      typedef typename ModelType::RangeType RangeType;
      typedef typename ModelType::JacobianRangeType JacobianRangeType;

      typedef typename ModelType::EntityType EntityType;
      typedef typename ModelType::IntersectionType IntersectionType;

      typedef typename ModelType::RangeFieldType RangeFieldType;
      static const int dimRange = ModelType::dimRange;

      typedef DiagonalMatrix< RangeFieldType, dimRange > EigenValueMatrixType;

      MeanValueFlux ( const ModelType &model ) : model_( model ) {}

      std::string description () const { return std::string( "Mean Value Flux" ); }

      template< class FaceQuadrature >
      double numericalFlux ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                             const double time, const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside,
                             const int pt, const DomainType &normal, const RangeType &uLeft, const RangeType &uRight, RangeType &flux ) const
      {
        JacobianRangeType anaFlux;
        model_.advection( inside, time, faceQuadInside.point( pt ), uLeft, anaFlux );
        anaFlux.mv( normal, flux );
        model_.advection( outside, time, faceQuadOutside.point( pt ), uRight, anaFlux );
        anaFlux.umv( normal, flux );
        flux *= 0.5;
        return normal.two_norm();
      }

      template< class FaceQuadrature >
      double boundaryFlux ( const IntersectionType &intersection, const EntityType &inside,
                            const double time, const FaceQuadrature &faceQuadInside,
                            const int pt, const DomainType &normal, const RangeType &uLeft, const RangeType &uBnd, RangeType &flux ) const
      {
        // one sided evaluation with boundary values
        JacobianRangeType anaFlux;
        model_.advection( inside, time, faceQuadInside.point( pt ), uBnd, anaFlux );
        anaFlux.mv( normal, flux );

        return normal.two_norm();
      }


      template< class FaceQuadrature >
      void linearNumericalFlux ( const IntersectionType &intersection, const EntityType &inside, const EntityType &outside,
                                 const double time, const FaceQuadrature &faceQuadInside, const FaceQuadrature &faceQuadOutside,
                                 const int pt, const DomainType &normal, const RangeType &uStarLeft, const RangeType &uStarRight,
                                 const RangeType &uLeft, const RangeType &uRight,
                                 RangeType &gLeft, RangeType &gRight ) const
      {
        JacobianRangeType anaFlux;
        model_.linearAdvection( inside, time, faceQuadInside.point( pt ), uStarLeft, uLeft, anaFlux );
        anaFlux.mv( normal, gLeft );
        model_.linearAdvection( outside, time, faceQuadOutside.point( pt ), uStarRight, uRight, anaFlux );
        anaFlux.mv( normal, gRight );
      }

      template< class FaceQuadrature >
      void linearBoundaryFlux ( const IntersectionType &intersection, const EntityType &inside,
                                const double time, const FaceQuadrature &faceQuadInside,
                                const int pt, const DomainType &normal, const RangeType &uStarLeft,
                                const RangeType &uStarBnd, const RangeType &uLeft, const RangeType &uBnd, RangeType &flux ) const
      {
        JacobianRangeType anaFlux;
        model_.linearAdvection( inside, time, faceQuadInside.point( pt ), uStarBnd, uBnd, anaFlux );
        anaFlux.mv( normal, flux );
      }

    private:
      const ModelType &model_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OPERATOR_FLUXES_MEANVALUE_HH
