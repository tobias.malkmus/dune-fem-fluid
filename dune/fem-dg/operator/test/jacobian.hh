#ifndef DUNE_FEM_DG_OPERATOR_TEST_JACOBIAN_HH
#define DUNE_FEM_DG_OPERATOR_TEST_JACOBIAN_HH

#include <algorithm>
#include <functional>
#include <iostream>
#include <random>

#include <dune/common/dynmatrix.hh>

#include <dune/istl/io.hh>

#include <dune/fem/io/parameter.hh>

namespace Dune
{

  namespace Fem
  {

    // jacobianTest
    // ------------

    template< class Operator >
    void jacobianTest ( Operator &op )
    {
      std::cout<<"Starting testing the jacobian operator ... "<<std::endl;

      std::uniform_real_distribution< double > distribution( -1.0, 1.0 );
      std::default_random_engine randomEngine;
      auto random = std::bind( distribution, randomEngine );

      typename Operator::JacobianOperatorType jac( "name", op.space(), op.space() );

      typename Operator::DomainFunctionType arg( "arg", op.space() );
      typename Operator::DomainFunctionType argOp( "argOp", op.space() );
      typename Operator::DomainFunctionType uStar( "ustar", op.space() );

      const int localBlockSize = Operator::DomainFunctionType::DiscreteFunctionSpaceType::localBlockSize;

      const int size = uStar.size();

      for( auto &dof : uStar.dofVector() )
        dof = random();

      typename Operator::RangeFunctionType jacDest( "jacDest", op.space() );
      typename Operator::RangeFunctionType opDest( "opDest", op.space() );
      typename Operator::RangeFunctionType dest( "dest", op.space() );

      typedef typename Operator::RangeFieldType RangeFieldType;

      const double eps = Dune::Fem::Parameter::getValue< double >( "testoperator.eps", 1e-8 );

      // get jacobian operator
      op.jacobian( uStar, jac );

      DynamicMatrix< RangeFieldType > linearized, real, diff;

      linearized.resize( size, size );
      real.resize( size, size );
      diff.resize( size, size );

      for( int i = 0; i < size; ++i )
      {
        // e_i
        arg.clear();
        const int iblock = i / localBlockSize;

        arg.dofVector()[ iblock ][ i % localBlockSize ] = 1.0;

        // compute Jac( e_i , jDest )
        jac( arg, jacDest );

        // compute ( F( uStar + eps e_i ) - F( uStar - eps e_i ) ) / (eps*2)
        argOp.assign( uStar );
        argOp.axpy( eps, arg );
        op( argOp, opDest );

        argOp.assign( uStar );
        argOp.axpy( -eps, arg );
        op( argOp, dest );

        opDest -= dest;
        opDest /= 2.0*eps;

        for( int j = 0; j < size; ++j )
        {
          const int jBlock = j / localBlockSize;
          const int comp = j % localBlockSize;
          double value1 = opDest.dofVector()[ jBlock ][ comp ];
          double value2 = jacDest.dofVector()[ jBlock ][ comp ];

          if( std::abs( value1 ) < eps )
            value1 = 0.;
          linearized[ j ][ i ] = value1;

          if( std::abs( value2 ) < eps )
            value2 = 0.;
          real[ j ][ i ] = value2;

          if( std::abs( value1 - value2 ) > 10 * eps )
            diff[ j ][ i ] = value1 - value2;
        }
      }

      // if diff is too large write files
      if( diff.frobenius_norm() > size * size * eps )
      {
        std::cerr<< "Comparison of JacobianOperator and FD linerization no successful |diff| = "<< diff.frobenius_norm()<<std::endl;
        std::cerr<< "See \"diff\" file for more info." <<std::endl;

        std::ofstream file1( "onthefly" );
        file1 << linearized;
        file1.close();

        std::ofstream file2( "real" );
        file2 << real;
        file2.close();

        std::ofstream filediff( "diff" );
        filediff << diff;
        filediff.close();
      }
    }


    // jacobianTest
    // ------------

    template< class Operator, class Tp >
    void jacobianTest ( Operator &op, const Tp &tp )
    {
      op.setLambda( 1.0 );
      op.setTime( tp.time() );

      jacobianTest( op );
    }

  } // namespace Fem

} // namespace Dune

#endif //#ifndef DUNE_FEM_DG_OPERATOR_TEST_JACOBIAN_HH
