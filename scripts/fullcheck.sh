#!/bin/bash

# check for parameter pointing to DUNE base directory
# ---------------------------------------------------

DUNECONTROL="dune-common/bin/dunecontrol"

if test \( $# -lt 1 \) -o ! -e $1/$DUNECONTROL ; then
  echo "Usage: $0 <dune-base-dir>"
  exit 1
fi

echo "Full Check of dune-fem-fluid"
echo "----------------------------"

echo
echo "Host Name: $HOSTNAME"
echo "Host Type: $HOSTTYPE"

# set up some variables
# ---------------------

WORKINGDIR=`pwd`
cd $1
DUNEDIR=`pwd`
SCRIPTSDIR="$DUNEDIR/dune-fem-fluid/scripts"
OPTSDIR="$SCRIPTSDIR/opts"

errors=0

OPTFILES=$(cd $OPTSDIR ; ls *.opts)

first=true
for OPTS in $OPTFILES ; do
  echo
  echo "Performing checks for $OPTS..."

  cd $WORKINGDIR
  # perform headercheck only ones
  if ! $SCRIPTSDIR/check-opts.sh $DUNEDIR dune-fem-fluid $OPTS $first; then
    errors=$((errors+1))
  fi
  first=false
done

if test $errors -gt 0 ; then
  exit 1
else
  exit 0
fi
