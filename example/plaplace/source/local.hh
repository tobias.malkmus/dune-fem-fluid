#ifndef LOCALSTUFF_HH
#define LOCALSTUFF_HH

#include <dune/common/fmatrix.hh>


Dune::FieldMatrix< double, 2, 2 > rotation ( double eps )
{
//  return {{ eps, 0 }, { 0, eps} };
  return {{ eps, 0 }, { 0, 1 }};

         /*
            return {{ std::cos( M_PI * eps ), -std::sin( M_PI * eps )},
                 { std::sin( M_PI * eps ), std::cos( M_PI * eps ) }};
          */
}


#endif // #ifndef LOCALSTUFF_HH
