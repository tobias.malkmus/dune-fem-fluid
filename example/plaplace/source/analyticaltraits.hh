#ifndef ANALYTICALTRAITS_HH
#define ANALYTICALTRAITS_HH

#include <fstream>

#include <dune/fem/misc/femeoc.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>

#include "models.hh"
#include "problem-linsing.hh"
#include "problem-pointsing.hh"
#include "problem-poly.hh"
#include "problem-sin.hh"


namespace __impl
{

  template< class GridPart, class FunctionSpace >
  struct AnalyticalTraits
  {
    // initial problem
    typedef Dune::Fem::SteadyStateProblemInterface< FunctionSpace > ProblemType;

    // the models
    typedef PlaplaceModel< GridPart, ProblemType > ModelType;
    typedef ProblemType InitialDataType;

    typedef typename FunctionSpace::RangeType RangeType;
    typedef typename FunctionSpace::RangeFieldType RangeFieldType;
    typedef typename FunctionSpace::JacobianRangeType JacobianRangeType;

    static ProblemType *problem ()
    {
      static const std::string probString[]  = { "poly", "sin", "linsing", "pointsing" };
      int problemId = Dune::Fem::Parameter::getEnum( "problem", probString, 0 );
      switch( problemId )
      {
      case 0:
        return new Dune::Fem::PolynomSolution< FunctionSpace >();
      case 1:
        return new Dune::Fem::PLaplaceEqnSolution< FunctionSpace >();
      case 2:
        return new Dune::Fem::LineSingularitySolution< FunctionSpace >();
      case 3:
        return new Dune::Fem::PointSingularitySolution< FunctionSpace >();
      default:
        abort();
        return 0;
      }
    }

    struct ErrorHandler
    {

      static const int dimDomain = FunctionSpace::dimDomain;
      static const int numErrors = 4;

      ErrorHandler ()
        : ids_
      {
        Dune::Fem::FemEoc::addEntry( std::string( "$L^2$-error" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "$H^1$-error" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "Functional-error" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "Functional-dg-error" ) )
      }
      {
        file_.open( "data_" + std::to_string( Dune::Fem::Parameter::getValue< int >( "quad.order", -1 ) ) );
//        file_.open( "data_" + std::to_string( Dune::Fem::Parameter::getValue< double >( "ipdg.penalty", 7.5 ) ) );
      }

      ~ErrorHandler ()
      {
        file_.close();
      }

      template< class Tp, class DiscreteSolution >
      void initializeStep ( const Tp &tp, int loop, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class Tp, class DiscreteSolution >
      void step ( const Tp &tp, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class Tp, class DiscreteSolution >
      void finalizeStep ( const Tp &tp, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class DiscreteSolution, class Monitor >
      void finalize ( int loop, const ModelType &model, const DiscreteSolution &u, const Monitor &monitor, double assembleTime ) const
      {
        // compute errors at final time
        GridPart &gridPart = u.space().gridPart();

        // now compute errors
        std::array< double, numErrors > error; error.fill( 0.0 );
        const ProblemType &f = model.problem();
        std::for_each( Dune::elements( gridPart ).begin(), Dune::elements( gridPart ).end(),
                       [ &u, &f, &error, &model, &gridPart ]
                         ( const typename DiscreteSolution::EntityType & entity )
                       {
                         auto local = u.localFunction( entity );
                         auto geometry = entity.geometry();
                         Dune::Fem::CachingQuadrature< GridPart, 0 > quad( entity, u.space().order() * 2 + 2 );
                         for( const auto &qp: quad )
                         {
                           const double weight = qp.weight() * geometry.integrationElement( qp.position() );
                           typename FunctionSpace::RangeType value, valueh;
                           typename FunctionSpace::JacobianRangeType dphi, dphih, Fu, Fuh;
                           local.evaluate( qp, valueh );
                           f.evaluate( geometry.global( qp.position() ), value );
                           value -= valueh;
                           local.jacobian( qp, dphih );
                           f.jacobian( geometry.global( qp.position() ), dphi );
                           Fu = dphi;
                           Fu *= std::pow( ( model.sd() + dphi.frobenius_norm2() ), (model.p() - 2.) / 4. );
                           Fuh = dphih;
                           Fuh *= std::pow( ( model.sd() + dphih.frobenius_norm2() ), (model.p() - 2.) / 4. );

                           Fu -= Fuh;
                           dphi -= dphih;

                           error[ 0 ] += value.two_norm2() * weight;
                           error[ 1 ] += value.two_norm2() * weight;
                           error[ 1 ] += dphi.frobenius_norm2() * weight;
                           error[ 2 ] += Fu.frobenius_norm2() * weight;
                         }

                         for( auto intersection: Dune::intersections( gridPart, entity ) )
                           if( intersection.neighbor()
                               && gridPart.indexSet().index( entity ) > gridPart.indexSet().index( intersection.outside() ) )
                           {
                             auto localNb = u.localFunction( intersection.outside() );
                             double vol = std::max( entity.geometry().volume(), intersection.outside().geometry().volume());
                             Dune::Fem::CachingQuadrature< GridPart, 1 > inside( gridPart, intersection, u.space().order() * 2 + 2,
                                                                                 Dune::Fem::CachingQuadrature< GridPart, 1 >::INSIDE );
                             Dune::Fem::CachingQuadrature< GridPart, 1 > outside( gridPart, intersection, u.space().order() * 2 + 2,
                                                                                  Dune::Fem::CachingQuadrature< GridPart, 1 >::OUTSIDE );
                             for( std::size_t qp = 0; qp < inside.nop(); ++qp )
                             {
                               typename FunctionSpace::RangeType val, valNb;
                               typename FunctionSpace::DomainType normal = intersection.integrationOuterNormal( inside.localPoint( qp ) );

                               local.evaluate( inside[ qp ], val );
                               localNb.evaluate( outside[ qp ], valNb );

                               typename FunctionSpace::JacobianRangeType jump;
                               for( std::size_t i = 0; i < FunctionSpace::RangeType::dimension; ++i )
                               {
                                 jump[ i ] = normal;
                                 jump *= (val[ i ] - valNb[ i ]) / vol;
                               }
                               double factor = std::pow( model.sd() + jump.frobenius_norm2(), (model.p() - 2.) / 2.0 );

                               error[ 3 ] += jump.frobenius_norm2() * factor * inside.weight( qp ) * vol;
                             }
                           }
                       } );

        error[ 3 ] += error[ 2 ];

        for( std::size_t i = 0; i < numErrors; ++i )
          Dune::Fem::FemEoc::setErrors( ids_[ i ], std::sqrt( error[ i ] ) );

        file_ << loop <<"\t";
        for( std::size_t i = 0; i < numErrors; ++i )
          file_ << std::sqrt( error[ i ] ) <<"\t";
        file_<<monitor.totalLinearSolverIterations_ <<"\t"<< monitor.totalNewtonIterations_ <<"\t"<< assembleTime <<std::endl;
      }

    private:
      std::array< std::size_t, numErrors > ids_;
      mutable std::ofstream file_;
    };

  };

} // namespace __impl

#endif //#ifndef ANALYTICALTRAITS_HH
