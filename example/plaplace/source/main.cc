// configure macros
#include <config.h>

#include "problemcreator.hh"

#include <dune/fem-dg/misc/simulate.hh>
#include <dune/fem/io/parameter.hh>

#ifndef POLORDER
static const int polOrder = 2;
#else
static const int polOrder = POLORDER;
#endif


int main ( int argc, char **argv )
{
  /* Initialize MPI (always do this even if you are not using MPI) */
  Dune::Fem::MPIManager::initialize( argc, argv );
  try {
    // *** Initialization
    Dune::Fem::Parameter::append( argc, argv );
    if( argc >= 2 )
      Dune::Fem::Parameter::append( argv[ 1 ] );
    else
      Dune::Fem::Parameter::append( "../data/parameter" );

    // write parameters used (before simulation starts)
    std::string parameterFilename = Dune::Fem::Parameter::getValue< std::string >( "fem.io.parameter.filename" );
    Dune::Fem::Parameter::write( parameterFilename );

    typedef Dune::GridSelector::GridType GridType;

    const std::string filekey = Dune::Fem::IOInterface::defaultGridKey( GridType::dimension );
    const std::string filename = Dune::Fem::Parameter::getValue< std::string >( filekey );

    // initialize grid with given macro file name
    Dune::GridPtr< GridType > gridptr = Dune::GridPtr< GridType >( filename );
    Dune::Fem::Parameter::appendDGF( filename );

    // load balance grid in case of parallel runs
    gridptr->loadBalance();

    // and refine the grid globally
    const int startLevel = Dune::Fem::Parameter::getValue< int >( "startlevel", 0 );
    for( int level = 0; level < startLevel; ++level )
      Dune::Fem::GlobalRefine::apply( *gridptr, 1 );

    // get grid reference
    GridType &grid = *gridptr;

    typedef ProblemCreator< GridType > ProblemCreatorType;
    typedef typename ProblemCreatorType::Algorithm< polOrder >::Type AlgorithmType;
    AlgorithmType algorithm( grid );

    // run simulation using some default stuff such as io, timings and eoc loops
    Dune::Fem::simulate( algorithm );

    // alternative:
    // algorithm.solve( 0 );
    // algorithm.finalize( 0 );
  }
  catch( const Dune::Exception &e )
  {
    std::cerr << e << std::endl;
    return 1;
  }
  catch( ... )
  {
    std::cerr << "Generic exception!" << std::endl;
    return 2;
  }
  return 0;
}
