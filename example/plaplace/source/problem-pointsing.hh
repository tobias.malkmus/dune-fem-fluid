#ifndef PLAPLACE_PROBLEM_POINTSING_HH
#define PLAPLACE_PROBLEM_POINTSING_HH

// dune-fem includes
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{

  namespace Fem
  {

    /**
     * @brief describes the initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class PointSingularitySolution
      : public SteadyStateProblemInterface< FunctionSpace >
    {
    public:
      typedef SteadyStateProblemInterface< FunctionSpace > BaseType;

      static const int dimDomain = BaseType::dimDomain;
      static const int domRange = BaseType::dimRange;

      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      /**
       * @brief define problem parameters
       */
      PointSingularitySolution ()
        : pexponent_( Parameter::getValue< double >( "problem.pexponent" ) ),
          strainfix_( Parameter::getValue< double >( "problem.strainfix" ) ),
          beta_( Parameter::getValue< double >( "problem.lessregularity.beta" ) ),
          alpha_( (beta_ - 1.)*2./pexponent_ +1.05 )
      {                                                             /*@LST0E@*/
        myName = "Point Singularity ";
      }

      /**
       * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const /*@LST0S@@LST0E@*/
      {
        res[ 0 ] = std::pow( arg.two_norm(), alpha_ );
      }

      void jacobian ( const DomainType &arg, JacobianRangeType &jac ) const
      {
        jac = 0;
        jac[ 0 ] = arg;
        const double factor = alpha_ * std::pow(  arg.two_norm(), alpha_ -2. );
        jac *= factor;
      }

      bool hasSource () const { return true; }
      bool hasWeakSource () const { return true; }

      /**
       * @brief evaluate stiff source function
       */
      double source ( const DomainType &arg,
                      const RangeType &u,
                      RangeType &res ) const        /*@LST0S@@LST0E@*/
      {
        res = 0.;

        // time step restriction from the stiff source term
        return 0.;
      }


      void weakSource ( const DomainType &arg, JacobianRangeType &res ) const
      {
        jacobian( arg, res );
        double val = res.frobenius_norm2();
        val += strainfix_;
        res *= std::pow( val, ( pexponent_ -2.0 ) / 2.0 );
      }

      /**
       * @brief latex output for EocOutput
       */
      std::string description () const
      {
        std::ostringstream ofs;
        ofs << "Problem: " << myName;

        return ofs.str();
      }

    private:
      const double pexponent_;
      const double strainfix_;

    public:
      double beta_;
      double alpha_;
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune

#endif  // #ifndef PLAPLACE_PROBLEM_POINTSING_HH
