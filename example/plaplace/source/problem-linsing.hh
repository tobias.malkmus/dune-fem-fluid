#ifndef  DUNE_PROBLEM_LIN_PLAPLACE_HH__
#define  DUNE_PROBLEM_LIN_PLAPLACE_HH__


// dune-fem includes
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{

  namespace Fem
  {

    /**
     * @brief describes the initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class LineSingularitySolution
      : public SteadyStateProblemInterface< FunctionSpace >
    {
    public:
      typedef SteadyStateProblemInterface< FunctionSpace > BaseType;

      static const int dimDomain = BaseType::dimDomain;
      static const int dimRange = BaseType::dimRange;

      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      /**
       * @brief define problem parameters
       */
      LineSingularitySolution ()
        : pexponent_( Parameter::getValue< double >( "problem.pexponent" ) ),
          strainfix_( Parameter::getValue< double >( "problem.strainfix" ) ),
          beta_( Parameter::getValue< double >( "problem.lessregularity.beta" ) ),
          alpha_( (beta_ - 0.5)*2./pexponent_ +1.05 )
      {
        myName = "Line Singularity ";
      }

      /**
       * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const
      {
        res[ 0 ] = std::pow( std::abs( arg[ 0 ] - 0.5 ), alpha_ ) + arg[ 1 ];
      }

      void boundaryValue ( const DomainType &arg, const RangeType &u, RangeType &res ) const
      {
        evaluate( arg, res );
      }

      void jacobian ( const DomainType &arg, JacobianRangeType &jac ) const
      {
        jac = 0;
        jac[ 0 ][ 0 ] = alpha_ * std::pow( std::abs( arg[ 0 ] - 0.5 ), alpha_ -1. );
        jac[ 0 ][ 0 ] *= arg[ 0 ] / std::abs( arg[ 0 ] - 0.5 );

        jac[ 0 ][ 1 ] = 1.;
      }

      bool hasSource () const { return true; }
      bool hasWeakSource () const { return true; }

      /**
       * @brief evaluate stiff source function
       */
      double source ( const DomainType &arg, const RangeType &u, RangeType &res ) const
      {
        res = 0.;
        return 0.;
      }


      void weakSource ( const DomainType &arg, JacobianRangeType &res ) const
      {
        jacobian( arg, res );
        double val = res.frobenius_norm2();
        val += strainfix_;
        res *= std::pow( val, ( pexponent_ -2.0 ) / 2.0 );
      }


      /**
       * @brief latex output for EocOutput
       */
      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName;

        return ofs.str();
      }

    private:
      const double pexponent_;
      const double strainfix_;

    public:
      double beta_;
      double alpha_;
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune
#endif  /*DUNE_PROBLEM_HH__*/

