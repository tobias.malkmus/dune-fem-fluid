#ifndef  DUNE_PROBLEM_ONE_PLAPLACE_HH__
#define  DUNE_PROBLEM_ONE_PLAPLACE_HH__

// dune-fem includes
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>

namespace Dune
{

  namespace Fem
  {

    /**
     * @brief describes the initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class PLaplaceEqnSolution
      : public SteadyStateProblemInterface< FunctionSpace >
    {
    public:
      typedef SteadyStateProblemInterface< FunctionSpace >  BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;


      /**
       * @brief define problem parameters
       */
      PLaplaceEqnSolution ()
        : pexponent_( Parameter::getValue< double >( "problem.pexponent" ) ),
          strainfix_( Parameter::getValue< double >( "problem.strainfix" ) ),
          eps_( Parameter::getValue< double >( "problem.eps" ) )
      {
        std::stringstream name;
        name << "Smooth problem with p: " << pexponent_;
        myName = name.str();
      }

      /**
       * @brief evaluates \f$ u_0(x) \f$
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const      /*@LST0S@@LST0E@*/
      {
        res = sin( arg[ 0 ] + arg[ 1 ] );
      }

      void jacobian ( const DomainType &arg, JacobianRangeType &Jac ) const
      {
        Jac = 0;

        const double xx = arg[ 0 ];
        const double yy = arg[ 1 ];
        Jac[ 0 ][ 0 ] = cos( xx+yy );
        Jac[ 0 ][ 1 ] = cos( xx+yy );
      }


      bool hasSource () const { return false; }
      bool hasWeakSource () const { return true; }

      void weakSource ( const DomainType &arg, JacobianRangeType &res ) const
      {
        jacobian( arg, res );

        double val = res.frobenius_norm2();
        val += strainfix_;
        res *= std::pow( val, (pexponent_ -2.0 ) /2.0 );
      }


      /**
       * @brief evaluate stiff source function
       */
      double source ( const DomainType &arg,
                      const RangeType &u,
                      RangeType &res ) const /*@LST0S@@LST0E@*/
      {
        res = 0.;
        // time step restriction from the stiff source term
        return 0.;
      }

      /**
       * @brief latex output for EocOutput
       */
      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName <<" eps: "<< eps_<<" ";
        return ofs.str();
      }

    private:
      const double pexponent_;
      const double strainfix_;
      const double eps_;

    public:
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune
#endif  /*DUNE_PROBLEM_HH__*/

