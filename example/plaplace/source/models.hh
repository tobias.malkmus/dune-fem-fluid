#ifndef PLAPALCE_MODEL_HH
#define PLAPALCE_MODEL_HH

#include <dune/fem-fluid/probleminterfaces.hh>
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/model/default.hh>

#include "local.hh"

/**********************************************
 * Analytical model                           *
 *********************************************/

/**
 * @brief Traits class for NavierStokesModel
 */
template< class GridPart, class Problem >
class PlaplaceModelTraits
{
public:
  typedef GridPart GridPartType;
  typedef typename GridPartType::GridType GridType;
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
  typedef typename GridPartType::IntersectionIteratorType IntersectionIterator;
  typedef typename IntersectionIterator::Intersection IntersectionType;

  typedef Problem ProblemType;
  typedef typename ProblemType::FunctionSpaceType FunctionSpaceType;

  static const int dimDomain = FunctionSpaceType::dimDomain;
  static const int dimRange = FunctionSpaceType::dimRange;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
  typedef Dune::FieldVector< RangeFieldType, dimDomain-1 >                   FaceDomainType;
};


template< class GridPartType, class Problem >
class PlaplaceModel
  : public DefaultModel< PlaplaceModelTraits< GridPartType, Problem > >
{
  typedef PlaplaceModel< GridPartType, Problem > ThisType;
  typedef DefaultModel< PlaplaceModelTraits< GridPartType, Problem > > BaseType;

public:
  typedef typename GridPartType::GridType GridType;
  typedef PlaplaceModelTraits< GridPartType, Problem >                     Traits;
  static const int dimDomain = Traits::dimDomain;
  static const int dimRange  = Traits::dimRange;
  typedef typename Traits::DomainType DomainType;
  typedef typename Traits::RangeType RangeType;
  typedef typename Traits::RangeFieldType RangeFieldType;
  typedef typename Traits::FaceDomainType FaceDomainType;
  typedef typename Traits::JacobianRangeType JacobianRangeType;

  typedef typename Traits::EntityType EntityType;
  typedef typename Traits::IntersectionType IntersectionType;

  typedef typename Traits::ProblemType ProblemType;

  const static bool hasFlux = false;
  const static bool hasDiffusion = true;
  const static bool hasSource = true;
  const static bool hasMass = false;

  // for production runs this should be switched off, if no weak source exists !!!
  const static bool hasWeakSource = true;

  const static bool isEvolutionProblem = false;


  // Diffusion
  // ---------

  struct Diffusion
  {
    Diffusion () {}

    Diffusion ( double p, double sd, double eps, const RangeType &, const JacobianRangeType &jac )
      : jac_( jac ), eps_( eps )
    {
      viscosity_ = jac_.frobenius_norm2();
      viscosity_ += sd;
      viscosity_ = std::pow( viscosity_, ( p - 2.) / 2.0 );
    }

    Diffusion ( double p, double sd, double eps, const RangeType &, const JacobianRangeType &jac, const JacobianRangeType &shift )
      : jac_( jac ), eps_( eps )
    {
      viscosity_ = jac_.frobenius_norm2();
      viscosity_ += shift.frobenius_norm2();
      viscosity_ += sd;
      viscosity_ = std::pow( viscosity_, ( p - 2.) / 2.0 );
    }

    void operator() ( JacobianRangeType &diff ) const
    {
      diff = jac_;
      diff *= viscosity_;
    }

  protected:
    JacobianRangeType jac_;
    double viscosity_;
    const double eps_;
    Dune::FieldMatrix< double, 2, 2 > rot_;
  };


  // LinearDiffusion
  // ---------------

  struct LinearDiffusion
  {
    LinearDiffusion () {}

    LinearDiffusion ( double p, double sd, double eps, const RangeType &, const JacobianRangeType &jac )
      : jac_( jac ), eps_( eps )
    {
      viscosity1_ = jac.frobenius_norm2();
      viscosity1_ += sd;
      viscosity2_ = std::pow( viscosity1_, (p-4.) /2. );
      viscosity1_ *= viscosity2_;
      viscosity2_ *= ( p - 2. ) * eps_;
    }

    LinearDiffusion ( double p, double sd, double eps, const RangeType &, const JacobianRangeType &jac, const JacobianRangeType &shift )
      : jac_( jac ), shift_( shift ), eps_( eps )
    {
      viscosity1_ = jac.frobenius_norm2();
      viscosity1_ += shift.frobenius_norm2();
      viscosity1_ += sd;
      viscosity2_ = std::pow( viscosity1_, (p-4.) /2. );
      viscosity1_ *= viscosity2_;
      viscosity2_ *= ( p - 2. ) * eps_;
    }

    LinearDiffusion ( const LinearDiffusion & ) = default;
    LinearDiffusion ( LinearDiffusion && ) = default;

    LinearDiffusion &operator= ( const LinearDiffusion & ) = default;

    void operator() ( const RangeType &val, const JacobianRangeType &grad, JacobianRangeType &A ) const
    {
      A = grad;
      double scp = 0.;
      for( int i = 0; i < dimRange; ++i )
        scp += A[ i ] *jac_[ i ];

      A *= viscosity1_;
      A.axpy( viscosity2_ * scp, jac_ );
    }

    void operator() ( const RangeType &val, const JacobianRangeType &grad, const JacobianRangeType &shift, JacobianRangeType &A ) const
    {
      A = grad;
      double scp = 0.;
      for( int i = 0; i < dimRange; ++i )
      {
        scp += shift_[ i ] * shift[ i ];
        scp += A[ i ] *jac_[ i ];
      }

      A *= viscosity1_;
      A.axpy( viscosity2_ * scp, jac_ );
    }

  protected:
    JacobianRangeType jac_, shift_;
    double viscosity1_, viscosity2_;
    double eps_;
    Dune::FieldMatrix< double, 2, 2 > rot_;
  };

public:

  /**
   * @brief Constructor
   *
   * initializes model parameter
   *
   * @param problem Class describing the initial(t=0) and exact solution
   */
  PlaplaceModel ( const ProblemType &problem )
    : problem_( problem ),
      problemHasSource( problem.hasSource() ),
      problemHasWeakSource( problem.hasWeakSource() ),
      p_( Dune::Fem::Parameter::getValue< double >( "problem.pexponent" ) ),
      sd_( Dune::Fem::Parameter::getValue< double >( "problem.strainfix" ) ),
      eps_( Dune::Fem::Parameter::getValue< double >( "problem.eps" ) )
  {}

  const ProblemType &problem () const { return problem_; }

  void setLambda ( double lambda ) const {}

  void setRelaxFactor ( double f ) const {}

  template< class ... Args >
  Diffusion diffusion ( const EntityType &en,
                        const double time,
                        const DomainType &x,
                        Args ... args ) const
  {
    return Diffusion( p_, sd_, eps_, args ... );
  }


  template< class ... Args >
  LinearDiffusion linearDiffusion ( const EntityType &en,
                                    const double time,
                                    const DomainType &x,
                                    Args ... args ) const
  {
    return LinearDiffusion( p_, sd_, eps_, args ... );
  }



  void addScaledMassTerm ( const EntityType &entity,
                           const double time,
                           const DomainType &x,
                           const double scale,
                           const RangeType &u,
                           RangeType &mass ) const
  {}

  //! weakSource
  void weakSource ( const EntityType &en,
                    const double time,
                    const DomainType &x,
                    JacobianRangeType &s ) const
  {
    if( problemHasWeakSource )
    {
      DomainType xgl = en.geometry().global( x );
      problem().weakSource( xgl, time, s );
      s *= -1;
    }
  }


  //! source
  double source ( const EntityType &en,
                  const double time,
                  const DomainType &x,
                  const RangeType &u,
                  const JacobianRangeType &jac,
                  RangeType &s ) const
  {
    stiffSource( en, time, x, u, jac, s );

    double ret = 0;
    if( problemHasSource )
    {
      DomainType xgl = en.geometry().global( x );
      RangeType nonStiffsource;
      ret = problem().source( xgl, time, u, nonStiffsource );
      s -= nonStiffsource;
    }

    return ret;
  }


  void stiffSource ( const EntityType &en,
                     const double time,
                     const DomainType &x,
                     const RangeType &u,
                     const JacobianRangeType &jac,
                     RangeType &s ) const
  {
    s = 0;
  }


  //! linearize Source
  void linearSource ( const EntityType &en,
                      const double time,
                      const DomainType &x,
                      const RangeType &uStar,
                      const JacobianRangeType &jacUStar,
                      const RangeType &u,
                      const JacobianRangeType &jac,
                      RangeType &s ) const
  {
    linearStiffSource( en, time, x, uStar, jacUStar, u, jac, s );
  }



  void linearStiffSource ( const EntityType &en,
                           const double time,
                           const DomainType &x,
                           const RangeType &uStar,
                           const JacobianRangeType &jacUStar,
                           const RangeType &u,
                           const JacobianRangeType &jac,
                           RangeType &s ) const
  {
    s = 0;
  }

  /**
   * @brief advection term \f$F\f$
   *
   * @param en entity on which to evaluate the advection term
   * @param time current time of TimeProvider
   * @param x coordinate local to entity
   * @param u \f$U\f$
   * @param f \f$f(U)\f$
   */
  void advection ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   JacobianRangeType &f ) const
  {
    f = 0;
  }


  void linearAdvection ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &uStar,
                         const RangeType &u,
                         JacobianRangeType &f ) const
  {
    f = 0;
  }

  void maxSpeed ( const IntersectionType &intersection,
                  const DomainType &normal,
                  const double time,
                  const DomainType &x,
                  const RangeType &u,
                  double &advspeed,
                  double &totalspeed ) const
  {
    advspeed = 0.;
    totalspeed = 0.1;
  }

  void stress ( const EntityType &en,
                const double time,
                const DomainType &x,
                const RangeType &u,
                const JacobianRangeType &jac,
                JacobianRangeType &stress ) const
  {}

  // return the eigenvalues of the advection operator
  // assumption: eigenValue[0] > ...  > eigenValue[r]
  void eigenValues ( const IntersectionType &intersection,
                     const double time,
                     const DomainType &x,
                     const DomainType &normal,
                     const RangeType &u,
                     RangeType &eigenValue ) const
  {
    eigenValue = 0.;
  }

  void eigenValues ( const IntersectionType &intersection,
                     const double time,
                     const DomainType &x,
                     const DomainType &normal,
                     const RangeType &uStar,
                     const RangeType &u,
                     RangeType &eigenValue ) const
  {
    eigenValues( intersection, time, x, normal, u, eigenValue );
  }

  /**
   * @brief diffusion term \f$A\f$
   */
  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   JacobianRangeType &A ) const
  {
    diffusion( en, time, x, u, jac ) ( A );
  }

  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   const JacobianRangeType &shift,
                   JacobianRangeType &A ) const
  {
    diffusion( en, time, x, u, jac, shift ) ( A );
  }


  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &ustar,
                         const JacobianRangeType &jacUStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         JacobianRangeType &A ) const
  {
    LinearDiffusion linDiff = linearDiffusion( en, time, x, ustar, jacUStar );
    linDiff( u, jac, A );
  }


  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &ustar,
                         const JacobianRangeType &jacUStar,
                         const JacobianRangeType &shiftStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         const JacobianRangeType &shift,
                         JacobianRangeType &A ) const
  {
    LinearDiffusion linDiff = linearDiffusion( en, time, x, ustar, jacUStar, shiftStar );
    linDiff( u, jac, shift, A );
  }

  /**
   * @brief checks for existence of dirichlet boundary values
   */
  bool hasBoundaryValue ( const IntersectionType &it ) const { return true; }

  // if we have a boundary value we can still have a Neumann Type BC
  bool hasLinearBoundaryValue ( const IntersectionType &it ) const { return true; }

  /**

   * @brief neuman boundary values \f$g_N\f$ for pass2
   */

  double boundaryFlux ( const IntersectionType &it,
                        const EntityType &entity,
                        const double time,
                        const FaceDomainType &x,
                        const DomainType &normal,
                        const RangeType &uLeft,
                        const JacobianRangeType &jacLeft,
                        RangeType &bFlux ) const
  {
    bFlux = 0;
    return normal.two_norm();
  }


  /** \brief boundary flux
   */

  void linearBoundaryFlux ( const IntersectionType &it,
                            const EntityType &entity,
                            const double time,
                            const FaceDomainType &x,
                            const DomainType &normal,
                            const RangeType &uStar,
                            const JacobianRangeType &linJacLeft,
                            const RangeType &uLeft,
                            const JacobianRangeType &jacLeft,
                            RangeType &gLeft ) const
  {
    gLeft = 0;
  }


  /**
   * @brief dirichlet boundary values
   */

  void linearBoundaryValue ( const IntersectionType &it,
                             const double time,
                             const FaceDomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const RangeType &uLeft,
                             const JacobianRangeType &jacLeft,
                             RangeType &uRight ) const
  {
    // inflow or dirichlet data
    uRight = 0.;
  }

  void boundaryValue ( const IntersectionType &it,
                       const double time,
                       const FaceDomainType &x,
                       const RangeType &uLeft,
                       const JacobianRangeType &jacLeft,
                       RangeType &uRight ) const
  {
    // inflow or dirichlet data
    const DomainType xgl = it.geometry().global( x );
    problem().boundaryValue( xgl, time, uLeft, uRight );
  }

  double p () const { return p_; }
  double sd () const { return sd_; }

protected:

  const ProblemType &problem_;
  const bool problemHasSource;
  const bool problemHasWeakSource;
  const double p_;
  const double sd_;
  mutable double eps_;
};

#endif // #ifndef PLAPALCE_MODEL_HH
