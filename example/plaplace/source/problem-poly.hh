#ifndef  DUNE_PROBLEM_SIX_PLAPLACE_HH__
#define  DUNE_PROBLEM_SIX_PLAPLACE_HH__


// dune-fem includes
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>

#include "local.hh"

namespace Dune
{

  namespace Fem
  {

    template< class FunctionSpace >
    class PolynomSolution
      : public SteadyStateProblemInterface< FunctionSpace >
    {
      typedef SteadyStateProblemInterface< FunctionSpace > BaseType;

    public:
      static const int domDomain = BaseType::dimDomain;
      static const int dimRange = BaseType::dimRange;

      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      /**
       * @brief define problem parameters
       */
      PolynomSolution ()
        : pexponent_( Parameter::getValue< double >( "problem.pexponent" ) ),
          strainfix_( Parameter::getValue< double >( "problem.strainfix" ) ),
          eps_( Parameter::getValue< double >( "problem.eps" ) )
      {
        myName = "Polynomial solution";
      }

      /** * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const /*@LST0S@@LST0E@*/
      {
        res[ 0 ] = arg[ 0 ]*arg[ 1 ];
      }

      void boundaryValue ( const DomainType &arg, const RangeType &u, RangeType &res ) const
      {
        res[ 0 ] = arg[ 0 ]*arg[ 1 ];
      }

      void jacobian ( const DomainType &arg, JacobianRangeType &Jac ) const
      {
        Jac = 0.0;
        Jac[ 0 ][ 0 ] = arg[ 1 ];
        Jac[ 0 ][ 1 ] = arg[ 0 ];
      }

      bool hasSource () const { return true; }
      bool hasWeakSource () const { return true; }

      void weakSource ( const DomainType &arg, JacobianRangeType &res ) const
      {
        jacobian( arg, res );

        //FieldMatrix< double, 2, 2 > rot = rotation( eps_ );
        double val = res.frobenius_norm2();
        val += strainfix_;
        /*
        JacobianRangeType help( res );
        rot.mv( help[ 0 ], res[ 0 ] );
        */
        res *= std::pow( val, (pexponent_ -2.0 ) / 2.0 );
      }

      void boundaryFlux ( const DomainType &normal, const DomainType &arg,
                          const RangeType &u, RangeType &res ) const
      {
        /*
           JacobianRangeType jac(0);
           jacobian( arg, time, jac );

           double val = jac.frobenius_norm();
           val += strainfix_;
           jac *= std::pow( val, pexponent_ -2.0 );

           jac.mv( normal, res );
         */
        res[ 0 ] = arg * normal;
      }

      /**
       * @brief evaluate non stiff source function
       */
      double source ( const DomainType &arg, const RangeType &u, RangeType &res ) const
      {
        res[ 0 ] = 0.; // duvt( t )* arg[0]*arg[0]*arg[1];
        // time step restriction from the non stiff source term
        return 0.0;
      }

      /**
       * @brief latex output for EocOutput
       */
      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName <<" eps: "<< eps_<<" ";
        return ofs.str();
      }

    private:
      const double pexponent_;
      const double strainfix_;
      const double eps_;

    public:
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune
#endif  /*DUNE_PROBLEM_HH__*/

