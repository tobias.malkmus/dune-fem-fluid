#ifndef ANALYTICALTRAITS_HH
#define ANALYTICALTRAITS_HH

#include <dune/fem/misc/femeoc.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>

#include "models.hh"
#include "problem-exactstokes.hh"
#include "problem-smooth.hh"


namespace __impl
{

  template< class GridPart, class FunctionSpace >
  struct AnalyticalTraits
  {
    // initial problem
    typedef Dune::Fem::EvolutionProblemInterface< FunctionSpace > ProblemType;

    // the models
    typedef NavierStokesModel< GridPart, ProblemType > ModelType;

    typedef ProblemType InitialDataType;
    typedef typename InitialDataType::TimeDependentFunctionType TimeDependentFunctionType;

    typedef typename FunctionSpace::RangeType RangeType;
    typedef typename FunctionSpace::RangeFieldType RangeFieldType;
    typedef typename FunctionSpace::JacobianRangeType JacobianRangeType;

    static ProblemType *problem ()
    {
      static const std::string probString[]  = { "exactstokes", "smooth" };
      int problemId = Dune::Fem::Parameter::getEnum( "problem", probString, 0 );
      switch( problemId )
      {
      case 0:
        return new Dune::Fem::ExactStokesSolution< FunctionSpace >();
      case 1:
        return new Dune::Fem::SmoothSolution< FunctionSpace >();
      default:
        abort();
        return 0;
      }
    }

    struct ErrorHandler
    {

      static const int dimDomain = FunctionSpace::dimDomain;
      static const int numErrors = 3;

      ErrorHandler ()
        : ids_
      {
        Dune::Fem::FemEoc::addEntry( std::string( "$L^2$-error Velocity" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "$H^1$-error Velocity" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "$L^2$-error Pressure" ) )
      }
      {}


      template< class Tp, class DiscreteSolution >
      void initializeStep ( const Tp &tp, int loop, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class Tp, class DiscreteSolution >
      void step ( const Tp &tp, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class Tp, class DiscreteSolution >
      void finalizeStep ( const Tp &tp, const ModelType &model, const DiscreteSolution &u ) const
      {
        // compute errors at final time
        GridPart &gridPart = u.space().gridPart();

        // first compute mean value of pressure difference
        double meanValuePDiff = 0.;
        std::for_each( Dune::elements( gridPart ).begin(), Dune::elements( gridPart ).end(),
                       [ &u, f = model.problem().fixedTimeFunction( tp.time() ), &meanValuePDiff ]
                         ( const typename DiscreteSolution::EntityType & entity )
                       {
                         auto local = u.localFunction( entity );
                         auto geometry = entity.geometry();
                         Dune::Fem::CachingQuadrature< GridPart, 0 > quad( entity, u.space().order() * 2 );
                         for( const auto &qp: quad )
                         {
                           const double weight = qp.weight() * geometry.integrationElement( qp.position() );
                           typename FunctionSpace::RangeType value, valueh;
                           local.evaluate( qp, valueh );
                           f.evaluate( geometry.global( qp.position() ), value );
                           meanValuePDiff += ( value[ dimDomain ] - valueh[ dimDomain ] ) * weight;
                         }
                       } );

        // now compute errors
        std::array< double, numErrors > error; error.fill( 0.0 );
        std::for_each( Dune::elements( gridPart ).begin(), Dune::elements( gridPart ).end(),
                       [ &u, f = model.problem().fixedTimeFunction( tp.time() ), &error, meanValuePDiff, &model ]
                         ( const typename DiscreteSolution::EntityType & entity )
                       {
                         auto local = u.localFunction( entity );
                         auto geometry = entity.geometry();
                         Dune::Fem::CachingQuadrature< GridPart, 0 > quad( entity, u.space().order() * 2 );
                         for( const auto &qp: quad )
                         {
                           const double weight = qp.weight() * geometry.integrationElement( qp.position() );
                           typename FunctionSpace::RangeType value, valueh;
                           typename FunctionSpace::JacobianRangeType dphi, dphih;
                           local.evaluate( qp, valueh );
                           f.evaluate( geometry.global( qp.position() ), value );
                           value -= valueh;
                           value[ dimDomain ] -= meanValuePDiff;

                           local.jacobian( qp, dphih );
                           f.jacobian( geometry.global( qp.position() ), dphi );
                           dphi -= dphih;

                           error[ 2 ] += value[ dimDomain ] * value[ dimDomain ] * weight;

                           value[ dimDomain ] = 0.0;
                           error[ 0 ] += value.two_norm2() * weight;
                           error[ 1 ] += value.two_norm2() * weight;
                           dphi[ dimDomain ] = 0.0;
                           error[ 1 ] += dphi.frobenius_norm2() * weight;
                         }
                       } );

        for( std::size_t i = 0; i < numErrors; ++i )
          Dune::Fem::FemEoc::setErrors( ids_[ i ], std::sqrt( error[ i ] ) );
      }

    private:
      std::array< std::size_t, numErrors > ids_;
    };

  };

} // namespace __impl

#endif //#ifndef ANALYTICALTRAITS_HH
