#ifndef SRC_MOVINGDOMAIN_SOURCE_DOMAIN_HH
#define SRC_MOVINGDOMAIN_SOURCE_DOMAIN_HH

#include <dune/fem/function/common/function.hh>

template< class FunctionSpace >
class Domain
{
  typedef Domain< FunctionSpace > ThisType;

public:

  struct Coordinate
    : public Dune::Fem::Function< FunctionSpace, Coordinate >
  {
    Coordinate ( double t ) : t_( t ) {}

    Coordinate ( const Coordinate & ) = default;
    Coordinate ( Coordinate && ) = default;

    typedef typename FunctionSpace::DomainType DomainType;
    typedef typename FunctionSpace::RangeType RangeType;
    typedef typename FunctionSpace::JacobianRangeType JacobianRangeType;
    typedef typename FunctionSpace::HessianRangeType HessianRangeType;

    void evaluate ( const DomainType &x, RangeType &y ) const
    {
      y = x;
      y[ 0 ] += 0.1 * std::sin( 2.0 * M_PI * t_ ) * x[ 0 ];
    }

    void jacobian ( const DomainType &x, JacobianRangeType &jac ) const {}
    void hessian ( const DomainType &x, HessianRangeType &hess ) const {}

  protected:
    double t_;
  };

  struct Velocity
    : public Dune::Fem::Function< FunctionSpace, Velocity >
  {
    Velocity ( double t ) : t_( t ) {}

    Velocity ( const Velocity & ) = default;
    Velocity ( Velocity && ) = default;

    typedef typename FunctionSpace::DomainType DomainType;
    typedef typename FunctionSpace::RangeType RangeType;
    typedef typename FunctionSpace::JacobianRangeType JacobianRangeType;
    typedef typename FunctionSpace::HessianRangeType HessianRangeType;

    void evaluate ( const DomainType &x, RangeType &y ) const
    {
      y = 0;
      y[ 0 ] += 0.1 * 2.0 * M_PI * std::cos( 2.0 * M_PI * t_ ) * x[ 0 ];
    }

    void jacobian ( const DomainType &x, JacobianRangeType &jac ) const {}
    void hessian ( const DomainType &x, HessianRangeType &hess ) const {}

  protected:
    double t_;
  };


  Domain () {}


  // return coordinate function fixed at timepoint t
  Coordinate coordinate ( const double t ) const { return Coordinate( t ); }

  // return domain velocity function fixed at timepoint t
  Velocity velocity ( double t ) const { return Velocity( t ); }

};

#endif // #ifndef SRC_MOVINGDOMAIN_SOURCE_DOMAIN_HH
