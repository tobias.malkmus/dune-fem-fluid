#ifndef DOMAINDISCRETETRAITS_HH
#define DOMAINDISCRETETRAITS_HH

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/lagrange.hh>

#include "domain.hh"
#include "domaindescriptor.hh"

namespace __impl
{

  template< class GridPart, int polOrd >
  struct DomainDiscreteTraits
  {
    typedef Dune::Fem::FunctionSpace< double, double, GridPart::dimension, GridPart::dimension > FunctionSpaceType;
    typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPart, polOrd > DiscreteFunctionSpaceType;
    typedef Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;

    typedef Dune::Fem::DomainDescriptor< DiscreteFunctionType, Domain > DomainDescriptorType;
  };

} // namespace __impl

#endif // #ifndef DOMAINDISCRETETRAITS_HH
