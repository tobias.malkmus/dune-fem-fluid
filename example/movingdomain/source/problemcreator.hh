#ifndef PROBLEMCREATOR_HH
#define PROBLEMCREATOR_HH

#ifndef POLORDER
#define POLORDER 2
#endif


// dune-fem includes
#include <dune/grid/io/file/dgfparser/dgfparser.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/geogridpart.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fem-dg/algorithm/evolution.hh>

// local includes
#include "analyticaltraits.hh"
#include "discretetraits.hh"
#include "domaindiscretetraits.hh"


template< class Grid, template< class, int > class DomainTraits = __impl::DomainDiscreteTraits >
struct ProblemCreator
{
  typedef ProblemCreator< Grid > ThisType;

  typedef Grid GridType;
  typedef Dune::Fem::AdaptiveLeafGridPart< GridType > HostGridPartType;

  // type of moving domain desription
  using DomainDiscreteTraits = DomainTraits< HostGridPartType, 1 >;

  typedef typename DomainDiscreteTraits::DiscreteFunctionType DiscreteCoordFunctionType;
  typedef typename DomainDiscreteTraits::DomainDescriptorType DomainDescriptorType;

  typedef Dune::Fem::GeoGridPart< DiscreteCoordFunctionType > GridPartType;

  typedef Dune::Fem::FunctionSpace< double, double, GridPartType::dimension, GridPartType::dimension + 1 > FunctionSpaceType;

  static inline std::string moduleName () { return ""; }

  // everything about the analytical model
  using AnalyticalTraits = __impl::AnalyticalTraits< GridPartType, FunctionSpaceType >;

  // every thing about the discretization
  template< int polOrder >
  using DiscreteTraits = __impl::DiscreteTraits< GridPartType, FunctionSpaceType, typename AnalyticalTraits::ModelType, polOrder >;

  template< int polOrder >
  struct Algorithm
  {
    typedef Dune::Fem::MovingDomainEvolutionAlgorithm< GridType, ThisType, polOrder > Type;
  };
};

#endif // #ifndef SRC_PROBLEMS_PNSEQ_PROBLEMCREATOR_HH
