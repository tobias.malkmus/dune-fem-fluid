#ifndef FLUID_SRC_MOVINGDOMAIN_SOURCE_DOMAINDESCRIPTOR_HH
#define FLUID_SRC_MOVINGDOMAIN_SOURCE_DOMAINDESCRIPTOR_HH

#include <dune/fem/function/common/gridfunctionadapter.hh>
#include <dune/fem/space/common/interpolate.hh>

namespace Dune
{

  namespace Fem
  {

    template< class DiscreteFunction, template< class > class Domain >
    class DomainDescriptor
    {
      typedef DomainDescriptor< DiscreteFunction, Domain > ThisType;

    public:
      typedef DiscreteFunction DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;

      typedef Domain< FunctionSpaceType > DomainType;

      DomainDescriptor ( GridPartType &gridPart )
        : gridPart_( gridPart ),
          space_( gridPart ),
          coordFunction_( "coordinate function", space_ ),
          domainVelocity_( "Domain Velocity", space_ )
      {}

      template< class TimeProvider >
      void initialize ( TimeProvider &tp, int loop )
      {
        const double time = tp.time();
        interpolate( gridFunctionAdapter( domain.coordinate( time ), gridPart_, 2 *space_.order() ), coordFunction_ );
        interpolate( gridFunctionAdapter( domain.velocity( time ), gridPart_, 2 *space_.order() ), domainVelocity_ );
      }

      void setTime ( double time )
      {
        interpolate( gridFunctionAdapter( domain.coordinate( time ), gridPart_, 2 *space_.order() ), coordFunction_ );
        interpolate( gridFunctionAdapter( domain.velocity( time ), gridPart_, 2 *space_.order() ), domainVelocity_ );
      }

      const DiscreteFunctionType &coordFunction () const { return coordFunction_; }

      DiscreteFunctionType &domainVelocity () { return domainVelocity_; }
      const DiscreteFunctionType &domainVelocity () const { return domainVelocity_; }

    protected:
      GridPartType gridPart_;

      DiscreteFunctionSpaceType space_;
      DiscreteFunctionType coordFunction_;

      DiscreteFunctionType domainVelocity_;

      DomainType domain;
    };

  } // namespace Fem

} // namespace Dune
#endif // #ifndef FLUID_SRC_MOVINGDOMAIN_SOURCE_DOMAINDESCRIPTOR_HH
