#ifndef DUNE_HEAT_PROBLEM_INITIAL_HH
#define DUNE_HEAT_PROBLEM_INITIAL_HH

#include <dune/common/version.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{
  namespace Fem
  {

    /**
     * @brief describes the initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class InitialValueProblem
      : public EvolutionProblemInterface< FunctionSpace >
    {
    public:
      typedef EvolutionProblemInterface< FunctionSpace > BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      /**
       * @brief define problem parameters
       */
      InitialValueProblem ()
        : BaseType(),
          starttime_( Parameter::getValue< double >( "problem.starttime", 0.0 ) ),
          m_( Parameter::getValue< double >( "problem.mu" ) ),
          v_( Parameter::getValue< DomainType >( "problem.velocity" ) )
      {
        myName = "InitialValueProblem ";
      }

      bool hasWeakSource () const { return false; }
      bool hasSource () const { return false; }

      double starttime () const { return starttime_; }

      void evaluate ( const DomainType &arg, RangeType &res ) const { evaluate( arg, starttime_, res ); }

      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const
      {
        res = ((arg[ 0 ] - arg[ 1 ] ) > 0 );
      }

      void boundaryValue ( const DomainType &xgl, double t, const RangeType &u, RangeType &bnd ) const
      {
        bnd = u;
      }

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &jac ) const
      {
        jac = 0;
      }

      double weakSource ( const DomainType &arg, const double t, JacobianRangeType &res ) const
      {
        res = 0;
        return 0.;
      }

      double source ( const DomainType &arg, const double t, const RangeType &u, RangeType &res ) const
      {
        res = 0;
        return 0.0;
      }

      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName
            << ", End time: " << Parameter::getValue< double >( "problem.endtime" );

        return ofs.str();
      }

    private:
      double starttime_;
      const double m_;
      const DomainType v_;

    public:
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune
#endif  // #ifndef DUNE_HEAT_PROBLEM_INITIAL_HH
