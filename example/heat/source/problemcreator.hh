#ifndef HEAT_PROBLEMCREATOR_HH
#define HEAT_PROBLEMCREATOR_HH

#ifndef POLORDER
#define POLORDER 1
#endif

// dune-fem includes
#include <dune/grid/io/file/dgfparser/dgfparser.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fem-dg/algorithm/evolution.hh>


// local includes
#include "analyticaltraits.hh"
#include "discretetraits.hh"


template< class Grid >
struct HeatProblemCreator
{
  typedef HeatProblemCreator< Grid > ThisType;

  typedef Grid GridType;
  typedef Dune::Fem::LeafGridPart< GridType > HostGridPartType;
  typedef HostGridPartType GridPartType;

  typedef Dune::Fem::FunctionSpace< double, double, GridPartType::dimension, GridPartType::dimension > FunctionSpaceType;

  static inline std::string moduleName () { return ""; }

  // everything about the analytical model
  using AnalyticalTraits = __impl::AnalyticalTraits< GridPartType, FunctionSpaceType >;

  // everything about the discretization
  template< int polOrder >
  using DiscreteTraits = __impl::DiscreteTraits< GridPartType, FunctionSpaceType, typename AnalyticalTraits::ModelType, polOrder >;

  // define type of used algorithm
  template< int polOrder >
  struct Algorithm
  {
    typedef Dune::Fem::EvolutionAlgorithm< GridType, ThisType, polOrder > Type;
  };

};

#endif // #ifndef HEAT_PROBLEMCREATOR_HH
