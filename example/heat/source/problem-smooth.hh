#ifndef DUNE_HEAT_PROBLEM_SMOOTH_HH
#define DUNE_HEAT_PROBLEM_SMOOTH_HH

#include <dune/common/version.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{

  namespace Fem
  {

    template< class FunctionSpace >
    class SmoothSolution
      : public EvolutionProblemInterface< FunctionSpace >
    {
    public:
      typedef EvolutionProblemInterface< FunctionSpace > BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;

      SmoothSolution ()
        : BaseType(),
          starttime_( Parameter::getValue< double >( "problem.starttime", 0.0 ) ),
          mu_( Dune::Fem::Parameter::getValue< double >( "problem.mu" ) ),
          v_( Dune::Fem::Parameter::getValue< DomainType >( "problem.velocity" ) ),
          myName( "C^inf solution" )
      {}

      bool hasWeakSource () const { return true; }
      bool hasSource () const { return true; }

      double starttime () const { return starttime_; }

      void evaluate ( const DomainType &arg, RangeType &res ) const { evaluate( arg, starttime_, res ); }


      double uvt ( double time ) const
      {
        return (1.+exp( -100*time ));
      }


      double duvt ( double time ) const
      {
        return -100* exp( -100*time );
      }

      /**
       * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const
      {
        res = sin( 2. * arg[ 0 ] * M_PI ) * cos( 2.0 * arg[ 1 ] * M_PI );
        res *= uvt( t );
      }

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &jac ) const
      {
        double time = uvt( t );
        jac[ 0 ][ 0 ] = time * 2.0 *M_PI *cos( 2.0* arg[ 0 ] * M_PI ) * cos( 2.0* arg[ 1 ] * M_PI );
        jac[ 0 ][ 1 ] = time * 2.0 * -M_PI *sin( 2.0 *arg[ 0 ] *M_PI ) * sin( 2.0 * arg[ 1 ] * M_PI );

        for( std::size_t i = 1; i< dimRange; ++i )
          jac[ i ] = jac[ 0 ];
      }


      double weakSource ( const DomainType &arg, const double t, JacobianRangeType &res ) const
      {
        res = 0;
        RangeType value;
        evaluate( arg, t, value );
        jacobian( arg, t, res );

        res *= mu_;

        for( int i = 0; i < dimRange; ++i )
          res[ i ].axpy( -value[ i ], v_ );

        return 0.;
      }

      double source ( const DomainType &arg, const double t, const RangeType &u, RangeType &res ) const
      {
        res = sin( 2.0 * M_PI * arg[ 0 ] ) * cos( 2.0 * M_PI * arg[ 1 ] ) * duvt( t );
        return 0.0;
      }

      std::string description () const
      {
        std::ostringstream ofs;
        ofs << "Problem: " << myName
            << ", End time: " << Parameter::getValue< double >( "problem.endtime" );
        return ofs.str();
      }

    private:
      double starttime_;
      const double mu_;
      const DomainType v_;

    public:
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_HEAT_PROBLEM_SMOOTH_HH
