# install these headers
set(HEADERS
  analyticaltraits.hh
  discretetraits.hh
  models.hh
  problem-initial.hh
  problem-poly.hh
  problem-smooth.hh
  problemcreator.hh
)

if(NOT GRIDTYPE)
  set(GRIDTYPE ALUGRID_SIMPLEX)
endif()

if(NOT GRIDDIM)
  set(GRIDDIM 2)
endif()

add_executable(heat_istl main.cc)
add_dune_mpi_flags( heat_istl )
target_compile_definitions( heat_istl PRIVATE "${GRIDTYPE}" "GRIDDIM=${GRIDDIM}" "USE_ASSERT_THROW" "USE_ZLIB" "ISTLVEC" )
target_link_dune_default_libraries( heat_istl )

add_executable(heat_adaptive main.cc)
add_dune_mpi_flags( heat_adaptive )
target_compile_definitions( heat_adaptive PRIVATE "${GRIDTYPE}" "GRIDDIM=${GRIDDIM}" "USE_ASSERT_THROW" "USE_ZLIB" )
target_link_dune_default_libraries( heat_adaptive )

add_executable(heat_adaptive_automatic main.cc)
add_dune_mpi_flags( heat_adaptive_automatic )
target_compile_definitions( heat_adaptive_automatic PRIVATE "${GRIDTYPE}" "GRIDDIM=${GRIDDIM}" "USE_ASSERT_THROW" "USE_ZLIB" "AUTOMATICDIFF" )
target_link_dune_default_libraries( heat_adaptive_automatic )

install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/example/heat/source)
