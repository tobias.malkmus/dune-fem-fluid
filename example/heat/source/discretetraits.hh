#ifndef DISCRETETRAITS_HH
#define DISCRETETRAITS_HH

#include <dune/fem/space/discontinuousgalerkin.hh>
#include <dune/fem/space/brezzidouglasmarini.hh>

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/operator/linear/istloperator.hh>

#include <dune/fem/operator/common/automaticdifferenceoperator.hh>

#include <dune/fem-dg/operator/dg/dgoperatorbase.hh>
#include <dune/fem-dg/operator/dg/helmholtz.hh>

#include <dune/fem-dg/operator/discretemodel/default.hh>

// numerical fluxes
#include <dune/fem-dg/operator/fluxes/ipdg.hh>
#include <dune/fem-dg/operator/fluxes/linearipdg.hh>
#include <dune/fem-dg/operator/fluxes/primalldgflux.hh>
#include <dune/fem-dg/operator/fluxes/meanvalue.hh>

// zoo of solvers
#include <dune/fem/solver/rungekutta/implicit.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/newtoninverseoperator.hh>
#include <dune/fem/solver/oemsolver.hh>
#include <dune/fem/solver/pardginverseoperators.hh>
#include <dune/fem-dg/algorithm/monitor.hh>


namespace __impl
{


  // DiscreteTraits
  // --------------

  template< class GridPart, class FunctionSpace, class Model, int polOrder >
  struct DiscreteTraits  
  {
#if HAVE_DUNE_LOCALFUNCTIONS
    typedef Dune::Fem::BDMDiscreteFunctionSpace< FunctionSpace, GridPart, polOrder, Dune::Fem::CachingStorage > DiscreteFunctionSpaceType;
#else
    typedef Dune::Fem::DiscontinuousGalerkinSpace< FunctionSpace, GridPart, polOrder, Dune::Fem::CachingStorage > DiscreteFunctionSpaceType;
#endif

#ifdef ISTLVEC
    typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
    typedef Dune::Fem::ISTLLinearOperator< DiscreteFunctionType, DiscreteFunctionType > JacobianOperatorType;
#else
    typedef Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;

#ifdef AUTOMATICDIFF
    typedef Dune::Fem::AutomaticDifferenceLinearOperator< DiscreteFunctionType > JacobianOperatorType;
#else
    typedef Dune::Fem::SparseRowLinearOperator< DiscreteFunctionType, DiscreteFunctionType > JacobianOperatorType;
#endif //#ifdef AUTOMATICDIFF

#endif // #ifdef ISTLVEC

    typedef Dune::Fem::DiscreteModelDefault<
      JacobianOperatorType,
      Dune::Fem::MeanValueFlux< Model >,
//      Dune::Fem::PrimalLdgFlux< Model >
//      Dune::Fem::LinearIpDgFlux< Model >
      Dune::Fem::IpDgFlux< Model >
      > DiscreteModelType;


    typedef Dune::Fem::DgOperator< JacobianOperatorType, DiscreteModelType > BasicOperatorType;
    typedef Dune::Fem::DGHelmholtzOperator< BasicOperatorType > OperatorType;

    // Solver Zoo
    struct Solver
    {
#ifdef ISTLVEC
      typedef Dune::Fem::ISTLGMResOp< DiscreteFunctionType, JacobianOperatorType > BasicLinearSolverType;
      typedef BasicLinearSolverType LinearSolverType;
#else
      typedef Dune::Fem::ParDGGeneralizedMinResInverseOperator< DiscreteFunctionType > BasicLinearSolverType;
      typedef BasicLinearSolverType LinearSolverType;
#endif

      typedef Dune::Fem::NewtonInverseOperator< JacobianOperatorType, LinearSolverType > NonLinearSolverType;
      typedef DuneODE::ImplicitRungeKuttaSolver< OperatorType, NonLinearSolverType > Type;
    };

    typedef Dune::Fem::SolverMonitor< DiscreteFunctionType > SolverMonitorType;

    typedef std::tuple< DiscreteFunctionType * > IOTupleType;
  };

} // namespace __impl

#endif // #ifndef DISCRETETRAITS_HH
