#ifndef HEAT_MODEL_HH
#define HEAT_MODEL_HH

#include <dune/fem-fluid/probleminterfaces.hh>
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/model/default.hh>


/**********************************************
 * Analytical model                           *
 *********************************************/

/**
 * @brief Traits class for HeatModel
 */
template< class GridPart, class Problem >
class HeatModelTraits
{
public:
  typedef GridPart GridPartType;
  typedef typename GridPartType::GridType GridType;
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
  typedef typename GridPartType::IntersectionIteratorType IntersectionIterator;
  typedef typename IntersectionIterator::Intersection IntersectionType;

  typedef Problem ProblemType;
  typedef typename ProblemType::FunctionSpaceType FunctionSpaceType;

  static const int dimDomain = FunctionSpaceType::dimDomain;
  static const int dimRange = FunctionSpaceType::dimRange;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
  typedef Dune::FieldVector< RangeFieldType, dimDomain-1 >                   FaceDomainType;
};


template< class GridPartType, class Problem >
class HeatModel
  : public DefaultModel< HeatModelTraits< GridPartType, Problem > >
{
  typedef DefaultModel< HeatModelTraits< GridPartType, Problem > > BaseType;
  typedef HeatModel< GridPartType, Problem > ThisType;

public:
  typedef typename GridPartType::GridType GridType;
  typedef HeatModelTraits< GridPartType, Problem >                     Traits;
  static const int dimDomain = Traits::dimDomain;
  static const int dimRange  = Traits::dimRange;
  typedef typename Traits::DomainType DomainType;
  typedef typename Traits::RangeType RangeType;
  typedef typename Traits::RangeFieldType RangeFieldType;
  typedef typename Traits::FaceDomainType FaceDomainType;
  typedef typename Traits::JacobianRangeType JacobianRangeType;

  typedef typename Traits::EntityType EntityType;
  typedef typename Traits::IntersectionType IntersectionType;

  typedef typename Traits::ProblemType ProblemType;

  const static bool hasFlux = true;
  const static bool hasDiffusion = true;
  const static bool hasSource = true;
  const static bool hasMass = true;

  // for production runs this should be switched off, if no weak source exists !!!
  const static bool hasWeakSource = true;

  const static bool isEvolutionProblem = true;


  // Diffusion
  // ---------

  struct Diffusion
  {
    Diffusion () {}

    Diffusion ( double mu, const RangeType &, const JacobianRangeType &jac )
      : jac_( jac )
    {
      jac_ *= mu;
    }

    Diffusion ( double mu, const RangeType &, const JacobianRangeType &jac, const JacobianRangeType &shift )
      : jac_( jac )
    {
      jac_ *= mu;
    }

    void operator() ( JacobianRangeType &diff ) const { diff = jac_; }

  protected:
    JacobianRangeType jac_;
  };


  // LinearDiffusion
  // ---------------

  struct LinearDiffusion
  {
    LinearDiffusion () {}

    LinearDiffusion ( const LinearDiffusion & ) = default;
    LinearDiffusion ( LinearDiffusion && ) = default;

    LinearDiffusion &operator= ( const LinearDiffusion & ) = default;

    LinearDiffusion ( double mu, const RangeType &, const JacobianRangeType &jac ) : viscosity_( mu ) {}
    LinearDiffusion ( double mu, const RangeType &, const JacobianRangeType &jac, const JacobianRangeType &shift ) : viscosity_( mu ) {}

    void operator() ( const RangeType &val, const JacobianRangeType &grad, JacobianRangeType &A ) const
    {
      A = grad;
      A *= viscosity_;
    }

    void operator() ( const RangeType &val, const JacobianRangeType &grad, const JacobianRangeType &shift, JacobianRangeType &d ) const
    {
      d = grad;
      d *= viscosity_;
    }

  protected:
    double viscosity_;
  };

protected:
  HeatModel ( const HeatModel &otehr ) = delete;
  const HeatModel &operator=( const HeatModel &other ) = delete;

public:

  /**
   * @brief Constructor
   *
   * initializes model parameter
   *
   * @param problem Class describing the initial(t=0) and exact solution
   */
  HeatModel ( const ProblemType &problem )
    : problem_( problem ),
      mu_( Dune::Fem::Parameter::getValue< double >( "problem.mu" ) ),
      v_( Dune::Fem::Parameter::getValue< DomainType >( "problem.velocity" ) ),
      problemHasSource( problem.hasSource() ),
      problemHasWeakSource( problem.hasWeakSource() )
  {}

  const ProblemType &problem () const { return problem_; }

  void setLambda ( double lambda ) const {}
  void setRelaxFactor ( double f ) const {}

  template< class ... Args >
  Diffusion diffusion ( const EntityType &en,
                        const double time,
                        const DomainType &x,
                        Args ... args ) const
  {
    DomainType xgl = en.geometry().global( x );
    return Diffusion( mu_* diff( xgl ), args ... );
  }


  template< class ... Args >
  LinearDiffusion linearDiffusion ( const EntityType &en,
                                    const double time,
                                    const DomainType &x,
                                    Args ... args ) const
  {
    DomainType xgl = en.geometry().global( x );
    return LinearDiffusion( mu_ * diff( xgl ), args ... );
  }



  void addScaledMassTerm ( const EntityType &entity,
                           const double time,
                           const DomainType &x,
                           const double scale,
                           const RangeType &u,
                           RangeType &mass ) const
  {
    mass.axpy( scale, u );
  }

  //! weakSource
  void weakSource ( const EntityType &en,
                    const double time,
                    const DomainType &x,
                    JacobianRangeType &s ) const
  {
    if( problemHasWeakSource )
    {
      DomainType xgl = en.geometry().global( x );
      problem().weakSource( xgl, time, s );
      s *= -1;
    }
  }


  //! source
  double source ( const EntityType &en,
                  const double time,
                  const DomainType &x,
                  const RangeType &u,
                  const JacobianRangeType &jac,
                  RangeType &s ) const
  {
    stiffSource( en, time, x, u, jac, s );

    double ret = 0;
    if( problemHasSource )
    {
      DomainType xgl = en.geometry().global( x );
      RangeType nonStiffsource;
      ret = problem().source( xgl, time, u, nonStiffsource );
      s -= nonStiffsource;
    }

    return ret;
  }


  void stiffSource ( const EntityType &en,
                     const double time,
                     const DomainType &x,
                     const RangeType &u,
                     const JacobianRangeType &jac,
                     RangeType &s ) const
  {
    s = 0;
  }


  //! linearize Source
  void linearSource ( const EntityType &en,
                      const double time,
                      const DomainType &x,
                      const RangeType &uStar,
                      const JacobianRangeType &jacUStar,
                      const RangeType &u,
                      const JacobianRangeType &jac,
                      RangeType &s ) const
  {
    linearStiffSource( en, time, x, uStar, jacUStar, u, jac, s );
  }



  void linearStiffSource ( const EntityType &en,
                           const double time,
                           const DomainType &x,
                           const RangeType &uStar,
                           const JacobianRangeType &jacUStar,
                           const RangeType &u,
                           const JacobianRangeType &jac,
                           RangeType &s ) const
  {
    s = 0;
  }

  void advection ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   JacobianRangeType &f ) const
  {
    f = 0;
    for( int d = 0; d < dimRange; ++d )
      f[ d ].axpy( u[ d ], v_ );
  }

  void linearAdvection ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &uStar,
                         const RangeType &u,
                         JacobianRangeType &f ) const
  {
    f = 0;
    for( int d = 0; d < dimRange; ++d )
      f[ d ].axpy( u[ d ], v_ );
  }

  void maxSpeed ( const IntersectionType &intersection,
                  const DomainType &normal,
                  const double time,
                  const DomainType &x,
                  const RangeType &u,
                  double &advspeed,
                  double &totalspeed ) const
  {
    advspeed = 0.;
    totalspeed = 0.1;
  }


  // return the eigenvalues of the advection operator
  // assumption: eigenValue[0] > ...  > eigenValue[r]
  void eigenValues ( const IntersectionType &intersection,
                     const double time,
                     const DomainType &x,
                     const DomainType &normal,
                     const RangeType &u,
                     RangeType &eigenValue ) const
  {
    eigenValue = 0.;
  }

  void eigenValues ( const IntersectionType &intersection,
                     const double time,
                     const DomainType &x,
                     const DomainType &normal,
                     const RangeType &uStar,
                     const RangeType &u,
                     RangeType &eigenValue ) const
  {
    eigenValues( intersection, time, x, normal, u, eigenValue );
  }

  /**
   * @brief diffusion term \f$A\f$
   */
  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   JacobianRangeType &A ) const
  {
    diffusion( en, time, x, u, jac ) ( A );
  }

  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   const JacobianRangeType &shift,
                   JacobianRangeType &A ) const
  {
    diffusion( en, time, x, u, jac, shift ) ( A );
  }

  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &ustar,
                         const JacobianRangeType &jacUStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         JacobianRangeType &A ) const
  {
    linearDiffusion( en, time, x, ustar, jacUStar )( u, jac, A );
  }


  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &ustar,
                         const JacobianRangeType &jacUStar,
                         const JacobianRangeType &shiftStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         const JacobianRangeType &shift,
                         JacobianRangeType &A ) const
  {
    linearDiffusion( en, time, x, ustar, jacUStar, shiftStar )( u, jac, shift, A );
  }

  /**
   * @brief checks for existence of dirichlet boundary values
   */
  bool hasBoundaryValue ( const IntersectionType &it ) const { return true; }

  // if we have a boundary value we can still have a Neumann Type BC
  bool hasLinearBoundaryValue ( const IntersectionType &it ) const { return true; }

  /**

   * @brief neuman boundary values \f$g_N\f$ for pass2
   */

  double boundaryFlux ( const IntersectionType &it,
                        const EntityType &entity,
                        const double time,
                        const FaceDomainType &x,
                        const DomainType &normal,
                        const RangeType &uLeft,
                        const JacobianRangeType &jacLeft,
                        RangeType &bFlux ) const
  {
    bFlux = 0;
    return normal.two_norm();
  }


  /** \brief boundary flux
   */
  void linearBoundaryFlux ( const IntersectionType &it,
                            const EntityType &entity,
                            const double time,
                            const FaceDomainType &x,
                            const DomainType &normal,
                            const RangeType &uStar,
                            const JacobianRangeType &linJacLeft,
                            const RangeType &uLeft,
                            const JacobianRangeType &jacLeft,
                            RangeType &gLeft ) const
  {
    gLeft = 0;
  }


  /**
   * @brief dirichlet boundary values
   */
  void linearBoundaryValue ( const IntersectionType &it,
                             const double time,
                             const FaceDomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const RangeType &uLeft,
                             const JacobianRangeType &jacLeft,
                             RangeType &uRight ) const
  {
    // inflow or dirichlet data
    uRight = 0.;
  }

  void boundaryValue ( const IntersectionType &it,
                       const double time,
                       const FaceDomainType &x,
                       const RangeType &uLeft,
                       const JacobianRangeType &jacLeft,
                       RangeType &uRight ) const
  {
    // inflow or dirichlet data
    const DomainType xgl = it.geometry().global( x );
    problem().boundaryValue( xgl, time, uLeft, uRight );
  }

protected:

  double diff ( const DomainType & x ) const
  {
    return 1;
    DomainType d( x );
    d[ 0 ] -= 0.5;
    d[ 1 ] -= 0.5;
    if( d.two_norm() > 0.25 )
      return 0.001;
    else
      return 1.0;

  }

  const ProblemType &problem_;
  const double mu_;
  const DomainType v_; 
  const bool problemHasSource;
  const bool problemHasWeakSource;
};

#endif // #ifndef NAVIERSTOKE_MODEL_HH
