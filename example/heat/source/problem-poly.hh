#ifndef DUNE_HEAT_PROBLEM_POLY_HH
#define DUNE_HEAT_PROBLEM_POLY_HH

#include <dune/common/version.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{
  namespace Fem
  {

    /**
     * @brief describes the initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class PolynomialSolution
      : public EvolutionProblemInterface< FunctionSpace >
    {
    public:
      typedef EvolutionProblemInterface< FunctionSpace > BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      /**
       * @brief define problem parameters
       */
      PolynomialSolution ()
        : BaseType(),
          starttime_( Parameter::getValue< double >( "problem.starttime", 0.0 ) ),
          m_( Parameter::getValue< double >( "problem.mu" ) ),
          v_( Parameter::getValue< DomainType >( "problem.velocity" ) )
      {
        myName = "Polynom Problem";
      }

      bool hasWeakSource () const { return true; }
      bool hasSource () const { return true; }

      double starttime () const { return starttime_; }

      void evaluate ( const DomainType &arg, RangeType &res ) const { evaluate( arg, starttime_, res ); }

      double time ( const double t ) const { return 1.+exp( -100*t ); }
//      double time ( const double t ) const { return sin( M_PI * t ); }

      double dtime ( const double t ) const { return exp( -100*t ) * -100; }
//      double dtime ( const double t ) const { return M_PI * cos( M_PI *t ); }

      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const
      {
        res = time( t ) * ( arg[ 0 ] * arg[ 1 ] *arg[ 0 ] - 0.5);
      }

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &jac ) const
      {
        jac[ 0 ][ 0 ] = time( t ) * ( 2. * arg[ 1 ] * arg [ 0 ] );
        jac[ 0 ][ 1 ] = time( t ) * ( arg[ 0 ] * arg[ 0 ] );
        for( std::size_t i = 1; i < dimRange; ++i)
          jac[ i ] = jac[ 0 ];
      }

      double weakSource ( const DomainType &arg, const double t, JacobianRangeType &res ) const
      {
        RangeType value;
        evaluate( arg, t, value );
        jacobian( arg, t, res );
        res *= m_;

        for( int i = 0; i < dimRange; ++i )
          res[ i ].axpy( - value[ i ], v_ );

        return 0.;
      }

      double source ( const DomainType &arg, const double t, const RangeType &u, RangeType &res ) const
      {
        res = dtime( t ) * ( arg[ 0 ] * arg[ 1 ] *arg[ 0 ] - 0.5);
        return 0.0;
      }

      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName
            << ", End time: " << Parameter::getValue< double >( "problem.endtime" );

        return ofs.str();
      }

    private:
      double starttime_;
      const double m_;
      const DomainType v_;

    public:
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune
#endif  // #ifndef DUNE_HEAT_PROBLEM_POLY_HH
