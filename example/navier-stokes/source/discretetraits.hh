#ifndef DISCRETETRAITS_HH
#define DISCRETETRAITS_HH

#include <dune/fem/space/mapper/tuplenonblockmapper.hh>
#include <dune/fem/space/metaspace.hh>
#include <dune/fem/space/taylorhood.hh>

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/linear/spoperator.hh>

#include <dune/fem/operator/common/automaticdifferenceoperator.hh>

#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/operator/matrix/metaspaceistlmatrixadapter.hh>
#include <dune/fem/operator/matrix/taylorhoodistlmatrixadapter.hh>

#include <dune/fem-dg/operator/dg/dgoperatorbase.hh>
#include <dune/fem-dg/operator/dg/nsehelmholtz.hh>

#include <dune/fem-dg/operator/discretemodel/default.hh>

// numerical fluxes
#include <dune/fem-dg/operator/fluxes/ipdg.hh>
#include <dune/fem-dg/operator/fluxes/meanvalue.hh>
#include <dune/fem-dg/operator/fluxes/skewsymmetryns.hh>

// zoo of solvers
#include <dune/fem/solver/basicimplict.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/newtoninverseoperator.hh>
#include <dune/fem/solver/oemsolver.hh>
#include <dune/fem/solver/umfpacksolver.hh>
#include <dune/fem/solver/pardginverseoperators.hh>
#include <dune/fem-dg/algorithm/monitor.hh>


namespace __impl
{


  // DiscreteTraits
  // --------------

  template< class GridPart, class FunctionSpace, class Model, int polOrder >
  struct DiscreteTraits
  {
    // accuracy of quadrature order
    static const int quadOrder = 2 * polOrder;

    typedef Dune::Fem::FunctionSpace< double, double, GridPart::dimension, GridPart::dimension > VelocityFunctionSpaceType;
    typedef Dune::Fem::FunctionSpace< double, double, GridPart::dimension, 1 > PressureFunctionSpaceType;

    typedef Dune::Fem::TaylorHoodDiscontinuousGalerkinSpace< VelocityFunctionSpaceType, GridPart, polOrder, Dune::Fem::CachingStorage > BaseDiscreteFunctionSpaceType;
    typedef Dune::Fem::TupleNonBlockMapper< typename BaseDiscreteFunctionSpaceType::BlockMapperType, BaseDiscreteFunctionSpaceType::numVelocityShapeFunctions,
                                            BaseDiscreteFunctionSpaceType::numPressureShapeFunctions > BlockMapperType;

    typedef Dune::Fem::MetaDiscreteFunctionSpace< BaseDiscreteFunctionSpaceType, BlockMapperType, 1 > DiscreteFunctionSpaceType;

#ifdef ISTLVEC
    typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
    typedef Dune::Fem::ISTLLinearOperator< DiscreteFunctionType, DiscreteFunctionType > JacobianOperatorType;
#else
    typedef Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;

#ifdef AUTOMATICDIFF
    typedef Dune::Fem::AutomaticDifferenceLinearOperator< DiscreteFunctionType > JacobianOperatorType;
#else
    typedef Dune::Fem::SparseRowLinearOperator< DiscreteFunctionType, DiscreteFunctionType > JacobianOperatorType;
#endif //#ifdef AUTOMATICDIFF

#endif // #ifdef ISTLVEC

    typedef Dune::Fem::DiscreteModelDefault<
      JacobianOperatorType,
#ifdef LLF
      Dune::Fem::MeanValueFlux< Model >,
#else
      Dune::Fem::SkewSymetricNSFlux< Model >,
#endif      
      Dune::Fem::IpDgFlux< Model >
      > DiscreteModelType;


    typedef Dune::Fem::DgOperator< JacobianOperatorType, DiscreteModelType > BasicOperatorType;

    typedef Dune::Fem::NSEDGHelmholtzOperator< BasicOperatorType > OperatorType;

    // Solver Zoo
    struct Solver
    {
#ifdef ISTLVEC
      typedef Dune::Fem::ISTLGMResOp< DiscreteFunctionType, JacobianOperatorType > BasicLinearSolverType;
//      typedef Dune::Fem::UMFPACKOp< DiscreteFunctionType, JacobianOperatorType > BasicLinearSolverType;
      typedef BasicLinearSolverType LinearSolverType;
#else
      typedef Dune::Fem::ParDGGeneralizedMinResInverseOperator< DiscreteFunctionType > BasicLinearSolverType;
      typedef BasicLinearSolverType LinearSolverType;
#endif

      typedef Dune::Fem::NewtonInverseOperator< JacobianOperatorType, LinearSolverType > NonLinearSolverType;
      typedef FemODE::ImplicitOdeSolver< OperatorType, NonLinearSolverType, Dune::Fem::SolverMonitor< DiscreteFunctionType > > Type;
    };

    typedef typename Solver::Type::MonitorType SolverMonitorType;

    typedef std::tuple< DiscreteFunctionType * > IOTupleType;
  };

} // namespace __impl

#endif // #ifndef DISCRETETRAITS_HH
