#ifndef  DUNE_PROBLEM_TWO_PSTOKES_HH__
#define  DUNE_PROBLEM_TWO_PSTOKES_HH__

#include <dune/common/version.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{
  namespace Fem
  {

    /**
     * @brief describes the initial and exact solution of the advection-diffusion model
     */
    template< class FunctionSpace >
    class ExactStokesSolution
      : public EvolutionProblemInterface< FunctionSpace >
    {
    public:
      typedef EvolutionProblemInterface< FunctionSpace > BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      /**
       * @brief define problem parameters
       */
      ExactStokesSolution ()
        : BaseType(),
          starttime_( Parameter::getValue< double >( "problem.starttime", 0.0 ) ),
          alpha_( Parameter::getValue< double >( "problem.alpha", 0 ) )
      {
        myName = "Exact Stokes Problem";
      }

      bool hasWeakSource () const { return true; }
      bool hasSource () const { return true; }

      double starttime () const { return starttime_; }

      double epsilon () const { return epsilon_; }

      /**
       * @brief evaluates \f$ u_0(x) \f$
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const      /*@LST0S@@LST0E@*/
      {
        evaluate( arg, starttime_, res );
      }

//      double time ( const double t ) const { return 1.+exp( -100*t ); }
      double time ( const double t ) const { return sin( M_PI * t ); }

//      double dtime ( const double t ) const { return exp( -100*t ) * -100; }
      double dtime ( const double t ) const { return M_PI * cos( M_PI *t ); }

      /**
       * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const /*@LST0S@@LST0E@*/
      {
        res[ 0 ] = -exp( arg[ 0 ] ) * (arg[ 1 ] * cos( arg[ 1 ] ) +  sin( arg[ 1 ] ));
        res[ 1 ] = exp( arg[ 0 ] ) * arg[ 1 ] * sin( arg[ 1 ] );
        res[ dimDomain ] =  2.*exp( arg[ 0 ] ) * sin( arg[ 1 ] );

        res *= time( t );
      }

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &jac ) const
      {
        jac[ 0 ][ 0 ] = time( t ) * -exp( arg[ 0 ] ) *(arg[ 1 ] * cos( arg[ 1 ] ) +  sin( arg[ 1 ] ));
        jac[ 0 ][ 1 ] = time( t ) * -exp( arg[ 0 ] ) *( cos( arg[ 1 ] ) + cos( arg[ 1 ] ) - arg[ 1 ]*sin( arg[ 1 ] ));
        jac[ 1 ][ 0 ] = time( t ) * exp( arg[ 0 ] ) * arg[ 1 ] * sin( arg[ 1 ] );
        jac[ 1 ][ 1 ] = time( t ) * exp( arg[ 0 ] ) * (arg[ 1 ] * cos( arg[ 1 ] ) +  sin( arg[ 1 ] ));

        jac[ dimDomain ][ 0 ] = time( t ) * 2.*exp( arg[ 0 ] ) * sin( arg[ 1 ] );
        jac[ dimDomain ][ 1 ] = time( t ) * 2.*exp( arg[ 0 ] ) * cos( arg[ 1 ] );
      }

      double weakSource ( const DomainType &arg,
                          const double t,
                          JacobianRangeType &res ) const /*@LST0S@@LST0E@*/
      {
        RangeType value;
        evaluate( arg, t, value );
        jacobian( arg, t, res );
        JacobianRangeType Du( res );

        for( int r = 0; r < dimDomain; ++r )
          for( int i = 0; i < dimDomain; ++i )
            res[ r ][ i ] = 0.5 * ( Du[ r ][ i ] + Du[ i ][ r ] );

        res[ dimDomain ] = 0.;

        for( int i = 0; i < dimDomain; ++i )
          for( int j = 0; j < dimDomain; ++j )
            res[ i ][ j ] -= alpha_* value[ i ] * value[ j ];

        // time step restriction from the stiff source term
        return 0.;
      }

      /**
       * @brief evaluate stiff source function
       */
      double source ( const DomainType &arg,
                      const double t,
                      const RangeType &u,
                      RangeType &res ) const
      {
        res[ 0 ] = -exp( arg[ 0 ] ) * (arg[ 1 ] * cos( arg[ 1 ] ) +  sin( arg[ 1 ] ));
        res[ 1 ] = exp( arg[ 0 ] ) * arg[ 1 ] * sin( arg[ 1 ] );
        res[ dimDomain ] =  0.;

        res *= dtime( t );

        res[ 0 ] += time( t ) * 2.*exp( arg[ 0 ] ) * sin( arg[ 1 ] );
        res[ 1 ] += time( t ) * 2.*exp( arg[ 0 ] ) * cos( arg[ 1 ] );

        return 0.0;
      }

      /**
       * @brief latex output for EocOutput
       */
      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName
            << ", Epsilon: " << epsilon_
            << ", End time: " << Parameter::getValue< double >( "problem.endtime" );

        return ofs.str();
      }


      /*  \brief finalize the simulation using the calculated numerical
       *  solution u for this problem
       *
       *  \param[in] variablesToOutput Numerical solution in the suitably chosen variables
       *  \param[in] eocloop Specific EOC loop
       */
      template< class DiscreteFunctionType >
      void finalizeSimulation ( DiscreteFunctionType &variablesToOutput,
                                const int eocloop ) const
      {}

    private:
      DomainType velocity_;
      double starttime_;
      const double alpha_;

    public:
      double epsilon_;
      std::string myName;
    };
  }
}
#endif  /*DUNE_PROBLEM_HH__*/

