#ifndef  DUNE_PROBLEM_THREE_STOKES_HH__
#define  DUNE_PROBLEM_THREE_STOKES_HH__

#include <dune/common/version.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{

  namespace Fem
  {

    template< class FunctionSpace >
    class SymFlowProblem
      : public EvolutionProblemInterface< FunctionSpace >
    {
    public:
      typedef EvolutionProblemInterface< FunctionSpace > BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      SymFlowProblem () : BaseType() {}

      bool hasWeakSource () const { return false; }
      bool hasSource () const { return false; }

      double starttime () const { return 0; }

      /**
       * @brief evaluates \f$ u_0(x) \f$
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const
      {
        res = 0;
      }


      /**
       * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const /*@LST0S@@LST0E@*/
      {
        res = 0;
        if( t > 0 )
        {
          res[ 0 ] = arg[ 1 ] *( 1. - arg[ 1 ] );
          res[ dimDomain ] = 2.0 - 2.0 * arg[ 0 ];
        }
      }

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &jac ) const
      {
        jac = 0;
        jac[ 0 ][ 1 ] = -0.5  + 0.5 * arg[ 1 ];
        jac[ dimDomain ][ 0 ] = -2.0;
      }


      double weakSource ( const DomainType &arg,
                          const double t,
                          JacobianRangeType &res ) const
      {
        res = 0;
        // time step restriction from the stiff source term
        return 0.;
      }
      /**
       * @brief evaluate stiff source function
       */
      double source ( const DomainType &arg,
                      const double t,
                      const RangeType &u,
                      RangeType &res ) const         /*@LST0S@@LST0E@*/
      {
        res = 0;
        return 0.0;
      }

      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: SymFlowProblem "
            << ", End time: " << Parameter::getValue< double >( "problem.endtime" );

        return ofs.str();
      }

    private:
    };

  } // namespace Fem

} // namespace Dune

#endif  // #ifndef  DUNE_PROBLEM_THREE_STOKES_HH__
