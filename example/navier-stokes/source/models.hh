#ifndef NAVIERSTOKE_MODEL_HH
#define NAVIERSTOKE_MODEL_HH

#include <dune/fem-fluid/probleminterfaces.hh>
#include <dune/fem/io/parameter.hh>

// local includes
#include <dune/fem-fluid/model/default.hh>

template< class Jacobian >
static inline Jacobian SymetricGradient ( const Jacobian &jac )
{
  Jacobian sym( 0 );
  for( std::size_t i = 0; i < Jacobian::cols; ++i )
    for( std::size_t j = 0; j < Jacobian::cols; ++j )
      sym[ i ][ j ] = 0.5 * ( jac[ i ][ j ] + jac[ j ][ i ] );
  return sym;
}


/**********************************************
 * Analytical model                           *
 *********************************************/

/**
 * @brief Traits class for NavierStokesModel
 */
template< class GridPart, class Problem >
class NavierStokesModelTraits
{
public:
  typedef GridPart GridPartType;
  typedef typename GridPartType::GridType GridType;
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
  typedef typename GridPartType::IntersectionIteratorType IntersectionIterator;
  typedef typename IntersectionIterator::Intersection IntersectionType;

  typedef Problem ProblemType;
  typedef typename ProblemType::FunctionSpaceType FunctionSpaceType;

  static const int dimDomain = FunctionSpaceType::dimDomain;
  static const int dimRange = FunctionSpaceType::dimRange;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
  typedef Dune::FieldVector< RangeFieldType, dimDomain-1 >                   FaceDomainType;
};


template< class GridPartType, class Problem >
class NavierStokesModel
  : public DefaultModel< NavierStokesModelTraits< GridPartType, Problem > >
{
  typedef DefaultModel< NavierStokesModelTraits< GridPartType, Problem > > BaseType;
  typedef NavierStokesModel< GridPartType, Problem > ThisType;

public:
  typedef typename GridPartType::GridType GridType;
  typedef NavierStokesModelTraits< GridPartType, Problem >                     Traits;
  static const int dimDomain = Traits::dimDomain;
  static const int dimRange  = Traits::dimRange;
  typedef typename Traits::DomainType DomainType;
  typedef typename Traits::RangeType RangeType;
  typedef typename Traits::RangeFieldType RangeFieldType;
  typedef typename Traits::FaceDomainType FaceDomainType;
  typedef typename Traits::JacobianRangeType JacobianRangeType;

  typedef typename Traits::EntityType EntityType;
  typedef typename Traits::IntersectionType IntersectionType;

  typedef typename Traits::ProblemType ProblemType;

  const static bool hasFlux = true;
  const static bool hasDiffusion = true;
  const static bool hasSource = true;
  const static bool hasMass = true;

  // for production runs this should be switched off, if no weak source exists !!!
  const static bool hasWeakSource = true;

  const static bool isEvolutionProblem = true;


  // Diffusion
  // ---------

  struct Diffusion
  {
    Diffusion () {}

    Diffusion ( double RE, const RangeType &, const JacobianRangeType &jac )
      : jac_( SymetricGradient( jac ) )
    {
      jac_ *= 2.0 / RE;
    }

    Diffusion ( double RE, const RangeType &, const JacobianRangeType &jac, const JacobianRangeType &shift )
      : jac_( SymetricGradient( jac ) )
    {
      jac_ *= 2.0 / RE;
    }

    void operator() ( JacobianRangeType &diff ) const { diff = jac_; }

  protected:
    JacobianRangeType jac_;
  };


  // LinearDiffusion
  // ---------------

  struct LinearDiffusion
  {
    LinearDiffusion () {}

    LinearDiffusion ( const LinearDiffusion & ) = default;
    LinearDiffusion ( LinearDiffusion && ) = default;

    LinearDiffusion &operator= ( const LinearDiffusion & ) = default;

    LinearDiffusion ( double RE, const RangeType &, const JacobianRangeType &jac ) : viscosity_( 2.0 / RE ) {}
    LinearDiffusion ( double RE, const RangeType &, const JacobianRangeType &jac, const JacobianRangeType &shift ) : viscosity_( 2.0 / RE ) {}

    void operator() ( const RangeType &val, const JacobianRangeType &grad, JacobianRangeType &A ) const
    {
      A = SymetricGradient( grad );
      A *= viscosity_;
    }

    void operator() ( const RangeType &val, const JacobianRangeType &grad, const JacobianRangeType &shift, JacobianRangeType &d ) const
    {
      d = SymetricGradient( grad );
      d *= viscosity_;
    }

  protected:
    double viscosity_;
  };

protected:
  NavierStokesModel ( const NavierStokesModel &otehr ) = delete;
  const NavierStokesModel &operator=( const NavierStokesModel &other ) = delete;

public:

  /**
   * @brief Constructor
   *
   * initializes model parameter
   *
   * @param problem Class describing the initial(t=0) and exact solution
   */
  NavierStokesModel ( const ProblemType &problem )
    : problem_( problem ),
      reynoldNumber_( Dune::Fem::Parameter::getValue< double >( "problem.reynoldsnumber" ) ),
      alpha_( Dune::Fem::Parameter::getValue< double >( "problem.alpha", 0 ) ),
      alphaHalf_( alpha_ * 0.5 ),
      pressurePenalty_( Dune::Fem::Parameter::getValue< double >( "problem.pressurepenalty", 0 ) ),
      lambdaInv_( 0.0 ),
      problemHasSource( problem.hasSource() ),
      problemHasWeakSource( problem.hasWeakSource() )
  {}

  const ProblemType &problem () const { return problem_; }

  void setLambda ( double lambda ) const
  {
    if( lambda == 0 )
      lambdaInv_ = 0.0;
    else
      lambdaInv_ = 1.0 / lambda;
  }

  void setRelaxFactor ( double f ) const {}

  template< class ... Args >
  Diffusion diffusion ( const EntityType &en,
                        const double time,
                        const DomainType &x,
                        Args ... args ) const
  {
    return Diffusion( reynoldNumber_, args ... );
  }


  template< class ... Args >
  LinearDiffusion linearDiffusion ( const EntityType &en,
                                    const double time,
                                    const DomainType &x,
                                    Args ... args ) const
  {
    return LinearDiffusion( reynoldNumber_, args ... );
  }



  void addScaledMassTerm ( const EntityType &entity,
                           const double time,
                           const DomainType &x,
                           const double scale,
                           const RangeType &u,
                           RangeType &mass ) const
  {
    for( int i = 0; i < dimDomain; ++i )
      mass[ i ] += scale * u[ i ];
  }

  //! weakSource
  void weakSource ( const EntityType &en,
                    const double time,
                    const DomainType &x,
                    JacobianRangeType &s ) const
  {
    if( problemHasWeakSource )
    {
      DomainType xgl = en.geometry().global( x );
      problem().weakSource( xgl, time, s );
      s *= -1;
    }
  }


  //! source
  double source ( const EntityType &en,
                  const double time,
                  const DomainType &x,
                  const RangeType &u,
                  const JacobianRangeType &jac,
                  RangeType &s ) const
  {
    stiffSource( en, time, x, u, jac, s );

    double ret = 0;
    if( problemHasSource )
    {
      DomainType xgl = en.geometry().global( x );
      RangeType nonStiffsource;
      ret = problem().source( xgl, time, u, nonStiffsource );
      s -= nonStiffsource;
    }

    return ret;
  }


  void stiffSource ( const EntityType &en,
                     const double time,
                     const DomainType &x,
                     const RangeType &u,
                     const JacobianRangeType &jac,
                     RangeType &s ) const
  {
    s = 0;
    if( alpha_ > 0 )
      for( int i = 0; i < dimDomain; ++i )
        for( int j = 0; j < dimDomain; ++j )
          s[ i ] += alphaHalf_ * jac[ i ][ j ] * u[ j ];
  }


  //! linearize Source
  void linearSource ( const EntityType &en,
                      const double time,
                      const DomainType &x,
                      const RangeType &uStar,
                      const JacobianRangeType &jacUStar,
                      const RangeType &u,
                      const JacobianRangeType &jac,
                      RangeType &s ) const
  {
    linearStiffSource( en, time, x, uStar, jacUStar, u, jac, s );
  }



  void linearStiffSource ( const EntityType &en,
                           const double time,
                           const DomainType &x,
                           const RangeType &uStar,
                           const JacobianRangeType &jacUStar,
                           const RangeType &u,
                           const JacobianRangeType &jac,
                           RangeType &s ) const
  {
    s = 0;
    if( alpha_ > 0 )
      for( int i = 0; i < dimDomain; ++i )
        for( int j = 0; j < dimDomain; ++j )
          s[ i ] += alphaHalf_ * ( jacUStar[ i ][ j ] * u[ j ] + jac[ i ][ j ] * uStar[ j ] );
  }

  /**
   * @brief advection term \f$F\f$
   *
   * @param en entity on which to evaluate the advection term
   * @param time current time of TimeProvider
   * @param x coordinate local to entity
   * @param u \f$U\f$
   * @param f \f$f(U)\f$
   */
  void advection ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   JacobianRangeType &f ) const
  {
    f = 0;
    for( int d = 0; d < dimDomain; ++d )
    {
      for( int k = 0; k < dimDomain; ++k )
        f[ d ][ k ] += alphaHalf_ * u[ d ] * u[ k ];

      f[ d ][ d ] += u[ dimDomain ] * lambdaInv_;
      f[ dimDomain ][ d ] -= u[ d ] * lambdaInv_;
    }
  }

  void linearAdvection ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &uStar,
                         const RangeType &u,
                         JacobianRangeType &f ) const
  {
    f = 0;
    //f = ( Ep, u);

    for( int d = 0; d < dimDomain; ++d )
    {
      for( int k = 0; k < dimDomain; ++k )
      {
        f[ d ][ k ] += alphaHalf_ * uStar[ d ] * u[ k ];
        f[ d ][ k ] += alphaHalf_ * u[ d ] * uStar[ k ];
      }

      f[ d ][ d ] += u[ dimDomain ] * lambdaInv_;
      f[ dimDomain ][ d ] -= u[ d ] * lambdaInv_;
    }
  }

  void maxSpeed ( const IntersectionType &intersection,
                  const DomainType &normal,
                  const double time,
                  const DomainType &x,
                  const RangeType &u,
                  double &advspeed,
                  double &totalspeed ) const
  {
    advspeed = 0.;
    totalspeed = 0.1;
  }

  void stress ( const EntityType &en,
                const double time,
                const DomainType &x,
                const RangeType &u,
                const JacobianRangeType &jac,
                JacobianRangeType &stress ) const
  {
    diffusion( en, time, x, u, jac, stress );

    for( int i = 0; i < dimDomain; ++i )
      stress[ i ][ i ] -= u[ dimDomain ];
  }

  // return the eigenvalues of the advection operator
  // assumption: eigenValue[0] > ...  > eigenValue[r]
  void eigenValues ( const IntersectionType &intersection,
                     const double time,
                     const DomainType &x,
                     const DomainType &normal,
                     const RangeType &u,
                     RangeType &eigenValue ) const
  {
    eigenValue = 0.;
#ifdef LLF
    double un = 0;
    for( int i=0; i<dimDomain;++i)
      un += u[ i ] * normal[ i ];
    eigenValue = 0.5 * std::abs( un );
#endif
    eigenValue[ dimDomain ] = pressurePenalty_ * normal.two_norm() * lambdaInv_ * lambdaInv_;
  }

  void eigenValues ( const IntersectionType &intersection,
                     const double time,
                     const DomainType &x,
                     const DomainType &normal,
                     const RangeType &uStar,
                     const RangeType &u,
                     RangeType &eigenValue ) const
  {
    eigenValue = 0.;
#ifdef LLF
    double un = 0, uk = 0;
    for( int i=0; i<dimDomain;++i)
    {
      un += uStar[ i ] * normal[ i ];
      uk += u[ i ] * uStar[ i ]; 
    }
    eigenValue = 0.5 * std::abs( un );
#endif
    eigenValue[ dimDomain ] = pressurePenalty_ * normal.two_norm() * lambdaInv_ * lambdaInv_;
   
    eigenValue[ dimDomain ] = pressurePenalty_ * normal.two_norm() * lambdaInv_ * lambdaInv_;
  }

  /**
   * @brief diffusion term \f$A\f$
   */
  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   JacobianRangeType &A ) const
  {
    Diffusion dif( diffusion( en, time, x, u, jac ) );
    dif( A );
  }

  void diffusion ( const EntityType &en,
                   const double time,
                   const DomainType &x,
                   const RangeType &u,
                   const JacobianRangeType &jac,
                   const JacobianRangeType &shift,
                   JacobianRangeType &A ) const
  {
    Diffusion dif( diffusion( en, time, x, u, jac, shift ) );
    dif( A );
  }


  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &ustar,
                         const JacobianRangeType &jacUStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         JacobianRangeType &A ) const
  {
    LinearDiffusion linDiff = linearDiffusion( en, time, x, ustar, jacUStar );
    linDiff( u, jac, A );
  }


  void linearDiffusion ( const EntityType &en,
                         const double time,
                         const DomainType &x,
                         const RangeType &ustar,
                         const JacobianRangeType &jacUStar,
                         const JacobianRangeType &shiftStar,
                         const RangeType &u,
                         const JacobianRangeType &jac,
                         const JacobianRangeType &shift,
                         JacobianRangeType &A ) const
  {
    LinearDiffusion linDiff = linearDiffusion( en, time, x, ustar, jacUStar, shiftStar );
    linDiff( u, jac, shift, A );
  }

  /**
   * @brief checks for existence of dirichlet boundary values
   */
  bool hasBoundaryValue ( const IntersectionType &it ) const { return it.boundaryId() != 2; }

  // if we have a boundary value we can still have a Neumann Type BC
  bool hasLinearBoundaryValue ( const IntersectionType &it ) const { return it.boundaryId() != 2; }

  /**

   * @brief neuman boundary values \f$g_N\f$ for pass2
   */

  double boundaryFlux ( const IntersectionType &it,
                        const EntityType &entity,
                        const double time,
                        const FaceDomainType &x,
                        const DomainType &normal,
                        const RangeType &uLeft,
                        const JacobianRangeType &jacLeft,
                        RangeType &bFlux ) const
  {
    bFlux = 0;

    JacobianRangeType f( 0 );

    double jump = 0.;
    for( int d = 0; d < dimDomain; ++d )
    {
      jump += normal[ d ] * uLeft[ d ];
      f[ d ][ d ] += 0.5 * uLeft[ dimDomain ] * lambdaInv_;
      f[ dimDomain ][ d ] -= uLeft[ d ] * lambdaInv_;
    }
    f.mv( normal, bFlux );

    jump *= ( jump > 0 ) ?  1.0 : 0.5;
    for( int d = 0; d < dimDomain; ++d )
      bFlux[ d ] += alphaHalf_ * jump * uLeft[ d ];

    f = jacLeft;
    f[ dimDomain ] = 0;
    f.usmtv( -1./ reynoldNumber_, normal, bFlux );

    return normal.two_norm();
  }


  /** \brief boundary flux
   */

  void linearBoundaryFlux ( const IntersectionType &it,
                            const EntityType &entity,
                            const double time,
                            const FaceDomainType &x,
                            const DomainType &normal,
                            const RangeType &uStar,
                            const JacobianRangeType &linJacLeft,
                            const RangeType &uLeft,
                            const JacobianRangeType &jacLeft,
                            RangeType &gLeft ) const
  {
    JacobianRangeType f( 0 );

    double jump = 0, jumpStar = 0.;
    for( int d = 0; d < dimDomain; ++d )
    {
      jump += normal[ d ] * uLeft[ d ];
      jumpStar += normal[ d ] * uStar[ d ];

      f[ d ][ d ] += 0.5 * uLeft[ dimDomain ] * lambdaInv_;
      f[ dimDomain ][ d ] -= uLeft[ d ] * lambdaInv_;
    }

    f.mv( normal, gLeft );

    double jFactor = ( jumpStar > 0 ) ? 1.0 : 0.5;
    for( int d = 0; d < dimDomain; ++d )
      gLeft[ d ] += alphaHalf_ * jFactor * ( jump * uStar[ d ] + jumpStar * ( uLeft[ d ] ) );

    f = jacLeft;
    f[ dimDomain ] = 0;
    f.usmtv( -1./ reynoldNumber_, normal, gLeft );
  }


  /**
   * @brief dirichlet boundary values
   */

  void linearBoundaryValue ( const IntersectionType &it,
                             const double time,
                             const FaceDomainType &x,
                             const RangeType &uStar,
                             const JacobianRangeType &jacStar,
                             const RangeType &uLeft,
                             const JacobianRangeType &jacLeft,
                             RangeType &uRight ) const
  {
    // inflow or dirichlet data
    uRight = 0.;
    uRight[ dimDomain ] =  uLeft[ dimDomain ];
  }

  void boundaryValue ( const IntersectionType &it,
                       const double time,
                       const FaceDomainType &x,
                       const RangeType &uLeft,
                       const JacobianRangeType &jacLeft,
                       RangeType &uRight ) const
  {
    // inflow or dirichlet data
    const DomainType xgl = it.geometry().global( x );
    problem().boundaryValue( xgl, time, uLeft, uRight );
    uRight[ dimDomain ] =  uLeft[ dimDomain ];
  }

protected:

  const ProblemType &problem_;
  const double reynoldNumber_;
  const double alpha_;
  const double alphaHalf_;
  const double pressurePenalty_;
  mutable double lambdaInv_;
  const bool problemHasSource;
  const bool problemHasWeakSource;
};

#endif // #ifndef NAVIERSTOKE_MODEL_HH
