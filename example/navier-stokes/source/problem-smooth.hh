#ifndef  DUNE_PROBLEM_THREE_PSTOKES_HH__
#define  DUNE_PROBLEM_THREE_PSTOKES_HH__

#include <dune/common/version.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include <dune/fem-fluid/probleminterfaces.hh>


namespace Dune
{

  namespace Fem
  {

    template< class FunctionSpace >
    class SmoothSolution
      : public EvolutionProblemInterface< FunctionSpace >
    {
    public:
      typedef EvolutionProblemInterface< FunctionSpace > BaseType;

      enum { dimDomain = BaseType::dimDomain };
      enum { dimRange = BaseType::dimRange };
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::HessianRangeType HessianRangeType;


      SmoothSolution ()
        : BaseType(),
          starttime_( Parameter::getValue< double >( "problem.starttime", 0.0 ) ),
          alpha_( Parameter::getValue< double >( "problem.alpha", 0 ) ),
          reynoldsNumber_( Dune::Fem::Parameter::getValue< double >( "problem.reynoldsnumber" ) ),
          myName( "C^inf solution with alpha: " + std::to_string( alpha_ ) )
      {}

      bool hasWeakSource () const { return true; }
      bool hasSource () const { return true; }

      double starttime () const { return starttime_; }

      /**
       * @brief evaluates \f$ u_0(x) \f$
       */
      void evaluate ( const DomainType &arg, RangeType &res ) const
      {
        evaluate( arg, starttime_, res );
      }


      double uvt ( double time ) const
      {
        return (1.+exp( -100*time ));
      }


      double duvt ( double time ) const
      {
        return -100* exp( -100*time );
      }

      /**
       * @brief evaluate exact solution
       */
      void evaluate ( const DomainType &arg, const double t, RangeType &res ) const /*@LST0S@@LST0E@*/
      {
        res[ 0 ] =  sin( 2. * arg[ 0 ] * M_PI ) * cos( 2.0 * arg[ 1 ] * M_PI );
        res[ 1 ] = -cos( 2. * arg[ 0 ] * M_PI ) * sin( 2.0 * arg[ 1 ] * M_PI );
        res *= uvt( t );

        res[ dimDomain ] = cos( 2.0 * arg[ 0 ] * M_PI );
      }

      void jacobian ( const DomainType &arg, const double t, JacobianRangeType &jac ) const
      {
        double time = uvt( t );
        jac[ 0 ][ 0 ] = time * 2.0 *M_PI *cos( 2.0* arg[ 0 ] * M_PI ) * cos( 2.0* arg[ 1 ] * M_PI );
        jac[ 0 ][ 1 ] = time * 2.0 * -M_PI *sin( 2.0 *arg[ 0 ] *M_PI ) * sin( 2.0 * arg[ 1 ] * M_PI );
        jac[ 1 ][ 0 ] = time * 2.0 *M_PI *sin( 2.0*arg[ 0 ] * M_PI ) * sin( 2.0 * arg[ 1 ] * M_PI );
        jac[ 1 ][ 1 ] = time * 2.0 * -M_PI *cos( 2.0*arg[ 0 ] *M_PI ) * cos( 2.0 * arg[ 1 ] * M_PI );

        jac[ dimDomain ][ 0 ] = -2.0 *M_PI *sin( 2.0 * arg[ 0 ] *M_PI );
        jac[ dimDomain ][ 1 ] = 0.;
      }


      double weakSource ( const DomainType &arg,
                          const double t,
                          JacobianRangeType &res ) const
      {
        res = 0;
        RangeType value;
        evaluate( arg, t, value );
        jacobian( arg, t, res );

        JacobianRangeType Du( res );

        for( int r = 0; r < dimDomain; ++r )
          for( int i = 0; i < dimDomain; ++i )
            res[ r ][ i ] = 1.0 / reynoldsNumber_ * ( Du[ r ][ i ] + Du[ i ][ r ] );

        res[ dimDomain ] = 0.;

        for( int i = 0; i < dimDomain; ++i )
          for( int j = 0; j < dimDomain; ++j )
            // + or - not yet clear
            res[ i ][ j ] -= alpha_* value[ i ] * value[ j ];


        // time step restriction from the stiff source term
        return 0.;
      }
      /**
       * @brief evaluate stiff source function
       */
      double source ( const DomainType &arg,
                      const double t,
                      const RangeType &u,
                      RangeType &res ) const         /*@LST0S@@LST0E@*/
      {
        // d_T u
        res[ 0 ] =  sin( 2.0 * M_PI * arg[ 0 ] ) * cos( 2.0 * M_PI * arg[ 1 ] ) * duvt( t );
        res[ 1 ] = -cos( 2.0 * M_PI * arg[ 0 ] ) * sin( 2.0 * M_PI * arg[ 1 ] ) * duvt( t );

        // div p
        res[ 0 ] +=  -2.0 *M_PI *sin( 2.0 * M_PI * arg[ 0 ] );

        // div condition
        res[ dimDomain ] =  0.;
        return 0.0;
      }

      std::string description () const
      {
        std::ostringstream ofs;

        ofs << "Problem: " << myName
            << ", End time: " << Parameter::getValue< double >( "problem.endtime" );

        return ofs.str();
      }

    private:
      double starttime_;
      const double alpha_;
      const double reynoldsNumber_;

    public:
      std::string myName;
    };

  } // namespace Fem

} // namespace Dune

#endif  // #ifndef  DUNE_PROBLEM_THREE_PSTOKES_HH__
