#ifndef ANALYTICALTRAITS_HH
#define ANALYTICALTRAITS_HH

#include <dune/fem/misc/femeoc.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>

#include "models.hh"
#include "problem-exactstokes.hh"
#include "problem-smooth.hh"
#include "problem-symflow.hh"


namespace __impl
{

  template< class GridPart, class FunctionSpace >
  struct AnalyticalTraits
  {
    // initial problem
    typedef Dune::Fem::EvolutionProblemInterface< FunctionSpace > ProblemType;

    // the models
    typedef NavierStokesModel< GridPart, ProblemType > ModelType;

    typedef ProblemType InitialDataType;
    typedef typename InitialDataType::TimeDependentFunctionType TimeDependentFunctionType;

    typedef typename FunctionSpace::RangeType RangeType;
    typedef typename FunctionSpace::RangeFieldType RangeFieldType;
    typedef typename FunctionSpace::JacobianRangeType JacobianRangeType;

    static ProblemType *problem ()
    {
      static const std::string probString[]  = { "exactstokes", "smooth", "symflow" };
      int problemId = Dune::Fem::Parameter::getEnum( "problem", probString, 0 );
      switch( problemId )
      {
      case 0:
        return new Dune::Fem::ExactStokesSolution< FunctionSpace >();
      case 1:
        return new Dune::Fem::SmoothSolution< FunctionSpace >();
      case 2:
        return new Dune::Fem::SymFlowProblem< FunctionSpace >();
      default:
        abort();
        return 0;
      }
    }

    struct ErrorHandler
    {

      static const int dimDomain = FunctionSpace::dimDomain;
      static const int numErrors = 4;

      ErrorHandler ()
        : ids_
      {
        Dune::Fem::FemEoc::addEntry( std::string( "$L^2$-error Velocity" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "$H^1$-error Velocity" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "$L^2$-error Pressure" ) ),
        Dune::Fem::FemEoc::addEntry( std::string( "$L^2$-error divergence" ) )
      }
      {}


      template< class Tp, class DiscreteSolution >
      void initializeStep ( const Tp &tp, int loop, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class Tp, class DiscreteSolution >
      void step ( const Tp &tp, const ModelType &model, const DiscreteSolution &u ) const
      {}

      template< class Tp, class DiscreteSolution >
      void finalizeStep ( const Tp &tp, const ModelType &model, const DiscreteSolution &u ) const
      {
        // compute errors at final time
        GridPart &gridPart = u.space().gridPart();

        // first compute mean value of pressure difference
        double meanValuePDiff = 0.;
        std::for_each( Dune::elements( gridPart ).begin(), Dune::elements( gridPart ).end(),
                       [ &u, f = model.problem().fixedTimeFunction( tp.time() ), &meanValuePDiff ]
                         ( const typename DiscreteSolution::EntityType & entity )
                       {
                         auto local = u.localFunction( entity );
                         auto geometry = entity.geometry();
                         Dune::Fem::CachingQuadrature< GridPart, 0 > quad( entity, u.space().order() * 2 );
                         for( const auto &qp: quad )
                         {
                           const double weight = qp.weight() * geometry.integrationElement( qp.position() );
                           typename FunctionSpace::RangeType value, valueh;
                           local.evaluate( qp, valueh );
                           f.evaluate( geometry.global( qp.position() ), value );
                           meanValuePDiff += ( value[ dimDomain ] - valueh[ dimDomain ] ) * weight;
                         }
                       } );

        meanValuePDiff = gridPart.grid().comm().sum( meanValuePDiff );

        // now compute errors
        std::array< double, numErrors > error; error.fill( 0.0 );
        std::for_each( Dune::elements( gridPart ).begin(), Dune::elements( gridPart ).end(),
                       [ &gridPart, &u, f = model.problem().fixedTimeFunction( tp.time() ), &error, meanValuePDiff, &model ]
                         ( const typename DiscreteSolution::EntityType & entity )
                       {
                         auto local = u.localFunction( entity );
                         auto geometry = entity.geometry();
                         Dune::Fem::CachingQuadrature< GridPart, 0 > quad( entity, u.space().order() * 2 );
                         for( const auto &qp: quad )
                         {
                           const double weight = qp.weight() * geometry.integrationElement( qp.position() );
                           typename FunctionSpace::RangeType value, valueh;
                           typename FunctionSpace::JacobianRangeType dphi, dphih;
                           local.evaluate( qp, valueh );
                           f.evaluate( geometry.global( qp.position() ), value );
                           value -= valueh;
                           value[ dimDomain ] -= meanValuePDiff;

                           local.jacobian( qp, dphih );
                           f.jacobian( geometry.global( qp.position() ), dphi );

                           double val = 0;
                           for( std::size_t i = 0; i < dimDomain; ++i )
                             val += dphih[ i ][ i ];
                           error[ 3 ] += val * val * weight;

                           dphi -= dphih;

                           error[ 2 ] += value[ dimDomain ] * value[ dimDomain ] * weight;

                           value[ dimDomain ] = 0.0;
                           error[ 0 ] += value.two_norm2() * weight;
                           error[ 1 ] += value.two_norm2() * weight;
                           dphi[ dimDomain ] = 0.0;
                           error[ 1 ] += dphi.frobenius_norm2() * weight;

                         }

                         // todo add skeleton parts for div error
                         for( auto intersection : Dune::intersections( gridPart, entity ) )
                         {
                           Dune::Fem::CachingQuadrature< GridPart, 1 > quadInside( gridPart, intersection, 
                               u.space().order() * 2, Dune::Fem::CachingQuadrature< GridPart, 1 >::INSIDE );
                           if( intersection.neighbor() )
                           {
                             Dune::Fem::CachingQuadrature< GridPart, 1 > quadOutside( gridPart, intersection, 
                                 u.space().order() * 2, Dune::Fem::CachingQuadrature< GridPart, 1 >::OUTSIDE );

                             auto localNb = u.localFunction( intersection.outside() );
                             for( std::size_t qp = 0; qp < quadInside.nop(); ++qp )
                             {
                               typename FunctionSpace::RangeType value, valueNb;
                               double weight = quadInside.weight( qp ) * intersection.integrationOuterNormal( quadInside.localPoint( qp ) ).two_norm();

                               local.evaluate( quadInside[ qp ], value );
                               localNb.evaluate( quadOutside[ qp ], valueNb );
                               value -= valueNb;
                               value[ dimDomain ] = 0;
                               error[ 3 ] += value.two_norm2() * weight;
                             }
                           }
                           else if ( intersection.boundary() )
                           {
                             for( std::size_t qp = 0; qp < quadInside.nop(); ++qp )
                             {
                               typename FunctionSpace::RangeType value, valueNb;
                               double weight = quadInside.weight( qp ) * intersection.integrationOuterNormal( quadInside.localPoint( qp ) ).two_norm();

                               local.evaluate( quadInside[ qp ], value );
                               f.evaluate( intersection.geometry().global( quadInside.localPoint( qp ) ), valueNb );
                               value -= valueNb;
                               value[ dimDomain ] = 0;
                               error[ 3 ] += value.two_norm2() * weight;
                             }
                           }
                         }
                       } );

        for( std::size_t i = 0; i < numErrors; ++i )
          error[ i ] = gridPart.grid().comm().sum( error[ i ] );
        for( std::size_t i = 0; i < numErrors; ++i )
          Dune::Fem::FemEoc::setErrors( ids_[ i ], std::sqrt( error[ i ] ) );
      }

    private:
      std::array< std::size_t, numErrors > ids_;
    };

  };

} // namespace __impl

#endif //#ifndef ANALYTICALTRAITS_HH
